import pickle

from django.conf import settings
import pika


class Rabbit():
    """Base class for RabbitMQ connection. """

    def __init__(self):
        host = settings.RABBIT_HOST
        user = settings.RABBIT_LOGIN
        password = settings.RABBIT_PASSWORD

        self.__credentials = pika.PlainCredentials(user, password)
        self.__parameters = pika.ConnectionParameters(
            host=host, credentials=self.__credentials, heartbeat=600,
            blocked_connection_timeout=600)
        self.connection = None

    def connect(self):
        """Tries to create RabbitMQ connection

        Returns:
            RabbitMQ connection.
        """

        conn = pika.BlockingConnection(self.__parameters)

        return conn


class RpcClient(Rabbit):

    def __init__(self, queue_name: str):
        super().__init__()
        self.queue_name = queue_name
        self.response = None

    def send(self, message):
        """Sends message.

        Args:
            message: Message.
        """
        with self.connect() as conn:
            channel = conn.channel()
            channel.basic_consume(
                'amq.rabbitmq.reply-to',
                self.on_reply_from_server,
                auto_ack=True)
            channel.basic_publish(
                exchange='',
                routing_key=self.queue_name,
                body=pickle.dumps(message),
                properties=pika.BasicProperties(
                    reply_to='amq.rabbitmq.reply-to'))

            channel.start_consuming()

        return pickle.loads(self.response)

    def on_reply_from_server(self, ch, method, properties, body):
        self.response = body
        ch.close()
