from django.contrib.postgres.fields import JSONField
from django.db import models

from common.rabbit import RpcClient


class Service(models.Model):
    name = models.CharField(
        unique=True,
        editable=False,
        max_length=100,
        help_text="Name of the service."
    )

    queue_name = models.CharField(
        max_length=100,
        help_text="Name of the service queue."
    )

    task = JSONField()

    objects = models.Manager()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._connect_to_rabbit()

    def _connect_to_rabbit(self):
        self.rpc = RpcClient(self.queue_name)

    def call(self):
        response = self.rpc.send(self.task)

        return response
