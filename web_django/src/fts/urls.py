from django.urls import path

from . import views

app_name = 'fts'
urlpatterns = [
    path('', views.full_text_search, name='full-text-search'),
    path('rules', views.rules, name="rules"),
    path('add-rule', views.add_rule, name='add-fts-rule'),
    path('delete-rule/<int:rule>', views.delete_rule, name='delete rule'),
    path('track-rule/<int:rule>', views.track_rule, name='track rule'),
    path('untrack-rule/<int:rule>', views.untrack_rule, name='track rule'),
    path('get-mentions', views.get_mentions, name='fts-mentions'),
    path('set-viewed-mention', views.set_viewed_mention, name='set-viewed'),
]
