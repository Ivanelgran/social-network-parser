from datetime import datetime, timedelta

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect

from .models import FTSRule, ViewedMentions, FtsMentions, TopMine
from services.models import Service
from web.sync import rule_sync
# import the logging library
import logging
import json

# Get an instance of a logger
logger = logging.getLogger(__name__)


@login_required
def full_text_search(request):
    today = datetime.today()
    yesterday = today - timedelta(days=1)

    start_date = yesterday.strftime("%Y-%m-%dT%H:%M:%S")
    end_date = today.strftime("%Y-%m-%dT%H:%M:%S")

    rule_sync()

    rules = FTSRule.objects.all()

    filters = request.session.get("fts_filters")
    if filters is None:
        request.session['fts_filters'] = {}

    context = {
        'start_date': start_date,
        'end_date': end_date,
        'rules': rules,
        'filters': filters
    }

    return render(request, 'fts/mentions.html', context)


@login_required
def rules(request):
    rule_sync()
    rules = FTSRule.objects.all()

    return render(request, "fts/rules.html", {'rules': rules})


@login_required
def add_rule(request):
    if request.method == 'POST':
        social = ' '.join(request.POST.getlist('social'))
        value = request.POST['value']

        FTSRule.objects.create(
            social=social,
            is_active=True,
            value=value
        )
        
        cass = Service.objects.get(name="db-worker")
        cass.task['task_name'] = "insert_rules"
        cass.task['args'] = {
            'rule': value,
            'words': value,
            'is_active': True,
            'social': social
        }
        cass.call()

    return render(request, 'fts/add_rule.html')


@login_required
def delete_rule(request, rule):
    rule = FTSRule.objects.get(pk=rule)
    rule.delete()

    cass = Service.objects.get(name="db-worker")
    cass.task['task_name'] = "drop_rule"
    cass.task['args'] = {
        'tag': rule.value,
        'social': rule.social
    }
    cass.call()

    return JsonResponse({"success": True}, status=200)


@login_required
def track_rule(request, rule):
    rule = FTSRule.objects.get(pk=rule)
    rule.users.add(request.user)

    cass = Service.objects.get(name="db-worker")

    cass.task['task_name'] = "get_active_rules"
    rules = cass.call()
    rule_tag = [r for r in rules if r['value'] == rule.value][0]['tag']

    cass.task['task_name'] = "set_tracked_rules"
    cass.task['args'] = {
        'username': request.user.username,
        'tag': rule_tag,
        'social': rule.social
    }
    cass.call()

    fts = Service.objects.get(name="fts-service")
    fts.task = {'track_rule': True}
    fts.call()

    return JsonResponse({"success": True}, status=200)


@login_required
def untrack_rule(request, rule):
    rule = FTSRule.objects.get(pk=rule)
    rule.users.remove(request.user)

    cass = Service.objects.get(name="db-worker")

    cass.task['task_name'] = "get_active_rules"
    rules = cass.call()
    rule_tag = [r for r in rules if r['value'] == rule.value][0]['tag']

    cass.task['task_name'] = "set_tracked_rules"
    cass.task['args'] = {
        'username': request.user.username,
        'tag': rule_tag,
        'social': rule.social,
        'state': False

    }
    cass.call()

    fts = Service.objects.get(name="fts-service")
    fts.task = {'track_rule': True}
    fts.call()

    return JsonResponse({"success": True}, status=200)


@login_required
def get_mentions(request):
    top_phrases = []
    fields = ['rule_id', 'start_date', 'end_date', 'drop_dupl', 'sort_viewed', 'get_topmine']
    params = {
        field: request.GET.get(field) for field in fields
    }
    logger.error(params['get_topmine'])
    mention_grabber = FtsMentions(**params)
    mentions = mention_grabber.get(user=request.user)
    texts = [post['text'] for post in mentions]
    texts = list(dict.fromkeys(texts))
    top_mine_grabber = TopMine(texts)
    top_phrases = top_mine_grabber.get_freq_phrases() if params['get_topmine'] == 'true' else [] # Получение топа фраз по частоте
    top_words = top_mine_grabber.get_freq_words() if params['get_topmine'] == 'true' else [] # Получение топа слов по частоте

    request.session['fts_filters']['rule_id'] = request.GET.get('rule_id')
    request.session['fts_filters']['drop_dupl'] = mention_grabber.drop_dupl
    request.session['fts_filters']['sort_viewed'] = mention_grabber.sort_viewed
    request.session.modified = True
    del top_mine_grabber

    return JsonResponse({"success": True, "data": mentions, "top_phrases": top_phrases, "top_words": top_words}, status=200)


@login_required
def set_viewed_mention(request):
    user = request.user
    type, social, owner_id, post_id, add_id = request.POST.get('id').split('*')
    row = ViewedMentions(
        user=user,
        type=type,
        social=social,
        owner_id=owner_id,
        post_id=post_id,
        add_id=add_id
    )
    row.save()
    return JsonResponse({"success": True}, status=200)
