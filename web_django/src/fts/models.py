from datetime import datetime
import logging

from django.contrib.auth.models import User
# from sklearn.feature_extraction.text import TfidfVectorizer
from django.db import models
from collections import Counter

import pandas as pd
import pickle
import nltk
import json
import codecs
import re
import math
import heapq
import sys
import pymystem3


from posts.models import Post, Reply, TopicPost
from services.models import Service
from django.conf import settings
import logging

logger = logging.getLogger(__name__)

nltk.download('punkt')
TfidfVectorizer = pickle.load(open("tfidf_vectorizer_messages.pkl", "rb"))

with open('stopwords.json', 'r') as json_file:
    stop_words = json.load(json_file)


class FTSRule(models.Model):
    social = models.CharField(max_length=10)
    is_active = models.BooleanField()
    value = models.CharField(max_length=300)
    users = models.ManyToManyField(User)

    def socials(self):
        return self.social.split(' ')


class ViewedMentions(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=20)
    social = models.CharField(max_length=10)
    owner_id = models.CharField(max_length=100)
    post_id = models.CharField(max_length=100)
    add_id = models.CharField(max_length=100)

class TopMine():
    def __init__(self, texts, min_support=2, alpha=4, max_phrase_size=3):
        self.texts = texts
        self.logger = logging.getLogger(__name__)
        self.mystem = pymystem3.Mystem()
        self.min_support = min_support
        self.alpha = alpha
        self.max_phrase_size = max_phrase_size

    def get_processed_text(self):
        new_text = []
        for text in self.texts:
            text = self._tokenize_text(text)
            text = self._remove_stop_words(text)
            text = self._lemmatize_text(text)
            new_text.append(' '.join(text))
        
        return new_text

    def get_freq_phrases(self):
        text = self.get_processed_text()
        phrase_miner = PhraseMining(text, self.min_support, self.max_phrase_size, self.alpha);
        partitioned_docs, index_vocab = phrase_miner.mine()
        frequent_phrases = phrase_miner.get_frequent_phrases(self.min_support)

        # with open('ignore_phrases.json', 'r') as file:
        #     data = json.load(file)

        # frequent_phrases = [phrase for phrase in frequent_phrases if not phrase[0] in data]

        return frequent_phrases[:30]

    def get_freq_words(self):
        freq = nltk.FreqDist(nltk.word_tokenize(' '.join(self.get_processed_text())))

        # with open('ignore_words.json', 'r') as file:
        #     data = json.load(file)

        # freq = [phrase for phrase in freq.most_common(100) if not phrase[0] in data]

        return freq.most_common(30)

    def _tokenize_text(self, text):
        tokenize_text = nltk.word_tokenize(text)

        return [w.lower() for w in tokenize_text if w.isalpha()]

    def _remove_stop_words(self, text):

        return [w for w in text if w not in stop_words]

    def _lemmatize_text(self, text):
        lem = self.mystem.lemmatize((" ").join(text))
        return [w for w in lem if w.isalpha() and len(w)>1]



class FtsMentions():

    def __init__(self, rule_id, start_date, end_date, drop_dupl, sort_viewed, get_topmine):
        self.rule_id = rule_id
        self.start_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S")
        self.end_date = datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S")
        self.drop_dupl = drop_dupl == 'true'
        self.sort_viewed = sort_viewed == 'true'

        self.rule = FTSRule.objects.get(id=rule_id)
        
        self.logger = logging.getLogger(__name__)

    def _get_elastic_mentions(self):
        fts_service = Service.objects.get(name="fts-service")

        fts_service.task['words'] = self.rule.value
        fts_service.task['social'] = self.rule.social
        fts_service.task['start_date'] = str(self.start_date.date())
        fts_service.task['start_time'] = str(self.start_date.time())
        fts_service.task['end_date'] = str(self.end_date.date())
        fts_service.task['end_time'] = str(self.end_date.time())

        index_posts = fts_service.call()
        posts = [dict(Post(**post)) for post in index_posts[0]]
        topic_posts = [dict(TopicPost(**post)) for post in index_posts[2]]
        replies = [dict(Reply(**reply)) for reply in index_posts[1]]

        return posts, topic_posts, replies

    def _get_cass_mentions(self):
        cass = Service.objects.get(name="db-worker")

        start_posts_date = f"{self.start_date.date()} {self.start_date.time()}"
        end_posts_date = f"{self.end_date.date()} {self.end_date.time()}"
        posts_date = (f"date >= '{start_posts_date}' "
                      f"AND date <= '{end_posts_date}'")
        args = {
            'tag': self.rule.tag,
            'date': [posts_date],
            'limit': self.count
        }
        cass.task['task_name'] = "get_posts_by_rule"
        cass.task['args'] = args

        post_data, topic_post_data, replies_data, count = cass.call()
        posts = [dict(Post(**post)) for post in post_data]
        topic_posts = [dict(TopicPost(**post)) for post in topic_post_data]
        replies = [dict(Reply(**reply)) for reply in replies_data]

        return posts, topic_posts, replies

    def _is_mention_viewed(self, type, social, owner_id, post_id, add_id):
        row = ViewedMentions.objects.filter(
            user__username=self.user,
            type=type,
            social=social,
            owner_id=owner_id,
            post_id=post_id,
            add_id=add_id
        )

        return len(row) > 0

    def _get_mentions_df(self):
        df = pd.DataFrame()

        for data in self._get_elastic_mentions():
            df = pd.concat([df, pd.DataFrame(data)], sort=False)
        # for data in self._get_cass_mentions():
        #     df = pd.concat([df, pd.DataFrame(data)], sort=False)

        is_viewed = []
        viewed_ids = []
        for index, row in df.iterrows():
            args = {
                'type': row['mention_type'],
                'social': row['social'],
                'owner_id': row['owner_id'],
                'post_id': row['post_id']
            }
            if row['mention_type'] == 'post':
                args['add_id'] = ""
            elif row['mention_type'] == 'reply':
                args['add_id'] = row['reply_id']
            elif row['mention_type'] == 'topic_post':
                args['add_id'] = row['topic_id']
            is_mention_viewed = self._is_mention_viewed(**args)
            is_viewed.append(is_mention_viewed)
            viewed_ids.append('*'.join(args.values()))
        df['is_viewed'] = is_viewed
        df['viewed_id'] = viewed_ids
        
        if df.shape[0]:
            df['date'] =  pd.to_datetime(df['date'])

        return df

    def _add_attachments(self, df):
        cass = Service.objects.get(name="db-worker")
        args = {
            'posts': df[df.mention_type == 'post'][
                ['social', 'owner_id', 'post_id']
            ].to_dict('records')
        }
        cass.task['task_name'] = "get_posts_attachments"
        cass.task['args'] = args

        posts_atts = cass.call()
        posts_atts = pd.DataFrame(posts_atts)

        if "reply_id" not in df.columns:
            df['reply_id'] = None

        if posts_atts.shape[0] > 0:
            posts_atts = posts_atts.drop_duplicates([
                'social', 'owner_id', 'post_id'])
            posts_atts = posts_atts[[
                'social', 'owner_id', 'post_id', 'link', 'show_link']]
            posts_atts.columns = [
                'social', 'owner_id', 'post_id', 'att_link', 'att_show_link'
            ]
            posts_atts['reply_id'] = None
            df = pd.merge(df, posts_atts, how="left",
                          on=['social', 'owner_id', 'post_id', 'reply_id'])
            # df.link.fillna("", inplace=True)
        else:
            df['attachments'] = ""

        return df

    def build_df(self):
        df = self._get_mentions_df()

        if not df.shape[0]:
            return df

        df.drop_duplicates(subset='date', inplace=True)

        if self.drop_dupl:
            df.drop_duplicates(subset='text', inplace=True)

        if self.sort_viewed:
            df.sort_values(
                ['is_viewed', 'date'],
                ascending=[True, False],
                inplace=True
            )
        else:
            df.sort_values('date', ascending=False, inplace=True)

        df = self._add_attachments(df)

        df.replace({
            'post': 'Пост',
            'topic_post': 'Обсуждение',
            'reply': 'Комментарий'
        }, inplace=True)
        df.fillna("", inplace=True)

        return df

    def get(self, user):
        self.user = user
        df = self.build_df()

        return df.to_dict('records')

class PhraseMining(object):
    """
    PhraseMining performs frequent pattern mining followed by agglomerative clustering
    on the input corpus and then stores the results in intermediate files.
    :param min_support:
        minimum support threshold which must be satisfied by each phrase during frequent
        pattern mining.
    :param max_phrase_size:
        maximum allowed phrase size.
    :param alpha:
        threshold for the significance score.
    """

    def __init__(self, text, min_support=10, max_phrase_size=40, alpha=4):
        self.min_support = min_support
        self.max_phrase_size = max_phrase_size
        self.alpha = alpha
        self.text = text

    def mine(self):
        return self._run_phrase_mining(self.min_support, self.max_phrase_size, self.alpha, self.text)

    def _frequentPatternMining(self, documents, min_support, max_phrase_size, word_freq, active_indices):
        """
        Performs frequent pattern mining to collect aggregate counts for all contiguous phrases in the 
        input document that satisfy a certain minimum support threshold.

        Parameters:
        @documents: the input corpus
        @min_support: minimum support threshold which must be satisfied by each phrase.
        @max_phrase_size: maximum allowed phrase size
        @word_freq: raw frequency of each word in the input corpus
        @active_indices: set of active indices
        """ 
        hash_counter = word_freq
        n = 2
        
        #iterate until documents is empty
        while(len(documents) > 0):
            temp_documents = []
            new_active_indices = []
            #go over each document
            for d_i,doc in enumerate(documents):
                #get set of indices of phrases of length n-1 with min support
                new_word_indices = []
                word_indices = active_indices[d_i]
                for index in word_indices:
                    words = doc.split()
                    if index+n-2 < len(words):
                        key = ""
                        for i in range(index, index+n-2+1):
                            if i == index+n-2:
                                key = key + words[i]
                            else:
                                key = key + words[i] + " "

                        #check if the phrase 'key' meets min support
                        if hash_counter[key] >= min_support:
                            new_word_indices.append(index)

                #remove the current document if there is no more phrases of length
                #n which satisfy the minimum support threshold
                if len(new_word_indices) != 0:
                    new_active_indices.append(new_word_indices)
                    temp_documents.append(doc)
                    words = doc.split()
                    for idx, i in enumerate(new_word_indices[:-1]):
                        phrase = ""
                        if (new_word_indices[idx+1] == i + 1):
                            for idx in range(i, i+n):
                                if idx == i+n-1:
                                    phrase += words[idx]
                                else:
                                    phrase += words[idx] + " "
                        hash_counter[phrase] += 1

            documents = temp_documents
            active_indices = new_active_indices
            n += 1
            if n == max_phrase_size:
                break
        #import code; code.interact(local=dict(globals(), **locals()))
        hash_counter = Counter(x for x in hash_counter.elements() if hash_counter[x] >= min_support)
        
        return hash_counter 

    def _agglomerative_clustering(self, doc, hash_counter, alpha, total_words):
        """
        Performs agglomerative clustering to get meaningful phrases from the input document.

        Parameters:
        @doc: input corpus
        @hash_counter: map from phrases to their respective raw frequency
        @alpha: threshold for the significance score
        @total_words: total count of the words in input corpus.
        """
        sig_map = {}
        phrases = doc.split()
        while(True):
            max_sig = float("-inf")
            max_pair = -1
            for index, word in enumerate(phrases[:-1]):
                phrase = phrases[index]+" "+phrases[index+1]
                if phrase not in sig_map:
                    sig_score = self._significance_score(phrases[index], phrases[index+1], hash_counter, total_words)
                    sig_map[phrase] = sig_score
                
                if(max_sig < sig_map[phrase]):
                    max_sig = sig_map[phrase]
                    max_pair = index

            if(max_sig < alpha):
                break
                
            #merge max pair
            merged_phrase = phrases[max_pair] + " "+ phrases[max_pair+1]
            
            #fix phrases
            phrases[max_pair] = merged_phrase
            phrases.pop(max_pair+1)
        
        return phrases
            
    def _significance_score(self, phrase1, phrase2, hash_counter, total_words):
        """
        Calculates the signifance score of the phrase obtained by joining phrase1 
        and phrase2. The significance score basically measures how unlikely is the
        new phrase. The more unlikely it is, the more informative it will be.

        Parameters:
        @phrase1: first phrase
        @phrase2: second phrase
        @hash_counter: map from phrases to their respective raw frequency
        @total_words: total count of the words in input corpus.
        """
        combined_phrase = phrase1+" "+phrase2
        combined_size = len(combined_phrase.split())
        actual_occurence = hash_counter[combined_phrase]
        numerator = hash_counter[phrase1]*hash_counter[phrase2]
        
        if actual_occurence == 0:
            return float("-inf")
        
        denominator = total_words * total_words
        independent_prob = numerator/denominator
        independent_prob *= 2
        
        expected_occurence = independent_prob*total_words
        
        return (actual_occurence-expected_occurence)/math.sqrt(max(actual_occurence, expected_occurence))

    def _get_true_frequency(self, hash_counter):
        """
        Updates the raw frequency of the phrases to get their true frequencies.
        """
        true_counter = Counter(hash_counter)
        for key in hash_counter:
            val = key.split()
            if len(val) <= 1:
                continue
            substr1 = " ".join(val[0:-1])
            substr2 = " ".join(val[1:])
            true_counter[substr1] -= hash_counter[key]
            true_counter[substr2] -= hash_counter[key]
        return true_counter

    def _get_stopwords(self):
        """
        Returns a list of stopwords.
        """
        f = codecs.open("topmine_src/stopwords.txt", encoding="utf-8")
        stopwords = set()
        for line in f:
            stopwords.add(line.rstrip())
        return stopwords

    def _get_word_freq(self, documents):
        """
        Calculates the frequency of each word in the input document.
        """
        total_words = 0
        word_freq = Counter()
        active_indices = []
        for doc_index, doc in enumerate(documents):
            words = doc.split()
            word_indices = []
            for word_index, word in enumerate(words):
                word_freq[word] += 1
                word_indices.append(word_index)
                total_words += 1
            active_indices.append(word_indices)

        return total_words, word_freq, active_indices

    def _get_partitioned_docs(self, document_range, doc_phrases):
        """
        Partitions the input document based on the punctuations.
        """
        partitioned_docs = []
        start = 0
        end = 0
        for idx in document_range:
            end = idx
            final_doc = []
            for i in range(start, end):
                final_doc.extend(doc_phrases[i])
            partitioned_docs.append(final_doc)
            start = end

        return partitioned_docs

    def _process_partitioned_docs(self, partitioned_docs):
        self.vocab = {}
        self.index_vocab = []
        self.partitioned_docs = []
        word_counter = 0
        for document_index, document in enumerate(partitioned_docs):
            document_of_phrases = []
            for phrase in document:
                phrases_of_words = []
                for word in phrase.split():
                    if word not in self.vocab:
                        self.vocab[word] = word_counter
                        self.index_vocab.append(word)
                        word_counter += 1
                    phrases_of_words.append(self.vocab[word])
                document_of_phrases.append(phrases_of_words)
            self.partitioned_docs.append(document_of_phrases)

    def _preprocess_input(self, text):
        """
        Performs preprocessing on the input document. Includes stopword removal.
        """
        # f = codecs.open(filename, 'r', encoding="utf-8")
        documents = []
        document_range = []
        i = 0
        num_docs = 0
        for line in text:
            line_lowercase = line.lower()
            sentences_no_punc = re.split(r"[.,;!?]",line_lowercase)
            stripped_sentences = []
            for sentence in sentences_no_punc:
                stripped_sentences.append(re.sub(u'[^A-Za-z0-9а-яА-ЯёЁ]+', ' ', sentence))
            sentences_no_punc = stripped_sentences
            i += len(sentences_no_punc)
            document_range.append(i)
            documents.extend(sentences_no_punc)
            num_docs += 1

        documents = [doc.strip() for doc in documents]

        # remove stop-words
        # documents2 = []
        # for doc in documents:
        #     documents2.append(' '.join([word for word in doc.split() if word not in stopwords]))

        # documents = documents2[:]

        return documents, document_range, num_docs

    def _run_phrase_mining(self, min_support, max_phrase_size, alpha, text):
        """
        Runs the phrase mining algorithm.

        Parameters:
        @min_support: minimum support threshold which must be satisfied by each phrase.
        @max_phrase_size: maximum allowed phrase size
        @alpha: threshold for the significance score
        """

        # stopwords = self._get_stopwords()
        # logger.error(text)

        documents, document_range, num_docs = self._preprocess_input(text)

        #calculate frequency of all words
        total_words, word_freq, active_indices = self._get_word_freq(documents)

        vocab_size = len(word_freq)

        #run frequent pattern mining 
        hash_counter = self._frequentPatternMining(documents, min_support, max_phrase_size, word_freq, active_indices)

        #run agglomerative clustering
        doc_phrases = []
        for doc in documents:
            doc_phrases.append(self._agglomerative_clustering(doc, hash_counter, alpha, total_words))

        #update true count of each phrase
        self.true_counter = self._get_true_frequency(hash_counter)

        partitioned_docs = self._get_partitioned_docs(document_range, doc_phrases)
        self._process_partitioned_docs(partitioned_docs)

        return self.partitioned_docs, self.index_vocab

    def get_frequent_phrases(self, min_support):
        """
        Returns the most frequent phrases in the corpus that occur more than 
        the minimum support in descending order of frequency
        """
        frequent_phrases = []
        for key,value in self.true_counter.most_common():
            if value >= min_support and len(key.split(" "))>1:
                frequent_phrases.append([key, value])
            elif value < min_support:
                break
        return frequent_phrases
