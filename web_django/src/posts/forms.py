from datetime import datetime

from django import forms
from django.utils import timezone

from posts.models import TrackedPosts, TrackedCities, TrackedTopics
from services.models import Service

class AddPostForm(forms.Form):
    social = forms.CharField()
    owner_id = forms.CharField()
    post_id = forms.CharField()
    end_date = forms.DateTimeField()
    city = forms.CharField()
    topic = forms.CharField()
    new_city = forms.BooleanField(required=False)
    new_topic = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(AddPostForm, self).__init__(*args, **kwargs)

    def _add_new_city(self):
        if self.cleaned_data['new_city']:
            city = TrackedCities.objects.filter(name=self.cleaned_data['city'])
            if city.exists():
                return

            new_city = TrackedCities(name=self.cleaned_data['city'])
            new_city.save()

            cass = Service.objects.get(name="db-worker")
            cass.task['task_name'] = "insert_data"
            cass.task['args'] = {
                'table': 'tracked_post_cities',
                'insert_cols': ['city'],
                'insert_values': [[self.cleaned_data['city']]]
            }
            cass.call()

    def _add_new_topic(self):
        if self.cleaned_data['new_topic']:
            topic = TrackedTopics.objects.filter(
                name=self.cleaned_data['topic']
            )
            if topic.exists():
                return

            new_topic = TrackedTopics(name=self.cleaned_data['topic'])
            new_topic.save()

            cass = Service.objects.get(name="db-worker")
            cass.task['task_name'] = "insert_data"
            cass.task['args'] = {
                'table': 'tracked_post_topics',
                'insert_cols': ['topic'],
                'insert_values': [[self.cleaned_data['topic']]]
            }
            cass.call()

    def has_post(self):
        cass = Service.objects.get(name="db-worker")
        cass.task['task_name'] = "get_post"
        cass.task['args'] = {
            'social': self.cleaned_data['social'],
            'owner_id': self.cleaned_data['owner_id'],
            'post_id': self.cleaned_data['post_id']
        }
        post = cass.call()

        return post is not None

    def parse_post(self):
        parser = Service.objects.get(name='parser')
        parser.task['task_name'] = "post"
        parser.task['args'] = {
            'social': self.cleaned_data['social'],
            'owner_id': self.cleaned_data['owner_id'],
            'post_id': self.cleaned_data['post_id']
        }
        post = parser.call()
        if post != 'ok':
            raise Exception("Can't parse post")

    def save_to_cass(self):
        cass = Service.objects.get(name="db-worker")

        ttl = int((self.cleaned_data['end_date'] - timezone.now())\
            .total_seconds())
        cass.task['task_name'] = "set_posts_monitoring"
        cass.task['args'] = {
            'username': self.user.username,
            'social': self.cleaned_data['social'],
            'owner_id': self.cleaned_data['owner_id'],
            'post_id': self.cleaned_data['post_id'],
            'state': True,
            'ttl': ttl
        }
        cass.call()

        cass.task['task_name'] = "set_tracked_state"
        cass.task['args'] = {
            'username': self.user.username,
            'social': self.cleaned_data['social'],
            'owner_id': self.cleaned_data['owner_id'],
            'post_id': self.cleaned_data['post_id'],
            'state': True,
            'end_date': self.cleaned_data['end_date'],
            'post_city': self.cleaned_data['city'],
            'post_topic': self.cleaned_data['topic']
        }
        cass.call()

    def save_post(self):
        self._add_new_city()
        self._add_new_topic()

        if not self.has_post():
            self.parse_post()

        self.save_to_cass()

        post = TrackedPosts(
            user=self.user,
            social=self.cleaned_data['social'],
            owner_id=self.cleaned_data['owner_id'],
            post_id=self.cleaned_data['post_id'],
            from_date=timezone.now(),
            to_date=self.cleaned_data['end_date'],
            city=TrackedCities.objects.get(name=self.cleaned_data['city']),
            topic=TrackedTopics.objects.get(name=self.cleaned_data['topic']),
        )
        post.save()
