from django.contrib.auth.models import User
from django.db import models

from services.models import Service


class TrackedCities(models.Model):
    name = models.CharField(max_length=100)


class TrackedTopics(models.Model):
    name = models.CharField(max_length=100)


class TrackedPosts(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    city = models.ForeignKey(TrackedCities, on_delete=models.CASCADE)
    topic = models.ForeignKey(TrackedTopics, on_delete=models.CASCADE)
    social = models.CharField(max_length=10)
    owner_id = models.CharField(max_length=20)
    post_id = models.CharField(max_length=20)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    installs = models.IntegerField(default=0)

    @property
    def link(self):
        if self.social == "vk":
            link = f'https://vk.com/wall{self.owner_id}_{self.post_id}'
        elif self.social == "ok":
            link = f'https://ok.ru/group/{self.owner_id}/topic/{self.post_id}'
        elif self.social == "inst":
            link = f'https://instagram.com/p/{self.post_id}'
        else:
            link = ""

        return link

    def get_post_data(self):
        cass = Service.objects.get(name="db-worker")
        cass.task['task_name'] = "get_post"
        cass.task['args'] = {
            'social': self.social,
            'owner_id': self.owner_id,
            'post_id': self.post_id
        }

        post_data = cass.call()

        return post_data

    def get_group_data(self):
        group_id = self.owner_id
        if self.social == 'vk' and group_id.startswith('-'):
            group_id = group_id[1:]

        neo4j = Service.objects.get(name="db-worker")
        neo4j.task = {
            'type': 'select',
            'table': 'Group',
            'items': {'social': self.social, 'group_id': group_id}
        }

        data = neo4j.call()

        return data[0]

    def get_stat(self):
        cass = Service.objects.get(name="db-worker")
        cass.task['task_name'] = "get_post_stat"
        cass.task['args'] = {
            'social': self.social,
            'owner_id': self.owner_id,
            'post_id': self.post_id,
            'count': None
        }

        post_data = cass.call()

        return post_data

    def get_data(self):
        data = {
            'tr_post_id': self.id,
            'tracked_city': self.city.name,
            'tracked_topic': self.topic.name,
            'link': self.link,
            'installs': self.installs
        }

        post_data = self.get_post_data()
        data.update(post_data)

        group_data = self.get_group_data()
        data['name'] = group_data['name']

        stat = self.get_stat()
        stat.append({
            'likes': post_data['likes'] or 0,
            'shares': post_data['shares'] or 0,
            'views': post_data['views'] or 0,
            'replies': post_data['replies'] or 0
        })

        for field in ['likes', 'shares', 'views', 'replies']:
            data[field] = max(map(lambda x: x[field] or 0, stat))

        return data


class Post():

    mention_type = "post"
    fields = [
        'social',
        'owner_id',
        'post_id',
        'date',
        'text',
        'likes',
        'shares',
        'views',
        'replies',
        'link',
        'mention_type',
        'is_viewed',
        'highlight'
    ]

    def __init__(self, **kwargs):
        self.social = kwargs.get('social', '')
        self.owner_id = kwargs.get('owner_id', '')
        self.post_id = kwargs.get('post_id', '')

        self.date = kwargs.get('date')
        self.text = kwargs.get('text', '')
        self.highlight = kwargs.get('highlight', self.text)
        self.likes = kwargs.get('likes', 0)
        self.shares = kwargs.get('shares', 0)
        self.views = kwargs.get('views', 0)
        self.replies = kwargs.get('replies', 0)
        self.is_viewed = False

    def __iter__(self):
        for field in self.fields:
            yield (field, getattr(self, field))

    @property
    def link(self):
        if self.social == "vk":
            link = f'https://vk.com/wall{self.owner_id}_{self.post_id}'
        elif self.social == "ok":
            link = f'https://ok.ru/group/{self.owner_id}/topic/{self.post_id}'
        elif self.social == "inst":
            link = f'https://instagram.com/p/{self.post_id}'
        else:
            link = ""

        return link


class Reply():

    mention_type = "reply"
    fields = [
        'social',
        'owner_id',
        'post_id',
        'reply_id',
        'date',
        'text',
        'parent_reply',
        'likes',
        'replies',
        'link',
        'mention_type',
        'is_viewed',
        'highlight'
    ]

    def __init__(self, **kwargs):
        self.social = kwargs.get('social', '')
        self.owner_id = kwargs.get('owner_id', '')
        self.post_id = kwargs.get('post_id', '')
        self.reply_id = kwargs.get('reply_id', '')

        self.date = kwargs.get('date')
        self.text = kwargs.get('text', '')
        self.highlight = kwargs.get('highlight', self.text)
        self.parent_reply = kwargs.get('parent_reply') or None
        self.likes = kwargs.get('likes', 0)
        self.replies = kwargs.get('replies', 0)
        self.is_viewed = False

    def __iter__(self):
        for field in self.fields:
            yield (field, getattr(self, field))

    @property
    def link(self):
        if self.social == "vk":
            link = 'https://vk.com/wall{}_{}?reply={}'.format(
                self.owner_id,
                self.post_id,
                self.reply_id
            )
            if self.parent_reply is not None:
                link += f"&thread={self.parent_reply}"

        elif self.social == "ok":
            link = f'https://ok.ru/group/{self.owner_id}/topic/{self.post_id}'
        elif self.social == "inst":
            link = f'https://instagram.com/p/{self.post_id}'
        else:
            link = ""

        return link


class TopicPost():

    mention_type = "topic_post"
    fields = [
        'social',
        'owner_id',
        'post_id',
        'topic_id',
        'date',
        'text',
        'likes',
        'link',
        'mention_type',
        'is_viewed',
        'highlight'
    ]

    def __init__(self, **kwargs):
        self.social = kwargs.get('social', '')
        self.owner_id = kwargs.get('owner_id', '')
        self.topic_id = kwargs.get('topic_id', '')
        self.post_id = kwargs.get('post_id', '')

        self.date = kwargs.get('date')
        self.text = kwargs.get('text', '')
        self.highlight = kwargs.get('highlight', self.text)
        self.likes = kwargs.get('likes', 0)
        self.is_viewed = False

    def __iter__(self):
        for field in self.fields:
            yield (field, getattr(self, field))

    @property
    def link(self):
        if self.social == "vk":
            link = 'https://vk.com/topic{}_{}?post={}'.format(
                self.owner_id,
                self.topic_id,
                self.post_id
            )
        else:
            link = ""

        return link


class PostStat():
    pass
