from django.urls import path

from . import views

app_name = 'posts'
urlpatterns = [
    path('<str:social>/<str:owner_id>/<str:post_id>', views.post, name='post'),
    path('monitor', views.monitor, name='monitor'),
    path('tracked', views.tracked, name='tracker-posts'),
    path('tracked-posts', views.tracked_posts, name='tracked-posts'),
    path('add-post', views.add_post, name='add posts'),
    path('update-post-installs', views.update_post_installs)
]
