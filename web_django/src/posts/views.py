from datetime import datetime, timedelta
import json

from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from django.utils.timezone import make_aware
import requests

# from common.rabbit import RpcClient
# from services.models import Service
from .forms import AddPostForm
from .models import Post, TrackedCities, TrackedTopics, TrackedPosts
from web.sync import data_sync, tracked_posts_sync


@login_required
def post(request, social, owner_id, post_id):
    post = get_object_or_404(Post, social=social,
                             owner_id=owner_id, post_id=post_id)
    # stat = Stat.objects.filter(
    #     post__id=post.id
    # ).order_by(F('insert_time').desc())[0]
    context = {
        'post': post
        # 'stat': stat
    }

    return render(request, 'posts/post.html', context)


@login_required
def monitor(request):
    context = None

    return render(request, 'posts/monitor.html', context)


@login_required
def tracked(request):
    today = datetime.today()
    yesterday = today - timedelta(days=1)

    start_date = yesterday.strftime("%Y-%m-%d")
    end_date = today.strftime("%Y-%m-%d")

    data_sync('tracked_post_cities', TrackedCities, 'city')
    data_sync('tracked_post_topics', TrackedTopics, 'topic')
    tracked_posts_sync()

    cities = TrackedCities.objects.order_by('name').all()
    topics = TrackedTopics.objects.order_by('name').all()
    context = {
        'start_date': start_date,
        'end_date': end_date,
        'cities': cities,
        'topics': topics
    }

    return render(request, 'posts/tracked.html', context)


@login_required
def tracked_posts(request):
    start_date = datetime.strptime(request.GET['start_date'], '%Y-%m-%d')
    start_date = make_aware(start_date)
    end_date = datetime.strptime(request.GET['end_date'], '%Y-%m-%d')
    end_date = make_aware(end_date)
    cities = json.loads(request.GET['cities'])
    topics = json.loads(request.GET['topics'])

    date_filter = Q(from_date__gte=start_date, from_date__lte=end_date)\
        | Q(to_date__gte=start_date, to_date__lte=end_date)\
        | Q(from_date__lte=start_date, to_date__gte=end_date)

    posts = TrackedPosts.objects.filter(date_filter)
    
    if len(cities):
        posts = posts.filter(city__id__in=cities)
    if len(topics):
        posts = posts.filter(topic__id__in=topics)

    posts = [post.get_data() for post in posts]

    return JsonResponse({"success": True, "posts": posts}, status=200)


@login_required
def add_post(request):
    form = AddPostForm(request.POST, user=request.user)

    if form.is_valid():
        form.save_post()
        return JsonResponse({'success': True}, status=200)
    else:
        return JsonResponse({'errors': form.errors}, status=400)

@csrf_exempt
def update_post_installs(request):
    post = TrackedPosts.objects.get(id=request.POST['post_id'])
    post.installs = request.POST['installs']
    post.save()

    return JsonResponse({'success': True}, status=200)
