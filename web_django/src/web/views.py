import json

from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from fts.models import FTSRule
from notifications.models import Notification
from notifications.signals import notify
from webpush import send_user_notification
from webpush.models import PushInformation

from .forms import RegisterForm


@login_required
def index(request):
    return render(request, 'index.html')


def log_in(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect(request.GET.get('next', '/'))
        else:
            context['error'] = True
    return render(request, 'login.html', context)


def log_out(request):
    logout(request)
    return redirect('/login')


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
        else:
            errors = form.errors.as_data()
    else:
        form = RegisterForm()
        errors = {}
    return render(request, 'register.html', {'form': form, 'errors': errors})


@login_required
def notifications(request):
    request.user.notifications.mark_all_as_read()

    page = request.GET.get('page', 1)
    notifications_list = Notification.objects.filter(
        actor_object_id=request.user.id).order_by('-timestamp')
    paginator = Paginator(notifications_list, 16)

    try:
        notifications = paginator.page(page)
    except PageNotAnInteger:
        notifications = paginator.page(1)
    except EmptyPage:
        notifications = paginator.page(paginator.num_pages)

    [ntf.mark_as_read() for ntf in notifications]

    return render(request, 'notifications.html',
                  {'paginator': paginator, 'notifications': notifications})


@require_POST
@csrf_exempt
def send_push(request):
    body = request.body
    data = json.loads(body)

    if 'title' not in data or 'body' not in data:
        return JsonResponse(
            status=400, data=data
        )

    rule = FTSRule.objects.filter(value=data['rule'])
    if rule.exists():
        rule = rule[0]
        payload = {
            'head': data['title'],
            'body': data['body'][:150],
            'url': data['link']
        }

        for user in rule.users.all():
            send_user_notification(user=user, payload=payload, ttl=1000)
            notify.send(user, recipient=user, verb='Ntf', **payload)

    return JsonResponse(
        status=200, data={"message": "Web push successful"}
    )
