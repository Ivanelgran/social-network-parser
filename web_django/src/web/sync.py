from datetime import datetime

from django.contrib.auth.models import User
from django.utils.timezone import make_aware

from fts.models import FTSRule
from posts.models import TrackedPosts, TrackedCities, TrackedTopics
from services.models import Service


def rule_sync():
    cass = Service.objects.get(name="db-worker")
    cass.task['task_name'] = "select_data"
    cass.task['args'] = {'table': 'rules'}

    cass_rules = cass.call()

    rules = FTSRule.objects.all()

    # for rule in rules:
    #     has_rule = [*filter(
    #         lambda x: x['social'] == rule.social and x['value'] == rule.value,
    #         cass_rules
    #     )]
    #     if not len(has_rule):
    #         rule.delete()

    for rule in cass_rules:
        has_rule = [*filter(
            lambda x: rule['social'] == x.social and rule['value'] == x.value,
            rules
        )]

        if not len(has_rule):
            new_rule = FTSRule(
                social=rule['social'],
                value=rule['value'],
                is_active=rule['is_active']
            )
            new_rule.save()


def data_sync(table_name: str, model, field: str):
    cass = Service.objects.get(name="db-worker")
    cass.task['task_name'] = "select_data"
    cass.task['args'] = {'table': table_name}

    cass_data = cass.call()
    model_data = model.objects.all()

    for data in cass_data:
        has_data = [*filter(lambda x: data[field] == x.name, model_data)]

        if not len(has_data):
            new_data = model(name=data[field])
            new_data.save()


def tracked_posts_sync():
    cass = Service.objects.get(name="db-worker")
    cass.task['task_name'] = "select_data"
    cass.task['args'] = {'table': 'tracked_posts'}

    cass_posts = cass.call()
    
    for cass_post in cass_posts:
        if cass_post['post_city'] is None or cass_post['post_topic'] is None:
            continue

        post = TrackedPosts.objects.filter(
            social=cass_post['social'],
            owner_id=cass_post['owner_id'],
            post_id=cass_post['post_id']
        )
        if not post.exists():

            cass.task['task_name'] = "get_tracked_from_date"
            cass.task['args'] = {
                'social': cass_post['social'],
                'owner_id': cass_post['owner_id'],
                'post_id': cass_post['post_id']
            }

            from_date = cass.call()
            from_date = datetime.fromtimestamp(int(from_date/1e6))
            from_date = make_aware(from_date)

            new_post = TrackedPosts(
                user=User.objects.get(id=1),
                city=TrackedCities.objects.get(name=cass_post['post_city']),
                topic=TrackedTopics.objects.get(name=cass_post['post_topic']),
                social=cass_post['social'],
                owner_id=cass_post['owner_id'],
                post_id=cass_post['post_id'],
                from_date=from_date,
                to_date=cass_post['end_date']
            )
            new_post.save()
