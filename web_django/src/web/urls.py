"""web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
import notifications.urls

import web.views as general_views

urlpatterns = [
    path('', general_views.index, name='index'),
    path('login/', general_views.log_in, name='login'),
    path('logout/', general_views.log_out, name='logout'),
    path('register/', general_views.register, name='register'),
    path('notifications/', general_views.notifications, name='notifications'),
    path('webpush/', include('webpush.urls')),
    path('send_push', general_views.send_push),
    path('inbox/notifications/', include(notifications.urls)),
    path('posts/', include('posts.urls')),
    path('fts/', include('fts.urls')),

    path('admin/', admin.site.urls),
]
