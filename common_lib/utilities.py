import time
import logging
import pickle
from bottle import request

LOGGER = logging.getLogger('WebService.service_info')


def time_it(fn):
    """Декоратор, служит для измерения времени выполнения функций, выводит результат в логи"""
    def _time(*args, **kwargs):
        LOGGER.info(f'Start "{fn.__name__}" method!')

        start_time = time.time()
        result = fn(*args, **kwargs)
        time_spent = time.time() - start_time

        LOGGER.info(f'Stop "{fn.__name__}" method! --- {time_spent} seconds ---')
        return result

    return _time


def show_ip_address(fn):
    """Декоратор для отображения IP адресса запроса"""
    def _get_ip(*args, **kwargs):
        client_ip = request.environ.get('HTTP_X_FORWARDED_FOR') or request.environ.get('REMOTE_ADDR')
        LOGGER.info(f'REMOTE IP: "{client_ip}"! FUNCTION: {fn.__name__}')
        result = fn(*args, **kwargs)
        return result

    return _get_ip


def load_pickle(file_name, verbose=True):
    with open(file_name, 'rb') as fp:
        data = pickle.load(fp)
    if verbose:
        print(f'{file_name} - загружен!')
    return data


def save_pickle(obj, file, verbose=True):
    with open(file, 'wb') as fp:
        pickle.dump(obj, fp)
    if verbose:
        print(f'{file} - сохранен!')
