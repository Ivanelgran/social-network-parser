import neomodel

from common_lib.src.config import get_config
from common_lib.src import neo4j_models


config = get_config("./common_lib/config/db_worker.yml")

NEO4J_LOGIN = config['neo4j']['login']
NEO4J_PASS = config['neo4j']['password']
NEO4J_HOST = config['neo4j']['host']
NEO4J_PORT = config['neo4j']['port']

url = f'bolt://{NEO4J_LOGIN}:{NEO4J_PASS}@{NEO4J_HOST}:{NEO4J_PORT}'
neomodel.config.DATABASE_URL = url


class Models():
    person = neo4j_models.Person
    group = neo4j_models.Group
    school = neo4j_models.School
    university = neo4j_models.University


def select_with_filters(table, filters):
    result = table.nodes.filter(**filters).all()

    return result


def update_node(table, keys, items):
    node = table.nodes.filter(**keys).first()
    [*map(lambda item: setattr(node, item[0], item[1]), items.items())]
    node.save()
