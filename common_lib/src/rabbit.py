from datetime import datetime
import pickle
import time

from cassandra.cqlengine.query import ContextQuery
import pika

from common_lib.cass_worker import (
    CassandraTables, setup_connection, insert_data)
from common_lib.src.retry import retry
from common_lib.src.config import get_config


class Rabbit():
    """Base class for RabbitMQ connection. """

    def __init__(self):

        config = get_config('./common_lib/config/rabbit.yml')

        host = config['host']
        user = config['login']
        password = config['password']

        self.__credentials = pika.PlainCredentials(user, password)
        self.__parameters = pika.ConnectionParameters(
            host=host, credentials=self.__credentials, heartbeat=600,
            blocked_connection_timeout=600)
        self.connection = None

    @retry(5)
    def connect(self):
        """Tries to create RabbitMQ connection 5 times

        Returns:
            RabbitMQ connection.
        """

        conn = pika.BlockingConnection(self.__parameters)

        return conn


class Client(Rabbit):
    """RabbitMQ client."""

    def __init__(self, queue_name: str):
        """Inits RabbitMQ client class.

        Args:
            queue_name: Queue name.
        """
        super().__init__()
        self.queue_name = queue_name

    def send(self, message):
        """Sends message.

        Args:
            message: Message.
        """
        with self.connect() as conn:
            channel = conn.channel()
            channel.queue_declare(queue=self.queue_name)
            channel.basic_publish(exchange='',
                                  routing_key=self.queue_name,
                                  body=pickle.dumps(message))


class Server(Rabbit):
    """RabbitMQ server class."""
    def start(self, queue: str, callback: pika.frame.Method):
        """Starts RabbitMQ server.

        Args:
            queue: Name of the RabbitMQ queue.
            callback: Function that is called when message arrives.
        """
        self.connection = self.connect()
        channel = self.connection.channel()
        channel.queue_declare(queue=queue)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue=queue, on_message_callback=callback)
        channel.start_consuming()
        self.connection.close()


class RpcClient(Rabbit):

    def __init__(self, queue_name: str):
        super().__init__()
        self.queue_name = queue_name
        self.response = None

    def send(self, message):
        """Sends message.

        Args:
            message: Message.
        """
        with self.connect() as conn:
            channel = conn.channel()
            channel.basic_consume(
                'amq.rabbitmq.reply-to',
                self.on_reply_from_server,
                auto_ack=True)
            channel.basic_publish(
                exchange='',
                routing_key=self.queue_name,
                body=pickle.dumps(message),
                properties=pika.BasicProperties(
                    reply_to='amq.rabbitmq.reply-to'))

            channel.start_consuming()

        return pickle.loads(self.response)

    def on_reply_from_server(self, ch, method, properties, body):
        self.response = body
        ch.close()


class RpcServer(Rabbit):
    def start(self, queue: str, callback: pika.frame.Method):
        self.connection = self.connect()
        channel = self.connection.channel()
        channel.queue_declare(queue=queue, exclusive=True, auto_delete=True)
        channel.basic_consume(queue=queue, on_message_callback=callback)
        channel.start_consuming()
        self.connection.close()


class TaskManager():

    def __init__(self, queue_name, timeout=60):
        self.client = Client(queue_name)
        self.timeout = timeout

    def __check_task_status(self, conn, date):
        with ContextQuery(
            CassandraTables.rabbit_tasks, connection=conn
        ) as table:
            record = table(date=date).first()

        return record.status

    def __delete_completed_task(self, conn, date):
        with ContextQuery(
            CassandraTables.rabbit_tasks, connection=conn
        ) as table:
            table(date=date).delete()

    def __track_task(self, date):
        conn = setup_connection()

        start_time = time.time()
        time_left = time.time() - start_time
        while time_left < self.timeout:
            if self.__check_task_status(conn, date):
                self.__delete_completed_task(conn, date)
                return True

            time.sleep(1)
            time_left = time.time() - start_time

        return False

    def run_task(self, task_name, args=None, track_progress=False):
        task = {'task_name': task_name}

        if args is not None:
            task['args'] = args

        if track_progress:
            task_date = datetime.now()
            task['task_date'] = task_date
            insert_data(CassandraTables.rabbit_tasks,
                        {'date': task_date, 'status': False})
            self.client.send(task)
            return self.__track_task(task_date)
        else:
            self.client.send(task)

        return True
