import yaml


def get_config(path: str):
    with open(path) as config_file:
        config = yaml.safe_load(config_file)
    return config
