from neomodel import (
    StructuredNode, StringProperty, IntegerProperty, Relationship,
    RelationshipTo, RelationshipFrom, FloatProperty, DateTimeProperty)


class Group(StructuredNode):
    social = StringProperty(required=True)
    group_id = StringProperty(required=True)

    deactivated = StringProperty()
    is_closed = IntegerProperty()
    last_date = DateTimeProperty()
    last_update = DateTimeProperty()
    main_city = IntegerProperty()
    main_prop = FloatProperty()
    members_count = IntegerProperty()
    our_prop = FloatProperty()
    name = StringProperty()
    type = StringProperty()

    subscribers = RelationshipFrom("Person", "SUBSCRIBES")

    def __iter__(self):
        relationships = ['subscribers']
        fields = [*filter(lambda x: x not in relationships,
                          self.defined_properties().keys())]
        for field in fields:
            yield (field, self.__dict__[field])


class Person(StructuredNode):
    social = StringProperty(required=True)
    person_id = StringProperty(required=True)

    alcohol = IntegerProperty()
    birthday = StringProperty()
    city_id = IntegerProperty()
    country_id = IntegerProperty()
    deactivated = StringProperty()
    first_name = StringProperty()
    hometown = StringProperty()
    is_closed = IntegerProperty()
    last_name = StringProperty()
    life_main = IntegerProperty()
    occupation_id = IntegerProperty()
    occupation_name = StringProperty()
    occupation_type = StringProperty()
    people_main = IntegerProperty()
    political = IntegerProperty()
    relation = IntegerProperty()
    religion = StringProperty()
    sex = IntegerProperty()
    smoking = IntegerProperty()
    wall_default = StringProperty()

    friends = Relationship("Person", "FRIEND")
    subscribes = RelationshipTo("Group", "SUBSCRIBES")
    schools = RelationshipTo("School", "EDUCATION")
    universities = RelationshipTo("University", "EDUCATION")

    def __iter__(self):
        relationships = ['friends', 'subscribes', 'schools', 'universities']
        fields = [*filter(lambda x: x not in relationships,
                          self.defined_properties().keys())]
        for field in fields:
            yield (field, self.__dict__[field])


class School(StructuredNode):
    id = IntegerProperty(required=True)

    city = IntegerProperty()
    country = IntegerProperty()
    name = StringProperty()

    students = RelationshipFrom("Person", "EDUCATION")

    def __iter__(self):
        relationships = ['students']
        fields = [*filter(lambda x: x not in relationships,
                          self.defined_properties().keys())]
        for field in fields:
            yield (field, self.__dict__[field])


class University(StructuredNode):
    id = IntegerProperty(required=True)

    city = IntegerProperty()
    country = IntegerProperty()
    name = StringProperty()

    students = RelationshipFrom("Person", "EDUCATION")

    def __iter__(self):
        relationships = ['students']
        fields = [*filter(lambda x: x not in relationships,
                          self.defined_properties().keys())]
        for field in fields:
            yield (field, self.__dict__[field])
