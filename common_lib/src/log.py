import logging
import logging.config
import yaml
from typing import Union


with open('./common_lib/config/logging.yml') as config_file:
    config = yaml.safe_load(config_file)
    logging.config.dictConfig(config)


def get_logger(name: str, level: Union[int, str] = logging.INFO):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    return logger
