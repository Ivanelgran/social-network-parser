from common_lib.src.rabbit import RpcClient
from common_lib.cass_worker import (
    CassandraTables, select_data, setup_connection)


class Post():

    def __init__(self, social, owner_id, post_id, date, post_type, text,
                 replies, *args, **kwargs):
        self.__social = social
        self.__owner_id = owner_id
        self.__post_id = post_id
        self.date = date
        self.post_type = post_type
        self.text = text
        self.replies = replies

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return (
            self.__class__ == other.__class__ and self.id == other.id
        )

    @property
    def id(self):
        return self.make_post_id(self.social, self.owner_id, self.post_id)

    @property
    def social(self):
        return self.__social

    @property
    def owner_id(self):
        return self.__owner_id

    @property
    def post_id(self):
        return self.__post_id

    @staticmethod
    def make_post_id(social, owner_id, post_id):
        return social + '==' + owner_id + '==' + post_id

    @staticmethod
    def get_id_components(post_id):
        social, owner_id, post_id = post_id.split('==')

        return social, owner_id, post_id


class Reply():

    def __init__(self, social, owner_id, post_id, reply_id, author, date,
                 likes, replies, text, parent_reply_id, *args, **kwargs):
        self.__social = social
        self.__owner_id = owner_id
        self.__post_id = post_id
        self.__reply_id = reply_id
        self.author = author
        self.date = date
        self.likes = likes
        self.replies = replies
        self.text = text
        self.parent_reply_id = parent_reply_id

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return (
            self.__class__ == other.__class__ and
            self.id == other.id
        )

    @property
    def id(self):
        return self.make_post_id(self.social, self.owner_id,
                                 self.post_id, self.reply_id)

    @property
    def social(self):
        return self.__social

    @property
    def owner_id(self):
        return self.__owner_id

    @property
    def post_id(self):
        return self.__post_id

    @property
    def reply_id(self):
        return self.__reply_id

    @staticmethod
    def make_post_id(social, owner_id, post_id, reply_id):
        return social + '==' + owner_id + '==' + post_id + '==' + reply_id

    @staticmethod
    def get_id_components(str_reply_id):
        social, owner_id, post_id, reply_id = str_reply_id.split('==')
        return social, owner_id, post_id, reply_id


class PostManager():

    def __init__(self, logger):
        self.conn = setup_connection()
        self.logger = logger

    def __get_post_info(self, social, owner_id, post_id, update=False):
        if update:
            self.__update_post_info(social, owner_id, post_id)

        filters = {'social': social,
                   'owner_id': owner_id,
                   'post_id': post_id}
        post_info = select_data(CassandraTables.all_posts,
                                filters=filters,
                                conn=self.conn)

        if not len(post_info):
            return None

        return post_info[0]

    def __get_post_replies(self, social, owner_id, post_id, update=False):
        if update:
            self.__update_post_replies(social, owner_id, post_id)

        filters = {'social': social,
                   'owner_id': owner_id,
                   'post_id': post_id}
        replies = select_data(CassandraTables.replies,
                              filters=filters,
                              conn=self.conn)

        all_replies = dict()
        for reply_info in replies:
            reply_info['parent_reply_id'] = reply_info['reply_to'] \
                or reply_info['parent_reply']
            reply = Reply(**reply_info)
            all_replies[reply.reply_id] = reply

        return all_replies

    def __update_post_info(self, social, owner_id, post_id):
        task = {'task_name': 'post',
                'args': {'social': social,
                         'owner_id': owner_id,
                         'post_id': post_id}}
        parser = RpcClient('parser_tasks')
        parser.send(task)

    def __update_post_replies(self, social, owner_id, post_id):
        task = {'task_name': 'replies',
                'args': {'social': social,
                         'owner_id': owner_id,
                         'post_id': post_id}}
        parser = RpcClient('parser_tasks')
        parser.send(task)

    def get_posts(self, post_ids, update_replies=False):
        posts = set()

        str_posts_ids = set()
        for i, post_id in enumerate(post_ids):
            self.logger.info(f"Get posts: {i}/{len(post_ids)} {post_id}")
            if not isinstance(post_id, str):
                post_id = Post.make_post_id(**post_id)

            if post_id in str_posts_ids:
                continue
            else:
                str_posts_ids.add(post_id)

            post = self.get_single_post(post_id, update_replies)
            if post is not None:
                posts.add(post)

        return posts

    def get_single_post(self, post_id, update_replies=False):
        social, owner_id, post_id = Post.get_id_components(post_id)

        post_info = self.__get_post_info(social, owner_id, post_id,
                                         update=True)

        if post_info is None:
            self.logger.warning(
                f"Can't find post {social} {owner_id} {post_id}")
            return None

        replies = self.__get_post_replies(social, owner_id, post_id,
                                          update=update_replies)
        post_info['replies'] = replies

        post = Post(**post_info)

        return post

    def update_posts_replies(self, old_posts):
        posts = set()

        for old_post in old_posts:
            self.__update_post_replies(old_post.social,
                                       old_post.owner_id,
                                       old_post.post_id)

            post = self.get_single_post(old_post.id)
            posts.add(post)

        return posts
