from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model


class AllPosts(Model):
    __table_name__ = 'all_posts'
    __options__ = {'gc_grace_seconds': 60}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True, partition_key=True)
    post_id = columns.Text(primary_key=True)
    copy_owner_id = columns.Text()
    copy_post_id = columns.Text()
    date = columns.DateTime()
    likes = columns.Integer()
    post_type = columns.Text()
    replies = columns.Integer()
    sentiment = columns.Integer()
    shares = columns.Integer()
    text = columns.Text()
    topic = columns.Integer()
    views = columns.Integer()

    def __init__(self, social=None, owner_id=None, post_id=None,
                 copy_owner_id=None, copy_post_id=None, date=None,
                 likes=None, post_type=None, replies=None, sentiment=None,
                 shares=None, text=None, topic=None, views=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Bots(Model):
    __table_name__ = 'bots'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    login = columns.Text(primary_key=True)
    city = columns.Text()
    comment = columns.Text()
    name = columns.Text()
    password = columns.Text()
    status_template = columns.Text()
    template_profil = columns.Text()

    def __init__(self, social=None, login=None, city=None, comment=None,
                 name=None, password=None, status_template=None,
                 template_profil=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class BotsPatterns(Model):
    __table_name__ = 'bots_patterns'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    title = columns.Text(primary_key=True)
    career = columns.Map(key_type=columns.Text, value_type=columns.Text)
    description = columns.Text()
    high_education = columns.Map(columns.Text, columns.Text)
    interests = columns.Map(key_type=columns.Text, value_type=columns.Text)
    lifestyle = columns.Map(key_type=columns.Text, value_type=columns.Text)
    main = columns.Map(key_type=columns.Text, value_type=columns.Text)
    military = columns.Map(key_type=columns.Text, value_type=columns.Text)
    relatives = columns.Map(key_type=columns.Text, value_type=columns.Text)
    school = columns.Map(key_type=columns.Text, value_type=columns.Text)

    def __init__(self, social=None, title=None, career=None,
                 description=None, high_education=None, interests=None,
                 lifestyle=None, main=None, military=None, relatives=None,
                 school=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Groups(Model):
    __table_name__ = 'groups'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    group_id = columns.Text(primary_key=True)
    comments = columns.Float()
    is_closed = columns.Boolean()
    main_city = columns.Text()
    main_prop = columns.Float()
    members_count = columns.Integer()
    name = columns.Text()
    our_prop = columns.Float()
    post_per_day = columns.Float()
    views_per_post = columns.Float()

    def __init__(self, social=None, group_id=None, comments=None,
                 is_closed=None, main_city=None, main_prop=None,
                 members_count=None, name=None, our_prop=None,
                 post_per_day=None, views_per_post=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class IgnoreGroups(Model):
    __table_name__ = 'ignore_groups'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    group_id = columns.Text(primary_key=True)

    def __init__(self, social=None, group_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class InstDates(Model):
    __table_name__ = 'inst_dates'
    __options__ = {'gc_grace_seconds': 0}

    date = columns.DateTime(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)

    def __init__(self, date=None, owner_id=None, post_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class InstPosts(Model):
    __table_name__ = 'inst_posts'
    __options__ = {'gc_grace_seconds': 0}

    owner_id = columns.Text(primary_key=True, partition_key=True)
    post_id = columns.Text(primary_key=True)
    date = columns.DateTime()
    geo = columns.Text()
    likes = columns.Integer()
    replies = columns.Integer()
    text = columns.Text()

    def __init__(self, owner_id=None, post_id=None, date=None, geo=None,
                 likes=None, replies=None, text=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class KeywordPosts(Model):
    __table_name__ = 'keyword_posts'
    __options__ = {'gc_grace_seconds': 0}

    hashtag = columns.Text(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)

    def __init__(self, hashtag=None, social=None, owner_id=None, post_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Notifications(Model):
    __table_name__ = 'notifications'
    __options__ = {'gc_grace_seconds': 0}

    username = columns.Text(primary_key=True, partition_key=True)
    date = columns.DateTime(primary_key=True, clustering_order="DESC")
    info = columns.Map(key_type=columns.Text, value_type=columns.Text)
    is_old = columns.Boolean()

    def __init__(self, username=None, date=None, info=None, is_old=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Persons(Model):
    __table_name__ = 'persons'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    person_id = columns.Text(primary_key=True)
    birthday = columns.DateTime()
    city_id = columns.Integer()
    country_id = columns.Integer()
    first_name = columns.Text()
    last_name = columns.Text()
    sex = columns.Integer()

    def __init__(self, social=None, person_id=None, birthday=None,
                 city_id=None, country_id=None, first_name=None,
                 last_name=None, sex=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class PostAttachments(Model):
    __table_name__ = 'post_attachments'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True, partition_key=True)
    post_id = columns.Text(primary_key=True)
    attachment_id = columns.Integer(primary_key=True)
    header = columns.Text()
    link = columns.Text()
    show_link = columns.Text()
    type = columns.Text()

    def __init__(self, social=None, owner_id=None, post_id=None,
                 attachment_id=None, header=None, link=None,
                 show_link=None, type=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class PostsMonitoring(Model):
    __table_name__ = 'posts_monitoring'
    __options__ = {'gc_grace_seconds': 0}

    username = columns.Text(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)

    def __init__(self, username=None, social=None, owner_id=None,
                 post_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class PostStats(Model):
    __table_name__ = 'post_stats'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True, partition_key=True)
    post_id = columns.Text(primary_key=True)
    ts = columns.DateTime(primary_key=True)
    likes = columns.Integer()
    replies = columns.Integer()
    shares = columns.Integer()
    views = columns.Integer()

    def __init__(self, social=None, owner_id=None, post_id=None, ts=None,
                 likes=None, replies=None, shares=None, views=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class PostsDates(Model):
    __table_name__ = 'posts_dates'
    __options__ = {'gc_grace_seconds': 0}

    date = columns.DateTime(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)

    def __init__(self, date=None, social=None, owner_id=None, post_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class PostsRules(Model):
    __table_name__ = 'posts_rules'
    __options__ = {'gc_grace_seconds': 0}

    rule = columns.Text(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    date = columns.DateTime()
    reply_id = columns.Text()
    type = columns.Text()

    def __init__(self, rule=None, social=None, owner_id=None, post_id=None,
                 date=None, reply_id=None, type=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class PostsVectors(Model):
    __table_name__ = 'posts_vectors'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    vector = columns.List(value_type=columns.Float)

    def __init__(self, social=None, owner_id=None, post_id=None, vector=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class RabbitTasks(Model):
    __table_name__ = 'rabbit_tasks'
    __options__ = {'gc_grace_seconds': 0}

    date = columns.DateTime(primary_key=True)
    completed = columns.Boolean()

    def __init__(self, date=None, status=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Replies(Model):
    __table_name__ = 'replies'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True, partition_key=True)
    post_id = columns.Text(primary_key=True, partition_key=True)
    reply_id = columns.Text(primary_key=True)
    author = columns.Text()
    date = columns.DateTime()
    likes = columns.Integer()
    parent_reply = columns.Text()
    replies = columns.Integer()
    reply_to = columns.Text()
    sticker = columns.Text()
    text = columns.Text()

    def __init__(self, social=None, owner_id=None, post_id=None, reply_id=None,
                 author=None, date=None, likes=None, parent_reply=None,
                 replies=None, reply_to=None, sticker=None, text=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Rules(Model):
    __table_name__ = 'rules'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    tag = columns.Text(primary_key=True)
    is_active = columns.Boolean()
    value = columns.Text()

    def __init__(self, social=None, tag=None, is_active=None, value=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class SmmResponses(Model):
    __table_name__ = 'smm_responses'
    __options__ = {'gc_grace_seconds': 0}

    date = columns.DateTime(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    reply_id = columns.Text(primary_key=True)
    parent_reply_id = columns.Text()
    response_time = columns.Float()
    operator = columns.Integer()
    parent_operator = columns.Integer()
    has_incident = columns.Boolean()

    def __init__(self, date=None, social=None, owner_id=None, post_id=None,
                 reply_id=None, parent_reply_id=None, response_time=None,
                 operator=None, parent_operator=None, has_incident=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class SuggestionGroups(Model):
    __table_name__ = 'suggestion_groups'
    __options__ = {'gc_grace_seconds': 0}

    date = columns.DateTime(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text()
    state = columns.Text()

    def __init__(self, date=None, social=None, owner_id=None, post_id=None,
                 state=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Suggestions(Model):
    __table_name__ = 'suggestions'
    __options__ = {'gc_grace_seconds': 0}

    date = columns.DateTime(primary_key=True)
    text = columns.Text()
    title = columns.Text()
    author = columns.Text()

    def __init__(self, date=None, text=None, title=None, author=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TaskGroups(Model):
    __table_name__ = 'task_groups'
    __options__ = {'gc_grace_seconds': 0}

    task_id = columns.Integer(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    group_id = columns.Text(primary_key=True)

    def __init__(self, task_id=None, social=None, group_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TaskKeywords(Model):
    __table_name__ = 'task_keywords'
    __options__ = {'gc_grace_seconds': 0}

    task_id = columns.Integer(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    keyword = columns.Text(primary_key=True)

    def __init__(self, task_id=None, social=None, keyword=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Tasks(Model):
    __table_name__ = 'tasks'
    __options__ = {'gc_grace_seconds': 0}

    task_id = columns.Integer(primary_key=True)
    author = columns.Text()
    date = columns.DateTime()
    is_active = columns.Boolean()
    is_keyword = columns.Boolean()
    groups_count = columns.Integer()
    label = columns.Text()
    social = columns.Text()

    def __init__(self, task_id=None, author=None, date=None, is_active=None,
                 is_keyword=None, groups_count=None, label=None, social=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TopPosts(Model):
    __table_name__ = 'top_posts'
    __options__ = {'gc_grace_seconds': 0}

    level = columns.Integer(primary_key=True, partition_key=True)
    topic = columns.Integer(primary_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    probability = columns.Float()
    text = columns.Text()

    def __init__(self, level=None, topic=None, social=None, owner_id=None,
                 post_id=None, probability=None, text=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TopicInPost(Model):
    __table_name__ = 'topic_in_post'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True, partition_key=True)
    post_id = columns.Text(primary_key=True)
    topic = columns.Integer(primary_key=True)
    probability = columns.Float()

    def __init__(self, social=None, owner_id=None, post_id=None, topic=None,
                 probability=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TopicPosts(Model):
    __table_name__ = 'topic_posts'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    date = columns.DateTime()
    likes = columns.Integer()
    sentiment = columns.Integer()
    text = columns.Text()
    topic = columns.Integer()
    topic_id = columns.Text()

    def __init__(self, social=None, owner_id=None, post_id=None, date=None,
                 likes=None, sentiment=None, text=None, topic=None,
                 topic_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TopicVectors(Model):
    __table_name__ = 'topic_vectors'
    __options__ = {'gc_grace_seconds': 0}

    words = columns.Text(primary_key=True)
    threshold = columns.Float()
    vector = columns.List(value_type=columns.Float)

    def __init__(self, words=None, threshold=None, vector=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Topics(Model):
    __table_name__ = 'topics'
    __options__ = {'gc_grace_seconds': 0}

    level = columns.Integer(primary_key=True, partition_key=True)
    topic = columns.Integer(primary_key=True)
    title = columns.Text()

    def __init__(self, level=None, topic=None, title=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TraceTemplates(Model):
    __table_name__ = 'trace_templates'
    __options__ = {'gc_grace_seconds': 0}

    level = columns.Integer(primary_key=True, partition_key=True)
    topic = columns.Integer(primary_key=True)
    date = columns.DateTime()
    trace = columns.Text()

    def __init__(self, level=None, topic=None, date=None, trace=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Traces(Model):
    __table_name__ = 'traces'
    __options__ = {'gc_grace_seconds': 60}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True, partition_key=True)
    post_id = columns.Text(primary_key=True)
    date = columns.DateTime(primary_key=True)
    author = columns.Text()
    bot = columns.Text()
    reply_id = columns.Text()
    status = columns.Text()
    text = columns.Text()

    def __init__(self, social=None, owner_id=None, post_id=None, date=None,
                 author=None, bot=None, reply_id=None, status=None, text=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class TrackedPosts(Model):
    __table_name__ = 'tracked_posts'
    __options__ = {'gc_grace_seconds': 0}

    username = columns.Text(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    end_date = columns.DateTime()
    post_city = columns.Text()
    post_topic = columns.Text()


class TrackedPostCities(Model):
    __table_name__ = 'tracked_post_cities'
    __options__ = {'gc_grace_seconds': 0}

    city = columns.Text(primary_key=True)


class TrackedPostTopics(Model):
    __table_name__ = 'tracked_post_topics'
    __options__ = {'gc_grace_seconds': 0}

    topic = columns.Text(primary_key=True)


class TrackedRules(Model):
    __table_name__ = 'tracked_rules'
    __options__ = {'gc_grace_seconds': 0}

    user = columns.Text(primary_key=True, partition_key=True)
    tag = columns.Text(primary_key=True)
    social = columns.Text(primary_key=True)

    def __init__(self, user=None, tag=None, social=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class UserGroups(Model):
    __table_name__ = 'user_groups'
    __options__ = {'gc_grace_seconds': 0}

    username = columns.Text(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    group_id = columns.Text(primary_key=True)
    state = columns.Integer()

    def __init__(self, username=None, social=None, group_id=None, state=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class UserPosts(Model):
    __table_name__ = 'user_posts'
    __options__ = {'gc_grace_seconds': 0}

    username = columns.Text(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)

    def __init__(self, username=None, social=None, owner_id=None,
                 post_id=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class Users(Model):
    __table_name__ = 'users'
    __options__ = {'gc_grace_seconds': 0}

    username = columns.Text(primary_key=True, partition_key=True)
    hash = columns.Text(primary_key=True)
    boss = columns.Boolean()

    def __init__(self, username=None, hash=None, boss=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)


class UsersFts(Model):
    __table_name__ = 'users_fts'
    __options__ = {'gc_grace_seconds': 0}

    username = columns.Text(primary_key=True, partition_key=True)
    type = columns.Text(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    add_id = columns.Text(primary_key=True)


class VectorTopicsProb(Model):
    __table_name__ = 'vector_topics_prob'
    __options__ = {'gc_grace_seconds': 0}

    social = columns.Text(primary_key=True, partition_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    topic = columns.Text(primary_key=True)
    probability = columns.Float()

    def __init__(self, social=None, owner_id=None, post_id=None, topic=None,
                 probability=None):
        kwargs = locals()
        del kwargs['self']
        super().__init__(**kwargs)

# -- Materialized Views -- #


class TracesByDate(Model):
    __table_name__ = 'traces_by_date'
    __options__ = {'gc_grace_seconds': 60}

    date = columns.DateTime(primary_key=True, partition_key=True)
    social = columns.Text(primary_key=True)
    owner_id = columns.Text(primary_key=True)
    post_id = columns.Text(primary_key=True)
    author = columns.Text()
    bot = columns.Text()
    reply_id = columns.Text()
    status = columns.Text()
    text = columns.Text()
