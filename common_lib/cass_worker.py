from cassandra.cqlengine import connection
from cassandra.cqlengine.query import BatchQuery, ContextQuery

from common_lib.src import config
from common_lib.src.retry import retry
from common_lib.cass_models import (
    AllPosts, Bots, BotsPatterns, Groups, IgnoreGroups, InstDates,
    InstPosts, KeywordPosts, Notifications, Persons, PostAttachments,
    PostStats, PostsDates, PostsRules, PostsVectors, Replies, Rules,
    SmmResponses, RabbitTasks, PostsMonitoring, UsersFts,
    SuggestionGroups, Suggestions, TaskGroups, TaskKeywords, Tasks, TopPosts,
    TopicInPost, TopicPosts, TopicVectors, Topics, TraceTemplates, Traces,
    TrackedPosts, TrackedRules, UserGroups, UserPosts, Users, VectorTopicsProb,
    TracesByDate, TrackedPostCities, TrackedPostTopics)


@retry(3)
def setup_connection():
    CONFIG = config.get_config('./common_lib/config/db_worker.yml')
    host = CONFIG['cassandra']['host']
    keyspace = CONFIG['cassandra']['keyspace']

    connection.setup([host], keyspace, lazy_connect=True)


@retry(3)
def insert_data(table, data, conn=None):
    table_keys = table._columns.keys()

    insert = []
    if isinstance(data, list):
        insert.extend(data)
    elif isinstance(data, dict):
        insert.append(data)

    with BatchQuery() as b:
        for row in insert:
            args = {k: v for k, v in row.items() if k in table_keys}
            table.batch(b).create(**args)


@retry(3)
def update_data(table, keys, update_items, conn=None):
    with ContextQuery(table) as table:
        table(keys).update(update_items)


@retry(3)
def select_data(table, conn=None, filters=None):
    with ContextQuery(table) as table:
        if filters is not None:
            records = table.objects \
                .filter(**filters).allow_filtering().all()
        else:
            records = table.objects().all()

    records = list(map(lambda x: dict(x), records))

    return records


class CassandraTables():
    """All Cassandra tables

    Materialized views starts with _"""

    all_posts = AllPosts
    bots = Bots
    bots_patterns = BotsPatterns
    groups = Groups
    ignore_groups = IgnoreGroups
    inst_dates = InstDates
    inst_posts = InstPosts
    keyword_posts = KeywordPosts
    notifications = Notifications
    persons = Persons
    post_attachments = PostAttachments
    posts_monitoring = PostsMonitoring
    post_stats = PostStats
    posts_dates = PostsDates
    posts_rules = PostsRules
    posts_vectors = PostsVectors
    rabbit_tasks = RabbitTasks
    replies = Replies
    rules = Rules
    smm_responses = SmmResponses
    suggestion_groups = SuggestionGroups
    suggestions = Suggestions
    task_groups = TaskGroups
    task_keywords = TaskKeywords
    tasks = Tasks
    top_posts = TopPosts
    topic_in_post = TopicInPost
    topic_posts = TopicPosts
    topic_vectors = TopicVectors
    topics = Topics
    trace_templates = TraceTemplates
    traces = Traces
    tracked_posts = TrackedPosts
    tracked_cities = TrackedPostCities
    tracked_topics = TrackedPostTopics
    tracked_rules = TrackedRules
    user_groups = UserGroups
    user_posts = UserPosts
    users = Users
    users_fts = UsersFts
    vector_topics_prob = VectorTopicsProb

    _traces_by_date = TracesByDate


class CassCommandHandler():

    def __init__(self, table_name):
        self.records = []
        self.table_name = table_name
        self.args = {'table': self.table_name}

        if self.table_name in vars(CassandraTables):
            self.primary_keys = list(
                vars(CassandraTables)[self.table_name]._primary_keys.keys()
            )

        self.complex_filter = dict()

    def add_filter(self, field, operator, value):
        operators = {"in": "__in",
                     ">": "__gt",
                     ">=": "__gte",
                     "<": "__lt",
                     "<=": "__lte",
                     "=": ""}

        filter = {f'{field}{operators[operator]}': value}
        self.complex_filter.update(filter)

    def add_count(self, count):
        self.args['count'] = count

    def add_sorting(self, column, ascending=True):
        self.args['order_by'] = column
        if not ascending:
            self.args['sort_asc'] = False

    def add_record(self, record):
        self.records.append(record)

    def get_task_name(self):
        raise NotImplementedError

    def get_args(self):
        raise NotImplementedError

    def build_task(self):
        task_name = self.get_task_name()

        upd_args = self.get_args()
        if upd_args is not None:
            self.args.update(upd_args)

        task = {'task_name': task_name,
                'args': self.args}

        return task


class CassSelectHandler(CassCommandHandler):

    def get_task_name(self):
        if len(self.records) > 1:
            task_name = "get_multikeys_records"
        else:
            task_name = "get_records"

        return task_name

    def get_args(self):
        args = {}
        if len(self.records) > 1:
            all_keys = []
            for record in self.records:
                keys = dict((k, v) for k, v in record.items()
                            if v is not None and v)
                all_keys.append(keys)
            args.update({"all_keys": all_keys})
        elif len(self.records) == 1:
            keys = dict(
                (k, v) for k, v in self.records[0].items() if v is not None)
            if len(self.complex_filter) > 0:
                addkeys = dict(
                    (k, v) for k, v in self.complex_filter.items())
                keys.update(addkeys)
            args.update({"keys": keys})
        elif len(self.complex_filter) > 0:
            keys = dict(
                (k, v) for k, v in self.complex_filter.items())
            args.update({"keys": keys})
        else:
            return None

        return args


class CassInsertHandler(CassCommandHandler):

    def get_task_name(self):
        if len(self.records) > 1:
            task_name = "insert_records"
        else:
            task_name = "insert_record"

        return task_name

    def get_args(self):
        args = {}
        if len(self.records) > 1:
            all_values = []
            for record in self.records:
                values = dict(
                    (k, v) for k, v in record.items() if v is not None)
                all_values.append(values)
            args.update({"all_values": all_values})
        elif len(self.records) == 1:
            values = dict(
                (k, v) for k, v in self.records[0].items() if v is not None)
            args.update({"values": values})
        else:
            return None

        return args


class CassDeleteHandler(CassCommandHandler):

    def get_task_name(self):
        if len(self.records) > 1:
            task_name = "delete_records"
        else:
            task_name = "delete_record"

        return task_name

    def get_args(self):
        args = {}

        if len(self.records) > 1:
            all_keys = []
            for record in self.records:
                keys = dict((k, v) for k, v in record.items() if v is not None)
                all_keys.append(keys)
            args.update({"all_keys": all_keys})
        elif len(self.records) == 1:
            keys = dict(
                (k, v) for k, v in self.records[0].items() if v is not None)
            args.update({"keys": keys})
        else:
            return None

        return args


class CassUpdateHandler(CassCommandHandler):

    def get_task_name(self):
        if len(self.records) > 1:
            task_name = "update_records"
        else:
            task_name = "update_record"

        return task_name

    def get_args(self):
        args = {}

        if len(self.records) > 1:
            all_values = []
            for record in self.records:
                record_dict = dict(
                    (k, v) for k, v in record.items() if v is not None)
                keys = dict(
                    (k, v) for k, v in record_dict.items()
                    if k in self.primary_keys)
                values = dict(
                    (k, v) for k, v in record_dict.items()
                    if k not in self.primary_keys)
                all_values.append((keys, values))
            args.update({"all_values": all_values})
        elif len(self.records) == 1:
            record_dict = dict(
                (k, v) for k, v in self.records[0].items() if v is not None)
            keys = dict(
                (k, v) for k, v in record_dict.items()
                if k in self.primary_keys)
            values = dict(
                (k, v) for k, v in record_dict.items()
                if k not in self.primary_keys)
            args.update({"keys": keys, 'values': values})
        else:
            return None

        return args


class CassQueryGenerator():
    all_tables = [
            v for k, v in CassandraTables.__dict__.items()
            if not k.startswith("__")
            and not k.startswith("_")
        ]

    mat_views = [
            v for k, v in CassandraTables.__dict__.items()
            if not k.startswith("__")
            and k.startswith("_")
        ]

    command_handlers = {'select': CassSelectHandler,
                        'insert': CassInsertHandler,
                        'delete': CassDeleteHandler,
                        'update': CassUpdateHandler}

    def __init__(self, rpc, table, query_type):
        self.rpc = rpc
        self.table = table.__table_name__
        self.handler = self.command_handlers[query_type](self.table)

        if query_type == 'select':
            self.query_rpc_command = self.rpc.call
        else:
            self.query_rpc_command = self.rpc.send_message

    def add_record(self, record):
        self.handler.add_record(dict(record))

    def run_task(self):
        task = self.handler.build_task()
        result = self.query_rpc_command(task)

        return result
