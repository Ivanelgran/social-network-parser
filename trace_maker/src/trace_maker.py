from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import time
import requests
from vk_api import VkApiError, VkApi
import pickle
import logging
from pathlib import Path
import os
from datetime import datetime
from common_lib.queue_manager import RpcClient, make_server
from threading import Thread
from common_lib.cass_worker import CassandraTables, CassQueryGenerator

LOGGER = logging.getLogger('Collector')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')

SRC_DIR = Path(__name__).resolve().parents[0]

fh = logging.FileHandler(os.path.join(SRC_DIR, 'logs/logs.log'))
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)


class VkTraces():

    user_agent = ("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.3"
                  "6 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")

    def open_session(self, login, password):
        vk_session = VkApi(login, password)
        vk_session.auth()

        vk = vk_session.get_api()

        return vk

    def add_comment_trace(self, data):
        reply_id = self.add_trace_via_api(data)

        if reply_id is None:
            LOGGER.info("Cant use API. Use selenium...")
            reply_id = self.add_trace_via_selenium(data)

        return str(reply_id)

    def add_suggestion(self, data):
        result = self.add_suggestion_via_api(data)

        if result is None:
            LOGGER.info("Cant use API. Use selenium...")
            result = self.add_suggestion_via_selenium(data)

        return result

    def add_trace_via_api(self, data):
        vk = self.open_session(data['login'],
                               data['password'])

        owner_id = int(data['owner_id'])
        if owner_id > 0:
            owner_id = -1 * owner_id

        params = {'owner_id': owner_id,
                  'post_id': int(data['post_id']),
                  'message': data['trace']}
        if data.get('filename') is not None:
            att = self.get_uploaded_photo(vk, data['filename'])

            params['attachments'] = att

        try:
            result = vk.wall.createComment(**params)
        except VkApiError as e:
            LOGGER.exception(e.error['error_msg'])
            return None

        return result['comment_id']

    def add_suggestion_via_api(self, data):
        vk = self.open_session(data['login'],
                               data['password'])

        owner_id = int(data['owner_id'])
        if owner_id > 0:
            owner_id = -1 * owner_id

        params = {'owner_id': owner_id,
                  'message': data['trace']}
        if data.get('filename') is not None:
            att = self.get_uploaded_photo(vk, data['filename'])

            params['attachments'] = att

        need_subscribe = False
        try:
            vk.wall.post(**params)
        except VkApiError as e:
            if e.error['error_code'] == 214:
                need_subscribe = True
            else:
                LOGGER.exception(e.error['error_msg'])
                return None

        if need_subscribe:
            try:
                group_id = -1 * owner_id
                vk.groups.join(group_id=group_id)
                vk.wall.post(**params)
            except VkApiError as e:
                LOGGER.exception(e.error['error_msg'])
                return None

        return True

    def get_uploaded_photo(self, vk, filename):
        server = vk.photos.getWallUploadServer()

        data = requests.post(url=server['upload_url'],
                             files={'photo': open(filename, 'rb')})
        result = data.json()

        save = vk.photos.saveWallPhoto(server=result['server'],
                                       photo=result['photo'],
                                       hash=result['hash'])

        return f"photo{save[0]['owner_id']}_{save[0]['id']}"

    def add_trace_via_selenium(self, data):
        owner_id = data['owner_id']

        owner_id = int(data['owner_id'])
        if owner_id > 0:
            owner_id = -1 * owner_id

        url = "https://vk.com/"

        options = webdriver.ChromeOptions()
        options.add_argument(f'user-agent={self.user_agent}')
        options.add_argument('--no-sandbox')
        options.add_argument('headless')

        driver = webdriver.Chrome("data/chromedriver", options=options)
        driver.implicitly_wait(5)
        driver.get(url=url)

        driver.find_element_by_xpath("//input[@id='index_email']") \
            .send_keys(data['login'])
        driver.find_element_by_xpath(
            "//input[@id='index_pass']").send_keys(data['password'])
        time.sleep(0.5)
        driver.find_element_by_xpath(
            "//button[@id='index_login_button']").click()
        time.sleep(1)

        driver.get(url=f'https://vk.com/wall{owner_id}_{data["post_id"]}')

        try:
            loader = driver.find_element_by_xpath(
                "//div[@class='box_x_button']")
            if loader.get_attribute('aria-label') == 'Закрыть':
                loader.click()
        except NoSuchElementException:
            LOGGER.info("Loader not appeared")

        filename = data.get('filename')
        if filename is not None:
            try:
                driver.find_element_by_xpath(
                    "//button[@class='reply_box_photo']").click()
                driver.find_element_by_xpath(
                    "//button[@class='reply_box_photo']").click()
                time.sleep(1)
                driver.find_elements_by_xpath(
                    "//input[@type='file']")[1].send_keys(filename)
            except Exception:
                LOGGER.exception("Cant add image")
                error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
                driver.get_screenshot_as_file(f"error-{error_date}.png")
                with open("cant_add_img.txt", 'w') as f:
                    f.write(driver.find_element_by_xpath("//*").get_attribute("outerHTML"))
                driver.quit()
                return None
        else:
            driver.find_element_by_xpath("//div[@class='reply_fakebox']") \
                .click()

        time.sleep(1)

        owner_id = data['owner_id']
        post_id = data['post_id']
        trace = data['trace']

        try:
            driver.find_element_by_xpath(
                f"//div[@id='reply_field-{owner_id}_{post_id}']") \
                    .send_keys(trace)
            driver.find_element_by_xpath(
                f"//button[@id='reply_button-{owner_id}_{post_id}']").click()
        except Exception:
            LOGGER.exception("Cant post trace")
            error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
            driver.get_screenshot_as_file(f"error-{error_date}.png")
            with open("cant_post_trace.txt", 'w') as f:
                f.write(driver.find_element_by_xpath("//*").get_attribute("outerHTML"))
            driver.quit()
            return None

        time.sleep(0.5)
        for i in range(5):
            find = False
            result = driver.find_elements_by_xpath(
                "//div[@class='wall_reply_text']")
            for reply in result:
                if reply.text == trace:
                    find = True
                    try:
                        trace_reply_id = reply.find_element_by_xpath("..") \
                            .get_attribute("id").split('_')[1]
                    except Exception:
                        LOGGER.exception("cant find reply id")
                        trace_reply_id = ""
                    break
            if find:
                break
            time.sleep(1)
        else:
            error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
            driver.get_screenshot_as_file(f"error-{error_date}.png")
            driver.quit()
            return None

        driver.quit()

        return trace_reply_id

    def add_suggestion_via_selenium(self, data):
        owner_id = data['owner_id']
        trace = data['trace']
        login = data['login']
        password = data['password']
        filename = data.get('filename')

        owner_id = int(data['owner_id'])
        if owner_id < 0:
            owner_id = -1 * owner_id

        url = "https://vk.com/"

        options = webdriver.ChromeOptions()
        options.add_argument(f'user-agent={self.user_agent}')
        options.add_argument('--no-sandbox')
        options.add_argument('headless')

        driver = webdriver.Chrome("data/chromedriver", options=options)
        driver.implicitly_wait(5)
        driver.get(url=url)

        driver.find_element_by_xpath("//input[@id='index_email']") \
            .send_keys(login)
        driver.find_element_by_xpath(
            "//input[@id='index_pass']").send_keys(password)
        time.sleep(0.5)

        try:
            driver.find_element_by_xpath(
                "//button[@id='index_login_button']").click()
        except Exception:
            return None
        time.sleep(1)

        try:
            loader = driver.find_element_by_xpath("//div[@class='box_x_button']")
            if loader.get_attribute('aria-label') == 'Закрыть':
                loader.click()
        except NoSuchElementException:
            LOGGER.info("Loader not appeared")

        self.close_actualize(driver)

        driver.get(url=f'https://vk.com/club{owner_id}')

        time.sleep(1)

        suggest_block = driver.find_element_by_xpath(
            "//div[@id='page_block_submit_post']")

        try:
            suggest_block.find_element_by_tag_name("div")
        except NoSuchElementException:
            try:
                driver.find_element_by_xpath(
                    "//button[@id='public_subscribe']").click()
            except Exception:
                LOGGER.warning("Cant subscribe")
                return None
            driver.get(url=f'https://vk.com/club{owner_id}')
            suggest_block = driver.find_element_by_xpath(
                "//div[@id='page_block_submit_post']")

        self.close_actualize(driver)

        try:
            suggest_block.find_element_by_tag_name("div")
        except NoSuchElementException:
            LOGGER.info(f"Cant suggest post for VK group {owner_id}")
            driver.quit()
            return None

        self.close_actualize(driver)

        try:
            post_field = suggest_block.find_element_by_xpath(
                "//div[@id='post_field']")
            driver.execute_script(f"arguments[0].click()", post_field)
            post_field.send_keys(trace)
        except Exception:
            LOGGER.info(f"Cant suggest post for VK group {owner_id}")
            driver.quit()
            return None

        time.sleep(1)

        if filename is not None:
            try:
                driver.find_element_by_xpath("//input[@name='photo']") \
                    .send_keys(filename)
            except NoSuchElementException:
                LOGGER.exception("Cant add image")
                error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
                driver.get_screenshot_as_file(f"error-{error_date}.png")
                driver.quit()
                return None

        time.sleep(1)

        self.close_actualize(driver)

        try:
            send = suggest_block.find_element_by_xpath(
                f"//button[@id='send_post']")
            driver.execute_script(f"arguments[0].click()", send)
        except Exception:
            LOGGER.exception("Cant post trace")
            error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
            driver.get_screenshot_as_file(f"error-{error_date}.png")
            driver.quit()
            return None

        time.sleep(1)
        LOGGER.info(f"suggestion added")

        driver.quit()

        return True

    def close_actualize(self, driver):
        try:
            driver.find_element_by_xpath("//div[@id='actualize_controls']") \
                .click()
        except NoSuchElementException:
            pass


class OkTraces():

    user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'

    def add_comment_trace(self, data):
        owner_id = data['owner_id']
        post_id = data['post_id']
        trace = data['trace']
        login = data['login']
        password = data['password']
        filename = data.get("filename")

        url = "https://ok.ru/"

        options = webdriver.ChromeOptions()
        options.add_argument(f'user-agent={self.user_agent}')
        options.add_argument('--no-sandbox')
        options.add_argument("headless")

        driver = webdriver.Chrome("data/chromedriver", options=options)
        driver.get(url=url)

        driver.find_element_by_xpath("//input[@id='field_email']") \
            .send_keys(login)
        driver.find_element_by_xpath(
            "//input[@id='field_password']").send_keys(password)
        time.sleep(1)
        driver.find_element_by_xpath("//input[@value='Войти']").click()
        time.sleep(1)
        driver.get(url=f'https://ok.ru/group/{owner_id}/topic/{post_id}')
        time.sleep(1)

        try:
            driver.find_element_by_xpath("//div[@class='comments_add']")
        except Exception:
            driver.find_element_by_xpath(
                "//div[@class='comments_form-stub']") \
                .find_element_by_tag_name('a').click()
            driver.get(url=f'https://ok.ru/group/{owner_id}/topic/{post_id}')

        time.sleep(1)

        for i in range(10):
            try:
                driver.find_element_by_xpath(
                    "//div[@class='itx_w']")
                break
            except Exception:
                time.sleep(1)
                driver.get(
                    url=f'https://ok.ru/group/{owner_id}/topic/{post_id}')
        else:
            LOGGER.exception("Cant find comment field")
            error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
            with open(f"error-{error_date}.txt", 'w') as f:
                f.write(
                    driver.find_element_by_xpath("//*").get_attribute("outerHTML"))
            driver.quit()
            return None

        if filename is not None:
            try:
                input_ = driver.find_element_by_xpath("//input[@type='file']")
                input_.send_keys(filename)
            except Exception:
                LOGGER.exception("Cant add image")
                error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
                driver.get_screenshot_as_file(f"error-{error_date}.png")
                driver.quit()
                return None

        try:
            inp = driver.find_element_by_xpath(
                "//div[@class='itx_w']").find_element_by_tag_name('div')
            driver.execute_script(f"arguments[0].click()", inp)
            time.sleep(0.5)
            driver.execute_script(f"arguments[0].innerText = '{trace}'", inp)

            button = inp.find_element_by_xpath(
                "//div[@class='comments_add-controls']"
            ).find_element_by_tag_name("button")
            driver.execute_script(f"arguments[0].click()", button)
        except Exception:
            LOGGER.exception("Cant post trace")
            error_date = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
            driver.get_screenshot_as_file(f"error-{error_date}.png")
            driver.quit()
            return None

        time.sleep(1)
        driver.quit()

        return True


class TraceMaker():

    def __init__(self):
        self.vk_traces = VkTraces()
        self.ok_traces = OkTraces()
        self.cass = RpcClient('db_tasks')
        self.LOGGER = logging.getLogger('Collector.TraceMaker')

    def save_image(self, filename):
        # with open("images/" + filename, 'wb') as handle:
        #     url = "https://web:8095/trace_images/" + filename
        #     self.LOGGER.info(url)
        #     response = requests.get(url, verify=False, stream=True)

        #     if not response.ok:
        #         print(response)
        #         return

        #     for block in response.iter_content(1024):
        #         if not block:
        #             break

        #         handle.write(block)

        save_path = os.path.join(SRC_DIR, 'images', filename)

        return save_path

    def add_comment_trace(self, data):
        self.LOGGER.info(f"new comment trace: {data}")

        social = data['social']
        owner_id = data['owner_id']
        post_id = data['post_id']

        trace_date = datetime.now()
        query = CassQueryGenerator(self.cass,
                                   CassandraTables.traces,
                                   'insert')
        trace = CassandraTables.traces(social=data['social'],
                                       owner_id=owner_id,
                                       post_id=post_id,
                                       date=trace_date,
                                       author=data['author'],
                                       bot=data['login'],
                                       text=data['trace'])
        query.add_record(trace)
        query.run_task()

        if data.get('filename') is not None:
            data['filename'] = self.save_image(data['filename'])

        comment_workers = {'vk': self.vk_traces,
                           'ok': self.ok_traces}

        if social not in comment_workers.keys():
            self.LOGGER.warning(f"Cant add trace for social {social}")
            return
        else:
            try:
                reply_id = comment_workers[social].add_comment_trace(data)
            except Exception:
                self.LOGGER.exception("Problem in new comment trace")
                reply_id = None

        if reply_id is None:
            self.LOGGER.warning(f"Cant add trace for {data}")
            query = CassQueryGenerator(self.cass,
                                       CassandraTables.traces,
                                       'update')
            trace = CassandraTables.traces(social=social, owner_id=owner_id,
                                           post_id=post_id, date=trace_date,
                                           status='failed')
            query.add_record(trace)
            query.run_task()
            self.LOGGER.info("trace failed")
            return

        query = CassQueryGenerator(self.cass,
                                   CassandraTables.traces,
                                   'update')
        trace = CassandraTables.traces(social=social, owner_id=owner_id,
                                       post_id=post_id, date=trace_date,
                                       status='completed', reply_id=reply_id)
        query.add_record(trace)
        query.run_task()
        self.LOGGER.info("Trace completed")

    def add_suggestion(self, data):
        self.LOGGER.info(f"new suggestion {data}")

        suggestion_date = datetime.now()
        # suggestion = (suggestion_date, data['title'],
        #               data['author'], data['trace'])

        # task = {'task_name': 'insert_suggestion',
        #         'args': {'suggestion': suggestion}}
        # self.cass.send_message(task)
        query = CassQueryGenerator(self.cass, CassandraTables.suggestions,
                                   'insert')
        suggestion = CassandraTables.suggestions(date=suggestion_date,
                                                 text=data['trace'],
                                                 title=data['title'],
                                                 author=data['author'])
        query.add_record(suggestion)
        query.run_task()

        if data.get('filename') is not None:
            data['filename'] = self.save_image(data['filename'])

        query = CassQueryGenerator(self.cass,
                                   CassandraTables.suggestion_groups,
                                   'insert')
        for group in data['groups']:
            gr = group.split("_")
            # groups.append((gr[0], gr[1]))
            record = CassandraTables.suggestion_groups(date=suggestion_date,
                                                       social=gr[0],
                                                       owner_id=gr[1],
                                                       state='in progress')
            query.add_record(record)
        query.run_task()

        for group in data['groups']:
            soc, gr = group.split("_")
            info = {'social': soc,
                    'owner_id': gr,
                    'trace': data['trace'],
                    'login': data['login'],
                    'password': data['password'],
                    'author': data['author'],
                    'filename': data.get('filename'),
                    'date': suggestion_date}
            self.add_suggest_post(info)
        self.LOGGER.info("Suggestion completed")

    def add_suggest_post(self, data):
        social = data['social']
        date = data['date']
        owner_id = data['owner_id']

        suggest_workers = {'vk': self.vk_traces}

        if social not in suggest_workers.keys():
            self.LOGGER.warning(f"Cant add suggestion for social {social}")
            return
        else:
            try:
                result = suggest_workers[social].add_suggestion(data)
            except Exception:
                self.LOGGER.exception(f"problem in sugg {data}")
                result = False

        if result:
            state = 'completed'
        else:
            state = 'failed'

        query = CassQueryGenerator(self.cass,
                                   CassandraTables.suggestion_groups,
                                   'update')
        sugg = CassandraTables.suggestion_groups(date=date,
                                                 social=social,
                                                 owner_id=owner_id,
                                                 state=state)
        query.add_record(sugg)
        query.run_task()


class TraceManager():
    rabbit_host = "rabbit"
    queue_name = "traces"

    def __init__(self):
        self.trace_maker = TraceMaker()
        self.method_list = [func for func in dir(self.trace_maker) if
                            callable(getattr(self.trace_maker, func))
                            and not func.startswith("__")]

    def on_request(self, ch, method, props, body):

        task = pickle.loads(body)

        if task.get('task_name') in self.method_list:
            task_method = getattr(self.trace_maker, task.get('task_name'))
            task_args = task['args']
            try:
                result = task_method(**task_args)
            except Exception:
                LOGGER.exception(f"problem in {task.get('task_name')}")
                result = None
        else:
            result = "Current method is not supported"

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=pickle.dumps(
                             result))
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    task_manager = TraceManager()

    traces_process = Thread(target=make_server,
                            args=(task_manager.queue_name,
                                  task_manager.on_request))

    traces_process.start()

    traces_process.join()
