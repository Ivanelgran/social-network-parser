# TODO: разбить на модули

import nltk
import logging
import numpy as np
import pickle
import string
import gensim
import pymorphy2
import multiprocessing
import threading
import time

from datetime import datetime, timedelta
from math import ceil

from common_lib.queue_manager import RpcClient, make_server
from nltk.corpus import stopwords
from multiprocessing import Process

nltk.download('punkt')
nltk.download('stopwords')
morph = pymorphy2.MorphAnalyzer()
re_mutex = threading.RLock()
manager = multiprocessing.Manager()
posts_data_by_proc = manager.list()

LOGGER = logging.getLogger('Model')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')

fh = logging.FileHandler('logs/logs.log', mode='w')
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)


class DocSim(object):
    def __init__(self, w2v_model, stopwords=[]):
        self.w2v_model = w2v_model
        self.stopwords = stopwords
        self.LOGGER = logging.getLogger('Model.doc_sim')

    def is_in_vocab(self, doc):
        ignore_words_dict = {}
        ignore_words_list = []
        words = [w for w in doc.split(" ") if w not in self.stopwords]
        for i in range(len(words)):
            if words[i] not in self.w2v_model.wv.vocab:
                ignore_words_dict['word'] = words[i]
                ignore_words_dict['index'] = i
                ignore_words_list.append(ignore_words_dict.copy())
                LOGGER.info('Слово ' + str(words[i]) + ' отсутсвует в словаре')

        return ignore_words_list

    def vectorize(self, doc):
        """Identify the vector values for each word in the given document"""
        # doc = doc.lower()
        try:
            words = [w for w in doc.split(" ") if w not in self.stopwords]
            word_vecs = []
            for word in words:
                try:
                    vec = self.w2v_model[word]
                    word_vecs.append(vec)
                except KeyError:
                    # Ignore, if the word doesn't exist in the vocabulary
                    pass
            # Assuming that document vector is the mean of all the word vectors
            try:
                vector = np.mean(word_vecs, axis=0)
            except:
                self.LOGGER.exception('Ошибка при усреднении вектора')
                vector = [0]
        except:
            self.LOGGER.exception('Ошибка векторизации')
            vector = [0]

        return vector

    def _cosine_sim(self, vecA, vecB):
        """Find the cosine similarity distance between two vectors."""
        try:
            csim = np.dot(vecA, vecB) / (np.linalg.norm(vecA) * np.linalg.norm(vecB))
        except Exception as e:
            self.LOGGER.exception('Исключение при подсчете косинусной близости')
            csim = 0.0
        if np.isnan(np.sum(csim)):
            csim = 0.0
        return csim

    def calculate_similarity(self, source_doc, target_docs=[], threshold=0):
        """Calculates & returns similarity scores between given source document & all
        the target documents."""
        if isinstance(target_docs, str):
            target_docs = [target_docs]

        source_vec = self.vectorize(source_doc)
        results = []
        for doc in target_docs:
            target_vec = self.vectorize(doc)
            sim_score = self._cosine_sim(source_vec, target_vec)
            results.append(sim_score)

        return results

    def calculate_sim_for_vectors(self, source_vec, target_docs):
        results = []
        source_vec = np.array(source_vec)
        for target_doc in target_docs:
            if target_doc['vector'] == "":
                sim_score = 0
            else:
                target_vec = np.array(target_doc['vector'])
                sim_score = self._cosine_sim(source_vec, target_vec)
            results.append(sim_score)

        return results


def tokenize_me(file_text):
    # firstly let's apply nltk tokenization
    tokens = nltk.word_tokenize(file_text)

    # let's delete punctuation symbols
    tokens = [i for i in tokens if (i not in string.punctuation)]

    # deleting stop_words
    stop_words = stopwords.words('russian')
    stop_words.extend(['что', 'это', 'так', 'вот', 'быть',
                       'как', 'в', '—', 'к', 'на'])
    tokens = [i for i in tokens if (i not in stop_words)]

    # cleaning words
    tokens = [i.replace("«", "").replace("»", "") for i in tokens]

    return tokens


def lemmatize_me(text, with_pos=False):
    for i in range(len(text)):
        p = morph.parse(text[i])[0]
        if with_pos:
            text[i] = "{0}_{1}".format(p.normal_form, p.tag.POS)
        else:
            text[i] = p.normal_form
    return text


def post_processing(post_data):
    for i in range(len(post_data)):
        post_data[i]['text'] = tokenize_me(post_data[i]['text'])
        post_data[i]['text'] = lemmatize_me(post_data[i]['text'])
        post_data[i]['text'] = ' '.join(post_data[i]['text'])
        posts_data_by_proc.append(post_data[i])


class BaseModel():
    save_path = "hier/"
    vw_path = "data/vw.txt"
    batch_path = "data/batches"
    dictionary_path = "data/dict.dict"
    indexes_child_path = "data/indexes_child.npy"
    queue_name = 'db_tasks'
    predict_start = False

    def __init__(self, set_cass=True):
        self.LOGGER = logging.getLogger('Model.artm_model')

        if set_cass:
            self.cass = RpcClient(self.queue_name, "Model.cass")

    def get_model(self, documents=None, type="main"):
        # self.load_post_model()
        self.load_top_words()
        self.load_topic_model()
        self.load_indexes_child()
        self.LOGGER.info("MODEL READY")

    def load_topic_model(self):
        # Load Google's pre-trained Word2Vec model.
        self.LOGGER.info('PREPARE TO LOADING THEME POST MODEL')
        self.topic_model = gensim.models.KeyedVectors.load_word2vec_format('./manual_themes_model/model.bin',
                                                                           binary=True)
        self.LOGGER.info('SUCCESS LOADING THEME POST MODEL')

    def load_post_model(self):
        # Load Google's pre-trained FastText model.
        self.LOGGER.info('PREPARE TO LOADING SIMILAR POST MODEL')
        self.post_model = gensim.models.KeyedVectors.load('./sim_posts_model/model.model')
        self.LOGGER.info('SUCCESS LOADING SIMILAR POST MODEL')

    def parting(self, xs, parts):
        part_len = ceil(len(xs) / parts)
        return [xs[part_len * k:part_len * (k + 1)] for k in range(parts)]

    def vectorize_text(self, text):
        ds = DocSim(self.topic_model)
        text = tokenize_me(text)
        text = lemmatize_me(text, with_pos=True)
        text = ' '.join(text)
        vec_result = ds.vectorize(text)
        try:
            result = list(vec_result)
        except:
            LOGGER.exception('ERROR WHILE VECTORIZE POST')
            result = 0
        del ds
        return result

    def check_words(self, text):
        clear_text = text
        ds = DocSim(self.topic_model)
        text = tokenize_me(text)
        text = lemmatize_me(text, with_pos=True)
        text = ' '.join(text)
        ignore_words = ds.is_in_vocab(text)
        clear_text = clear_text.split(' ')
        result = []
        if ignore_words:
            for word in ignore_words:
                index = word['index']
                result.append(clear_text[int(index)])
        del ds
        return result

    def get_vectors_for_posts(self, posts):
        result = []
        predict_list = []
        predict_data = {}

        ds = DocSim(self.topic_model)

        try:
            task = {'task_name': 'get_all_vector_topics',
                    'args': {}}
            topics = self.cass.call(task)

            for post in posts:
                if post['text'] is None or post['text'] == "":
                    continue

                post_text = post['text']
                post['vector'] = self.vectorize_text(post_text)
                if post['vector'] == 0:
                    return 0

                score = ds.calculate_sim_for_vectors(post['vector'], topics)
                for i in range(len(score)):
                    if float(score[i]) > float(topics[i]['threshold']):
                        predict_data['social'] = post['social']
                        predict_data['owner_id'] = post['owner_id']
                        predict_data['post_id'] = post['post_id']
                        predict_data['probability'] = score[i]
                        predict_data['topic'] = topics[i]['words']
                        predict_list.append(predict_data.copy())
                result.append(post)
            if predict_list:
                task = {'task_name': 'insert_vector_topics_prob',
                        'args': {'data': predict_list}}
                self.cass.call(task)
            del ds
            return result
        except Exception:
            LOGGER.exception('FAILED TO VECTOR POST')
            LOGGER.info(posts)
            return 0

    def get_nearest_for_post(self, source_vec, target_docs, count=5):

        ds = DocSim(self.topic_model)
        sim_scores = ds.calculate_sim_for_vectors(source_vec, target_docs)
        for f, post in enumerate(target_docs):
            post['score'] = sim_scores[f]
        target_docs.sort(key=lambda k: k['score'], reverse=True)
        del ds

        return target_docs[:count]

    def get_nearest_for_topic(self, source_vec, target_docs):

        ds = DocSim(self.topic_model)
        sim_scores = ds.calculate_sim_for_vectors(source_vec, target_docs)
        for f, post in enumerate(target_docs):
            post['score'] = sim_scores[f]
        target_docs.sort(key=lambda k: k['score'], reverse=True)
        data = pd.DataFrame(target_docs)
        max_value = data['score'].max()
        max_value = max_value.astype('float64')
        final_list = pd.DataFrame(data[:1])
        final_list.to_csv('Nearest.csv', mode='w', header=False, index=False)
        for i in range(0, 4):
            row = data.loc[data['score'].astype('float64') < float(max_value - 0.05)][:1]
            row.to_csv('Nearest.csv', mode='a', header=False, index=False)
            max_value = max_value - 0.05
        final_list = pd.read_csv('Nearest.csv', index_col=False)
        final_list.set_axis(['owner_id', 'post_id', 'score', 'social', 'vector'], axis=1,
                            inplace=True)
        final_list = final_list.drop_duplicates(subset='post_id')
        final_list = final_list.astype({'owner_id': str, 'post_id': str})
        final_list = final_list.to_dict('records')

        del ds

        return final_list

    def get_nearest_posts(self, post_text):
        start_time = time.time()
        posts_list = list()
        del_list = list()
        posts_data = dict()
        posts_data_copy = dict()

        post_text = post_text.encode("iso-8859-1").decode("utf-8")
        post_text = post_text.replace("<br>", "")
        f = 0
        ds = DocSim(self.topic_model)
        posts_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S")
        task = {'task_name': 'get_last_posts',
                'args': {'date': [f"date >= '{posts_date}'"]}}

        posts = self.cass.call(task)
        for i in range(len(posts)):
            try:
                if posts[i]['text']:
                    if posts[i]['text'] == '':
                        del_list.append(i)
                else:
                    del_list.append(i)
            except Exception:
                LOGGER.exception('ERROR WHILE PARSE GROUP INFO')

        del_list.reverse()
        for numbers in del_list:
            del posts[numbers]

        text_by_dict = {i['text']: i for i in posts}
        posts = list(text_by_dict.values())

        for i in range(len(posts)):
            posts_data.clear()
            posts_data['index'] = i
            posts_data['text'] = posts[i]['text']
            posts_data_copy = posts_data.copy()
            posts_list.append(posts_data_copy)
            del posts_data_copy

        posts_data.clear()
        posts_list = self.parting(posts_list, 20)

        p = multiprocessing.Pool(processes=4)
        p.imap_unordered(post_processing, posts_list)

        p.close()
        p.join()

        post_text = tokenize_me(post_text)
        post_text = lemmatize_me(post_text, with_pos=True)
        post_text = ' '.join(post_text)
        potstssss = list(posts_data_by_proc)
        posts_data_by_proc[:] = []
        potstssss.sort(key=lambda x: x['index'], reverse=False)
        result = list()
        for res in potstssss:
            result.append(res['text'])
        source_doc = post_text
        target_docs = result
        sim_scores = ds.calculate_similarity(source_doc, target_docs)
        for postt in posts:
            postt['score'] = sim_scores[f]
            f = f + 1
        posts.sort(key=lambda k: k['score'], reverse=True)
        posts = posts[1:6]

        groups_id = list()
        for postt in posts:
            groups_id.append(postt['owner_id'])
        task = {'task_name': 'get_groups_name',
                'args': {'groups_id': groups_id}}

        tasks = self.cass.call(task)

        for v in range(len(tasks)):
            if tasks[v]:
                posts[v]['name'] = tasks[v][0]['name']
            else:
                posts[v]['name'] = 'Имя группы'

        for postt in posts:
            postt['href'] = "/post?owner-id=" + str(postt['owner_id']) + \
                "&post-id=" + str(postt['post_id']) + \
                "&social=" + str(postt['social'])
            postt['group_href'] = self.get_group_href(
                postt['social'], postt['owner_id'])[0]

        print("--- %s seconds ---" % (time.time() - start_time))
        del sim_scores, result, potstssss, groups_id, ds
        return posts

    def get_group_href(self, socials, owner_ids):
        supported_socials = ['vk', 'ok', 'inst']
        result = []
        social = str(socials)
        owner_id = str(owner_ids)
        if social not in supported_socials:
            result.append("")
            return 0
        base = self.get_group_base(social)
        group = owner_id
        if social == "vk":
            group = owner_id[1:]

        result.append(base + group)

        return result

    def get_group_base(self, social):
        if social == "vk":
            return 'https://vk.com/club'
        elif social == "ok":
            return 'https://ok.ru/group/'
        elif social == "inst":
            return 'https://instagram.com/'

    def get_top_words(self):
        with open("data/top_words.pkl", 'rb') as f:
            top_words = pickle.load(f)

        return top_words

    def load_top_words(self):
        self.top_words = self.get_top_words()

    def load_indexes_child(self):
        self.indexes_child = np.load("data/indexes_child.npy")

    def build_foamtree(self, topics_states={}):
        self.LOGGER.info("BUILD FOAMTREE")
        answer = []
        for topic in range(25):
            label = ', '.join(self.top_words[f"topic_{topic}"][:3])
            groups = []
            for child in np.where(self.indexes_child == topic)[0]:
                info = {"label": ', '.join(self.top_words[f"child_topic_{child}"][:3]),
                        "selectable": True,
                        "groups": [],
                        "level": 1,
                        "topic": int(child)}
                if topics_states.get(str(child)) == 1:
                    info['is_tracked'] = True
                elif topics_states.get(str(child)) == -1:
                    info['is_hidden'] = True
                groups.append(info)
            answer.append({"label": label,
                           "level": 0,
                           "topic": int(topic),
                           "groups": groups})

        return answer


class ModelTaskManager():
    rabbit_host = "rabbit"
    rel_model_name = "rel_model_tasks"

    def __init__(self):
        self.model = BaseModel()
        self.model.get_model()
        self.method_list = [func for func in dir(self.model) if
                            callable(getattr(self.model, func))
                            and not func.startswith("__")]

    def on_request(self, ch, method, props, body):

        task = pickle.loads(body)

        if task.get('task_name') in self.method_list:
            task_method = getattr(self.model, task.get('task_name'))
            task_args = task['args']
            try:
                result = task_method(**task_args)
            except Exception:
                LOGGER.exception(f"Error occured in {task.get('task_name')}")
                result = None
        else:
            result = "Current method is not supported"

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=pickle.dumps(result))
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    task_manager = ModelTaskManager()
    rel_model_process = Process(target=make_server,
                                args=(task_manager.rel_model_name,
                                      task_manager.on_request))
    rel_model_process.start()
    rel_model_process.join()
