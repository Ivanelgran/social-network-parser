import ast
import os
import yaml
import logging
import logging.config
import json
import time
import pandas as pd
import numpy as np
import pickle
from collections import defaultdict
from calendar import monthrange
from pathlib import Path
from datetime import datetime, timedelta
from bottle import route, run, request, response, HTTPResponse
from common_lib.utilities import show_ip_address, time_it
from api_utils import get_top_words
from common_lib.queue_manager import RpcClient
from common_lib.src.rabbit import RpcClient as rpc_client
# from common_lib.cass_worker import CassandraTables, CassQueryGenerator

from common_lib.cass_worker import (
    CassandraTables, select_data, setup_connection)

SRC_DIR = Path(__name__).resolve().parents[0]

with open(os.path.join(SRC_DIR, 'config/config.yml')) as config_file:
    CONFIG = yaml.safe_load(config_file)
    logging.config.dictConfig(CONFIG)

LOGGER = logging.getLogger('api')
LOGGER.setLevel(logging.INFO)

rel_model = RpcClient('rel_model_tasks')
cass = RpcClient('db_api_tasks')
bots = RpcClient('bots_edit_tasks')
parser = RpcClient('parser_tasks')
fts_service = RpcClient('fts_service')
posts_collector = RpcClient('posts_collector')
traces_mq = RpcClient('traces')
setup_connection()

vw_path = 'model/pred/vw.txt'
all_cities = {1: 'Челябинск',
              2: 'Златоуст',
              3: 'Каменск-Уральский',
              4: 'Копейск',
              5: 'Касли',
              6: 'Коркино',
              7: 'Курган',
              8: 'Кыштым',
              9: 'Магнитогорск',
              10: 'Миасс',
              11: 'Озерск',
              12: 'Пласт',
              13: 'Рощино',
              14: 'Троицк',
              15: 'Чебаркуль',
              16: 'Еманжелинск',
              17: 'Южноуральск'}

city_dict = {'Челябинск': 158,
             'Златоуст': 794,
             'Каменск-Уральский': 1573,
             'Копейск': 5098,
             'Касли': 5542,
             'Коркино': 12126,
             'Курган': 74,
             'Кыштым': 6050,
             'Магнитогорск': 82,
             'Миасс': 7159,
             'Озерск': 941,
             'Пласт': 3034,
             'Рощино': 6820,
             'Троицк': 145,
             'Чебаркуль': 3255,
             'Еманжелинск': 5401,
             'Южноуральск': 1268}

all_att_types = {'image': 'Изобр.',
                 'video': "Видео",
                 'gif': "Гифок",
                 'audio': "Музыки",
                 'doc': "Докум."}


@route('/')
@show_ip_address
@time_it
def index():
    """
    Метод показывающий что сервис работает
    """
    response.content_type = 'application/json'
    try:
        data = {'API VERSION': API_VERSION}
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return data


@route('/tasks')
@show_ip_address
@time_it
def tasks():
    response.content_type = 'application/json'
    try:
        task = {'task_name': 'get_tasks',
                'args': {}}
        result = cass.call(task)
        result = pd.DataFrame(result)
        result = result[result.social != 'test'].to_dict('records')
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/bots-login', method='GET')
@show_ip_address
@time_it
def bots_login():
    response.content_type = 'application/json'
    try:
        LOGGER.info('bots-login')
        get_params = json.loads(request.query['params'])
        login = get_params['login']
        password = get_params['password']
        task = {'task_name': 'authentication_bot',
                'args': {'login': login, 'password': password}}
        result = bots.call(task)

        LOGGER.info(result)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/upload-template', method='GET')
@show_ip_address
@time_it
def upload_template():
    response.content_type = 'application/json'
    try:
        LOGGER.info('upload-template')
        get_params = json.loads(request.query['params'])
        title = get_params['title']
        social = get_params['social']
        new_template = get_params['new_template']
        LOGGER.info(new_template)

        if new_template == 'true':

            task = {'task_name': 'create_new_template_bot_patterns',
                    'args': {'social': social, 'title': title}}
            table = cass.call(task)

            if table is False:
                result = {'result': 'Ошибка базы данных'}
                return json.dumps(result, indent=4, sort_keys=True, default=str)

            elif table == 'Шаблон с таким названием и соц. сетью уже существует':
                result = {'result': table}
                return json.dumps(result, indent=4, sort_keys=True, default=str)
        else:
            task = {'task_name': 'select_bot_patterns',
                    'args': {'social': social, 'title': title}}
            table = cass.call(task)

            if table is False:
                result = {'result': 'Ошибка базы данных'}
                return json.dumps(result, indent=4, sort_keys=True, default=str)

        result = {'result': 'true', 'info': table}

        list = []
        for dic in table:
            info = {}
            for key, value in dic.items():
                if key == 'social' or key == 'title' or key == 'description':
                    info[key] = value
                else:
                    # LOGGER.info(i)
                    # LOGGER.info(dic[i])
                    # LOGGER.info(type(dic[i]))
                    try:
                        # s = ast.literal_eval(dic[i])
                        # LOGGER.info(s)
                        # LOGGER.info(type(s))

                        s = dict(value)

                        for j in s.keys():
                            info[j] = s[j]
                    except:
                        LOGGER.exception('Пустой словарь')

            list.append(info)

            result['info'] = list

        # task = {'task_name': 'upload_template',
        #         'args': {'social': social, 'title': title, 'new_template': new_template}}
        # result = bots.call(task)

        LOGGER.info(result)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/del-template', method='GET')
@show_ip_address
@time_it
def del_template():
    response.content_type = 'application/json'
    try:
        LOGGER.info('del_template')
        get_params = json.loads(request.query['params'])
        title = get_params['title']
        social = get_params['social']
        task = {'task_name': 'del_template_bot_patterns',
                'args': {'social': social, 'title': title}}

        if cass.call(task):
            result = True
        else:
            result = 'Ошибка базы данных'

        LOGGER.info(result)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/save-changes-text', method='GET')
@show_ip_address
@time_it
def bots_editing():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        dic_fields = get_params['value']
        src = get_params['src']

        LOGGER.info('_______save-changes-text______')
        LOGGER.info(dic_fields)
        task = {'task_name': src,
                'args': {'dic_fields': dic_fields}}
        result = bots.call(task)

        LOGGER.info(result)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/bots-editing-avatar', method='GET')
@show_ip_address
@time_it
def bots_editing_avatar():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        login = get_params['login']
        password = get_params['password']
        photo = get_params['photo']

        LOGGER.info('_______bots-editing-avatar______')
        task = {'task_name': 'add_avatar',
                'args': {'login': login,
                         'password': password,
                         'photo': photo}}
        result = bots.call(task)

        LOGGER.info(result)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/bots-editing-album', method='GET')
@show_ip_address
@time_it
def bots_editing_avatar():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        login = get_params['login']
        password = get_params['password']
        list_name = get_params['photo']
        album_lenght = get_params['album_lenght']
        album_name = get_params['album_name']

        LOGGER.info('_______bots-editing-album______')
        task = {'task_name': 'album_create',
                'args': {'login': login,
                         'password': password,
                         'list_name': list_name,
                         'album_lenght': album_lenght,
                         'album_name': album_name, }}
        result = bots.call(task)

        LOGGER.info(result)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/unique-groups-count')
@show_ip_address
@time_it
def unique_groups_count():
    response.content_type = 'application/json'
    try:
        task = {'task_name': 'get_parsed_groups',
                'args': {}}
        groups = cass.call(task)

        groups = [group['group_id'] for group in groups]
        result = str(len(set(groups))) or '0'
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return result


@route('/task-groups')
@show_ip_address
@time_it
def task_groups():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        task_id = get_params['task-id']
        is_keyword = get_params['is_keyword']
        if is_keyword != 'True':
            task = {'task_name': 'get_task_groups',
                    'args': {'task_id': task_id}}
            groups = cass.call(task)

            for group in groups:
                gr = group['group_id']
                group['group_id'] = gr[1:] if gr.startswith('-') else gr

            if len(groups) == 0:
                return json.dumps([], indent=4, sort_keys=True, default=str)

            groups_df = pd.DataFrame(groups)

            all_groups = list(groups_df['group_id'].values)

            # all_groups = [gr[1:] if gr.startswith('-') else gr for gr in all_groups]

            # socials = groups_df.social.value_counts().index
            # data = []
            # for soc in socials:
            #     str_groups = [
            #         f"'{group}'" for group in groups_df[
            #             groups_df.social == soc
            #             ].group_id.values
            #     ]
            #     str_groups = ','.join(str_groups)
            #     task = {'task_name': 'get_groups_info',
            #             'args': {'social': soc,
            #                      'groups': str_groups}}
            #     social_groups = cass.call(task)
            #     data.extend(social_groups)
            data = get_groups_info(groups)

            data_groups = [group['group_id'] for group in data]

            miss_groups = [
                group for group in all_groups if group not in data_groups]

            for group in miss_groups:
                social = groups_df[groups_df.group_id == group].iloc[0].social
                info = {'social': social,
                        'group_id': group,
                        'name': '',
                        'members_count': 0,
                        'our_prop': 0,
                        'post_per_day': 0,
                        'views_per_post': 0,
                        'comments': 0}
                data.append(info)

            groups_df = pd.DataFrame(data)
            groups_df['href'] = get_group_href(
                groups_df.social, groups_df.group_id)
            groups_df.fillna(0, inplace=True)
            data = groups_df.to_dict('records')
        else:
            task = {'task_name': 'get_task_keywords',
                    'args': {'task_id': task_id}}
            data = cass.call(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(data, indent=4, sort_keys=True, default=str)


def get_groups_info(groups):
    result = []

    groups_df = pd.DataFrame(groups)
    for social in groups_df.social.unique():
        data = groups_df[groups_df.social == social]
        group_ids = list(data.group_id.values)
        if social == 'vk':
            group_ids = [
                group_id[1:] if group_id.startswith('-') else group_id
                for group_id in group_ids
            ]

        # task = {'task_name': 'get_social_groups',
        #         'args': {'social': social,
        #                  'group_ids': group_ids}}
        task = {'type': 'select',
                'table': 'Group',
                'items': {'social': social},
                'additional_keys': [('group_id', 'IN', group_ids)]}
        groups = cass.call(task)

        if groups is not None:
            result.extend(groups)

    return result


@route('/groups')
@show_ip_address
@time_it
def groups():
    response.content_type = 'application/json'
    try:
        config = json.loads(request.query['params'])
        limits = []
        if config.get('members_count') is not None:
            limits.append(("members_count", ">=", config['members_count']))
        if config.get('our_prop') is not None:
            limits.append(("our_prop", ">=", config['our_prop']))
        if config.get('posts_per_day') is not None:
            limits.append(("post_per_day", ">=", config['posts_per_day']))
        if config.get('views_prop') is not None:
            limits.append(("views_per_post", ">=", config['views_prop']))
        if config.get('comments') is not None:
            limits.append(("comments", ">=", config['comments']))
        # if len(limits) == 0:
        #     limits = None

        task = {'type': 'select',
                'table': 'Group',
                'keys': {'social': 'vk'},
                'additional_keys': limits}
        groups = cass.call(task)

        groups = pd.DataFrame(groups).dropna()
        if config.get('cities') is not None and groups.shape[0]:
            cities = [city_dict[all_cities[i]] for i in config['cities']]
            LOGGER.info(cities)
            groups = groups[groups.main_city.isin(cities)]
        result = groups.to_dict('records')
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/parsed-groups')
@show_ip_address
@time_it
def parsed_groups():
    response.content_type = 'application/json'
    try:
        task = {'task_name': 'get_parsed_groups',
                'args': {}}
        result = cass.call(task, json_result=True)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return result


@route('/parse-group-info')
@show_ip_address
@time_it
def parse_group_info():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        social = get_params['social']
        group = get_params['group']
        LOGGER.info(f"PARSE INFO for {social} {group}")
        task = {'task_name': 'group_info',
                'args': {'social': 'vk',
                         'url': group}}
        parser = rpc_client('parser_tasks')
        info = parser.send(task)

        group_id = info['owner_id']

        task = {'type': 'select',
                'table': 'Group',
                'items': {'social': social,
                          'group_id': group_id}}
        group = cass.call(task)

        if group is None or not len(group):
            info = {'social': social,
                    'group_id': group_id,
                    'members_count': info['members_count']}

            task = {'type': 'insert',
                    'table': 'Group',
                    'items': info}
            cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(group_id, indent=4, sort_keys=True, default=str)


@route('/parse-keyword-info')
@show_ip_address
@time_it
def parse_keyword_info():
    response.content_type = 'application/json'
    try:
        # get_params = json.loads(request.query['params'])
        # social = get_params['social']
        # keyword = get_params['keyword']
        # LOGGER.info(f"PARSE KEYWORD INFO for {social} {keyword}")
        # if str(social) == 'inst':
        #     task = {'task_name': 'parse_keyword_group_info',
        #             'keyword': keyword}
        # else:
        #     LOGGER.warning("ERROR MATCHING KEYWORD AND SOCIAL NETWORK")

        # # Получаю имя группы, ID, members_count

        # info = parser.call(task)
        # group = {info['owner_id']: {'name': info['name'],
        #                             'members_count': info['members_count'],
        #                             'is_closed': False,
        #                             'our_prop': 0,
        #                             'post_per_day': 0,
        #                             'views_per_post': 0,
        #                             'comments': 0,
        #                             'social': social,
        #                             'main_city': '',
        #                             'main_prop': 0}}

        # LOGGER.info("SAVE TO DB")
        # insert_task = {'task_name': 'insert_groups',
        #                'args': {'groups': group}}
        # cass.send_message(insert_task)

        # result = info['owner_id']

        # LOGGER.info("READY")
        result = None
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/user-groups')
@show_ip_address
@time_it
def user_groups():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        username = get_params['username']
        hidden_groups = {}
        task = {'task_name': 'get_user_groups',
                'args': {'username': username}}
        user_groups = cass.call(task)
        for group in user_groups:
            if group['state'] == -1:
                if hidden_groups.get(group['social']) is None:
                    hidden_groups[group['social']] = []
                hidden_groups[group['social']].append(group['group_id'])

        # По таску берет social u group_id
        task = {'task_name': 'get_parsed_groups',
                'args': {}}
        groups = cass.call(task)

        # groups_df = pd.DataFrame(groups)
        # socials = groups_df.social.value_counts().index

        # TODO: Разобраться с добавление групп в таблицу Groups

        # data = []
        # for soc in socials:
        #     unique_groups = set(
        #         groups_df[groups_df.social == soc].group_id.values)
        #     unique_groups = ','.join([f"'{group}'" for group in unique_groups])

        #     task = {'task_name': 'get_groups_info',
        #             'args': {'social': soc,
        #                      'groups': unique_groups}}
        #     social_groups = cass.call(task)
        #     data.extend(social_groups)
        data = get_groups_info(groups)

        for group in data:
            if hidden_groups.get(group['social']) is not None and \
                    group['group_id'] in hidden_groups[group['social']]:
                group['is_hidden'] = True
            else:
                group['is_hidden'] = False
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(data, indent=4, sort_keys=True, default=str)


@route('/visual')
@show_ip_address
@time_it
def visual():
    path_to_visual = "./visual/ftl.txt"
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        topics_states = json.loads(get_params['topics_states'])
        LOGGER.info(topics_states)
        task = {'task_name': 'build_foamtree',
                'args': {'topics_states': topics_states}}
        groups = rel_model.call(task)
        groups = {"groups": groups, "label": 'ARTM'}
        with open(path_to_visual, 'w') as f:
            f.write(json.dumps(groups))

        with open(path_to_visual, "r", encoding='utf-8') as f:
            data = f.read()
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(data, indent=4, sort_keys=True, default=str)


@route('/top-words')
@show_ip_address
@time_it
def top_words():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        topic = get_params['topic']
        task = {'task_name': 'get_top_words',
                'args': {}}
        result = rel_model.call(task)[f'child_topic_{topic}']
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/test-topic', method='GET')
@show_ip_address
@time_it
def test_topic():
    response.content_type = 'application/json'
    try:
        LOGGER.info("Testing topics")
        param = json.loads(request.query['params'])

        task = {'task_name': 'vectorize_text',
                'args': {'text': param['words']}}
        source_vector = rel_model.call(task)
        source_vector = [float(i) for i in source_vector]

        t = time.time()
        task = {'task_name': 'get_posts_vectors_for_count',
                'args': {}}
        target_vectors = cass.call(task)
        LOGGER.info('Posts count - ' + str(len(target_vectors)))

        LOGGER.info(f"Getting post in {time.time() - t}")

        t = time.time()
        task = {'task_name': 'get_nearest_for_topic',
                'args': {'source_vec': source_vector,
                         'target_docs': target_vectors}}
        nearest_posts = rel_model.call(task)
        LOGGER.info(f"Calculating similarity in {time.time() - t}")

        t = time.time()
        task = {'task_name': 'get_posts',
                'args': {'posts': nearest_posts}}
        posts = cass.call(task)
        LOGGER.info(f"Getting posts data in {time.time() - t}")

        t = time.time()
        nearest_posts = pd.DataFrame(nearest_posts)
        posts = pd.DataFrame(posts)
        result = pd.merge(nearest_posts, posts, how="left",
                          on=['social', 'owner_id', 'post_id'])
        groups = result[['social', 'owner_id']].to_dict("records")
        result['name'] = get_groups_name(groups)
        result = result.to_dict("records")
        # groups_id = list()
        # for postt in result:
        #     groups_id.append(postt['owner_id'])
        # task = {'task_name': 'get_groups_name',
        #         'args': {'groups_id': groups_id}}

        # tasks = cass.call(task)

        # for v in range(len(tasks)):
        #     if tasks[v]:
        #         result[v]['name'] = tasks[v][0]['name']
        #     else:
        #         result[v]['name'] = 'Имя группы'
        for post in result:
            post['href'] = "/post?owner-id=" + str(post['owner_id']) + "&post-id=" + str(post['post_id']) + \
                           "&social=" + str(post['social'])
            post['group_href'] = get_href_for_group(post['social'],
                                                    post['owner_id'])

        LOGGER.info(f"Finishing data formation in {time.time() - t}")

    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/get-nearest-posts')
@show_ip_address
@time_it
def get_nearest_posts():
    response.content_type = 'application/json'
    try:
        LOGGER.info("getting nearest posts")
        get_params = json.loads(request.query['params'])

        t = time.time()
        task = {'task_name': 'get_post_vector',
                'args': {'social': get_params['social'],
                         'owner_id': get_params['owner_id'],
                         'post_id': get_params['post_id']}}
        source_vector = cass.call(task)
        LOGGER.info(f"{time.time() - t}")

        t = time.time()

        task = {'task_name': 'get_last_vectors',
                'args': {}}
        target_vectors = posts_collector.call(task)

        target_vectors = [vect for vect in target_vectors if (
                vect['social'] != get_params['social']
                or vect['owner_id'] != get_params['owner_id']
                or vect['post_id'] != get_params['post_id'])]

        LOGGER.info(f"{time.time() - t}")

        t = time.time()
        task = {'task_name': 'get_nearest_for_post',
                'args': {'source_vec': source_vector['vector'],
                         'target_docs': target_vectors}}
        nearest_posts = rel_model.call(task)
        LOGGER.info(f"{time.time() - t}")

        t = time.time()
        task = {'task_name': 'get_posts',
                'args': {'posts': nearest_posts}}
        posts = cass.call(task)
        LOGGER.info(f"{time.time() - t}")

        t = time.time()
        nearest_posts = pd.DataFrame(nearest_posts)
        posts = pd.DataFrame(posts)

        result = pd.merge(nearest_posts, posts, how="left",
                          on=['social', 'owner_id', 'post_id'])
        groups = result[['social', 'owner_id']].to_dict("records")
        result['name'] = get_groups_name(groups)
        result = result.to_dict("records")
        # groups_id = list()
        # for postt in result:
        #     groups_id.append(postt['owner_id'])
        # task = {'task_name': 'get_groups_name',
        #         'args': {'groups_id': groups_id}}

        # tasks = cass.call(task)

        # for v in range(len(tasks)):
        #     if tasks[v]:
        #         result[v]['name'] = tasks[v][0]['name']
        #     else:
        #         result[v]['name'] = 'Имя группы'
        for post in result:
            post['href'] = "/post?owner-id=" + str(post['owner_id']) + \
                           "&post-id=" + str(post['post_id']) + \
                           "&social=" + str(post['social'])
            post['group_href'] = get_href_for_group(
                post['social'], post['owner_id'])

        LOGGER.info(f"{time.time() - t}")

    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


def get_groups_name(groups):
    result = []
    for group in groups:
        info = get_group_info(group['social'], group['owner_id'])
        if info is not None and 'name' in info:
            result.append(info['name'])
        else:
            result.append('Имя группы')

    return result


@route('/trace-template')
@show_ip_address
@time_it
def trace_template():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        topic = get_params['topic']
        task = {'task_name': 'get_trace_template',
                'args': {'level': 1, 'topic': topic}}
        trace = cass.call(task)
        if trace is None or len(trace) == 0:
            result = ""
        else:
            result = trace[0]['trace']
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    result = {'trace': result}

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/top-posts')
@show_ip_address
@time_it
def top_posts():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        topic = get_params['topic']

        def clean_hashtags(text):
            split_text = text.split()
            words_count = tags_count = 0
            for i in split_text:
                if i.startswith('#'):
                    tags_count += 1
                else:
                    words_count += 1

            return words_count > tags_count

        task = {'task_name': 'get_top_posts',
                'args': {'level': 1, 'topic': topic}}
        posts_for_topic = cass.call(task)
        if posts_for_topic is None:
            return
        df = pd.DataFrame(posts_for_topic)
        df.drop_duplicates(subset='text', inplace=True)
        df['is_relevant'] = df.text.apply(clean_hashtags)
        df = df[df.is_relevant].sort_values(
            by='probability', ascending=False).iloc[:100]

        result = df.to_dict('records')
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/post-info')
@show_ip_address
@time_it
def post_info():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        owner_id = get_params['owner-id']
        post_id = get_params['post-id']
        social = get_params['social']

        if social == 'vk' and owner_id[0] != "-":
            owner_id = "-" + owner_id
        task = {'task_name': 'get_post',
                'args': {'social': social,
                         'owner_id': owner_id,
                         'post_id': post_id}}
        post_data = cass.call(task)
        group = get_group_info(social, owner_id)
        if group is not None and 'error' not in group:
            post_data['name'] = group.get('name') or owner_id
        else:
            post_data['name'] = owner_id
        task = {'task_name': 'get_vector_post_topics',
                'args': {'social': social,
                         'owner_id': owner_id,
                         'post_id': post_id}}
        post_topics = cass.call(task)

        task = {'task_name': 'get_post_stat',
                'args': {'social': social,
                         'owner_id': owner_id,
                         'post_id': post_id,
                         'count': None}}
        cur_post_stat = cass.call(task)
        post_stat = None
        if len(cur_post_stat) > 0:
            cur_post_stat = pd.DataFrame(cur_post_stat).sort_values('ts')
            post_stat = {'likes': list(cur_post_stat.likes.values),
                         'shares': list(cur_post_stat.shares.values),
                         'views': list(cur_post_stat.views.values),
                         'replies': list(cur_post_stat.replies.values),
                         'len': list(cur_post_stat.ts.astype('str').values)}
        if post_topics is not None:
            result = {'post_data': post_data,
                      'post_topics': post_topics[:10],
                      'post_stat': post_stat}
        else:
            result = {'post_data': post_data,
                      'post_topics': '',
                      'post_stat': post_stat}
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


def get_group_info(social, owner_id):
    if social == 'vk' and owner_id.startswith('-'):
        group_id = owner_id[1:]
    else:
        group_id = owner_id

    task = {'type': 'select',
            'table': 'Group',
            'items': {'social': social,
                      'group_id': group_id}}
    group = cass.call(task)

    if len(group):
        return group[0]


@route('/bots-info')
@show_ip_address
@time_it
def bots_info():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        social = get_params.get('social')
        LOGGER.info("bots-info")
        task = {'task_name': 'get_bots',
                'args': {'social': social}}
        result = cass.call(task, json_result=True)
        LOGGER.info("bots-info complete")
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return result


@route('/notifications')
@show_ip_address
@time_it
def notifications():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        username = get_params.get('username')
        task = {'task_name': 'get_notifications',
                'args': {'username': username}}
        result = cass.call(task)
        for info in result:
            info['info'] = dict(info['info'])
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/new-posts')
@show_ip_address
@time_it
def new_posts():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        # result = get_new_posts(get_params)
        task = {'task_name': 'get_new_posts',
                'args': {'cond': get_params}}
        result = posts_collector.call(task)

        if len(result):
            save_viewed_posts(pd.DataFrame(result),
                              get_params['username'])
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/get-posts-by-rule')
@show_ip_address
@time_it
def get_posts_by_rule():
    response.content_type = 'application/json'
    try:
        param = json.loads(request.query['params'])
        result = get_posts_by_rules(param)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


def save_viewed_posts(posts_df, username):
    new_viewed_posts = posts_df[~posts_df['is_viewed']].to_dict('records')
    if len(new_viewed_posts) > 0:
        task = {'task_name': 'insert_viewed_posts',
                'args': {'username': username,
                         'posts': new_viewed_posts}}
        cass.send_message(task)


# TODO: функция должна влезать в экран
def get_new_posts(cond=None):
    result = []
    limits = None
    filter_viewed = False
    min_likes = min_replies = min_shares = min_views = None
    posts_count = cond.get('posts_count') or 100 if cond is not None else 100
    if cond is not None and len(cond) > 0:
        if cond.get('likes') is not None:
            min_likes = cond['likes']
        if cond.get('replies') is not None:
            min_replies = cond['replies']
        if cond.get('shares') is not None:
            min_shares = cond['shares']
        if cond.get('views') is not None:
            min_views = cond['views']
        if cond.get('filter_viewed') is not None:
            filter_viewed = cond['filter_viewed']

    # Сбор постов
    cur_t = time.time()
    if cond is not None and cond.get('start_date') is not None:
        posts_date = f"{cond['start_date']} {cond['start_time']}:00"
    else:
        posts_date = (
                datetime.now() - timedelta(days=1)
        ).strftime("%Y-%m-%d %H:%M:%S")
    posts_date = f"date >= '{posts_date}'"
    task = {'task_name': 'get_last_posts',
            'args': {'cond': limits,
                     'date': [posts_date]}}
    posts = cass.call(task)
    if posts is None or len(posts) == 0:
        LOGGER.info("NEW POSTS NOT FOUND")
        return []

    posts_df = pd.DataFrame(posts)
    LOGGER.info(f"Сбор постов {time.time() - cur_t}")

    # Фильтр по соцсети
    if cond is not None and cond.get('checked_socials') is not None:
        socials = cond.get('checked_socials')
        if len(socials) > 0:
            posts_df = posts_df[posts_df.social.isin(socials)]

        if posts_df.shape[0] == 0:
            return []

    # Сбор просмотренных постов
    cur_t = time.time()
    viewed_posts_ids = []
    task = {'task_name': 'get_viewed_posts',
            'args': {'username': cond['username']}}
    viewed_posts = cass.call(task)
    if viewed_posts is not None:
        for post in viewed_posts:
            viewed_posts_ids.append(
                str(post['owner_id']) + "_" + str(post['post_id']))
    posts_df['full_post_id'] = posts_df.owner_id.apply(
        str) + "_" + posts_df.post_id.apply(str)
    posts_df['is_viewed'] = posts_df.full_post_id.isin(viewed_posts_ids)
    LOGGER.info(f"Сбор просмотренных постов {time.time() - cur_t}")

    # Сбор групп
    cur_t = time.time()
    groups = set(posts_df.owner_id.values)
    socials = list(set(posts_df.social.values))
    groups_info = get_post_groups(groups, socials)
    LOGGER.info(f"Сбор групп {time.time() - cur_t}")

    # Добавление информации о группах
    posts_df = pd.merge(posts_df, groups_info,
                        left_on=['social', 'owner_id'],
                        right_on=['social', 'group_id'],
                        how='left')

    # Сбор групп, скрытых пользователем
    cur_t = time.time()
    hidden_groups = []
    task = {'task_name': 'get_user_groups',
            'args': {'username': cond['username']}}
    user_groups = cass.call(task)
    for group in user_groups:
        if group['state'] == -1:
            hidden_groups.append(group['group_id'])

    posts_df = posts_df[~posts_df.group_id.isin(hidden_groups)]

    if posts_df.shape[0] == 0:
        return []
    LOGGER.info(f"Сбор групп, скрытых пользователем {time.time() - cur_t}")

    # Фильтрация неподходящих городов
    cur_t = time.time()
    if cond.get('checked_cities') is not None:
        cities = [all_cities[int(i)] for i in cond['checked_cities']]
        if len(all_cities) == len(cond['checked_cities']):
            cities.append(np.nan)
            cities.append("")

        posts_df = posts_df[posts_df.main_city.isin(cities)]

    if posts_df.shape[0] == 0:
        return []
    LOGGER.info(f"Фильтрация неподходящих городов {time.time() - cur_t}")
    # Получаем информацию о темах
    cur_t = time.time()
    # task = {'task_name': 'get_posts_topics',
    #         'args': {'posts': posts_df[[
    #               'social', 'owner_id', 'post_id']].to_dict('records')}}
    # posts_topics = cass.call(task)

    task = {'task_name': 'get_vector_posts_topics',
            'args': {'posts': posts_df[[
                'social', 'owner_id', 'post_id']].to_dict('records')}}
    vector_posts_topics = cass.call(task)
    LOGGER.info(type(vector_posts_topics))
    # if len(posts_topics) > 0:
    #     posts_topics = pd.DataFrame(posts_topics)
    # else:
    #     posts_topics = pd.DataFrame(
    #         columns=[
    #   'social', 'owner_id', 'post_id', 'topic', 'probability'])

    # vector_posts_topics[:] = [d for d in vector_posts_topics]
    vector_posts_topics = pd.DataFrame(vector_posts_topics)
    if len(vector_posts_topics) == 0:
        vector_posts_topics = pd.DataFrame(
            columns=['social', 'owner_id', 'post_id', 'topic'])
    posts_df.drop(columns=['topic'], inplace=True)
    LOGGER.info(type(vector_posts_topics))
    LOGGER.info(type(posts_df))
    posts_df = pd.merge(posts_df, vector_posts_topics, how="left",
                        on=['social', 'owner_id', 'post_id'])
    posts_df.topic.fillna('-1', inplace=True)
    # posts_df.probability.fillna(0, inplace=True)

    posts_df['is_topic_tracked'] = False
    # posts_df.drop(columns=['topic'], inplace=True)
    # posts_df = pd.merge(posts_df, posts_topics, how="left",
    #                     on=['social', 'owner_id', 'post_id'])
    # posts_df.topic.fillna(-1, inplace=True)
    # posts_df.probability.fillna(0, inplace=True)
    # posts_df.topic = posts_df.topic.astype(int)
    #
    # if filter_tracked:
    #     topics_states = cond['topics_states']
    #     tracked_topics = []
    #     for topic, state in topics_states.items():
    #         if state == 1:
    #             tracked_topics.append(topic)
    #     posts_df['is_topic_tracked'] = posts_df.topic.isin(tracked_topics)
    # else:
    #     posts_df['is_topic_tracked'] = False
    # LOGGER.info(f"Получаем информацию о темах {time.time() - cur_t}")
    #

    # Фильтрация по отслеживаемым темам
    if cond.get('checked_topics') is not None and len(cond['checked_topics']):
        posts_df = posts_df[posts_df.topic.isin(cond.get('checked_topics'))]

    # # Фильтрация скрытых тем
    # posts_df['show_topic'] = posts_df.topic.apply(
    #     lambda x: cond['topics_states'].get(x) != -1)
    # posts_df = posts_df[posts_df.show_topic]
    #
    # if posts_df.shape[0] == 0:
    #     return []

    # Сбор статистики постов
    cur_t = time.time()
    task = {'task_name': 'get_posts_stat',
            'args': {'posts': posts_df[[
                'social', 'owner_id', 'post_id']].to_dict('records')}}
    posts_stats = cass.call(task)
    posts_stats = pd.DataFrame(posts_stats)

    posts_stats = posts_stats.groupby(['social', 'owner_id', 'post_id'])[
        ['likes', 'shares', 'views', 'replies']].agg(get_stat).reset_index()

    posts_stats['likes_increase'] = posts_stats.likes.apply(
        lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
    posts_stats['shares_increase'] = posts_stats.shares.apply(
        lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
    posts_stats['views_increase'] = posts_stats.views.apply(
        lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
    posts_stats['replies_increase'] = posts_stats.replies.apply(
        lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)

    posts_stats['likes'] = posts_stats.likes.apply(lambda x: x[0])
    posts_stats['shares'] = posts_stats.shares.apply(lambda x: x[0])
    posts_stats['views'] = posts_stats.views.apply(lambda x: x[0])
    posts_stats['replies'] = posts_stats.replies.apply(lambda x: x[0])

    posts_df = pd.merge(posts_df, posts_stats, how="left",
                        on=['social', 'owner_id', 'post_id'])
    posts_df.likes_y.fillna(posts_df.likes_x, inplace=True)
    posts_df.shares_y.fillna(posts_df.shares_x, inplace=True)
    posts_df.views_y.fillna(posts_df.views_x, inplace=True)
    posts_df.replies_y.fillna(posts_df.replies_x, inplace=True)
    posts_df.likes_increase.fillna(0, inplace=True)
    posts_df.shares_increase.fillna(0, inplace=True)
    posts_df.views_increase.fillna(0, inplace=True)
    posts_df.replies_increase.fillna(0, inplace=True)

    posts_df.drop(columns=['likes_x', 'shares_x',
                           'views_x', 'replies_x'], inplace=True)
    posts_df.rename(columns={'likes_y': 'likes', 'shares_y': 'shares',
                             'views_y': 'views', 'replies_y': 'replies'},
                    inplace=True)
    LOGGER.info(f"Сбор статистики постов {time.time() - cur_t}")

    # Финальная сортировка и фильтрация

    if min_likes is not None:
        posts_df = posts_df[posts_df.likes >= min_likes]
    if min_shares is not None:
        posts_df = posts_df[posts_df.shares >= min_shares]
    if min_views is not None:
        posts_df = posts_df[posts_df.views >= min_views]
    if min_replies is not None:
        posts_df = posts_df[posts_df.replies >= min_replies]

    if posts_df.shape[0] == 0:
        return []

    posts_df.drop_duplicates(subset=['social', 'owner_id', 'post_id'],
                             inplace=True)

    cur_t = time.time()
    if filter_viewed:
        posts_df['view_filter'] = ~posts_df.is_viewed
        sorting = ['view_filter', 'is_topic_tracked']
    else:
        sorting = ['is_topic_tracked']
    sorting.extend(cond.get("sort") or ['date'])
    posts_df = posts_df.sort_values(
        by=sorting, ascending=False).iloc[:posts_count]
    LOGGER.info(f"Финальная сортировка и фильтрация {time.time() - cur_t}")

    # Получение названий тем
    cur_t = time.time()
    topics = {-1: ''}
    task = {'task_name': 'get_all_topics',
            'args': {}}
    all_topics = cass.call(task)
    for topic in all_topics:
        topics[topic['topic']] = topic['title']
    LOGGER.info(f"Получение названий тем {time.time() - cur_t}")

    # Сбор вложений
    cur_t = time.time()
    task = {'task_name': 'get_posts_attachments',
            'args': {'posts': posts_df[[
                'social', 'owner_id', 'post_id']].to_dict('records')}}
    posts_atts = cass.call(task)
    posts_atts = pd.DataFrame(posts_atts)

    if posts_atts.shape[0] > 0:
        posts_atts = posts_atts[posts_atts.type == 'image'].drop_duplicates(
            ['social', 'owner_id', 'post_id'])
        posts_atts = posts_atts[['social', 'owner_id', 'post_id', 'link']]
        posts_df = pd.merge(posts_df, posts_atts, how="left",
                            on=['social', 'owner_id', 'post_id'])
        posts_df.link.fillna("", inplace=True)
    else:
        posts_df['attachments'] = ""
    LOGGER.info(f"Сбор вложений {time.time() - cur_t}")

    # Составление дополнительных полей
    cur_t = time.time()
    posts_df['href'] = "/post?owner-id=" + posts_df.owner_id.astype(str) + \
                       "&post-id=" + posts_df.post_id.astype(str) + \
                       "&social=" + posts_df.social.astype(str)
    posts_df['group_href'] = get_group_href(posts_df.social, posts_df.owner_id)
    posts_df['post_href'] = get_post_href(
        posts_df.social, posts_df.owner_id, posts_df.post_id)
    posts_df['date'] = posts_df['date'].astype(str)
    # posts_df['topic_title'] = posts_df['topic'].apply(lambda x: topics[x])

    task = {'task_name': 'get_tracked_posts',
            'args': {'username': cond['username']}}
    tracked_posts = cass.call(task)
    if len(tracked_posts) == 0:
        posts_df['is_tracked'] = False
    else:
        tracked_posts = pd.DataFrame(tracked_posts)[
            ['social', 'owner_id', 'post_id']]
        tracked_posts['is_tracked'] = True
        posts_df = pd.merge(posts_df, tracked_posts, how="left", on=[
            'social', 'owner_id', 'post_id'])
        posts_df.is_tracked.fillna(False, inplace=True)

    task = {'task_name': 'get_traces',
            'args': {}}
    traces = cass.call(task)
    if len(traces) == 0:
        posts_df['has_trace'] = False
    else:
        traces = pd.DataFrame(traces)
        traces = traces[traces.status == "completed"]
        traces = traces[['social', 'owner_id', 'post_id']]
        traces['has_trace'] = True
        posts_df = pd.merge(posts_df, traces, how="left", on=[
            'social', 'owner_id', 'post_id'])
        posts_df.has_trace.fillna(False, inplace=True)

    posts_df.comments.fillna(0, inplace=True)
    posts_df.name.fillna("", inplace=True)
    posts_df.group_id.fillna('0', inplace=True)
    posts_df.is_closed.fillna(False, inplace=True)
    posts_df.main_city.fillna("", inplace=True)
    posts_df.main_prop.fillna("", inplace=True)
    posts_df.members_count.fillna("", inplace=True)
    posts_df.our_prop.fillna("", inplace=True)
    posts_df.post_per_day.fillna("", inplace=True)
    posts_df.views_per_post.fillna("", inplace=True)

    if "sentiment" not in posts_df.columns:
        posts_df['sentiment'] = 0
    posts_df.sentiment.fillna(0, inplace=True)

    LOGGER.info(f"Составление дополнительных полей {time.time() - cur_t}")

    # Сохранение просмотренных постов
    cur_t = time.time()
    new_viewed_posts = posts_df[~posts_df['is_viewed']].to_dict('records')
    if len(new_viewed_posts) > 0:
        task = {'task_name': 'insert_viewed_posts',
                'args': {'username': cond['username'],
                         'posts': new_viewed_posts}}
        cass.send_message(task)
    LOGGER.info(f"Сохранение просмотренных постов {time.time() - cur_t}")
    result = posts_df.to_dict('records')

    return result


def is_fts_viewed(username, fts):
    filters = {'username': username,
               'type': fts['type'],
               'social': fts['social'],
               'owner_id': fts['owner_id']}

    if fts['type'] == 'post':
        filters['post_id'] = fts['post_id']
        filters['add_id'] = ""
    elif fts['type'] == "topic_post":
        filters['post_id'] = fts['topic_id']
        filters['add_id'] = fts['post_id']
    elif fts['type'] == 'reply':
        filters['post_id'] = fts['post_id']
        filters['add_id'] = fts['reply_id']

    records = select_data(CassandraTables.users_fts, filters=filters) or []

    return len(records) > 0


def get_posts_by_rules(cond=None):
    sort_viewed = cond['sort_viewed']
    LOGGER.info(f"Sort: {sort_viewed}")
    username = cond.get('username', '')
    tag = cond.get('tag')
    drop_dupl = cond.get('drop_dupl')
    task = {'task_name': 'get_rules_values',
            'args': {'tag': tag}}
    words = cass.call(task)
    social = cond.get('social')
    if cond is not None and cond.get('start_date') is not None:
        start_posts_date = f"{cond['start_date']} {cond['start_time']}:00"
        end_posts_date = f"{cond['end_date']} {cond['end_time']}:00"
    else:
        start_posts_date = (
                datetime.now() - timedelta(days=1)
        ).strftime("%Y-%m-%d %H:%M:%S")
        end_posts_date = (
            datetime.now()).strftime("%Y-%m-%d %H:%M:%S")
    posts_date = f"date >= '{start_posts_date}' AND date <= '{end_posts_date}'"
    limit = cond['start_number']
    # Запрос по индексу
    task = {'el_search': True,
            'words': words[0]['value'],
            'date': [posts_date],
            'limit': limit,
            'social': social,
            'start_time': cond['start_time'],
            'start_date': cond['start_date'],
            'end_time': cond['end_time'],
            'end_date': cond['end_date']
            }
    index_posts = fts_service.call(task)
    # Запрос в базу
    task = {'task_name': 'get_posts_by_rule',
            'args': {'tag': tag,
                     'date': [posts_date],
                     'limit': limit
                     }}
    index_count = int(len(index_posts[0])) + int(len(index_posts[1])) + int(len(index_posts[2]))
    LOGGER.info(f'FOUND POSTS WITH ELASSANDRA - "{index_count}"')
    try:
        post_data, topic_post_data, replies_data, count = cass.call(task)
        LOGGER.info(f'FOUND POSTS WITH API - "{count}"')
        post_data = post_data + index_posts[0]
        replies_data = replies_data + index_posts[1]
        topic_post_data = topic_post_data + index_posts[2]
    except:
        post_data, topic_post_data, replies_data = index_posts[0], index_posts[2], index_posts[1]
        count = int(len(index_posts[0])) + int(len(index_posts[1])) + int(len(index_posts[2]))
    for post in post_data:
        post['type'] = 'post'
        if post['social'] == 'vk':
            post['post_href'] = 'https://vk.com/wall' + str(post['owner_id']) + \
                                '_' + str(post['post_id'])
        elif post['social'] == 'ok':
            post['post_href'] = 'https://ok.ru/group/' + str(post['owner_id']) + \
                                '/topic/' + str(post['post_id'])
        else:
            post['post_href'] = 'https://instagram.com/p/' + str(post['post_id'])

        post['href'] = "/post?owner-id=" + str(post['owner_id']) + \
                       "&post-id=" + str(post['post_id']) + \
                       "&social=" + str(post['social'])
        post['is_viewed'] = is_fts_viewed(username, post)
    for topic_post in topic_post_data:
        topic_post['type'] = 'topic_post'
        topic_post['add_id'] = topic_post['post_id']
        topic_post['post_type'] = 'topic_post'
        if topic_post['social'] == 'vk':
            topic_post['post_href'] = 'https://vk.com/topic' + str(topic_post['owner_id']) + \
                                      '_' + str(topic_post['topic_id']) + '?post=' + str(topic_post['post_id'])
        elif topic_post['social'] == 'ok':
            topic_post['post_href'] = 'https://ok.ru/group/' + str(topic_post['owner_id']) + \
                                      '/topic/' + str(topic_post['post_id'])
        else:
            topic_post['post_href'] = 'https://instagram.com/p/' + str(topic_post['post_id'])
        topic_post['is_viewed'] = is_fts_viewed(username, topic_post)
    for reply in replies_data:
        reply['type'] = 'reply'
        reply['add_id'] = reply['reply_id']
        reply['post_type'] = 'comment'
        if reply['social'] == 'vk':
            reply['post_href'] = 'https://vk.com/wall' + str(reply['owner_id']) + \
                                 '_' + str(reply['post_id']) + '?reply=' + str(reply['reply_id'])
        elif reply['social'] == 'ok':
            reply['post_href'] = 'https://ok.ru/group/' + str(reply['owner_id']) + \
                                 '/topic/' + str(reply['post_id'])
        else:
            reply['post_href'] = 'https://instagram.com/p/' + str(reply['post_id'])
        reply['is_viewed'] = is_fts_viewed(username, reply)
    data = post_data + topic_post_data + replies_data
    for post in data:
        if isinstance(post['date'], str):
            try:
                post['date'] = datetime.strptime(post['date'], '%Y-%m-%d %H:%M:%S')
            except:
                post['date'] = datetime.strptime(post['date'], '%Y-%m-%d %H:%M:%S.%f')
    data.sort(key=lambda k: k['date'], reverse=True)
    posts_df = pd.DataFrame(data)
    posts_df = posts_df.drop_duplicates(subset='date')
    if json.loads(drop_dupl):
        posts_df = posts_df.drop_duplicates(subset='text')
    if posts_df.empty:
        return []
    task = {'task_name': 'get_posts_attachments',
            'args': {'posts': posts_df[[
                'social', 'owner_id', 'post_id']].to_dict('records')}}
    posts_atts = cass.call(task)
    posts_atts = pd.DataFrame(posts_atts)

    if posts_atts.shape[0] > 0:
        posts_atts = posts_atts.drop_duplicates([
            'social', 'owner_id', 'post_id'])
        posts_atts = posts_atts[[
            'social', 'owner_id', 'post_id', 'link', 'show_link']]
        posts_df = pd.merge(posts_df, posts_atts, how="left",
                            on=['social', 'owner_id', 'post_id'])
        posts_df.link.fillna("", inplace=True)
    else:
        posts_df['attachments'] = ""

    if sort_viewed:
        posts_df.sort_values(
            ['is_viewed', 'date'],
            ascending=[True, False],
            inplace=True
        )

    data = posts_df.to_dict('records')

    # with open('1.pkl', 'ab') as file_handler:
    #     new_posts = []
    #     for post in data:
    #         if post['type'] == 'post':
    #             new_posts.append(post['text'])
    #     pickle.dump(new_posts, file_handler)
    # with open('2.pkl', 'ab') as file_handler:
    #     new_posts = []
    #     for post in data:
    #         if post['type'] == 'reply':
    #             new_posts.append(post['text'])
    #     pickle.dump(new_posts, file_handler)
    result = list()
    if int(len(data)) >= int(limit):
        total_count = (int(len(data)) - int(limit) + count)
    else:
        total_count = int(len(data))
    result.append(data[:int(limit)])
    result.append(total_count)

    return result


def get_post_groups(groups, socials):
    task = {'task_name': 'get_groups_info',
            'args': {'social': socials,
                     'groups': ','.join([f"'{group}'" for group in groups])}}
    groups_info = cass.call(task)
    groups_info = pd.DataFrame(groups_info)

    return groups_info


def get_atts(atts):
    att_info = {}
    att_types = set(atts)
    att_list = list(atts)
    for t in att_types:
        att_info[all_att_types[t]] = att_list.count(t)
    post_attachments = []
    for att, count in att_info.items():
        post_attachments.append(f'{att}: {count}')

    return post_attachments


def get_stat(stats):
    stats = [s if s is not None and not np.isnan(s) else 0 for s in stats]
    return tuple(stats)


def get_group_href(socials, owner_ids):
    supported_socials = ['vk', 'ok', 'inst']
    result = []
    for i in range(len(socials)):
        social = socials.iloc[i]
        if social not in supported_socials:
            result.append("")
            continue
        base = get_group_base(social)
        group = owner_ids.iloc[i]
        if social == "vk" and group.startswith('-'):
            group = group[1:]

        result.append(base + group)

    return pd.Series(result)


def get_href_for_group(socials, owner_ids):
    supported_socials = ['vk', 'ok', 'inst']
    result = []
    social = str(socials)
    owner_id = str(owner_ids)
    if social not in supported_socials:
        result.append("")
        return 0
    base = get_group_base(social)
    group = owner_id
    if social == "vk":
        group = owner_id[1:]

    result.append(base + group)

    return result


def get_post_href(socials, owner_ids, post_ids):
    result = []
    for i in range(len(socials)):
        social = socials.iloc[i]
        base = get_post_base(social)
        group = owner_ids.iloc[i]
        if social == "vk":
            between = "_"
        elif social == "ok":
            between = "/topic/"
        elif social == "inst":
            between = "p/"
        post = post_ids.iloc[i]
        if social == "inst":
            result.append(base + between + post)
        else:
            result.append(base + group + between + post)

    return result


def get_group_base(social):
    if social == "vk":
        return 'https://vk.com/club'
    elif social == "ok":
        return 'https://ok.ru/group/'
    elif social == "inst":
        return 'https://instagram.com/'


def get_post_base(social):
    if social == "vk":
        return 'https://vk.com/wall'
    elif social == "ok":
        return 'https://ok.ru/group/'
    elif social == "inst":
        return 'https://instagram.com/'


@route('/tracked-posts')
@show_ip_address
@time_it
def tracked_posts():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        # if get_params['username'] == 'test':
        #     return []
        result = []
        username = get_params['username']
        task = {'task_name': get_params['task_name'],
                'args': {'username': username}}
        posts = cass.call(task)
        if len(posts) == 0:
            return json.dumps(result, indent=4, sort_keys=True, default=str)
        posts_df = pd.DataFrame(posts)
        posts_df = posts_df[posts_df.social.isin(['vk', 'ok', 'inst'])]

        if posts_df.shape[0] == 0:
            return json.dumps(result, indent=4, sort_keys=True, default=str)

        # groups = set(posts_df.owner_id.values)
        # socials = list(set(posts_df.social.values))
        # groups_info = get_post_groups(groups, socials)

        groups = posts_df.copy()
        groups['group_id'] = groups.owner_id
        groups_info = get_groups_info(groups)
        groups_info = pd.DataFrame(groups_info)

        posts_df = pd.merge(posts_df, groups_info, left_on=[
            'social', 'owner_id'], right_on=['social', 'group_id'], how='left')

        posts_df['href'] = "/post?owner-id=" + \
                           posts_df.owner_id.astype(str) + \
                           "&post-id=" + posts_df.post_id.astype(str) + \
                           "&social=" + posts_df.social.astype(str)
        posts_df['group_href'] = get_group_href(
            posts_df.social, posts_df.owner_id)
        posts_df['post_href'] = get_post_href(
            posts_df.social, posts_df.owner_id, posts_df.post_id)

        task = {'task_name': 'get_posts',
                'args': {'posts': posts_df.to_dict('records')}}
        posts_info = cass.call(task)
        posts_info = pd.DataFrame(posts_info)
        posts_df = pd.merge(posts_df, posts_info,
                            left_on=['social', 'owner_id', 'post_id'],
                            right_on=['social', 'owner_id', 'post_id'],
                            how='left')
        posts_df['date'] = posts_df['date'].astype(str)

        topics = {-1: '', None: ''}
        task = {'task_name': 'get_all_topics',
                'args': {}}
        all_topics = cass.call(task)
        for topic in all_topics:
            topics[topic['topic']] = topic['title']
        posts_df['topic_title'] = posts_df['topic'].apply(lambda x: topics.get(x, ''))

        task = {'task_name': 'get_posts_stat',
                'args': {'posts': posts_df[[
                    'social', 'owner_id', 'post_id']].to_dict('records')}}
        posts_stats = cass.call(task)
        posts_stats = pd.DataFrame(posts_stats)

        if posts_stats.shape[0]:
            posts_stats = posts_stats.groupby(['social', 'owner_id', 'post_id'])[
                ['likes', 'shares', 'views', 'replies']].agg(get_stat).reset_index()

            posts_stats['likes_increase'] = posts_stats.likes.apply(
                lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
            posts_stats['shares_increase'] = posts_stats.shares.apply(
                lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
            posts_stats['views_increase'] = posts_stats.views.apply(
                lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
            posts_stats['replies_increase'] = posts_stats.replies.apply(
                lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)

            posts_stats['likes'] = posts_stats.likes.apply(lambda x: x[0])
            posts_stats['shares'] = posts_stats.shares.apply(lambda x: x[0])
            posts_stats['views'] = posts_stats.views.apply(lambda x: x[0])
            posts_stats['replies'] = posts_stats.replies.apply(lambda x: x[0])

            posts_df = pd.merge(posts_df, posts_stats, how="left", on=[
                'social', 'owner_id', 'post_id'])
            posts_df.likes_y.fillna(posts_df.likes_x, inplace=True)
            posts_df.shares_y.fillna(posts_df.shares_x, inplace=True)
            posts_df.views_y.fillna(posts_df.views_x, inplace=True)
            posts_df.replies_y.fillna(posts_df.replies_x, inplace=True)
            posts_df.likes_increase.fillna(0, inplace=True)
            posts_df.shares_increase.fillna(0, inplace=True)
            posts_df.views_increase.fillna(0, inplace=True)
            posts_df.replies_increase.fillna(0, inplace=True)

            posts_df.drop(columns=['likes_x', 'shares_x',
                                   'views_x', 'replies_x'], inplace=True)
            posts_df.rename(columns={'likes_y': 'likes', 'shares_y': 'shares',
                                     'views_y': 'views', 'replies_y': 'replies'},
                            inplace=True)
        else:
            posts_df['likes_increase'] = 0
            posts_df['shares_increase'] = 0
            posts_df['views_increase'] = 0
            posts_df['replies_increase'] = 0

        result = posts_df.to_dict('records')
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/check-post')
@show_ip_address
@time_it
def check_post():
    response.content_type = 'application/json'
    for i in range(0, 2):
        try:
            get_params = json.loads(request.query['params'])
            end_date = datetime.strptime(get_params['end_date'], "%Y-%m-%d")
            task = {'task_name': 'get_post',
                    'args': {'social': str(get_params['social']),
                             'owner_id': str(get_params['group_id']),
                             'post_id': str(get_params['post_id'])}}
            post = cass.call(task)
            if post is not None:
                LOGGER.info('POST ALREADY EXIST')
                state = True
                ttl = int((end_date - datetime.now()).total_seconds())
                task = {'task_name': 'set_posts_monitoring',
                        'args': {'username': get_params["username"],
                                 'social': get_params['social'],
                                 'owner_id': get_params["group_id"],
                                 'post_id': get_params["post_id"],
                                 'state': state,
                                 'ttl': ttl}}
                cass.send_message(task)
                task = {'task_name': 'set_tracked_state',
                        'args': {'username': get_params["username"],
                                 'social': get_params['social'],
                                 'owner_id': get_params["group_id"],
                                 'post_id': get_params["post_id"],
                                 'state': state,
                                 'end_date': end_date,
                                 'post_city': get_params['city'],
                                 'post_topic': get_params['topic']}}
                cass.send_message(task)
                result = 'ok'
            else:
                LOGGER.info("POST DOESN'T EXIST")
                task = {'task_name': 'post',
                        'args': {'social': 'vk',
                                 'owner_id': get_params["group_id"],
                                 'post_id': get_params["post_id"]}}
                parser = rpc_client('parser_tasks')
                result = parser.send(task)
                if result == 'ok':
                    continue
                else:
                    result = 'bad_link'
                    return json.dumps(result, indent=4, sort_keys=True, default=str)
        except Exception as ex:
            LOGGER.exception(str(ex))
            return HTTPResponse(status=400, body={'error': str(ex)})

        return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/traces')
@show_ip_address
@time_it
def traces():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        traces = get_traces(**get_params)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(traces, indent=4, sort_keys=True, default=str)


@route('/suggestions')
@show_ip_address
@time_it
def suggestions():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        suggestions = get_suggestions(**get_params)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(suggestions, indent=4, sort_keys=True, default=str)


@route('/suggestion')
@show_ip_address
@time_it
def suggestion():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])
        task = {'task_name': 'get_suggestion',
                'args': {"date": get_params['date']}}
        suggestion = cass.call(task)[0]
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(suggestion, indent=4, sort_keys=True, default=str)


@route('/suggestion-groups')
@show_ip_address
@time_it
def suggestion_groups():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])

        task = {'task_name': 'get_suggestion_groups',
                'args': {"date": get_params['date']}}
        suggestions = cass.call(task)

        suggestions_df = pd.DataFrame(suggestions)

        for s in suggestions:
            s['group_id'] = s['owner_id']

        # str_groups = [
        #     f"'{group}'" for group in suggestions_df.owner_id.values
        # ]
        # str_groups = ','.join(str_groups)
        # task = {'task_name': 'get_groups_info',
        #         'args': {'social': "vk",
        #                  'groups': str_groups}}
        # data = cass.call(task)
        data = get_groups_info(suggestions)

        groups_df = pd.DataFrame(data)
        groups_df['link'] = get_group_href(
            groups_df.social, groups_df.group_id)
        groups_df.rename(columns={"group_id": "owner_id"}, inplace=True)

        data = pd.merge(suggestions_df, groups_df,
                        how="left", on=['social', 'owner_id'])

        data = data.to_dict('records')
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(data, indent=4, sort_keys=True, default=str)


@route('/smm-report')
@show_ip_address
@time_it
def smm_report():
    response.content_type = 'application/json'
    try:
        LOGGER.info("SMM report")
        get_params = json.loads(request.query['params'])
        LOGGER.info(f"SMM report params: {get_params}")
        smm_report = RpcClient('report')

        report_type = get_params['report_type']
        args = get_params.copy()
        del args['report_type']

        task = {'report_type': report_type,
                'args': args}
        report = smm_report.call(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(report, indent=4, sort_keys=True, default=str)


@route('/report')
@show_ip_address
@time_it
def report():
    months = {1: "января",
              2: "февраля",
              3: "марта",
              4: "апреля",
              5: "мая",
              6: "июня",
              7: "июля",
              8: "августа",
              9: "сентября",
              10: "октября",
              11: "ноября",
              12: "декабря"}
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])

        result = {}
        if get_params.get('report_type') == "month":

            year, month = get_params['year'], get_params['month']
            user = get_params.get('user')
            days_in_month = monthrange(year, month)[1]
            weeks_count = 5 if month != 2 else 4

            # Информация по следам
            traces = get_traces()
            traces = pd.DataFrame(traces)
            traces.date = pd.to_datetime(traces['date'])

            if user != "all":
                traces = traces[traces.author == user]

            if traces.shape[0] > 0:
                traces['day'] = [t.day for t in traces['date']]
                traces['month'] = [t.month for t in traces['date']]
                traces['year'] = [t.year for t in traces['date']]

                traces = traces[(traces.year == year)
                                & (traces.month == month)]

                traces['week'] = traces.day.apply(define_week)
                grouped = traces.groupby(['week'])

                traces_count = grouped['day'].count()
                for i in range(1, weeks_count + 1):
                    if i not in traces_count.index:
                        traces_count[i] = 0
                traces_count = traces_count.sort_index()

                traces_by_date = traces.groupby(['day'])['post_id'].count()
                for i in range(1, days_in_month + 1):
                    if i not in traces_by_date.index:
                        traces_by_date[i] = 0
                traces_by_date = traces_by_date.sort_index()

                likes = grouped['likes'].sum()
                for i in range(1, weeks_count + 1):
                    if i not in likes.index:
                        likes[i] = 0
                likes = likes.sort_index()

                replies = grouped['replies'].sum()
                for i in range(1, weeks_count + 1):
                    if i not in replies.index:
                        replies[i] = 0
                replies = replies.sort_index()
                trace_involvement = likes + replies

                likes_by_date = traces.groupby(['day'])['likes'].sum()
                for i in range(1, days_in_month + 1):
                    if i not in likes_by_date.index:
                        likes_by_date[i] = 0
                likes_by_date = likes_by_date.sort_index()

                replies_by_date = traces.groupby(['day'])['replies'].sum()
                for i in range(1, days_in_month + 1):
                    if i not in replies_by_date.index:
                        replies_by_date[i] = 0
                replies_by_date = replies_by_date.sort_index()
                trace_inv_by_date = likes_by_date + replies_by_date
            else:
                traces_count = pd.Series([0] * weeks_count)
                trace_involvement = pd.Series([0] * weeks_count)
                traces_by_date = pd.Series([0] * days_in_month)
                trace_inv_by_date = pd.Series([0] * weeks_count)

            # Иформация по вбросам
            start_date = datetime(year=year,
                                  month=month,
                                  day=1)

            task = {'task_name': 'get_suggestions_by_date',
                    'args': {"date": start_date}}
            suggestions = cass.call(task)
            suggestions = pd.DataFrame(suggestions)
            if suggestions.shape[0] > 0:
                suggestions.date = pd.to_datetime(suggestions['date'])
                suggestions['month'] = [t.month for t in suggestions['date']]

                suggestions = suggestions[suggestions.month == month]
                if user != "all":
                    suggestions = suggestions[suggestions.author == user]

            if suggestions.shape[0] > 0:
                suggestions['day'] = [t.day for t in suggestions['date']]

                suggestions['week'] = suggestions.day.apply(define_week)
                grouped = suggestions.groupby(['week'])

                # suggestions_count = grouped['day'].count()
                # for i in range(1, weeks_count + 1):
                #     if i not in suggestions_count.index:
                #         suggestions_count[i] = 0
                # suggestions_count = suggestions_count.sort_index()
                suggestions_count = pd.Series([0] * weeks_count)

                suggestion_posts = pd.DataFrame()
                for suggestion in suggestions.iterrows():
                    task = {'task_name': 'get_suggestion_groups',
                            'args': {'date': suggestion[1]['date']}}
                    posts = cass.call(task)
                    for post in posts:
                        if post['social'] == 'vk':
                            post['owner_id'] = '-' + post['owner_id']
                    week = suggestion[1]['week'] - 1
                    suggestions_count[week] += len(posts)
                    posts = pd.DataFrame(posts)
                    posts = posts[posts.state == "posted"]

                    if posts.shape[0] > 0:
                        task = {'task_name': 'get_posts_stat',
                                'args': {'posts': posts[[
                                    'social',
                                    'owner_id',
                                    'post_id']].to_dict('records')}}
                        posts_stats = cass.call(task)
                        posts_stats = pd.DataFrame(posts_stats)

                        if posts_stats.shape[0]:
                            posts_stats = posts_stats.groupby(
                                ['social', 'owner_id', 'post_id'])[
                                ['likes', 'shares', 'views', 'replies']] \
                                .agg(get_stat).reset_index()

                            posts_stats['likes'] = posts_stats.likes.apply(
                                lambda x: x[0])
                            posts_stats['shares'] = posts_stats.shares.apply(
                                lambda x: x[0])
                            posts_stats['views'] = posts_stats.views.apply(
                                lambda x: x[0])
                            posts_stats['replies'] = posts_stats.replies.apply(
                                lambda x: x[0])
                            posts_stats['date'] = suggestion[1]['date']

                            suggestion_posts = pd.concat(
                                [suggestion_posts, posts_stats])

                sugg_by_date = suggestions.groupby(['day'])['week'].count()
                for i in range(1, days_in_month + 1):
                    if i not in sugg_by_date.index:
                        sugg_by_date[i] = 0
                sugg_by_date = sugg_by_date.sort_index()

                if suggestion_posts.shape[0] > 0:
                    suggestion_posts['day'] = [
                        t.day for t in suggestion_posts['date']]
                    suggestion_posts['week'] = suggestion_posts.day.apply(define_week)
                    grouped = suggestion_posts.groupby(['week'])

                    likes = grouped['likes'].sum()
                    for i in range(1, weeks_count + 1):
                        if i not in likes.index:
                            likes[i] = 0
                    likes = likes.sort_index()

                    replies = grouped['replies'].sum()
                    for i in range(1, weeks_count + 1):
                        if i not in replies.index:
                            replies[i] = 0
                    replies = replies.sort_index()

                    suggestion_involvement = likes + replies

                    likes = suggestion_posts.groupby(['day'])['likes'].sum()
                    for i in range(1, days_in_month + 1):
                        if i not in likes.index:
                            likes[i] = 0
                    likes = likes.sort_index()

                    replies = suggestion_posts.groupby(['day'])['replies'].sum()
                    for i in range(1, days_in_month + 1):
                        if i not in replies.index:
                            replies[i] = 0
                    replies = replies.sort_index()

                    suggestion_inv_by_date = likes + replies

                    views = grouped['views'].sum()
                    for i in range(1, weeks_count + 1):
                        if i not in views.index:
                            views[i] = 0
                    views = views.sort_index()

                    views_by_date = suggestion_posts \
                        .groupby(['day'])['views'].sum()
                    for i in range(1, days_in_month + 1):
                        if i not in views_by_date.index:
                            views_by_date[i] = 0
                    views_by_date = views_by_date.sort_index()
                else:
                    views = pd.Series([0] * weeks_count)
                    suggestion_involvement = pd.Series([0] * weeks_count)
                    views_by_date = pd.Series([0] * days_in_month)
                    suggestion_inv_by_date = pd.Series([0] * days_in_month)

            else:
                suggestions_count = pd.Series([0] * weeks_count)
                views = pd.Series([0] * weeks_count)
                suggestion_involvement = pd.Series([0] * weeks_count)
                sugg_by_date = pd.Series([0] * days_in_month)
                views_by_date = pd.Series([0] * days_in_month)
                suggestion_inv_by_date = pd.Series([0] * days_in_month)

            involvement = trace_involvement.reset_index(drop=True) + \
                suggestion_involvement.reset_index(drop=True)

            inv_by_date = trace_inv_by_date.reset_index(drop=True) + \
                suggestion_inv_by_date.reset_index(drop=True)

            labels = defaultdict(list)
            for day in range(1, days_in_month + 1):
                week = define_week(day)
                labels[week].append(day)

            traces_by_date_range = []
            for i in range(1, days_in_month + 1):
                cur_day = i if i > 9 else f"0{i}"
                cur_month = month if month > 9 else f"0{month}"
                label = f"{year}-{cur_month}-{cur_day}"
                traces_by_date_range.append(label)

            week_labels = []
            month = months[month]
            for days in labels.values():
                label = f"{days[0]}-{days[-1]} {month}"
                week_labels.append(label)

            result['traces_count'] = list(traces_count.fillna(0))
            result['traces_sum'] = traces_count.sum()
            result['traces_by_date'] = list(traces_by_date)
            result['traces_by_date_range'] = traces_by_date_range
            result['involvement'] = list(involvement.fillna(0))
            result['involvement_sum'] = involvement.sum()
            result['inv_by_date'] = list(inv_by_date.fillna(0))
            result['suggestions_count'] = list(suggestions_count.fillna(0))
            result['suggestions_sum'] = suggestions_count.sum()
            result['sugg_by_date'] = list(sugg_by_date)
            result['views'] = list(views.fillna(0))
            result['views_sum'] = views.sum()
            result['views_by_date'] = list(views_by_date)
            result['week_labels'] = week_labels
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


def define_week(day):
    return ((day - 1) // 7) + 1


def get_traces(user=None, start_date=None, end_date=None):
    if start_date is None and end_date is None:
        task = {'task_name': 'get_traces',
                'args': {}}
    else:
        args = {}
        if start_date is not None:
            args['date'] = start_date + " 00:00:01"
        if end_date is not None:
            args['end_date'] = end_date + " 23:59:59"
        task = {'task_name': 'get_traces_by_date',
                'args': args}
    traces = cass.call(task)
    if len(traces):
        traces = pd.DataFrame(traces).sort_values(by='date', ascending=False)
        traces = traces[traces.social != 'test']
        if user is not None and user != 'all':
            traces = traces[traces.author == user]
        traces['post'] = get_post_href(
            traces.social, traces.owner_id, traces.post_id)
        traces = traces.to_dict('records')

        for trace in traces:
            if trace.get('reply_id') is not None and trace.get('reply_id') != "":
                task = {'task_name': 'get_reply',
                        'args': {'social': trace['social'],
                                 'owner_id': trace['owner_id'],
                                 'post_id': trace['post_id'],
                                 'reply_id': trace['reply_id']}}
                trace_stat = cass.call(task)
                trace['likes'] = trace_stat.get('likes') or 0
                trace['replies'] = trace_stat.get('replies') or 0
            else:
                trace['likes'] = 0
                trace['replies'] = 0

    return traces


def get_suggestions(user=None, start_date=None, end_date=None):
    if start_date is None and end_date is None:
        task = {'task_name': 'get_suggestions',
                'args': {}}
    else:
        args = {}
        if start_date is not None:
            args['date'] = start_date + " 00:00:01"
        if end_date is not None:
            args['end_date'] = end_date + " 23:59:59"
        task = {'task_name': 'get_suggestions_by_date',
                'args': args}
    suggestions = cass.call(task)
    if len(suggestions):
        suggestions = pd.DataFrame(suggestions) \
            .sort_values(by='date', ascending=False)
        if user is not None and user != 'all':
            suggestions = suggestions[suggestions.author == user]
        suggestions = suggestions.to_dict('records')

        for suggestion in suggestions:
            info = get_suggestion_info(suggestion['date'])
            suggestion.update(info)

    return suggestions


def get_suggestion_info(date):
    task = {'task_name': 'get_suggestion_groups',
            'args': {'date': str(date)}}
    suggestion_groups = cass.call(task)

    if not len(suggestion_groups):
        return

    posted_sugg = list(filter(lambda x: x['state'] == "posted",
                              suggestion_groups))

    posted_count = f"{len(posted_sugg)}/{len(suggestion_groups)}"

    likes = 0
    views = 0
    replies = 0
    shares = 0
    if len(posted_sugg):
        for post in posted_sugg:
            task = {'task_name': 'get_post_stat',
                    'args': {'social': post['social'],
                             'owner_id': post['owner_id'],
                             'post_id': post['post_id'],
                             'count': 1}}
            cur_post = cass.call(task)
            if len(cur_post):
                likes += cur_post[0]['likes']
                views += cur_post[0]['views']
                replies += cur_post[0]['replies']
                shares += cur_post[0]['shares']
            else:
                LOGGER.info(f"no stat for {post}")

    result = {'likes': likes,
              'views': views,
              'replies': replies,
              'shares': shares,
              'posted_count': posted_count}

    return result


@route('/trends')
@show_ip_address
@time_it
def trends():
    response.content_type = 'application/json'
    try:
        get_params = json.loads(request.query['params'])

        topics_states = get_params['topics_states']
        hidden_topics = []
        for topic, state in topics_states.items():
            if state == -1:
                hidden_topics.append(topic)

        LOGGER.info(get_params)
        result = {}

        period = get_params.get('period') or "day"

        if period == "hour":
            posts_date = (datetime.now() - timedelta(seconds=60)
                          ).strftime("%Y-%m-%d %H:%M:%S")
        elif period == "day":
            posts_date = (datetime.now() - timedelta(days=1)
                          ).strftime("%Y-%m-%d %H:%M:%S")
        elif period == "week":
            posts_date = (datetime.now() - timedelta(days=7)
                          ).strftime("%Y-%m-%d %H:%M:%S")
        cond = [f"date >= '{posts_date}'"]
        task = {'task_name': 'get_last_posts',
                'args': {'date': cond}}
        posts = cass.call(task)

        LOGGER.info(f"POSTS RECEIVED. COUNT: {len(posts)}")
        posts = pd.DataFrame(posts)

        posts = posts[~posts.topic.isin(hidden_topics)]

        LOGGER.info(posts.columns)

        task = {'task_name': 'get_posts_topics',
                'args': {'posts': posts[[
                    'social', 'owner_id', 'post_id']].to_dict('records')}}
        post_topics = cass.call(task)
        post_topics = pd.DataFrame(post_topics)

        posts.drop(columns=['topic'], inplace=True)
        posts = pd.merge(posts, post_topics, how="left",
                         on=['owner_id', 'post_id'])
        posts.topic.fillna(-1, inplace=True)
        posts.topic = posts.topic.astype(np.int32)

        LOGGER.info("TOPIC APPLIED")

        if get_params.get('my_groups'):
            hidden_groups = []
            task = {'task_name': 'get_user_groups',
                    'args': {'username': get_params['username']}}
            user_groups = cass.call(task)
            for group in user_groups:
                if group['state'] == -1:
                    hidden_groups.append(group['group_id'])

            posts = posts[~posts.owner_id.isin(hidden_groups)]

        LOGGER.info("GROUPS HIDE")

        result['topics_distribution'] = get_topics_distribution(posts)
        LOGGER.info("TOPIC DISTR REVC")
        top_words = get_popular_words(posts)
        LOGGER.info("TOP WORDS RECV")
        result['top_words'] = top_words

    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


def get_topics_distribution(posts):
    task = {'task_name': 'get_all_topics',
            'args': {}}
    topics = cass.call(task)
    topics.append(
        {'topic': -1, 'title': '!! Тема не определена !!', 'level': 1})
    topics = pd.DataFrame(topics)

    posts = pd.merge(posts, topics, how='left', on='topic')

    topics_distr = get_topics_distr(posts[['owner_id', 'title']])

    return topics_distr


def get_popular_words(posts, count=10):
    texts = list(posts.text)
    texts = [text for text in texts if len(text) > 0]

    result = get_top_words(texts, count)

    return result


def get_topics_distr(topics):
    result = topics.groupby(['title']).count().reset_index().sort_values(
        by=['owner_id', 'title'], ascending=[False, True])
    result.columns = ['topic', 'count']
    result = result.to_dict('records')

    return result


@route('/add-task', method='POST')
@show_ip_address
@time_it
def add_task():
    response.content_type = 'application/json'
    try:
        data = request.json
        groups = data["groups"]
        social = data['social']
        is_keyword = data['is_keyword']
        parser_task = {'date': datetime.now(),
                       'groups_count': len(groups),
                       'label': data['label'],
                       'author': data['username'],
                       'social': social,
                       'is_keyword': is_keyword}
        task = {'task_name': 'insert_task',
                'args': {'task': parser_task}}
        task_id = cass.call(task)
        if is_keyword:
            task = {'task_name': 'insert_task_keywords',
                    'args': {'task_id': task_id,
                             'keywords': groups,
                             'social': social}}
        else:
            task = {'task_name': 'insert_task_groups',
                    'args': {'task_id': task_id,
                             'groups': groups,
                             'social': social}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/add-rule', method='POST')
@show_ip_address
@time_it
def add_rule():
    response.content_type = 'application/json'
    try:
        param = request.json
        is_active = True
        init_rule = True
        task = {'task_name': 'insert_rules',
                'args': {'rule': param['label'],
                         'words': param['words'],
                         'is_active': is_active,
                         'social': param['social']}}
        cass.send_message(task)
        task = {'start_time': param['start_time'],
                'end_time': param['end_time'],
                'start_date': param['start_date'],
                'end_date': param['end_date'],
                'words': param['words'],
                'init_rule': init_rule,
                'rule': param['label'],
                'social': param['social']}
        fts_service.call(task)
        LOGGER.info('SUCCESS INIT RULE')
        result = {'ok': 'okay'}
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/add-topic', method='GET')
@show_ip_address
@time_it
def add_topic():
    response.content_type = 'application/json'
    try:
        param = json.loads(request.query['params'])
        task = {'task_name': 'vectorize_text',
                'args': {'text': param['words']}}
        topic = rel_model.call(task)
        task = {'task_name': 'insert_topics_vector',
                'args': {'words': param['words'], 'vector': topic, 'threshold': param['threshold']}}
        cass.send_message(task)
        result = {'ok': 'ok'}
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/track-topic', method='GET')
@show_ip_address
@time_it
def track_topic():
    response.content_type = 'application/json'
    try:
        param = json.loads(request.query['params'])
        task = {'task_name': 'set_tracked_rules',
                'args': {'username': param['username'], 'tag': param['tag'], 'social': param['social']}}
        cass_response = cass.call(task)
        task = {
            'track_rule': True
        }
        fts_service.call(task)
        if cass_response:
            result = 'ok'
        else:
            result = 'ERROR WHILE SET TRACKED RULES'
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/untrack-topic', method='GET')
@show_ip_address
@time_it
def untrack_topic():
    response.content_type = 'application/json'
    try:
        param = json.loads(request.query['params'])
        task = {'task_name': 'set_tracked_rules',
                'args': {'username': param['username'], 'tag': param['tag'], 'social': param['social'], 'state': False}}
        cass_response = cass.call(task)
        task = {
            'track_rule': True
        }
        fts_service.call(task)
        if cass_response:
            result = 'ok'
        else:
            result = 'ERROR WHILE DELETE TRACKED RULES'
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/check-topic', method='GET')
@show_ip_address
@time_it
def check_topic():
    response.content_type = 'application/json'
    try:
        param = json.loads(request.query['params'])
        task = {'task_name': 'check_words',
                'args': {'text': param['words']}}
        ignore_words = rel_model.call(task)
        result = {'ignore_words': ignore_words}
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/add-trace-template', method='PUT')
@show_ip_address
@time_it
def add_trace_template():
    response.content_type = 'application/json'
    try:
        data = request.json
        task = {'task_name': 'save_new_trace',
                'args': {'level': data["level"],
                         'topic': data["topic"],
                         'text': data["new_trace"]}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/save-image', method='PUT')
@show_ip_address
@time_it
def save_image():
    response.content_type = 'application/json'
    try:
        upload = request.files.media
        LOGGER.info(upload)
        if upload != "":
            LOGGER.info(upload.filename)
            name, ext = os.path.splitext(upload.filename)
            save_path = os.path.join(SRC_DIR, 'data', 'images')
            upload.save(save_path, overwrite=True)

        return HTTPResponse(status=200, body={'suggestion': 'in progress'})
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/add-suggestion', method='PUT')
@show_ip_address
@time_it
def add_suggestion():
    response.content_type = 'application/json'
    try:
        data = request.json

        args = {'groups': data['groups'],
                'trace': data['trace'],
                'login': data['login'],
                'password': data['password'],
                'author': data['author'],
                'title': data['title'],
                'filename': data.get('filename')}
        task = {'task_name': 'add_suggestion',
                'args': {'data': args}}
        LOGGER.info(f"new suggestion: {task}")
        traces_mq.send_message(task)

        return HTTPResponse(status=200, body={'suggestion': 'in progress'})
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/add-trace', method='PUT')
@show_ip_address
@time_it
def add_trace():
    response.content_type = 'application/json'
    try:
        data = request.json

        args = {'social': data['social'],
                'owner_id': data['owner_id'],
                'post_id': data['post_id'],
                'trace': data['trace'],
                'login': data['login'],
                'password': data['password'],
                'author': data['author'],
                'filename': data.get('filename')}

        task = {'task_name': 'add_comment_trace',
                'args': {'data': args}}

        LOGGER.info(f"new trace: {task}")
        traces_mq.send_message(task)

        return HTTPResponse(status=200, body={'trace': 'in progress'})
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/add-bot', method='PUT')
@show_ip_address
@time_it
def add_bot():
    response.content_type = 'application/json'
    try:
        data = request.json
        task = {'task_name': 'insert_bot',
                'args': {'bot': data}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/change-task-state', method='POST')
@show_ip_address
@time_it
def change_task_state():
    response.content_type = 'application/json'
    try:
        data = request.json
        task_id, state = data["task_id"], data["state"]
        task = {'task_name': 'change_task_state',
                'args': {'task_id': task_id, 'state': state}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/change-user-group-state', method='POST')
@show_ip_address
@time_it
def change_user_group_state():
    response.content_type = 'application/json'
    try:
        data = request.json
        task = {'task_name': 'set_user_group_state',
                'args': {'social': data['social'],
                         'username': data["username"],
                         'group_id': data["group_id"],
                         'state': data["state"]}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/change-post-track-state', method='POST')
@show_ip_address
@time_it
def change_post_track_state():
    response.content_type = 'application/json'
    try:
        data = request.json
        task = {'task_name': 'set_tracked_state',
                'args': {'username': data["username"],
                         'social': data['social'],
                         'owner_id': data["owner_id"],
                         'post_id': data["post_id"],
                         'state': data["state"]}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/change-post-monitoring-state', method='POST')
@show_ip_address
@time_it
def change_post_monitoring_state():
    response.content_type = 'application/json'
    try:
        data = request.json
        task = {'task_name': 'set_posts_monitoring',
                'args': {'username': data["username"],
                         'social': data['social'],
                         'owner_id': data["owner_id"],
                         'post_id': data["post_id"],
                         'state': data["state"]}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/change-bot-comment', method='POST')
@show_ip_address
@time_it
def change_bot_comment():
    response.content_type = 'application/json'
    try:
        data = request.json
        login, comment = data["login"], data["comment"]
        task = {'task_name': 'change_bot_comment',
                'args': {'social': data['social'],
                         'login': login,
                         'comment': comment}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/change-bot-template', method='POST')
@show_ip_address
@time_it
def change_bot_comment():
    response.content_type = 'application/json'
    try:
        LOGGER.info('----- change-bot-template -----')
        data = request.json
        template_profil = data['template_profil']
        social = data['social']
        login = data["login"]
        password = data["password"]

        task = {'task_name': 'change_bot_template',
                'args': {'social': social,
                         'login': login,
                         'template_profil': '',
                         'status_template': 'Запрос "{0}" обрабатывается'.format(template_profil)}}
        cass.send_message(task)

        task = {'task_name': 'apply_template_to_account',
                'args': {'social': social,
                         'login': login,
                         'password': password,
                         'template_profil': template_profil}}
        bots.send_message(task)

        # LOGGER.info(result)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})

    # return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/drop-task', method='POST')
@show_ip_address
@time_it
def drop_task():
    response.content_type = 'application/json'
    try:
        data = request.json
        task_id = data["task_id"]
        task = {'task_name': 'drop_task_and_groups',
                'args': {'task_id': task_id}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/drop-rule', method='POST')
@show_ip_address
@time_it
def drop_rule():
    response.content_type = 'application/json'
    try:
        data = request.json
        tag = data["tag"]
        social = data["social"]
        task = {'task_name': 'drop_rule',
                'args': {'tag': tag, 'social': social}}
        cass.send_message(task)
        task = {'task_name': 'drop_posts_rules',
                'args': {'tag': tag}}
        cass.send_message(task)
        task = {'task_name': 'get_tracked_rules',
                'args': {}}
        tracked_rules = cass.call(task)
        for rule in tracked_rules:
            if str(tag) == str(rule['tag']):
                task = {'task_name': 'set_tracked_rules',
                        'args': {'tag': tag, 'username': rule['user'], 'social': rule['social'], "state": False}}
                cass.send_message(task)
        task = {}
        fts_service.call(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/drop-topic', method='POST')
@show_ip_address
@time_it
def drop_topic():
    response.content_type = 'application/json'
    try:
        data = request.json
        words = data["words"]
        task = {'task_name': 'drop_topic',
                'args': {'words': words}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


@route('/drop-bots', method='POST')
@show_ip_address
@time_it
def drop_bots():
    response.content_type = 'application/json'
    try:
        data = request.json
        bots = data["bots"]
        socials = data['socials']
        task = {'task_name': 'drop_bots',
                'args': {'socials': socials,
                         'logins': bots}}
        cass.send_message(task)
    except Exception as ex:
        LOGGER.exception(str(ex))
        return HTTPResponse(status=400, body={'error': str(ex)})


class TraceValidationException(Exception):
    pass


if __name__ == '__main__':

    API_VERSION = CONFIG['info']['api_version']
    NUM_WORKERS = CONFIG['build']['workers']
    DEBUG = CONFIG['build']['debug']

    LOGGER.info(
        f"SERVICE CONFIG: API VERSION: {API_VERSION} DEBUG MODE: {DEBUG}")
    kwargs = {
        'host': '0.0.0.0',
        'port': 8090,
        'reload': True,
        'debug': DEBUG
    }
    if not DEBUG:
        kwargs['server'] = 'gunicorn'
        kwargs['workers'] = NUM_WORKERS

    run(**kwargs)
