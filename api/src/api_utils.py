import nltk
import pymystem3
import numpy as np
import re

from common_lib.utilities import load_pickle


def set_stop_words():
    nltk.download(['stopwords', 'punkt'])

    stop_words = []
    stop_words.extend(nltk.corpus.stopwords.words('russian'))
    stop_words.extend(nltk.corpus.stopwords.words('english'))
    stop_words.extend(['свой', 'который', 'наш', 'ваш',
                               'это', 'мочь', 'весь'])
    return stop_words


def get_top_words(texts, count=10):
    model_path = "data/trends/trends.pkl"

    vectorizer = load_pickle(model_path, verbose=False)
    reversed_vocab = {}
    for word, w_index in vectorizer.vocabulary_.items():
        reversed_vocab[w_index] = word

    lemm_texts = []
    mystem = pymystem3.Mystem()
    for text in texts:
        lemm_t = mystem.lemmatize(text)
        lemm_t = " ".join(re.findall(r"[a-zA-Zа-яА-Я\-]+", ' '.join(lemm_t)))

        lemm_texts.append(lemm_t)

    new_doc = ' '.join(lemm_texts)
    trends = vectorizer.transform([new_doc])
    for le, ri in zip(trends.indptr[:-1], trends.indptr[1:]):
        top_n_idx = trends.indices[le + np.argpartition(
            trends.data[le:ri], -count)[-count:]]

    top_words = []
    for ind in top_n_idx:
        top_words.append(reversed_vocab[ind])

    return top_words
