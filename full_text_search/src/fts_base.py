import logging

from common_lib.queue_manager import RpcClient

LOGGER = logging.getLogger('Full_text_search')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
fh = logging.FileHandler('logs/logs.log', mode='w')
fh.setFormatter(formatter)
LOGGER.addHandler(fh)
sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)


class FTSManager:
    def __init__(self):
        self.LOGGER = LOGGER
        self.cass = RpcClient('db_tasks')

    def space_destroyer(self, words):
        new_words = words.split('|')
        for i in range(len(new_words)):
            if new_words[i][-1:] == ' ':
                new_word = new_words[i][:-1]
                new_words[i] = new_word
            if new_words[i][:1] == ' ':
                new_word = new_words[i][1:]
                new_words[i] = new_word
        return new_words
