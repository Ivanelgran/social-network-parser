import time
import pymorphy2
import nltk
import string
import vk_api

from datetime import datetime
from fts_base import FTSManager
from nltk.corpus import stopwords


nltk.download('punkt')
nltk.download('stopwords')


login, password = '+79084930336', 'qazwsxedc123'
token = "c31fed1ec31fed1ec31fed1e9fc375389dcc31fc31fed1e9ff1a58a7ee64e8d02d4f154"


class VkApiHandler(FTSManager):

    def __init__(self):
        super().__init__()
        self.morph = pymorphy2.MorphAnalyzer()
        self.vk = self.vk_authorization()

    def vk_authorization(self):
        vk_session = vk_api.VkApi(token=token,
                                  captcha_handler=self.captcha_handler)
        try:
            self.session_auth = True
            vk = vk_session.get_api()
        except vk_api.AuthError as error_msg:
            self.LOGGER.warning(error_msg)
            self.LOGGER.warning('AUTHORIZATION ERROR')
            self.session_auth = False
            vk = None
        self.LOGGER.info('FTS SERVICE IS STARTING')

        return vk

    def captcha_handler(self, captcha):
        """ При возникновении капчи вызывается эта функция и ей передается объект
            капчи. Через метод get_url можно получить ссылку на изображение.
            Через метод try_again можно попытаться отправить запрос с кодом капчи
        """

        key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()

        return captcha.try_again(key)

    def ini_posts_by_rule(self, rule_data):
        self.LOGGER.info('START PARSE POSTS FROM API')
        rule_start_time = rule_data['start_date'] + ' ' + rule_data['start_time']
        rule_end_time = rule_data['end_date'] + ' ' + rule_data['end_time']
        start_time = int(time.mktime(time.strptime(rule_start_time, '%Y-%m-%d %H:%M')))
        end_time = int(time.mktime(time.strptime(rule_end_time, '%Y-%m-%d %H:%M')))
        words = self.space_destroyer(rule_data['words'])
        for word in words:
            word = str(word).replace('"', '')
            rule = rule_data['rule']
            social = 'vk'
            result = self.get_posts_by_api(word, rule, social, start_time=start_time, end_time=end_time)
            if result:
                self.LOGGER.info('SUCCESS GETTING POSTS')
            else:
                self.LOGGER.info(f'ERROR WHILE GETTING POSTS FOR TAG {rule} WITH WORDS {word}')

    def bg_api_search(self):
        while True:
            time.sleep(1800)
            try:
                self.LOGGER.info('START BG API SEARCH')
                task = {'task_name': 'get_active_rules',
                        'args': {}}
                api_rules = self.cass.call(task)
                if api_rules is not None:
                    api_rules[:] = [d for d in api_rules if 'vk' in d['social']]
                    for api_rule in api_rules:
                        rule = api_rule['tag']
                        self.LOGGER.info(f"GETTING POST FOR RULE: {rule}")
                        social = 'vk'
                        words = self.space_destroyer(api_rule['value'])
                        for word in words:
                            word = str(word).replace('"', '')
                            result = self.get_posts_by_api(word, rule, social)
                            if result:
                                self.LOGGER.info(f"SUCCESS GETTING POST FOR RULE: {rule}")
                            else:
                                self.LOGGER.info(f"ERROR WHILE GETTING POST FOR RULE: {rule}")
                    self.LOGGER.info('FINISH GETTING POST FROM BG API')

            except Exception as e:
                self.LOGGER.info('Ошибка при создании фонового процесса VK API')
                self.LOGGER.warning(e)

    def tokenize_text(self, file_text):
        # firstly let's apply nltk tokenization
        tokens = nltk.word_tokenize(file_text)

        # let's delete punctuation symbols
        tokens = [i for i in tokens if (i not in string.punctuation)]

        # deleting stop_words
        stop_words = stopwords.words('russian')
        stop_words.extend(['что', 'это', 'так', 'вот', 'быть',
                           'как', 'в', '—', 'к', 'на'])
        tokens = [i for i in tokens if (i not in stop_words)]

        # cleaning words
        tokens = [i.replace("«", "").replace("»", "") for i in tokens]

        return tokens

    def lemmatize_text(self, text, with_pos=False):
        for i in range(len(text)):
            p = self.morph.parse(text[i])[0]
            if with_pos:
                text[i] = "{0}_{1}".format(p.normal_form, p.tag.POS)
            else:
                text[i] = p.normal_form
        return text

    def insert_init_posts(self, data, rule, social, word):
        self.LOGGER.info('PARSE POSTS FROM API IN PROGRESS')
        init_post_data = dict()
        new_word = self.tokenize_text(word)
        new_word = self.lemmatize_text(new_word)
        rules_data = dict()
        rules_list = list()
        init_reply_list = list()
        init_attach_data = dict()
        init_posts_list = list()
        init_attach_list = list()
        try:
            for post in data['items']:
                text = str(post['text'])
                text = text.replace('#', ' #')
                post['text'] = " ".join(x for x in text.split() if not x.startswith(("http", "https") or len(x) > 30))
                init_post_data['social'] = str(social)
                init_post_data['rule'] = str(rule)
                if post['post_type'] == 'post':
                    init_post_data['owner_id'] = str(post['owner_id'])
                    init_post_data['post_id'] = str(post['id'])
                    init_post_data['copy_owner_id'] = str(post['owner_id'])
                    init_post_data['copy_post_id'] = str(post['id'])
                    init_post_data['date'] = datetime.fromtimestamp(int(post['date']))
                    init_post_data['likes'] = int(post['likes']['count'])
                    init_post_data['post_type'] = str(post['post_type'])
                    init_post_data['type'] = str(post['post_type'])
                    init_post_data['replies'] = int(post['comments']['count'])
                    init_post_data['shares'] = int(post['reposts']['count'])
                    init_post_data['text'] = str(post['text'])
                    text = str(post['text'])
                    new_text = self.tokenize_text(text)
                    new_text = self.lemmatize_text(new_text)
                    crossing = list(set(new_text) & set(new_word))
                    if crossing:
                        pass
                    else:
                        continue
                    try:
                        init_post_data['views'] = int(post['views']['count'])
                    except:
                        init_post_data['views'] = 0
                elif post['post_type'] == 'reply':
                    init_post_data['owner_id'] = str(post['owner_id'])
                    init_post_data['post_id'] = str(post['post_id'])
                    init_post_data['reply_id'] = str(post['id'])
                    init_post_data['parent_reply'] = ''
                    init_post_data['author'] = str(post['from_id'])
                    init_post_data['date'] = datetime.fromtimestamp(int(post['date']))
                    init_post_data['likes'] = int(post['likes']['count'])
                    init_post_data['replies'] = int(post['reposts']['count'])
                    init_post_data['sticker'] = ''
                    init_post_data['text'] = str(post['text'])
                    init_post_data['sentiment'] = 0
                else:
                    self.LOGGER.warning('Не могу определить тип поста из API')
                    self.LOGGER.info(post)

                rules_data['rule'] = str(rule)
                rules_data['social'] = str(social)
                rules_data['owner_id'] = str(init_post_data['owner_id'])
                if post['post_type'] == 'reply':
                    rules_data['post_id'] = str(post['post_id'])
                    rules_data['reply_id'] = str(post['id'])
                else:
                    rules_data['post_id'] = str(init_post_data['post_id'])
                    rules_data['reply_id'] = ''
                rules_data['date'] = datetime.fromtimestamp(int(post['date']))
                rules_data['type'] = str(post['post_type'])
                q = rules_data.copy()
                rules_list.append(q)

                if post['post_type'] == 'post':
                    d = init_post_data.copy()
                    init_posts_list.append(d)
                elif post['post_type'] == 'reply':
                    d = init_post_data.copy()
                    init_reply_list.append(d)
                else:
                    self.LOGGER.warning('Не могу сохранить пост из API')
                try:
                    k = 0
                    if post.get('attachments') is not None:
                        for attachment in post['attachments']:
                            k = k + 1
                            init_attach_data['social'] = social
                            if post['post_type'] == 'post':
                                init_attach_data['owner_id'] = str(post['owner_id'])
                                init_attach_data['post_id'] = str(post['id'])
                            elif post['post_type'] == 'reply':
                                init_attach_data['owner_id'] = str(post['owner_id'])
                                init_attach_data['post_id'] = str(post['post_id'])
                            else:
                                self.LOGGER.warning('Не могу определить тип поста из API')

                            if attachment['type'] == 'photo':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['photo']['text'])
                                init_attach_data['link'] = str(attachment['photo']['sizes'][0]['url'])
                                init_attach_data['show_link'] = str(attachment['photo']['sizes'][0]['url'])
                            elif attachment['type'] == 'doc':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['doc']['title'])
                                init_attach_data['link'] = str(attachment['doc']['url'])
                                init_attach_data['show_link'] = 'Гифка'
                            elif attachment['type'] == 'audio':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['audio']['title'])
                                init_attach_data['link'] = str(attachment['audio']['url'])
                                init_attach_data['show_link'] = 'Аудио'
                            elif attachment['type'] == 'video':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                link = 'https://vk.com/video' + str(attachment['video']['owner_id']) + '_' + str(
                                    attachment['video']['id'])
                                init_attach_data['header'] = str(attachment['video']['title'])
                                init_attach_data['link'] = str(link)
                                init_attach_data['show_link'] = str(attachment['video']['photo_320'])
                            elif attachment['type'] == 'link':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['link']['title'])
                                init_attach_data['link'] = str(attachment['link']['url'])
                                init_attach_data['show_link'] = 'Ссылка'
                            elif attachment['type'] == 'album':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['album']['title'])
                                link = 'https://vk.com/album' + str(attachment['album']['thumb']['owner_id']) + '_' + \
                                       str(attachment['album']['thumb']['album_id'])
                                init_attach_data['link'] = str(link)
                                init_attach_data['show_link'] = 'Альбом'
                            elif attachment['type'] == 'poll':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = 'Опрос:' + str(attachment['poll']['question'])
                                init_attach_data['link'] = 'https://vk.com/poll' + str(attachment['poll']['owner_id']) \
                                                           + '_' + str(attachment['poll']['id'])
                                init_attach_data['show_link'] = 'Опрос'
                            elif attachment['type'] == 'page':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['page']['title'])
                                init_attach_data['link'] = str(attachment['page']['view_url'])
                                init_attach_data['show_link'] = 'Страница'
                            elif attachment['type'] == 'market':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['market']['title'])
                                init_attach_data['link'] = 'https://vk.com/market' + str(
                                    attachment['market']['owner_id']) \
                                                           + '_' + str(attachment['market']['id'])
                                init_attach_data['show_link'] = 'Магазин'
                            elif attachment['type'] == 'market_album':
                                init_attach_data['attachment_id'] = int(k)
                                init_attach_data['type'] = str(attachment['type'])
                                init_attach_data['header'] = str(attachment['market_album']['title'])
                                init_attach_data['link'] = 'https://vk.com/market' + str(
                                    attachment['market_album']['owner_id']) \
                                                           + '?section=album_' + str(attachment['market_album']['id'])
                                init_attach_data['show_link'] = 'Альбом магазина'
                            else:
                                self.LOGGER.info('Проблема в распозновании вложения')
                                self.LOGGER.info(attachment)
                            f = init_attach_data.copy()
                            init_attach_list.append(f)

                except Exception as e:
                    self.LOGGER.warning(e)
                    self.LOGGER.info(post['attachments'])

        except Exception as e:
            self.LOGGER.exception(e)

        return rules_list, init_posts_list, init_reply_list, init_attach_list

    def get_posts_by_api(self, word, rule, social, start_time=None, end_time=None):
        tokens = ['ae562253ae562253ae562253b9ae3d5dbeaae56ae562253f3493d9d513c8863c7aac964',
                  'c31fed1ec31fed1ec31fed1e9fc375389dcc31fc31fed1e9ff1a58a7ee64e8d02d4f154',
                  "12d3f24c12d3f24c12d3f24ccd12b8c93a112d312d3f24c4fd8b36bf57da64eaf97aed7",
                  "5603dd4c5603dd4c5603dd4cd65668e639556035603dd4c0b089c42aa273b07888b5291",
                  "c1d97c57c1d97c57c1d97c5718c1b24723cc1d9c1d97c579cd23caf6e148d40ca60d546"]

        if self.session_auth:
            time.sleep(0.4)
            for i in range(0, 5):
                try:
                    d = self.vk.newsfeed.search(q=word, count=200, extended=1, start_time=start_time,
                                                end_time=end_time)

                    rules_lists, init_posts_lists, init_reply_lists, init_attach_lists = self.insert_init_posts(d, rule,
                                                                                                                social,
                                                                                                                word)
                    while len(d['items']) == 200:
                        time.sleep(0.4)
                        d = self.vk.newsfeed.search(q=word, count=200, extended=1, start_time=start_time,
                                                    end_time=end_time, start_from=d['next_from'])
                        rules, postss, replies, attachmentss = self.insert_init_posts(d, rule, social, word)
                        rules_lists = rules_lists + rules
                        init_posts_lists = init_posts_lists + postss
                        init_reply_lists = init_reply_lists + replies
                        init_attach_lists = init_attach_lists + attachmentss

                    self.LOGGER.info('FINISH TO DOWNLOAD POSTS FROM API')
                    rules_posts = False
                    task = {'task_name': 'insert_posts_rules',
                            'args': {'posts': rules_lists}}
                    self.cass.call(task)
                    task = {'task_name': 'insert_posts_data',
                            'args': {'posts': init_posts_lists,
                                     'rules_posts': rules_posts}}
                    self.cass.send_message(task)

                    task = {'task_name': 'insert_replies',
                            'args': {'replies': init_reply_lists}}
                    self.cass.send_message(task)

                    task = {'task_name': 'insert_posts_att',
                            'args': {'posts': init_attach_lists}}
                    self.cass.send_message(task)

                    return True
                except:
                    self.LOGGER.exception('VK API ERROR')
                    vk_session = vk_api.VkApi(token=tokens[i],
                                              captcha_handler=self.captcha_handler)
                    self.vk = vk_session.get_api()

        else:
            self.LOGGER.warning('ОШИБКА АВТОРИЗАЦИИ')
            return False
