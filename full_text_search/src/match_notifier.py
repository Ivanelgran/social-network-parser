import time
import pandas as pd

from datetime import datetime, timedelta
from fts_base import FTSManager
from elassandra_handler import Elassandra
from common_lib.queue_manager import RpcClient


class MatchNotifier(FTSManager):
    def __init__(self):
        super().__init__()
        self.elassandra = Elassandra()
        self.notification = RpcClient('notification')
        self.ntf_django = RpcClient('notifications_tasks')
        self.tasks = {}

    def bg_notificator(self):
        for i in range(0, 10):
            try:
                while True:
                    self.fs_notificator()
                    time.sleep(5)
            except:
                self.LOGGER.exception('ERROR IN LOADING BG_NOTIFICATOR')
                continue

    def data_handler(self, data):
        data = [x for x in data if x != []]
        pd_data = {}
        frame = ''
        if len(data) > 1:
            for i in range(len(data)):
                pd_data[i] = pd.DataFrame(data[i])

            for i in range(len(pd_data) - 1):
                if i == 0:
                    frame = pd_data[i].merge(pd_data[i + 1], how='outer')
                else:
                    frame = frame.merge(pd_data[i + 1], how='outer')
        else:
            frame = pd.DataFrame(data[0])

        return frame

    def fs_notificator(self):
        task = {'task_name': 'get_tracked_rules',
                'args': {}}
        all_tasks = self.cass.call(task)
        if all_tasks:
            tags_by_dict = {i['tag']: i for i in all_tasks}
            rules = list(tags_by_dict.keys())
            for rule in rules:
                if rule in self.tasks:
                    time.sleep(2)
                    new_data = self.get_posts_for_fsn(rule)
                    if not new_data.empty:
                        new_data['date'] = pd.to_datetime(new_data['date'])
                        try:
                            last_date = pd.to_datetime(self.tasks[rule][:1]['date']).dt.strftime(
                                '%Y-%m-%d %H:%M:%S.%f').to_string(
                                index=False)
                        except:
                            self.LOGGER.exception('DATE ERROR IN LAST POST')
                            last_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
                        mask = (new_data['date'] > last_date)
                        result = new_data.loc[mask]
                        if not result.empty:
                            for index, post in result.iterrows():
                                if post.get('copy_owner_id') and not pd.isnull(post['copy_owner_id']):
                                    link = 'https://vk.com/wall' + str(post['owner_id']) + \
                                           '_' + str(post['post_id'])
                                elif post.get('reply_id') and not pd.isnull(post['reply_id']):
                                    link = 'https://vk.com/wall' + str(post['owner_id']) + \
                                           '_' + str(post['post_id']) + '?reply=' + str(post['reply_id'])
                                else:
                                    link = 'https://vk.com/topic' + str(post['owner_id']) + \
                                           '_' + str(post['topic_id']) + '?post=' + str(post['post_id'])
                                try:
                                    all_tasks[:] = [d['user'] for d in all_tasks if rule in d['tag']]
                                except:
                                    self.LOGGER.info(rule)
                                    self.LOGGER.info(all_tasks)
                                    break
                                for user in all_tasks:
                                    info = {
                                        'author': user,
                                        'type': 'stp_notificator',
                                        'body': post['text'],
                                        'link': link
                                    }
                                    self.notification.send_message(info)
                                self.ntf_django.send_message({
                                    'task_name': 'send_ntf_to_django',
                                    'args': {
                                        'rule': self.get_rule_value(rule)[0]['value'],
                                        'data': info
                                    }
                                })
                        self.tasks[rule] = new_data
                    else:
                        self.LOGGER.info('NO POSTS FOR TASK ' + str(rule))
                else:
                    data = self.get_posts_for_fsn(rule)
                    self.tasks[rule] = data
        else:
            self.LOGGER.info('NO TRACKING TASKS FOR NOW')

    def get_rule_value(self, tag):
        task = {'task_name': 'get_rules_values',
                'args': {'tag': tag}}
        words = self.cass.call(task)

        return words

    def get_posts_for_fsn(self, tag):
        start_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
        start_time = (datetime.now() - timedelta(days=1)).strftime("%H:%M")
        end_date = (datetime.now()).strftime("%Y-%m-%d")
        end_time = (datetime.now()).strftime("%H:%M")
        words = self.get_rule_value(tag)
        if words:
            data = {'words': words[0]['value'],
                    'limit': '100',
                    'social': 'vk',
                    'start_time': start_time,
                    'start_date': start_date,
                    'end_time': end_time,
                    'end_date': end_date
                    }
            info = self.elassandra.get_posts_by_elassandra(data)

            if not self.is_list_empty(info):
                info = self.data_handler(info)
                info = info.sort_values(by='date', ascending=False)
                if str(tag) == 'тест':
                    self.LOGGER.info(info.to_string())
                return info
            else:
                return pd.DataFrame()
        else:
            return pd.DataFrame()

    def is_list_empty(self, in_list):
        if isinstance(in_list, list):
            return all(map(self.is_list_empty, in_list))
        return False
