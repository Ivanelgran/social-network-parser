import pickle
import threading
from multiprocessing import Process

import pika
from elassandra_handler import Elassandra
from match_notifier import MatchNotifier
from vk_api_handler import VkApiHandler
from vk_streaming_api import VkStreamingApi

from common_lib.queue_manager import make_server

elassandra = Elassandra()
vk_api_handler = VkApiHandler()
vk_streaming_api = VkStreamingApi()


def exec_task(ch, method, properties, body):
    info = 'ok'
    task = pickle.loads(body)
    if task.get('el_search') is not None:
        info = elassandra.get_posts_by_elassandra(task)
    elif task.get('track_rule') is not None:
        thread = threading.Thread(target=vk_streaming_api.streaming_starter)
        thread.start()
    elif task.get('init_rule') is not None:
        socials = task['social'].split(' ')
        for social in socials:
            if social == 'vk':
                vk_api_handler.ini_posts_by_rule(task)
    ch.basic_publish(exchange='',
                     routing_key=properties.reply_to,
                     properties=pika.BasicProperties(correlation_id=properties.correlation_id),
                     body=pickle.dumps(info))
    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    match_notifier = MatchNotifier()
    vk_api_handler = VkApiHandler()
    fts_service_name = 'fts_service'

    fts_service_process = Process(target=make_server,
                                  args=(fts_service_name,
                                        exec_task,))
    bg_api_search_process = Process(target=vk_api_handler.bg_api_search)
    fs_notificator_process = Process(target=match_notifier.bg_notificator)
    bg_api_search_process.start()
    fs_notificator_process.start()
    fts_service_process.start()
    fts_service_process.join()
    fs_notificator_process.join()
    bg_api_search_process.join()
