import time
import re

from datetime import datetime
from fts_base import FTSManager
from common_lib.queue_manager import RpcClient
from vkstreaming import Streaming, getServerUrl, VkError

response = getServerUrl('c31fed1ec31fed1ec31fed1e9fc375389dcc31fc31fed1e9ff1a58a7ee64e8d02d4f154')
api = Streaming(response["endpoint"], response["key"])
post_data = list()
attach_data = list()
posts = dict()
attach = dict()


class VkStreamingApi(FTSManager):

    def streaming_starter(self):
        try:
            try:
                api.stop()
            except:
                self.LOGGER.info('Не получилось закрыть поток')
            time.sleep(2)
            db_rules_dict = {}
            db_rules = []
            task = {'task_name': 'get_tracked_rules',
                    'args': {}}
            all_tasks = self.cass.call(task)
            tags_by_dict = {i['tag']: i for i in all_tasks}
            rules = list(tags_by_dict.keys())
            for tag in rules:
                task = {'task_name': 'get_rules_values',
                        'args': {'tag': tag}}
                rules = self.cass.call(task)
                self.LOGGER.info(rules)
                if '|' in rules[0]['value']:
                    comb_rule = self.space_destroyer(rules[0]['value'])
                    for i in range(len(comb_rule)):
                        db_rules_dict['tag'] = str(tag) + '_' + str(i)
                        db_rules_dict['value'] = re.sub("[!@#,./%^'$]", '', comb_rule[i])
                        db_rules.append(db_rules_dict.copy())
                else:
                    db_rules_dict['tag'] = tag
                    db_rules_dict['value'] = re.sub("[!@#,./%^'$]", '', rules[0]['value'])
                    db_rules.append(db_rules_dict.copy())
            value_by_dict = {i['value']: i for i in db_rules}
            db_rules = list(value_by_dict.values())
            api.update_rules(db_rules)
            rules = api.get_rules()
            for rule in rules:
                self.LOGGER.info(("{tag:15}:{value}").format(**rule))

            api.start()

        except VkError as e:
            self.LOGGER.warning(e.error_code)
            self.LOGGER.warning(e.message)

    def posts_handler(self, event):
        try:
            type = str(event['event_type'])
            posts['social'] = 'vk'
            if type == 'post':
                posts['owner_id'] = str(event['event_id']['post_owner_id'])
                posts['post_id'] = str(event['event_id']['post_id'])
                posts['copy_owner_id'] = str(event['event_id']['post_owner_id'])
                posts['copy_post_id'] = str(event['event_id']['post_id'])
            elif type == 'share':
                posts['owner_id'] = str(event['event_id']['post_owner_id'])
                posts['post_id'] = str(event['event_id']['post_id'])
                posts['copy_owner_id'] = str(event['event_id']['shared_post_owner_id'])
                posts['copy_post_id'] = str(event['event_id']['shared_post_id'])
            elif type == 'comment':
                posts['owner_id'] = str(event['event_id']['post_owner_id'])
                posts['post_id'] = str(event['event_id']['post_id'])
                posts['reply_id'] = str(event['event_id']['comment_id'])
                posts['author'] = str(event['author']['id'])
                posts['parent_reply'] = ''
                posts['sticker'] = ''
            elif type == 'topic_post':
                posts['owner_id'] = str(event['event_id']['topic_owner_id'])
                posts['topic_id'] = str(event['event_id']['topic_id'])
                posts['post_id'] = str(event['event_id']['topic_post_id'])
            posts['date'] = datetime.fromtimestamp(int(event['creation_time']))
            text = str(event['text'])
            text = text.replace('<br>', '\n')
            text = text.replace('#', ' #')
            text = " ".join(x for x in text.split() if not x.startswith(("http", "https")))
            if type == 'share':
                shared_text = str(event['shared_post_text'])
                shared_text = shared_text.replace('<br>', '\n')
                posts['text'] = text + '\n' + '-------------------' + '\n' + shared_text
            else:
                posts['text'] = text
            posts['likes'] = 0
            posts['replies'] = 0
            if type != 'comment':
                posts['shares'] = 0
                posts['views'] = 0
                if type == 'share':
                    posts['post_type'] = 'repost'
                else:
                    posts['post_type'] = type
            else:
                posts['post_type'] = type
            post_data.append(posts)
            rules_posts = False
            if type == 'post' or type == 'share':
                task = {'task_name': 'insert_posts_data',
                        'args': {'posts': post_data,
                                 'rules_posts': rules_posts}}
                self.cass.send_message(task)
            elif type == 'topic_post':
                task = {'task_name': 'insert_topic_posts_table',
                        'args': {'posts': post_data}}
                self.cass.send_message(task)
            elif type == 'comment':
                task = {'task_name': 'insert_replies',
                        'args': {'replies': post_data}}
                self.cass.send_message(task)

            if type == 'comment':
                insert_data = {
                    'rule': str(event['tags'][0]), 'social': 'vk',
                    'owner_id': str(event['event_id']['post_owner_id']),
                    'post_id': str(event['event_id']['post_id']),
                    'post_type': str(posts['post_type']),
                    'date': posts['date'],
                    'type': posts['post_type'],
                    'reply_id': posts['reply_id']
                }
            elif type == 'topic_post':
                insert_data = {
                    'rule': str(event['tags'][0]), 'social': 'vk',
                    'owner_id': str(event['event_id']['topic_owner_id']),
                    'post_id': str(event['event_id']['topic_post_id']),
                    'post_type': str(posts['post_type']),
                    'date': posts['date'],
                    'type': posts['post_type']
                }
            else:
                insert_data = {
                    'rule': str(event['tags'][0]), 'social': 'vk',
                    'owner_id': str(event['event_id']['post_owner_id']),
                    'post_id': str(event['event_id']['post_id']),
                    'post_type': str(posts['post_type']),
                    'date': posts['date'],
                    'type': posts['post_type']
                }
            insert_data_list = list()
            insert_data_list.append(insert_data)
            task = {'task_name': 'insert_posts_rules',
                    'args': {'posts': insert_data_list}}
            self.cass.call(task)

        except Exception as e:
            self.LOGGER.exception(e)

        posts.clear()
        post_data[:] = []
        f = 0

        try:
            if event.get('attachments') is not None:
                for attachment in event['attachments']:
                    f = f + 1
                    type = str(event['event_type'])
                    if type == 'post':
                        attach['owner_id'] = str(event['event_id']['post_owner_id'])
                        attach['post_id'] = str(event['event_id']['post_id'])
                    elif type == 'share':
                        attach['owner_id'] = str(event['event_id']['post_owner_id'])
                        attach['post_id'] = str(event['event_id']['post_id'])
                    elif type == 'comment':
                        attach['owner_id'] = str(event['event_id']['post_owner_id'])
                        attach['post_id'] = str(event['event_id']['post_id'])
                    elif type == 'topic_post':
                        attach['owner_id'] = str(event['event_id']['topic_owner_id'])
                        attach['post_id'] = str(event['event_id']['topic_post_id'])
                    else:
                        self.LOGGER.info('Проблема с распознованием ID')
                        self.LOGGER.info(event)

                    attach['social'] = 'vk'

                    if attachment['type'] == 'photo':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        attach['header'] = attachment['photo']['text']
                        attach['link'] = attachment['photo']['photo_604']
                        attach['show_link'] = attachment['photo']['photo_604']
                    elif attachment['type'] == 'doc':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        attach['header'] = attachment['doc']['title']
                        attach['link'] = attachment['doc']['url']
                        attach['show_link'] = 'Гифка'
                    elif attachment['type'] == 'audio':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        attach['header'] = attachment['audio']['title']
                        attach['link'] = attachment['audio']['url']
                        attach['show_link'] = 'Аудио'
                    elif attachment['type'] == 'video':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        link = 'https://vk.com/video' + str(attachment['video']['owner_id']) + '_' + str(
                            attachment['video']['id'])
                        attach['header'] = attachment['video']['title']
                        attach['link'] = link
                        attach['show_link'] = attachment['video']['photo_320']
                    elif attachment['type'] == 'link':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        attach['header'] = attachment['link']['title']
                        attach['link'] = attachment['link']['url']
                        attach['show_link'] = 'Ссылка'
                    elif attachment['type'] == 'album':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        attach['header'] = attachment['album']['title']
                        link = 'https://vk.com/album' + str(event['author']['id']) + '_' + str(
                            attachment['album']['thumb']['album_id'])
                        attach['link'] = link
                        attach['show_link'] = 'Альбом'
                    elif attachment['type'] == 'poll':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        attach['header'] = 'Опрос:' + str(attachment['poll']['question'])
                        attach['link'] = event['event_url']
                        attach['show_link'] = 'Опрос'
                    elif attachment['type'] == 'page':
                        attach['attachment_id'] = int(f)
                        attach['type'] = attachment['type']
                        attach['header'] = str(attachment['page']['title'])
                        attach['link'] = str(attachment['page']['view_url'])
                        attach['show_link'] = 'Страница'
                    elif attachment['type'] == 'market':
                        attach['attachment_id'] = int(f)
                        attach['type'] = str(attachment['type'])
                        attach['header'] = str(attachment['market']['title'])
                        attach['link'] = 'https://vk.com/market' + str(attachment['market']['owner_id']) \
                                         + '_' + str(attachment['market']['id'])
                        attach['show_link'] = 'Магазин'
                    else:
                        self.LOGGER.info('Проблема в распозновании вложения')
                        self.LOGGER.info(attachment)
                        self.LOGGER.info(event)
                    attach_copy = attach.copy()
                    attach_data.append(attach_copy)
                attach_data_copy = attach_data.copy()
                task = {'task_name': 'insert_posts_att',
                        'args': {'posts': attach_data_copy}}
                self.cass.send_message(task)
        except Exception as e:
            self.LOGGER.exception(e)
            self.LOGGER.info(event)

        attach.clear()
        attach_data[:] = []


@api.stream
def my_func(event):
    VkStreamingApi().posts_handler(event)
