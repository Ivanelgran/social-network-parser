import re
import requests
import json

from fts_base import FTSManager
from dateutil.parser import parse


class Elassandra(FTSManager):

    def query_handler(self, query):
        new_query = []
        words = re.findall(r'\-"\w+\ \w+\"', query)
        if words:
            for hard_word in words:
                query = query.replace(hard_word, '')
                hard_word = hard_word.replace('"', '').replace('-', '')
                result = hard_word.split(' ')
                for single_word in result:
                    new_query.append('-' + str(single_word))
        query = query.split(' ')
        for word in query:
            if not word == '':
                if not word.startswith('-'):
                    word = '+' + str(word)
                new_query.append(word)
        query = ' '.join(new_query)
        query = query.replace('"', '')

        return query

    def get_posts_by_elassandra(self, rule_data):
        index_data = []
        posts_data = []
        replies_data = []
        topics_data = []
        words = self.space_destroyer(rule_data['words'])
        for word in words:
            word = self.query_handler(word)
            self.LOGGER.info('START ELASSANDRA SEARCH FOR VALUE "' + str(word) + '"')
            socials = rule_data['social'].split(' ')
            limit = rule_data['limit']
            for social in socials:
                data = {"size": '10000',
                        "highlight": {
                            "fields": {
                                'text': {
                                    "fragment_size": 10000
                                }
                            },
                            "pre_tags": ['<mark>'],
                            "post_tags": ['</mark>']
                        },
                        "query": {"bool": {"must": {
                            "query_string": {"default_field": "text", "analyzer": "russian_analyzer", "query": word}},
                            "filter": [{"term": {"social": social}},
                                       {"range": {
                                           "date": {
                                               "gte": str(rule_data['start_date']) + 'T' + str(rule_data['start_time']),
                                               "lte": str(rule_data['end_date']) + 'T' + str(
                                                   rule_data['end_time'])}}}]}}}
                data = json.dumps(data)
                headers = {'Content-Type': 'application/json'}
                try:
                    posts_resp = requests.post('http://cassandra:9200/posts/_search', data=data, headers=headers)
                    replies_resp = requests.post('http://cassandra:9200/replies/_search', data=data, headers=headers)
                    topics_resp = requests.post('http://cassandra:9200/topic_posts/_search', data=data, headers=headers)
                    pr = json.loads(posts_resp.text)
                    # self.LOGGER.info(pr['hits']['hits'][0])
                    rr = json.loads(replies_resp.text)
                    # self.LOGGER.info(rr['hits']['hits'][0])
                    tr = json.loads(topics_resp.text)
                    self.LOGGER.info('SUCCESSFUL INDEX SEARCH')
                    post_index = pr['hits']['hits']
                    replies_index = rr['hits']['hits']
                    topics_index = tr['hits']['hits']

                    for post in post_index:
                        dt = parse(post['_source']['date'])
                        post['_source']['date'] = str(dt.date()) + ' ' + str(dt.time())
                        post['_source']['highlight'] = ''.join(post['highlight']['text'])
                        posts_data.append(post['_source'].copy())
                    for reply in replies_index:
                        dt = parse(reply['_source']['date'])
                        reply['_source']['date'] = str(dt.date()) + ' ' + str(dt.time())
                        reply['_source']['highlight'] = ''.join(reply['highlight']['text'])
                        replies_data.append(reply['_source'].copy())
                    for topic_post in topics_index:
                        dt = parse(topic_post['_source']['date'])
                        topic_post['_source']['date'] = str(dt.date()) + ' ' + str(dt.time())
                        topic_post['_source']['highlight'] = ''.join(topic_post['highlight']['text'])
                        topics_data.append(topic_post['_source'].copy())

                except:
                    self.LOGGER.exception('ELASSANDRA ERROR')

            index_data.append(posts_data)
            index_data.append(replies_data)
            index_data.append(topics_data)

        return index_data
