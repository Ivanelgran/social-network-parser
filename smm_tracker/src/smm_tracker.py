from datetime import datetime, timedelta
import re
import time

import cx_Oracle as orcl
import pymysql.cursors
import schedule

from common_lib.cass_worker import (
    CassandraTables, CassQueryGenerator, select_data, setup_connection)
from common_lib.queue_manager import RpcClient
from common_lib.src.objects import PostManager
from common_lib.src import log, config

setup_connection()


class Vividict(dict):
    """Вложенный словарь"""

    def __missing__(self, key):
        value = self[key] = type(self)()
        return value


class Worksheet():

    def __init__(self, logger):
        self.logger = logger
        self.worksheet = Vividict()

        CONFIG = config.get_config("common_lib/config/db_worker.yml")
        self.mysql_credentials = CONFIG['stp_mysql']

    def __select_worksheet(self, year: int, month: int):
        connection = pymysql.connect(
            host=self.mysql_credentials['host'],
            user=self.mysql_credentials['user'],
            password=self.mysql_credentials['password'],
            db=self.mysql_credentials['db'],
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor)

        sql = (f"SELECT * FROM worksheet_new WHERE year = {year} "
               f"AND month = {month} AND (userid = 486 or userid = 642 or userid = 407)")
        self.logger.info(sql)
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql)
                result = cursor.fetchall()
        finally:
            connection.close()

        self.logger.info(result)
        return result

    def __update_worksheet(self, year: int, month: int):
        worksheet = self.__select_worksheet(year, month)
        for user_worksheet in worksheet:
            for i in range(1, 32):
                is_working = user_worksheet[f"m{i}"] == 'ss'
                if is_working:
                    self.worksheet[year][month][i] = user_worksheet['userid']

    def get_operator(self, year: int, month: int, day: int) -> int:
        operator = self.worksheet[year][month][day]

        if not operator:
            self.__update_worksheet(year, month)
            operator = self.worksheet[year][month][day]

        return operator or 0


class OracleWorker():

    def __init__(self, logger):
        self.logger = logger

        CONFIG = config.get_config("common_lib/config/db_worker.yml")
        self.oracle_credentials = CONFIG['oracle']

    def _get_oracke_connection(self):
        dsn = orcl.makedsn(
            host=self.oracle_credentials['host'],
            port=self.oracle_credentials['port'],
            sid=self.oracle_credentials['sid']
        )
        connection = orcl.connect(
            user=self.oracle_credentials['user'],
            password=self.oracle_credentials['password'],
            dsn=dsn,
            encoding='UTF-8'
        )

        cur = connection.cursor()

        return connection, cur

    def run_query(self, query):
        conn, cur = self._get_oracke_connection()
        try:
            result = list(cur.execute(query))
        except Exception:
            result = []
            message = "Something went wrong while getting data from Oracle"
            self.logger.exception(message)
        finally:
            cur.close()
            conn.close()

        return result


class IncidentMatcher():

    __query_for_incidents = """
    select
        REG_COMMENTS
    from REGSYSREF.SD_INCIDENT
    where
        REG_DATE >= to_date('{0}', 'YYYY-MM-DD') and
        REG_DATE < to_date('{1}', 'YYYY-MM-DD') and
        CHANNEL_INT_ID = 6362 and
        REG_USER_ID IN (8605824,196524,698659)
    """

    def __init__(self, start_period: datetime, end_period: datetime, logger):
        self.logger = logger
        self.start_period = start_period
        self.end_period = end_period
        self.oracle_worker = OracleWorker(logger)
        self._get_incidents()

    def _get_incidents(self):
        str_start_period = self.start_period.strftime('%Y-%m-%d')
        str_end_period = self.end_period.strftime('%Y-%m-%d')

        self.incidents = self.oracle_worker.run_query(
            self.__query_for_incidents.format(str_start_period, str_end_period)
        )
        self.logger.info(f"incidents: {len(self.incidents)}")
        # self.logger.info(self.incidents)

        posts = self.get_posts()
        self.links = [*map(lambda x: f"{x['owner_id']}_{x['post_id']}", posts)]

    def get_posts(self):
        result = []
        for text in self.incidents:
            links = re.findall(r'wall[-0-9]+_[0-9]+', text[0])
            if len(links):
                post = links[0][4:].split('_')
                result.append({
                    'social': 'vk',
                    'owner_id': post[0],
                    'post_id': post[1]
                })

        return result

    def has_incident(self, reply):
        if not reply.text:
            return False

        post_id = f"{reply.owner_id}_{reply.post_id}"
        if post_id in self.links:
            return True

        for text in self.incidents:
            inc_text = text[0].replace('\r\n', '')
            if reply.text in inc_text:
                return True

        self.logger.info(reply.text)
        return False


class MentionWatcher():

    def __init__(self, vk_owner_id, logger, days, days_passed):
        self.days = days
        self.days_passed = days_passed
        self.cass = RpcClient("db_tasks")
        self.fts_service = RpcClient("fts_service")
        self.post_manager = PostManager(logger)
        self.logger = logger
        self.VK_OWNER_ID = vk_owner_id
        self.worksheet = Worksheet(logger)

        self.end_period = datetime.now() - timedelta(days=self.days_passed)
        self.start_period = self.end_period - timedelta(days=self.days)
        self.logger.info(f"start_period: {self.start_period}, end_period: {self.end_period}")
        self.incident_matcher = IncidentMatcher(
            start_period=self.start_period,
            end_period=self.end_period,
            logger=logger
        )

    def __get_mentions_from_elastic(self):
        words = 'интерсвязь'
        social = 'vk'
        limit = 10000

        start_date = datetime.date(self.start_period)
        start_time = datetime.time(self.start_period)

        end_date = datetime.date(self.end_period)
        end_time = datetime.time(self.end_period)

        task = {'el_search': True,
                'words': words,
                'limit': limit,
                'social': social,
                'start_time': start_time,
                'start_date': start_date,
                'end_time': end_time,
                'end_date': end_date}
        mentions = self.fts_service.call(task)

        return mentions

    def __get_group_replies(self):
        filters = {'date__gte': self.start_period,
                   'date__lte': self.end_period}
        group_replies = select_data(CassandraTables.replies, filters=filters)

        return group_replies

    def __get_posts_ids(self):
        elastic_mentions = self.__get_mentions_from_elastic()
        post_data, replies_data = elastic_mentions[0], elastic_mentions[1]

        group_replies = self.__get_group_replies()

        incident_posts = self.incident_matcher.get_posts()

        all_posts = set()
        posts_ids = []
        for mentions in [post_data, replies_data,
                         group_replies, incident_posts]:
            for post in mentions:
                str_id = post['social'] + post['owner_id'] + post['post_id']
                if str_id in all_posts:
                    continue

                post_ids = dict()
                for key in ['social', 'owner_id', 'post_id']:
                    post_ids[key] = post[key]
                posts_ids.append(post_ids)

                all_posts.add(str_id)

        return posts_ids

    def __find_responses(self, posts):
        responses = []
        for post in posts:
            for reply in post.replies.values():
                if reply.author != self.VK_OWNER_ID:
                    continue

                parent_reply_id = reply.parent_reply_id
                if parent_reply_id == "" or parent_reply_id == reply.reply_id:
                    parent_date = post.date
                else:
                    if parent_reply_id not in post.replies:
                        continue
                    parent_date = post.replies[parent_reply_id].date

                response_time = (reply.date - parent_date).seconds

                operator = self.worksheet.get_operator(
                    reply.date.year,
                    reply.date.month,
                    reply.date.day)

                parent_operator = self.worksheet.get_operator(
                    parent_date.year,
                    parent_date.month,
                    parent_date.day)

                has_incident = self.incident_matcher.has_incident(reply)

                response = {'date': reply.date,
                            'social': reply.social,
                            'owner_id': reply.owner_id,
                            'post_id': reply.post_id,
                            'reply_id': reply.reply_id,
                            'parent_reply_id': parent_reply_id,
                            'response_time': response_time,
                            'operator': operator,
                            'parent_operator': parent_operator,
                            'has_incident': has_incident}

                responses.append(response)

        return responses

    def __save_responses(self, responses):
        query = CassQueryGenerator(self.cass, CassandraTables.smm_responses,
                                   'insert')

        for response in responses:
            cur_response = CassandraTables.smm_responses(**response)
            query.add_record(cur_response)
        query.run_task()

    def __handle_responses(self, posts):
        responses = self.__find_responses(posts)
        self.__save_responses(responses)

    def run(self, update_replies):
        if not len(self.incident_matcher.incidents):
            return

        posts_ids = self.__get_posts_ids()

        posts = self.post_manager.get_posts(
            posts_ids, update_replies=update_replies
        )

        self.__handle_responses(posts)


def track_mentions(days=2, days_passed=0, update_replies=True):
    LOGGER.info(f"-----Start track mentions-----")
    mention_watcher = MentionWatcher(
        vk_owner_id=VK_OWNER_ID, logger=LOGGER,
        days=days, days_passed=days_passed
    )
    mention_watcher.run(update_replies=update_replies)
    LOGGER.info("-----Track mentions complete-----")


if __name__ == "__main__":
    LOGGER = log.get_logger('smm_tracker.main')
    VK_OWNER_ID = '-18970929'

    schedule.every().day.at("00:00").do(track_mentions)

    # track_mentions(30, update_replies=False)

    while True:
        schedule.run_pending()
        time.sleep(1)
