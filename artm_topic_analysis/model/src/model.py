# TODO: убрать лишние функции

import pandas as pd
import artm
import os
import pymystem3
import nltk
import logging
import numpy as np
import json
import signal

from utilities import save_pickle
from queue_manager import make_server
from artm.wrapper.exceptions import InvalidOperationException

LOGGER = logging.getLogger('model')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')

fh = logging.FileHandler('logs/logs.log', mode='w')
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)


class ArtmModel():
    save_path = "hier/"
    vw_path = "data/vw.txt"
    batch_path = "data/batches"
    dictionary_path = "data/dict.dict"
    indexes_child_path = "data/indexes_child.npy"
    queue_name = 'db_tasks'
    predict_start = False

    def __init__(self, level_0_topics_num=25, level_1_topics_num=50, trend_dir=None):
        self.level_0_topics_num = level_0_topics_num
        self.level_1_topics_num = level_1_topics_num
        self.vw = VWMaker()
        self.LOGGER = logging.getLogger('model.artm_model')
        self.phi = None
        self.top_words = None
        self.indexes_child = None
        self.set_logging_level()
        self.trend_dir = trend_dir

    def get_model(self, documents=None, type="main"):

        self.set_batch_and_dict(documents=documents)
        if len(os.listdir(self.save_path)) <= 1:
            if type == "main":
                self.build_model()
                # self.save_topics_in_posts()
            elif type == "trend":
                self.trend_data = {'posts_count': len(documents),
                                   'topics_count': self.level_0_topics_num}
                self.build_trend_model()
        else:
            self.load_model()

        # self.get_top_words()
        self.LOGGER.info("MODEL READY")

    def save_topics_in_posts(self):
        theta = self.model.get_level(1).get_theta().T
        i = 1
        posts = []
        for row in theta.iterrows():
            post = row[0].split('_')
            owner_id, post_id = post[0], post[1]
            data = row[1]
            for topic, prob in enumerate(data):
                if prob == 0:
                    continue
                data = {'owner_id': owner_id,
                        'post_id': post_id,
                        'level': 1,
                        'topic': topic,
                        'probability': prob}
                posts.append(data)
            if i % 100000 == 0:
                self.LOGGER.info(f"{i} POSTS APPLIED")
            i += 1

    def set_logging_level(self, level=2):
        lc = artm.messages.ConfigureLoggingArgs()
        lc.minloglevel = 2
        lib = artm.wrapper.LibArtm(logging_config=lc)
        lib.ArtmConfigureLogging(lc)

    def set_top_words(self):
        self.phi = self.model.get_phi()
        top_words = {}
        for i in [0, 1]:
            for topic in self.model.get_level(i).topic_names:
                words = list(self.phi.sort_values(by=f'level{i}_' + topic, ascending=False).iloc[:20].index)
                top_words[topic] = words
        self.top_words = top_words

    def get_top_words(self):
        if self.top_words is None:
            self.set_top_words()

        return self.top_words

    def set_psi(self):
        if 'indexes_child.npy' in os.listdir('./data/'):
            self.load_indexes_child()
        elif self.indexes_child is None:
            self.LOGGER.info("PSI INIT START")
            psi = self.model.get_level(1).get_psi()
            self.LOGGER.info("PSI PSI GETTED")
            batch = artm.messages.Batch()
            batch_name = './phi1.batch'
            self.LOGGER.info("PSI INIT COMPLETE")

            with open(batch_name, "rb") as f:
                batch.ParseFromString(f.read())

            self.LOGGER.info("PSI FILE READED")

            Ntw = np.zeros(len(self.model.get_level(0).topic_names))

            self.LOGGER.info("PSI Ntw READY")

            for i, item in enumerate(batch.item):
                for (token_id, token_weight) in zip(item.field[0].token_id, item.field[0].token_weight):
                    Ntw[i] += token_weight
                self.LOGGER.info(f"PSI {i} COMPLETE")

            Nt1t0 = np.array(psi) * Ntw
            psi_bayes = (Nt1t0 / Nt1t0.sum(axis=1)[:, np.newaxis]).T

            self.indexes_child = np.argmax(psi_bayes, axis=0)
            self.save_indexes_child()
        self.LOGGER.info("PSI SET")

    def save_indexes_child(self):
        np.save(self.indexes_child_path, self.indexes_child)

    def load_indexes_child(self):
        self.indexes_child = np.load(self.indexes_child_path)

    def build_foamtree(self, topics_states={}):
        self.LOGGER.info("BUILD FOAMTREE")
        answer = []
        for topic in range(self.level_0_topics_num):
            label = ', '.join(self.top_words[f"topic_{topic}"][:3])
            groups = []
            for child in np.where(self.indexes_child == topic)[0]:
                info = {"label": ', '.join(self.top_words[f"child_topic_{child}"][:3]),
                        "selectable": True,
                        "groups": [],
                        "level": 1,
                        "topic": int(child)}
                if topics_states.get(str(child)) == 1:
                    info['is_tracked'] = True
                elif topics_states.get(str(child)) == -1:
                    info['is_hidden'] = True
                groups.append(info)
            answer.append({"label": label,
                           "level": 0,
                           "topic": int(topic),
                           "groups": groups})

        return answer

    def build_model(self):
        self.model = artm.hARTM(cache_theta=True, theta_columns_naming='title')

        self.LOGGER.info("INIT LEVEL 0")
        level0 = self.model.add_level(num_topics=self.level_0_topics_num)
        level0.initialize(dictionary=self.dictionary)
        self.LOGGER.info("FIT LEVEL 0")
        level0.fit_offline(self.bv, num_collection_passes=10)

        self.LOGGER.info("INIT LEVEL 1")
        level1 = self.model.add_level(num_topics=self.level_1_topics_num,
                                      topic_names=['child_topic_' + str(i) for i in range(self.level_1_topics_num)],
                                      parent_level_weight=1)
        level1.regularizers.add(artm.HierarchySparsingThetaRegularizer(name="HierSp", tau=1.0))
        level1.initialize(dictionary=self.dictionary)
        self.LOGGER.info("FIT LEVEL 1")
        level1.fit_offline(self.bv, num_collection_passes=10)
        level1.regularizers.add(artm.SmoothSparsePhiRegularizer(name='SparsePhi', dictionary=self.dictionary))
        level1.regularizers.add(artm.SmoothSparseThetaRegularizer(name='SparseTheta'))
        level1.regularizers['SparsePhi'].tau = -10
        level1.regularizers['SparseTheta'].tau = -0.5
        level1.fit_offline(self.bv, num_collection_passes=5)
        self.LOGGER.info("MODEL FITTED")

        self.save_model()

        self.set_psi()

    def build_trend_model(self):
        self.model = artm.ARTM(num_topics=self.level_0_topics_num, cache_theta=True, theta_columns_naming='title')
        self.model.scores.add(artm.TopTokensScore(name='TopTokensScore', num_tokens=20))
        self.model.initialize(dictionary=self.dictionary)
        self.model.fit_offline(self.bv, num_collection_passes=10)
        self.model.regularizers.add(
            artm.SmoothSparsePhiRegularizer(name='SparsePhi', dictionary=self.dictionary, tau=-10))
        self.model.regularizers.add(artm.SmoothSparseThetaRegularizer(name='SparseTheta', tau=-0.5))
        self.model.fit_offline(self.bv, num_collection_passes=5)

    def save_trend_data(self):
        self.model.save(self.save_path, model_name='p_wt')
        self.model.save(self.save_path, model_name='n_wt')

        self.trend_data['top_words'] = {}
        foamtree = []
        for topic_name in self.model.topic_names:
            tokens = self.model.score_tracker['TopTokensScore'].last_tokens[topic_name]
            self.trend_data['top_words'][topic_name] = tokens
            label = ', '.join(tokens[:3])
            foamtree.append({"label": label,
                             "groups": []})
        self.trend_data['foamtree'] = {"groups": foamtree,
                                       "label": 'Trends ARTM'}

        self.model.get_theta().to_csv(os.path.join(self.trend_dir, 'theta.csv'))

        theta = self.model.get_theta().T
        theta['topic'] = theta.idxmax(axis=1)
        grouped = theta.groupby(['topic'])[[theta.columns[0]]].count()
        grouped.columns = ['count']
        grouped.reset_index(inplace=True)
        self.trend_data['topics_distr'] = grouped.to_dict('records')

        save_pickle(self.trend_data, os.path.join(self.trend_dir, 'trend_info.pkl'))

    def save_model(self):
        for file in os.listdir(self.save_path):
            os.remove(self.save_path + file)
        self.model.save(self.save_path)
        self.LOGGER.info("MODEL SAVED")

    def load_model(self):
        self.LOGGER.info("load model...")
        self.model = artm.hARTM(cache_theta=True, theta_columns_naming='title')
        self.model.load(self.save_path)
        level0 = self.model.get_level(0)
        level0.reshape_topics(
            ['topic_' + str(i) for i in range(self.level_0_topics_num)]
        )
        level1 = self.model.get_level(1)
        level1.reshape_topics(
            ['child_topic_' + str(i) for i in range(self.level_1_topics_num)]
        )
        level1.regularizers.add(
            artm.HierarchySparsingThetaRegularizer(name="HierSp", tau=1.0)
        )
        level1.regularizers.add(
            artm.SmoothSparsePhiRegularizer(name='SparsePhi',
                                            dictionary=self.dictionary)
        )
        level1.regularizers.add(
            artm.SmoothSparseThetaRegularizer(name='SparseTheta')
        )
        level1.regularizers['SparsePhi'].tau = -10
        level1.regularizers['SparseTheta'].tau = -0.3
        self.load_indexes_child()
        self.LOGGER.info("MODEL LOADED")

    def set_batch_and_dict(self, documents=None):
        if len(os.listdir(self.batch_path)) <= 1:
            if not self.vw.check_exist(self.vw_path) and documents is None:
                task = {'task_name': 'get_all_posts',
                        'args': {}}
                posts = self.cass.call(task)
                self.vw.convert_to_vw(self.vw_path, posts)
            elif documents is not None:
                self.vw.convert_to_vw(self.vw_path, documents)
            self.bv = artm.BatchVectorizer(data_path=self.vw_path,
                                           data_format='vowpal_wabbit',
                                           target_folder=self.batch_path)
            self.dictionary = self.bv.dictionary
            self.dictionary.save(dictionary_path=self.dictionary_path)
        else:
            self.bv = artm.BatchVectorizer(data_path=self.batch_path,
                                           data_format='batches')
            self.dictionary = artm.Dictionary()
            self.dictionary.load(dictionary_path=self.dictionary_path)

    def predict_topics(self, posts):
        if posts is None or len(posts) == 0:
            return

        vw_path = 'pred/vw.txt'
        pred_batch_path = 'pred/batches/'
        if os.path.exists(vw_path):
            os.remove(vw_path)
        self.vw.convert_to_vw(vw_path, pd.DataFrame(posts))
        self.LOGGER.info('PREPARE BATCHES')
        for f in os.listdir(pred_batch_path):
            os.remove(pred_batch_path + f)
        self.LOGGER.info('MAKING BATCHES')
        self.LOGGER.info(f'BATCH PATH: {os.path.exists(pred_batch_path)}')
        t_bv = artm.BatchVectorizer(data_path=vw_path,
                                    data_format='vowpal_wabbit',
                                    target_folder=pred_batch_path)

        if len(os.listdir(pred_batch_path)) == 0:
            return {}, -1

        self.LOGGER.info('PREDICT')
        try:
            predict = self.model.transform(t_bv).iloc[self.level_0_topics_num:].T
        except InvalidOperationException:
            return

        posts = []
        max_topics = []
        for row in predict.iterrows():
            topics = {}
            data = row[1]
            max_prob = max_topic = 0
            for topic, prob in enumerate(data):
                if prob == 0:
                    continue
                if prob > max_prob:
                    max_prob = prob
                    max_topic = topic
                topics[topic] = prob
            posts.append(topics)
            max_topics.append(max_topic)

        self.LOGGER.info('TOPICS IS READY')

        return posts[0], max_topics[0]


class VWMaker():

    def __init__(self):
        self.prepared = False
        self.LOGGER = logging.getLogger('Model.vw_maker')
        self.prepare_converting()

    def prepare_converting(self):
        if not self.prepared:
            self.stop_words = self.set_stop_words()
            self.mystem = pymystem3.Mystem()
            self.prepared = True

    def check_exist(self, vw_path):
        return os.path.exists(vw_path)

    def convert_to_vw(self, vw_path, posts):
        # self.prepare_converting()
        self.LOGGER.info("START VW CONVERTING")
        with open(vw_path, 'w') as f:
            counter = 0
            for row in posts.iterrows():
                post = row[1]
                if len(post.text) == 0:
                    continue
                doc_str = f"{counter} |@default_class "
                words = self.lemmatize_and_tokenize(post.text)
                words = self.drop_stop_words(words)
                words_set = set(words)
                for w in words_set:
                    doc_str += f"{w}"
                    if words.count(w) > 1:
                        doc_str += f":{words.count(w)}"
                    doc_str += " "
                doc_str += "\n"
                f.write(doc_str)
                counter += 1
                if counter % 10000 == 0:
                    self.LOGGER.info(f"{counter} ROWS ADDED TO VW")
        self.LOGGER.info("END VW CONVERTING")

    def split_to_text_and_hashtags(self, text):
        raw_text = []
        tags = []
        words = text.replace("#", " #", 1).split()
        for word in words:
            if word.startswith("#"):
                if word.count("#") > 0:
                    cur_tags = word.split("#")
                    [tags.append("#" + t.replace(':', '')) for t in cur_tags if len(t) > 2]
                else:
                    tags.append(word)
            else:
                raw_text.append(word)

        return ' '.join(raw_text), tags

    def lemmatize_and_tokenize(self, text):
        raw_text, tags = self.split_to_text_and_hashtags(text)
        lem_text = self.mystem.lemmatize(raw_text)
        tokens = [word.lower() for word in lem_text if word.isalpha() and len(word) > 2]
        [tokens.append(tag) for tag in tags]

        return tokens

    def set_stop_words(self):
        nltk.download(['stopwords', 'punkt'])

        stop_words = []
        stop_words.extend(nltk.corpus.stopwords.words('russian'))
        stop_words.extend(nltk.corpus.stopwords.words('english'))
        stop_words.extend(['свой', 'который', 'наш', 'ваш',
                           'это', 'мочь', 'весь'])
        return stop_words

    def drop_stop_words(self, tokens):
        result = [word for word in tokens if word not in self.stop_words]

        return result


class ArtmTaskManager():
    rabbit_host = "rabbit"
    queue_name = 'artm_tasks'
    rel_model_name = "rel_model_tasks"

    def __init__(self):
        self.model = ArtmModel()
        self.model.get_model()
        self.method_list = [func for func in dir(self.model) if
                            callable(getattr(self.model, func)) and not func.startswith("__")]
        signal.signal(signal.SIGALRM, self.timeout_handler)

    def timeout_handler(self, signum, frame):
        raise TimeoutException

    def on_request(self, ch, method, props, body):
        task = json.loads(body)

        if task.get('task_name') in self.method_list:
            task_method = getattr(self.model, task.get('task_name'))
            task_args = task['args']
            signal.alarm(5)
            try:
                result = task_method(**task_args)
            except TimeoutException:
                LOGGER.info(f"time out {task.get('task_name')}")
                result = None
            except:
                LOGGER.exception(f"Error occured in {task.get('task_name')}")
                result = None
            else:
                signal.alarm(0)
        else:
            result = "Current method is not supported"

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=json.dumps(
                             result, indent=4, sort_keys=True, default=str))
        ch.basic_ack(delivery_tag=method.delivery_tag)


class TimeoutException(Exception):
    pass


if __name__ == "__main__":
    task_manager = ArtmTaskManager()
    make_server(task_manager.queue_name, task_manager.on_request)
