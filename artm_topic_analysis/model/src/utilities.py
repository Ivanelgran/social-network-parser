import pickle


def load_pickle(file_name, verbose=True):
    with open(file_name, 'rb') as fp:
        data = pickle.load(fp)
    if verbose:
        print(f'{file_name} - загружен!')
    return data


def save_pickle(obj, file, verbose=True):
    with open(file, 'wb') as fp:
        pickle.dump(obj, fp)
    if verbose:
        print(f'{file} - сохранен!')