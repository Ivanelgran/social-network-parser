"""
API TEMPLATE
"""
import os
import yaml
import logging.config
import traceback
import pickle
from pathlib import Path
from utilities import enable_cors, show_ip_address, time_it
from bottle import route, run, request, response, HTTPResponse

# from model import ArtmModel
# from model_controller import build_model
from queue_manager import RpcClient

SRC_DIR = Path(__name__).resolve().parents[0]

with open(os.path.join(SRC_DIR, 'config/config.yml')) as config_file:
    CONFIG = yaml.safe_load(config_file)
    logging.config.dictConfig(CONFIG)

LOGGER = logging.getLogger('api')
LOGGER.setLevel(logging.INFO)

model = RpcClient('artm_tasks')


@route('/', method='GET')
@route('/', method='OPTIONS')
@enable_cors
@show_ip_address
@time_it
def index():
    """
    Метод показывающий что сервис работает, с выводом отладочной информации
    """
    return '<h1>API service is running!</h1>'


@route('/top', method='GET')
@enable_cors
@show_ip_address
@time_it
def top():
    """
    Метод показывающий что сервис работает, с выводом отладочной информации
    """

    top_words = model.get_top_words()

    with open("data/top_words.pkl", "bw") as f:
        pickle.dump(top_words, f)

    return '<h1>API service is running!</h1>'


@route('/predict_topics', method='POST')
def predict_topics():
    """
    Пример POST запроса с получением JSON значений

    http://127.0.0.1:8080/post
    """
    response.content_type = 'application/json'
    try:
        data = request.json

        # topics, max_topic = model.predict_topics(data)
        task = {'task_name': 'predict_topics',
                'args': {'posts': data}}
        info = model.call(task)

        if info is None:
            return HTTPResponse(status=400, body={'error': "topics is None"})

        topics, max_topic = info

        result = {'topics': topics,
                  'max_topic': max_topic}
    except Exception as ex:
        LOGGER.exception("problem ocured")
        return HTTPResponse(status=400, body={'error': str(ex)})

    return result


if __name__ == '__main__':
    API_VERSION = CONFIG['info']['api_version']
    NUM_WORKERS = CONFIG['build']['workers']
    DEBUG = CONFIG['build']['debug']

    LOGGER.info(
        f"SERVICE CONFIG: API VERSION: {API_VERSION} DEBUG MODE: {DEBUG}"
    )

    kwargs = {
        'host': '0.0.0.0',
        'port': 8053,
        'reload': True,
        'debug': DEBUG
    }

    kwargs['server'] = 'gunicorn'
    kwargs['workers'] = NUM_WORKERS
    kwargs['timeout'] = 10

    run(**kwargs)
