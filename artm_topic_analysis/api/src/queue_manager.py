import pika
from pika.exceptions import ChannelClosedByBroker
import json
import uuid
import logging
import time
import threading


class RpcClient():

    rabbit_host = "rabbit"

    def __init__(self, queue_name, logger_name=None):
        self.queue_name = queue_name
        self.credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')
        self.response = None
        self.LOGGER = logging.getLogger(logger_name or 'api.scheduler')
        self.is_ready = False

        while not self.is_ready:
            self.test_connection()
            time.sleep(5)

    def test_connection(self):
        with pika.BlockingConnection(
            pika.ConnectionParameters(host=self.rabbit_host,
                                      credentials=self.credentials)
        ) as conn:
            channel = conn.channel()
            try:
                queueResult = channel.queue_declare(queue=self.queue_name,
                                                    passive=True)
            except ChannelClosedByBroker as e:
                if e.reply_code == 405:
                    self.is_ready = True
                elif e.reply_code == 404:
                    pass
                else:
                    self.LOGGER.exception("queue does not exist")
            except:
                self.LOGGER.exception("queue does not exist")
            else:
                self.is_ready = True

    def call(self, message, json_result=False, timer=999):
        with pika.BlockingConnection(
            pika.ConnectionParameters(host=self.rabbit_host,
                                      credentials=self.credentials)
        ) as conn:
            channel = conn.channel()
            channel.basic_consume(
                'amq.rabbitmq.reply-to',
                self.on_client_rx_reply_from_server,
                auto_ack=True)
            channel.basic_publish(
                exchange='',
                routing_key=self.queue_name,
                body=json.dumps(
                    message, indent=4, sort_keys=True, default=str),
                properties=pika.BasicProperties(
                    reply_to='amq.rabbitmq.reply-to'))

            channel.start_consuming()

        if json_result:
            return self.response
        else:
            return json.loads(self.response)

    def send_message(self, message):
        with pika.BlockingConnection(
            pika.ConnectionParameters(host=self.rabbit_host,
                                      credentials=self.credentials)
        ) as conn:
            channel = conn.channel()
            channel.basic_consume(
                'amq.rabbitmq.reply-to',
                self.on_client_rx_reply_from_server,
                auto_ack=True)
            channel.basic_publish(
                exchange='',
                routing_key=self.queue_name,
                body=json.dumps(
                    message, indent=4, sort_keys=True, default=str),
                properties=pika.BasicProperties(
                    reply_to='amq.rabbitmq.reply-to'))

    def on_client_rx_reply_from_server(self, ch, _method_frame, 
                                       _properties, body):

        self.response = body
        ch.close()


def make_server(queue, callback):

    rabbit_host = "rabbit"
    credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')

    with pika.BlockingConnection(
        pika.ConnectionParameters(host=rabbit_host,
                                  credentials=credentials)
    ) as conn:
        channel = conn.channel()
        channel.queue_declare(
            queue=queue, exclusive=True, auto_delete=True)
        channel.basic_consume(queue, callback)
        channel.start_consuming()
