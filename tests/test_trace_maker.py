from pathlib import Path
import os
import pytest
from configparser import ConfigParser
from trace_maker.src.trace_maker import VkTraces, OkTraces
from datetime import datetime


@pytest.mark.traces
class TestVkTraces():

    @pytest.fixture(scope="function", autouse=True)
    def setup(self):
        CONFIG = ConfigParser()
        CONFIG.read("test_data/config.ini", encoding='utf8')

        self.auth = dict(CONFIG['VK_AUTH'])
        self.trace_data = dict(CONFIG['VK_TRACE_DATA'])
        self.sugg_data = dict(CONFIG['VK_SUGG_DATA'])
        self.vk_traces = VkTraces()

    def test_open_session(self):
        self.vk_traces.open_session(**self.auth)

    @pytest.mark.parametrize('with_file', [True, False])
    def test_add_trace_via_api(self, with_file):
        data = {**self.auth, **self.trace_data}
        data['trace'] = data['trace'] + " " + str(datetime.now())
        if not with_file:
            data['filename'] = None
        else:
            SRC_DIR = Path(__name__).resolve().parents[0]
            data['filename'] = os.path.join(SRC_DIR, data['filename'])
        trace = self.vk_traces.add_trace_via_api(data)

        assert trace is not None, "Can't add vk trace via api"

    @pytest.mark.parametrize('with_file', [True, False])
    def test_add_trace_via_selenium(self, with_file):
        data = {**self.auth, **self.trace_data}
        data['trace'] = data['trace'] + " " + str(datetime.now())
        if not with_file:
            data['filename'] = None
        else:
            SRC_DIR = Path(__name__).resolve().parents[0]
            data['filename'] = os.path.join(SRC_DIR, data['filename'])
        trace = self.vk_traces.add_trace_via_selenium(data)

        assert trace is not None, "Can't add vk trace via selenium"

    @pytest.mark.parametrize('with_file', [True, False])
    def test_add_suggestion_via_api(self, with_file):
        data = {**self.auth, **self.sugg_data}
        data['trace'] = data['trace'] + " " + str(datetime.now())
        if not with_file:
            data['filename'] = None
        else:
            SRC_DIR = Path(__name__).resolve().parents[0]
            data['filename'] = os.path.join(SRC_DIR, data['filename'])
        trace = self.vk_traces.add_suggestion_via_api(data)

        assert trace is not None, "Can't add vk sugg via api"

    @pytest.mark.parametrize('with_file', [True, False])
    def test_add_sugg_via_selenium(self, with_file):
        data = {**self.auth, **self.sugg_data}
        data['trace'] = data['trace'] + " " + str(datetime.now())
        if not with_file:
            data['filename'] = None
        else:
            SRC_DIR = Path(__name__).resolve().parents[0]
            data['filename'] = os.path.join(SRC_DIR, data['filename'])
        trace = self.vk_traces.add_suggestion_via_selenium(data)

        assert trace is not None, "Can't add vk sugg via selenium"


@pytest.mark.traces
class TestOkTraces():

    @pytest.fixture(scope="function", autouse=True)
    def setup(self):
        CONFIG = ConfigParser()
        CONFIG.read("test_data/config.ini", encoding='utf8')

        self.auth = dict(CONFIG['OK_AUTH'])
        self.trace_data = dict(CONFIG['OK_TRACE_DATA'])
        self.ok_traces = OkTraces()

    @pytest.mark.parametrize('with_file', [True, False])
    def test_add_comment_trace(self, with_file):
        data = {**self.auth, **self.trace_data}
        data['trace'] = data['trace'] + " " + str(datetime.now())
        if not with_file:
            data['filename'] = None
        else:
            SRC_DIR = Path(__name__).resolve().parents[0]
            data['filename'] = os.path.join(SRC_DIR, data['filename'])
        trace = self.ok_traces.add_comment_trace(data)

        assert trace is not None, "Can't add ok trace via selenium"


# @pytest.mark.traces
# class TestTraceMaker():

#     def test_add_comment_trace(self)
