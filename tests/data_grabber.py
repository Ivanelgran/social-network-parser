import json
from random import randint, uniform, sample

DATA_DIR = "./test_data/"


def write_data(data, filename):
    with open(f"{DATA_DIR}{filename}.json", 'w') as f:
        json.dump(data, f, indent=4, sort_keys=True, default=str)


def save_task_ids():
    data = list(range(10))
    write_data(data, 'task_ids')


def save_groups():
    data = []

    members_count = [randint(0, 100000) for _ in range(5)]
    our_prop = [uniform(0, 2) for _ in range(5)]
    posts_per_day = [uniform(0, 2) for _ in range(5)]
    views_prop = [uniform(0, 2) for _ in range(5)]
    comments = [uniform(0, 1000) for _ in range(5)]
    all_cities = list(range(1, 18))
    cities = [sample(all_cities, randint(0, 17)) for _ in range(5)]

    for i in range(5):
        data.append((members_count[i],
                     our_prop[i],
                     posts_per_day[i],
                     views_prop[i],
                     comments[i],
                     cities[i]))
    write_data(data, 'groups')


def save_user_groups():
    data = ['Егор', 'test']
    write_data(data, 'user_groups')


def save_top_words():
    data = [randint(0, 49) for _ in range(3)]
    write_data(data, 'top_words')


def save_trace_template():
    data = [randint(0, 49) for _ in range(3)]
    write_data(data, 'trace_template')


def save_top_posts():
    data = [randint(0, 49) for _ in range(3)]
    write_data(data, 'top_posts')


def save_bots_info():
    data = ['vk', 'ok']
    write_data(data, 'bots_info')


def save_new_posts():
    data = ['Егор', 'test']
    write_data(data, 'new_posts')


def save_tracked_posts():
    data = ['Егор', 'test']
    write_data(data, 'tracked_posts')


def save_add_trace_template():
    data = [(-1, 0, 'test -1 0'),
            (-2, 10, 'test -2 10')]
    write_data(data, 'add_trace_template')


def save_add_trace():
    data = [('test',
             'test_group',
             'test_post',
             'trace',
             'test_login',
             'test_pass',
             'test')]
    write_data(data, 'add_trace')


def save_add_task():
    data = [(['test1', 'test2'], 'test 1', 'test', 'test'),
            (['test3', 'test5'], 'test 2', 'test', 'test')]
    write_data(data, 'add_task')


if __name__ == "__main__":
    load_data = [save_task_ids,
                 save_groups,
                 save_user_groups,
                 save_top_words,
                 save_trace_template,
                 save_top_posts,
                 save_bots_info,
                 save_new_posts,
                 save_tracked_posts,
                 save_add_trace,
                 save_add_trace_template,
                 save_add_task]

    for i in load_data:
        i()
