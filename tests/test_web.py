import pytest
from webtest import TestApp
# import requests
# import json
# WSGIProxy2

HOST = "http://127.0.0.1:8055"
DATA_DIR = "./test_data/"


def get_from_api(method, params={}):
    app = TestApp(HOST)
    app.set_cookie('user', 'test')

    resp = app.get(method, params=params)

    return resp


# GET-запросы

@pytest.mark.smoke
def test_web_work():
    method = "/"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_groups_tasks():
    method = "/groups-tasks"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_groups():
    method = "/groups"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_visual():
    method = "/visual"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_topics():
    method = "/topics"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_posts_monitor():
    method = "/posts-monitor"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_tracked_posts():
    method = "/tracked-posts"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_traces():
    method = "/traces"
    resp = get_from_api(method)
    assert resp.status_int == 200


@pytest.mark.smoke
def test_web_bots():
    method = "/bots"
    resp = get_from_api(method)
    assert resp.status_int == 200
