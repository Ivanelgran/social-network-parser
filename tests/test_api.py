import pytest
# from webtest import TestApp
import requests
import json
# WSGIProxy2

HOST = "http://127.0.0.1:8054"
DATA_DIR = "./test_data/"


# def get_from_api(method, params={}):
#     app = TestApp(HOST)
#     resp = app.get(method, params=params)

#     return resp

def get_from_api(method, params={}):
    response = requests.get(f"{HOST}{method}",
                            params={'params': json.dumps(params)})

    return response


def put_to_api(method, data={}):
    response = requests.put(f"{HOST}{method}", json=data)

    return response


def post_to_api(method, data={}):
    response = requests.post(f"{HOST}{method}", json=data)

    return response


def load_data(filename):
    with open(f"{DATA_DIR}{filename}.json") as f:
        data = json.load(f)

    return data


# GET-запросы

@pytest.mark.smoke
def test_api_work():
    method = "/"
    resp = get_from_api(method)
    assert resp.status_code == 200


@pytest.mark.smoke
def test_tasks():
    method = "/tasks"
    resp = get_from_api(method)
    assert resp.status_code == 200


@pytest.mark.smoke
def test_unique_groups_count():
    method = "/unique-groups-count"
    resp = get_from_api(method)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("task_id", load_data('task_ids'))
def test_task_groups(task_id):
    method = "/task-groups"
    params = {'task-id': task_id}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("members_count,our_prop,posts_per_day,"
                         + "views_prop,comments,cities", load_data('groups'))
def test_groups(members_count,
                our_prop,
                posts_per_day,
                views_prop,
                comments,
                cities):
    method = "/groups"
    params = {'members-count': members_count,
              'our-prop': our_prop,
              'posts-per-day': posts_per_day,
              'views-prop': views_prop,
              'comments': comments,
              'cities': cities}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
def test_parsed_groups():
    method = "/parsed-groups"
    resp = get_from_api(method)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("username", load_data('user_groups'))
def test_user_groups(username):
    method = "/user-groups"
    params = {'username': username}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
def test_visual():
    method = "/visual"
    params = {'topics_states': json.dumps({})}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("topic", load_data('top_words'))
def test_top_words(topic):
    method = "/top-words"
    params = {'topic': topic}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("topic", load_data('trace_template'))
def test_trace_template(topic):
    method = "/trace-template"
    params = {'topic': topic}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("topic", load_data('top_posts'))
def test_top_posts(topic):
    method = "/top-posts"
    params = {'topic': topic}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("social", load_data('bots_info'))
def test_bots_info(social):
    method = "/bots-info"
    params = {'social': social}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("username", load_data('new_posts'))
def test_new_posts(username):
    method = "/new-posts"
    params = {'username': username,
              'topics_states': {}}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize("username", load_data('tracked_posts'))
def test_tracked_posts(username):
    method = "/tracked-posts"
    params = {'username': username}
    resp = get_from_api(method, params)
    assert resp.status_code == 200


@pytest.mark.smoke
def test_traces():
    method = "/traces"
    resp = get_from_api(method)
    assert resp.status_code == 200


# PUT-запросы

@pytest.mark.smoke
@pytest.mark.parametrize("level,topic,new_trace",
                         load_data('add_trace_template'))
def test_add_trace_template(level, topic, new_trace):
    method = "/add-trace-template"
    data = {'level': level,
            'topic': topic,
            'new_trace': new_trace}
    resp = put_to_api(method, data)
    assert resp.status_code == 200


@pytest.mark.smoke
@pytest.mark.parametrize(
    "social,owner_id,post_id,trace,login,password,author",
    load_data('add_trace'))
def test_add_trace(social,
                   owner_id,
                   post_id,
                   trace,
                   login,
                   password,
                   author):
    method = "/add-trace"
    data = {'social': social,
            'owner_id': owner_id,
            'post_id': post_id,
            'trace': trace,
            'login': login,
            'password': password,
            'author': author}
    resp = put_to_api(method, data)
    assert resp.status_code == 200


# POST-запросы
@pytest.mark.smoke
@pytest.mark.parametrize("groups,label,username,social",
                         load_data('add_task'))
def test_add_task(groups, label, username, social):
    method = "/add-task"
    data = {'groups': groups,
            'label': label,
            'username': username,
            'social': social}
    resp = post_to_api(method, data)
    assert resp.status_code == 200


def test_f():
    with pytest.raises(ZeroDivisionError):
        1 / 0
