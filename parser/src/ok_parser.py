import logging
from datetime import datetime, timedelta
import re
import random
import time
from bs4 import BeautifulSoup as bs
from fake_useragent import UserAgent
from ip_controller import get_page
from functools import partial

import requests
from selenium import webdriver


class OkParser():
    """
    """

    ok_months = {'января': 1,
                 'февраля': 2,
                 'марта': 3,
                 'апреля': 4,
                 'мая': 5,
                 'июня': 6,
                 'июля': 7,
                 'августа': 8,
                 'сентября': 9,
                 'октября': 10,
                 'ноября': 11,
                 'декабря': 12}

    def __init__(self, bot_login=None, bot_password=None, logger=None):
        # self.LOGGER = logging.getLogger("parser.ok_parser")
        self.LOGGER = logger or logging.getLogger("parser.ok_parser")
        self.ua = UserAgent()
        self.limit = False
        self.cookies = ''

        self.bot_login = bot_login or 'ivanelgran'
        self.bot_password = bot_password or 'insta2019'

    def get_group_info(self, group_url, ua=None):
        self.LOGGER.info(f"Parse info for {group_url}")
        result = {}
        if ua is None:
            ua = self.ua.random
        html = get_page(group_url, user_agent=ua)
        bs_obj = bs(html.decode("utf-8-sig"), "lxml")

        match = re.search(r'groupId=(\d+)', str(bs_obj))
        owner_id = match.groups()[0]

        try:
            name = bs_obj.find('h1').get_text()
        except Exception:
            # self.LOGGER.exception(group_url)
            ua = self.ua.random
            html = get_page(group_url, user_agent=ua)
            bs_obj = bs(html.decode("utf-8-sig"), "lxml")
            name = bs_obj.find('h1').get_text()

        members_count = bs_obj.find('span', {'id': 'groupMembersCntEl'})
        members_count = members_count.get_text().replace('\xa0', '')
        members_count = int(members_count)

        result['owner_id'] = owner_id
        result['name'] = name
        result['members_count'] = members_count
        self.LOGGER.info(f"Info is ready")

        return result

    # Работает только если авторизоваться
    def get_post_stat(self, owner_id, post_id):

        post = {"owner_id": owner_id,
                "post_id": post_id}

        if not self.cookies:
            cookies = self.get_cookies(self.bot_login,
                                        self.bot_password)
        headers = self.create_headers(cookies, uir=True)
        html = requests.get('https://ok.ru/group/' + str(owner_id) + '/topic/' + str(post_id), headers=headers).content
        bs_obj = bs(html.decode("utf-8-sig"), "lxml")

        post = bs_obj.find("div", {"data-tid": post_id})
        stats = post.find_all('span', {'class': ['widget_count']})
        post['replies'] = int(stats[0].get_text().replace(u'\xa0', u''))
        post['shares'] = int(stats[1].get_text().replace(u'\xa0', u''))
        try:
            post['likes'] = int(stats[2].get_text().replace(u'\xa0', u''))
        except Exception:
            self.LOGGER.exception(stats[2].get_text())
            raise BaseException
        post['views'] = -1

        return post

    def get_last_post(self, group):
        url = f"https://ok.ru/group/{group}/topics"

        raw_posts = self.get_posts_from_url(url)

        if raw_posts is None or not len(raw_posts):
            # self.LOGGER.warning("Raw posts is None")
            return

        get_post_data = partial(self.get_post_data,
                                owner_id=group,
                                date_limit=None,
                                last_saved_post=None)

        posts = [*filter(lambda x: x is not None, map(get_post_data, raw_posts))]
        posts = sorted(posts, key=lambda x: x['date'], reverse=True)

        data = posts[0] if len(posts) else None

        return data

    def get_posts(self,
                  group,
                  date_limit: datetime = None,
                  last_saved_post: int = None,
                  request_delay: float = 0):
        all_posts = []
        all_attachments = []
        page = 2
        self.limit = False

        url = f"https://ok.ru/group/{group}/topics"

        raw_posts = self.get_posts_from_url(url)

        if raw_posts is None:
            # self.LOGGER.warning("Raw posts are None")
            return

        posts, atts = self.parse_posts(group, raw_posts, date_limit,
                                       last_saved_post)

        if len(posts) == 0:
            # self.LOGGER.warning("len(posts) == 0")
            return [], []

        all_posts.extend(posts)
        all_attachments.extend(atts)

        while True:

            if self.limit:
                break

            url = f"https://ok.ru/dk?cmd=AltGroupForumNewRB&st.gid={group}&st.cmd=anonymGroupForum&st.gid={group}&st.actualtopics=off"
            data = {'fetch': False,
                    'st.page': page,
                    'st.lastelem': all_posts[-1]['post_id'],
                    'gwt.requested': 'e162f958T1555411262241'}

            raw_posts = self.get_posts_from_url(url, data)

            if raw_posts is None:
                continue

            page += 1

            posts, atts = self.parse_posts(group, raw_posts, date_limit,
                                           last_saved_post)

            all_posts.extend(posts)
            all_attachments.extend(atts)

        delay = random.uniform(0.5 * request_delay, 1.5 * request_delay)
        time.sleep(delay)
        self.limit = False

        return all_posts, all_attachments

    def get_posts_from_url(self, url, data=None):
        counts = 10
        posts = None
        while counts > 0:
            ua = self.ua.random
            html = get_page(url, user_agent=ua, post_data=data)
            bs_obj = bs(html.decode("utf-8-sig"), "lxml")

            private = bs_obj.find('span', {'class': 'mctc_private'})
            if private is not None:
                # print(f"{url} is private")
                return

            posts = bs_obj.find_all("div", {'class': 'groups_post-w'})

            if len(posts) == 0:
                counts -= 1
                continue

            break

        if len(posts) == 0:
            # print("Reached limit")
            # with open(f'error.txt', 'w') as f:
            #     f.write(bs_obj.prettify())
            self.limit = True

        if len(posts) == 0:
            self.limit = True

        return posts

    def parse_posts(self, group, posts, date_limit, last_saved_post):
        all_posts = []
        all_attachments = []

        for post in posts:
            try:
                data = self.get_post_data(post, group, date_limit,
                                          last_saved_post)
            except Exception:
                self.LOGGER.exception(f"{group} {post}")
                data = None
            if data is None:
                continue
            atts = self.get_post_atts(post, group)

            all_posts.append(data)
            if atts is not None:
                all_attachments.extend(atts)

        return all_posts, all_attachments

    def get_post_data(self, post_info, owner_id, date_limit, last_saved_post):

        if self.limit:
            # self.LOGGER.warning("Limit reached")
            return

        promo = post_info.find('div', {'class': 'feed_label'})
        if promo is not None and promo.get_text().lower() == 'промо' or post_info.find('div', {'class': 'feed_ac'}).find('div', {'class': 'feed_pin'}) is not None:
            # self.LOGGER.warning("IS PROMO OR PINNED")
            return
        post = {}
        post['owner_id'] = owner_id
        post_id = post_info.find('a', {'class': 'widget_cnt'})['data-id']
        post['post_id'] = post_id
        post['copy_owner_id'] = post['copy_post_id'] = ""

        date = post_info.find('div', {'class': 'feed_date'}).get_text()
        date = self.parse_to_datetime(date)
        post['date'] = date

        if (last_saved_post is not None and int(post_id) <= int(last_saved_post)) \
                or (date_limit is not None and date < date_limit):
            # print(f"{post['post_id']} {date} {date_limit}")
            self.limit = True
            # self.LOGGER.warning("last_saved_post or date_limit reached")
            return

        stats = post_info.find_all('span', {'class': ['widget_count']})
        post['replies'] = int(stats[0].get_text().replace(u'\xa0', u''))
        post['shares'] = int(stats[1].get_text().replace(u'\xa0', u''))
        try:
            post['likes'] = int(stats[2].get_text().replace(u'\xa0', u''))
        except Exception:
            self.LOGGER.exception(stats[2].get_text())
            raise BaseException
        post['views'] = -1

        post['post_type'] = 'post'
        text = []
        if post_info.find('div', {'class': 'media_more'}) is not None:
            url = f"https://ok.ru/group/{owner_id}/topic/{post['post_id']}"
            ua = self.ua.random

            html = get_page(url, user_agent=ua)

            bs_obj = bs(html.decode("utf-8-sig"), "lxml")
            post_info = bs_obj.find('div', {'id': f"cnts_{post['post_id']}"}) or post_info

        media = post_info.find_all('div', {'class': 'media-block'})
        for m in media:
            if 'media-shared__v2' in m['class']:
                post['post_type'] = 'repost'
                try:
                    info = m.find('a', {'class': 'media-link_a'})['href'].split('/')
                    if info[1] == 'profile' or info[1] == 'group':
                        copy_owner_id = info[2]
                        copy_post_id = info[4]
                    elif info[1].startswith('group'):
                        copy_owner_id = info[1][5:]
                        copy_post_id = info[3]
                    else:
                        group_url = f"https://ok.ru/{info[1]}"
                        group_info = self.get_group_info(group_url)
                        copy_owner_id = group_info['owner_id']
                        copy_post_id = info[3]
                    post['copy_owner_id'] = copy_owner_id
                    post['copy_post_id'] = copy_post_id
                except Exception:
                    pass
            if 'media-text' in m['class']:
                for a in m.findAll("a"):
                    a.extract()
                text.append(m.get_text(separator='\n').replace('\ufeff', '').replace('\xa0', ''))
        post['text'] = ' '.join(text)

        return post

    def get_post_atts(self, post_info, owner_id):
        atts = []
        post_id = post_info.find('a', {'class': 'widget_cnt'})['data-id']
        media = post_info.find_all('div', {'class': 'media-block'})
        counter = 1
        for m in media:
            data = None
            if 'media-photos' in m['class']:
                data = self.parse_image(m)
            elif 'media-video' in m['class']:
                data = self.parse_video(m)
            elif 'media-music' in m['class']:
                data = self.parse_music(m)
            if data is not None:
                for att in data:
                    att['attachment_id'] = counter
                    att['owner_id'] = owner_id
                    att['post_id'] = post_id
                    counter += 1
                    atts.append(att)

        return atts

    def parse_image(self, media):
        result = []
        images = media.find_all('img')
        att_type = "image"
        header = ""
        for img in images:
            link = "https:" + img['src']
            result.append({'type': att_type,
                           'header': header,
                           'link': link})

        return result

    def parse_video(self, media):
        result = []
        link = ""
        att_type = "video"
        headers = media.find_all('div', {'class': 'video-card_n-w'})
        for h in headers:
            try:
                header = h.find('a')['title']
                result.append({'type': att_type,
                               'header': header,
                               'link': link})
            except AttributeError:
                header = "unknown"
                self.LOGGER.exception(media.prettify())

        return result

    def parse_music(self, media):
        result = []
        link = ""
        att_type = "audio"
        audios = media.find_all('div', {'class': 'track_cnt'})
        for music in audios:
            headers = music.find_all('a')
            header = headers[0].get_text() + " - " + headers[1].get_text()
            result.append({'type': att_type,
                           'header': header,
                           'link': link})

        return result

    def parse_to_datetime(self, date):
        today = datetime.now()
        solit_date = date.split()

        if solit_date[1] == "сегодня":
            year = today.year
            month = today.month
            day = today.day
            time = re.findall(r'\d{1,2}:\d\d', date)[0].split(":")
            hour = int(time[0])
            minute = int(time[1])
            second = 0
        elif solit_date[1] == "вчера":
            yesterday = today - timedelta(1)
            year = yesterday.year
            month = yesterday.month
            day = yesterday.day
            time = re.findall(r'\d{1,2}:\d\d', date)[0].split(":")
            hour = int(time[0])
            minute = int(time[1])
            second = 0
        elif len(solit_date[3]) == 4:
            year = int(solit_date[3])
            month = self.ok_months[solit_date[2]]
            day = int(solit_date[1])
            time = re.findall(r'\d{1,2}:\d\d', date)[0].split(":")
            hour = int(time[0])
            minute = int(time[1])
            second = 0
        elif solit_date[3] == 'в':
            year = today.year
            month = self.ok_months[solit_date[2]]
            day = int(solit_date[1])
            time = re.findall(r'\d{1,2}:\d\d', date)[0].split(":")
            hour = int(time[0])
            minute = int(time[1])
            second = 0

        result = datetime(int(year), int(month), int(day), int(hour),
                          int(minute), int(second))
        result += timedelta(hours=2)
        if result > today:
            self.LOGGER.info(f"Incorrect date: {result} -- {today}")
            result = result - timedelta(days=1)

        return result

    def get_post_replies(self, owner_id, post_id):
        for i in range(0, 2):
            try:
                token = None
                # replies_data = {}
                replies = []
                if not self.cookies:
                    cookies = self.get_cookies(self.bot_login,
                                               self.bot_password)
                headers = self.create_headers(cookies, uir=True)
                try:
                    if owner_id.isdigit():
                        r = requests.get('https://ok.ru/group/' + str(owner_id) + '/topic/' + str(post_id), headers=headers)
                    else:
                        r = requests.get('https://ok.ru/' + str(owner_id) + '/topic/' + str(post_id), headers=headers)
                    p = re.compile(r'gwtHash:"\w+')
                    k = re.compile(r'OK.tkn.set\w+')
                    data = bs(r.text, 'html.parser')
                    for script in data.find_all("script"):
                        if script:  
                            m = re.search(r'gwtHash:"\w+', str(script.string))
                            if m:
                                gwt = m.group(0)[9:]
                            v = re.search(r'OK\.tkn\.set\(\'\S+\'\)', str(script))
                            if v:
                                token = v.group(0)[12:-2]
                            k = re.search(r'st.groupId=\w+', str(script))
                            if k:
                                groupId = k.group(0)[11:]
                    if token is None:
                        cookies = self.get_cookies(self.bot_login,
                                                   self.bot_password)
                        continue
                    headers = self.create_headers(cookies, con_len='36',
                                                  token=token)
                    request_data = self.create_formdata(post_id, is_simple=False, gwt=gwt)
                    url = 'https://ok.ru/dk?st.cmd=altGroupForum&st.groupId=' + str(groupId) + '&cmd=MediaTopicLayerBody'
                    r = requests.post(url, headers=headers, data=request_data)
                    data = bs(r.text, 'html.parser')
                    # total_replies = data.find("a", {"data-module": "CommentWidgets"}).find("span", {"class": "widget_count"}).get_text()

                    url = 'https://ok.ru/group/' + str(groupId) + '/topic/' + str(post_id) + '?cmd=GroupDiscussion&gwt.requested=' + str(gwt) + '&st.cmd=altGroupForum&st.groupId=' + str(groupId) + '&st.page=1&st.fwd=off&fetch=true&gwt.requested=' + str(gwt)
                    headers = self.create_headers(cookies,
                                                  con_len='65',
                                                  token=token,
                                                  ocah='ExtLoader_GoTopClick')
                    request_data = self.create_formdata(post_id)
                    r = requests.post(url, headers=headers, data=request_data)
                    data = bs(r.text, 'html.parser')
                    try:
                        last_elem = r.headers['lastelem']
                    except Exception:
                        return replies
                    replies = self.replies_handler(data, groupId, post_id)

                    for i in range(2, 42):
                        url = 'https://ok.ru/group/' + str(groupId) + '/topic/' + str(post_id) + '?cmd=GroupDiscussion&gwt.requested=' + str(gwt) + '&st.cmd=altGroupForum&st.groupId=' + str(groupId) + '&st.page=' + str(i) +'&st.fwd=on&st.lastelem=' + str(last_elem) + '&fetch=true&gwt.requested=' + str(gwt)
                        headers = self.create_headers(
                            cookies,
                            con_len='65',
                            token=token,
                            ocah='ExtLoader_MoreClick')
                        request_data = self.create_formdata(post_id)
                        r = requests.post(url, headers=headers,
                                          data=request_data)
                        data = bs(r.text, 'html.parser')
                        try:
                            last_elem = r.headers['lastelem']
                        except Exception:
                            return replies
                        last_replies = self.replies_handler(data, groupId,
                                                            post_id)
                        replies = replies + last_replies

                    return replies
                except Exception:
                    self.LOGGER.exception("")
                    return []
            except Exception:
                self.LOGGER.exception("")
                return []
            break

    def get_driver(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        driver = webdriver.Chrome(executable_path='data/chromedriver',
                                  chrome_options=options)

        self.LOGGER.info(f'driver pid: {driver.service.process.pid}')
        return driver

    def get_cookies(self, login, password):
        pass
        # driver = None

        # try:
        #     driver = self.get_driver()
        #     driver.get('https://www.ok.ru')
        #     email = driver.find_element_by_name('st.email')
        #     email.send_keys(login)
        #     passwd = driver.find_element_by_name('st.password')
        #     passwd.send_keys(password)
        #     login = driver.find_element_by_xpath("//input[@value='Войти']")
        #     login.click()
        #     raw_cookies = driver.get_cookies()
        #     cookies = {}
        #     for cookie_value in raw_cookies:
        #         name = cookie_value['name']
        #         value = cookie_value['value']
        #         cookies[name] = value
        #     cookies = ', '.join("{!s}={!r}".format(key, val) for (key, val) in cookies.items())

        #         driver.quit()

        #     return cookies.replace("'", "")
        # except:
        #     if driver is not None:
        #         driver.quit()

    @staticmethod
    def create_headers(cookies, uir=False, con_len=None,
                       ocah=None, token=None):
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            # 'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,ru;q=0.8',
            'content-type': 'application/x-www-form-urlencoded',
            'cookie': cookies,
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
        }
        if uir:
            headers['upgrade-insecure-requests'] = '1'
        else:
            if con_len == '65':
                headers['ocah'] = ocah
            headers['accept'] = '*/*'
            headers['content-length'] = str(con_len)
            headers['origin'] = 'https://ok.ru'
            headers['referer'] = 'https://ok.ru/'
            headers['tkn'] = str(token)

        return headers

    @staticmethod
    def create_formdata(post_id, is_simple=True, gwt=None):
        request_data = {
                'st.did': str(post_id),
                'st.dtype': 'GROUP_TOPIC',
                'st.ctx': 'MediaTopicLayer'
            }
        if not is_simple:
            request_data = {
                'gwt.requested': str(gwt),
                'st.mt.id': str(post_id),
                'st.mt.ot': 'GROUP_THEME',
                'st.mt.wc': 'off',
                'st.mt.hn': 'off',
                'st.mt.ad': 'off',
                'st.mt.bi': '0'
            }

        return request_data

    @staticmethod
    def replies_handler(data, groupId, post_id):
        replies_data = {}
        replies = []
        for human in data.find_all('div', class_='comments_i'):
            timestamp = human['data-time'][:-3]
            dt_object = datetime.fromtimestamp(int(timestamp))
            replies_data['social'] = 'ok'
            replies_data['owner_id'] = groupId
            replies_data['post_id'] = post_id
            replies_data['reply_id'] = str(human['data-cid'])
            replies_data['author'] = str(human.find('a', class_='ucard_img_a')['href'][9:].split('?')[0])
            if not replies_data['author'].isdigit():
                profile = human.find('a', class_='ucard_img_a')['href']
                g = re.search(r'st.friendId=\w+', str(profile))
                if g:
                    replies_data['author'] = g.group(0)[12:]
                else:
                    f = re.search(r'st.referenceName=\w+', str(profile))
                    replies_data['author'] = f.group(0)[17:]

            replies_data['date'] = dt_object
            try:
                replies_data['likes'] = human.find('a', {"class": 'controls-list_counter'}).get_text()
            except Exception:
                replies_data['likes'] = '0'
            replies_data['parent_reply'] = ''
            replies_data['sticker'] = ''
            replies_data['text'] = human.find('div', class_='comments_text textWrap').find('div').get_text()
            replies.append(replies_data.copy())
        return replies
