from datetime import datetime, timedelta
from functools import partial
import time

import pandas as pd

from common_lib.cass_worker import (
    CassandraTables, setup_connection, select_data
)
from common_lib.src.neo4j_driver import Models, select_with_filters
from common_lib.src.rabbit import TaskManager


class Producer():

    def __init__(self, time_period, logger=None):
        self.conn = setup_connection()
        self.time_period = time_period
        self.logger = logger

    def _add_task(self, task_name, args=None):
        task_manager = TaskManager(self.queue_name)
        task_manager.run_task(self.task_name, args)

    def _make_tasks(self):
        raise NotImplementedError

    def run(self):
        self.logger.info((f"Start producer {self.task_name} "
                          f"with time_period: {self.time_period}"))
        while True:
            self._make_tasks()
            time.sleep(self.time_period)


class LastPosts(Producer):
    """Обновляет посты групп из активных заданий"""

    queue_name = "parser_middle_pr"
    task_name = "last_posts"

    def _make_tasks(self):
        active_tasks = select_data(table=CassandraTables.tasks,
                                   filters={'is_active': True},
                                   conn=self.conn)

        if not len(active_tasks):
            return

        tasks = [task['task_id'] for task in active_tasks]
        groups = select_data(table=CassandraTables.task_groups,
                             filters={'task_id__in': tasks},
                             conn=self.conn)

        if not len(groups):
            return

        all_groups = set()
        for group in groups:
            social = group['social']
            group_id = group['group_id']
            if group_id in all_groups:
                continue

            all_groups.add(group_id)
            args = {'social': social, 'owner_id': group_id}
            self._add_task(self.task_name, args)


class LastStat(Producer):
    """Обновляет статистику постов за последние сутки"""

    queue_name = "parser_middle_pr"
    task_name = "stat"

    def _make_tasks(self):
        last_date = datetime.now() - timedelta(days=1)
        last_posts = select_data(table=CassandraTables.posts_dates,
                                 filters={'date__gte': last_date},
                                 conn=self.conn)

        for post in last_posts:
            args = {'social': post['social'],
                    'owner_id': post['owner_id'],
                    'post_id': post['post_id'],
                    'with_replies': True}
            self._add_task(self.task_name, args)


class Traces(Producer):
    """Обновляет комментарии в постах со следами"""

    queue_name = "parser_high_pr"
    task_name = "replies"

    def _make_tasks(self):
        last_date = datetime.now() - timedelta(days=1)
        traces = select_data(table=CassandraTables._traces_by_date,
                             filters={'date__gte': last_date,
                                      'status': 'completed'},
                             conn=self.conn)

        for trace in traces:
            args = {'social': trace['social'],
                    'owner_id': trace['owner_id'],
                    'post_id': trace['post_id']}
            self._add_task(self.task_name, args)


class Suggestions(Producer):
    """Обновляет группы, где могут быть опубликованы вбросы"""

    queue_name = "parser_high_pr"
    task_name = "last_posts"

    def _make_tasks(self):
        last_date = datetime.now() - timedelta(days=1)
        suggestions = select_data(table=CassandraTables.suggestions,
                                  filters={'date__gte': last_date},
                                  conn=self.conn)

        if not len(suggestions):
            return

        dates = [sugg['date'] for sugg in suggestions]

        groups = select_data(table=CassandraTables.suggestion_groups,
                             filters={'date__in': dates},
                             conn=self.conn)

        groups = list(filter(lambda x: x['state'] == "completed", groups))

        for group in groups:
            args = {'social': group['social'],
                    'owner_id': group['owner_id']}
            self._add_task(self.task_name, args)


class TrackedPosts(Producer):
    """Обновляет статистику отслеживаемых постов"""

    queue_name = "parser_high_pr"
    task_name = "stat"

    def _make_tasks(self):
        monitoring_posts = select_data(table=CassandraTables.posts_monitoring,
                                       conn=self.conn)

        for post in monitoring_posts:
            args = {'social': post['social'],
                    'owner_id': post['owner_id'],
                    'post_id': post['post_id'],
                    'with_replies': True}
            self._add_task(self.task_name, args)


class OurGroups(Producer):
    """Массовый парсинг наших групп"""

    queue_name = "parser_low_pr"
    task_name = "last_posts"

    def ready_to_parse(self, group):
        try:
            last_date = group.last_date
            last_date_ts = last_date.timestamp()
            last_update = group.last_update
        except AttributeError:
            return True

        if last_date is None or last_update is None or not last_date_ts:
            return True

        seconds_per_day = 60*60*24
        now = datetime.today().replace(tzinfo=None)
        last_date = last_date.replace(tzinfo=None)
        last_update = last_update.replace(tzinfo=None)

        if last_date is None or pd.isnull(last_date):
            seconds_have_passed = seconds_per_day*183
        else:
            seconds_have_passed = int((now-last_date).total_seconds())
        since_last_update = int((now-last_update).total_seconds())

        if seconds_have_passed <= seconds_per_day:
            return True
        elif seconds_have_passed <= seconds_per_day*7:
            return since_last_update > seconds_per_day
        elif seconds_have_passed <= seconds_per_day*30:
            return since_last_update > seconds_per_day*7
        elif seconds_have_passed <= seconds_per_day*182:
            return since_last_update > seconds_per_day*14
        else:
            return since_last_update > seconds_per_day*30

    def get_groups(self):
        filters = {'our_prop__gte': 0.2,
                   'members_count__gte': 50}

        return select_with_filters(Models.group, filters)

    def build_args(self, group):
        return {'social': group.social,
                'owner_id': group.group_id}

    def _make_tasks(self):
        self.logger.info("Start our groups")
        groups = self.get_groups()
        add_task = partial(self._add_task, self.task_name)
        args = [*map(self.build_args, filter(self.ready_to_parse, groups))]
        self.logger.info(f"Groups count: {len(args)}")

        return [*map(add_task, args)]
