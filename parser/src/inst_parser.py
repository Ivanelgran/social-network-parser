# -*- coding: utf8 -*-
import json
import logging
import multiprocessing
import time
import traceback
from datetime import datetime
from urllib import request

import gc
import requests
from bs4 import BeautifulSoup as bs
from instagram.agents import *
from instagram.entities import Tag, Account
from math import ceil
from proxy_requests import ProxyRequests


class InstParser:
    __slots__ = ['count', 'v', 'f', 'agent', 'media', 'pointer', 'links', 'procs',
                 'now', 'inst_tag', 'LOGGER', 'post_data', 'attach_data', 'liq_dict', 'result']


    def __init__(self, logger=None):
        self.LOGGER = logger or logging.getLogger('Parser.inst_parser')
        self.count = 0
        self.v = 0
        self.media = {}
        self.pointer = {}
        self.links = []
        self.procs = []
        self.now = None

    def get_group_info(self, group_url):
        result = {}
        agent = WebAgent()
        try:
            str(group_url)
            index = group_url.find('https://www.instagram.com/')
            if index == -1:
                return 0
            group_url = group_url[26:]
            group_url = group_url.replace("/", "")
            account = Account(group_url)
            agent.update(account)
            result['owner_id'] = account.username
            result['name'] = account.full_name
            result['members_count'] = account.followers_count
            self.LOGGER.info(f"Info is ready")
            del agent
            del account

            return result
        except:
            return None

    def get_last_post(self, group: str):
        pass

    def get_posts(self, group, date_limit):
        manager = multiprocessing.Manager()
        self.post_data = manager.list()
        self.attach_data = manager.list()
        self.agent = WebAgent()
        self.inst_tag = Account(group)
        try:
            self.proc_create(i=1, post_count=50, inst_date=date_limit, array_parts=5)
            self.pointer.clear()
            del self.agent
            del self.inst_tag
            return list(self.post_data), list(self.attach_data)
        except Exception as e:
            self.LOGGER.warning(e)
            traceback.print_exc()
            del self.agent
            del self.inst_tag
            return None

    def get_posts_by_keyword(self, keyword, date_limit):
        manager = multiprocessing.Manager()
        self.post_data = manager.list()
        self.attach_data = manager.list()
        self.agent = WebAgent()
        # keyword = keyword.encode('utf-8')
        keyword = str(keyword).replace("#", "")
        keyword = request.quote(keyword)
        self.inst_tag = Tag(keyword)
        try:
            self.proc_create(i=1, post_count=1300, inst_date=date_limit, array_parts=25)
            self.pointer.clear()
            del self.agent
            del self.inst_tag
            return list(self.post_data), list(self.attach_data)
        except Exception as e:
            self.LOGGER.warning(e)
            traceback.print_exc()
            print(traceback.print_exc())
            del self.agent
            del self.inst_tag
            return None

    def proc_create(self, i, post_count, inst_date=None, array_parts=25):
        del self.links[:]
        self.media.clear()
        try:
            self.media[i], self.pointer[i] = self.agent.get_media(self.inst_tag, count=post_count, delay=1)
            for shortcode in self.media[i]:
                self.links.append('https://www.instagram.com/p/' + str(shortcode))
        except:
            time.sleep(10)
            self.media[i], self.pointer[i] = self.agent.get_media(self.inst_tag, count=post_count, delay=1)
            for shortcode in self.media[i]:
                self.links.append('https://www.instagram.com/p/' + str(shortcode))
        parts = int(len(self.links) / array_parts)
        if parts == 0:
            if array_parts == 25:
                parts = 52
            else:
                parts = 10
        self.links = self.parting(self.links, parts)
        del self.procs[:]
        self.LOGGER.info(f"CREATING PROCESS FOR READING INST DATA")
        for g in range(parts):
            if inst_date is None:
                proc = multiprocessing.Process(target=self.get_data, args=(self.links[g], g))
                self.procs.append(proc)
                proc.start()
            else:
                proc = multiprocessing.Process(target=self.get_data_by_date, args=(self.links[g], g, inst_date))
                self.procs.append(proc)
                proc.start()
        self.LOGGER.info(f"READY TO GET INST DATA")
        for proc in self.procs:
            proc.join()
            proc.terminate()
            gc.collect()
        self.LOGGER.info(f"END OF INST DATA PROCESSING")

    def parting(self, xs, parts):
        part_len = ceil(len(xs) / parts)
        return [xs[part_len * k:part_len * (k + 1)] for k in range(parts)]

    def get_data(self, linkss, g):
        print('Обновляю посты по количеству')
        db = []
        dates = []
        replies = []
        attach = []
        date_data = {}
        attach_data = {}
        replies_data = {}
        db_data = {}
        k = {g: 1}

        for i in range(len(linkss)):
            try:
                try:
                    headers = {"Connection": "close"}
                    r = requests.Session()
                    s = r.get(linkss[i], headers=headers)
                    page = s.content
                    r.keep_alive = False
                except Exception as e:
                    print(e)
                    r = ProxyRequests(linkss[i])
                    r.get()
                    page = r.get_raw().decode()
                self.count = self.count + 1
                data = bs(page, 'html.parser')
                body = data.find('body')
                script = body.find('script')
                raw = script.text.strip().replace('window._sharedData =', '').replace(';', '')
                json_data = json.loads(raw)
                posts = json_data['entry_data']['PostPage'][0]['graphql']['shortcode_media']
                posts = json.dumps(posts)
                posts = json.loads(posts)

                posts['owner'] = posts['owner']['username']
                # TODO: Переделать комментарии
                try:
                    date = datetime.fromtimestamp(
                        int(posts['edge_media_to_parent_comment']['edges'][0]['node']['created_at']))
                    # if self.now.date() == date.date():
                    #     pass
                    if posts['edge_media_to_parent_comment']['edges'][0]['node']['text'] is not None:
                        a = posts['edge_media_to_parent_comment']['edges']
                        for _ in a:
                            replies_data['owner_id'] = str(posts['owner'])
                            replies_data['post_id'] = str(posts['shortcode'])
                            replies_data['reply_id'] = str(_['node']['id'])
                            replies_data['author'] = str(_['node']['owner']['username'])
                            replies_data['date'] = date
                            replies_data['text'] = str(_['node']['text'])
                            replies_data['sticker'] = ' '
                            replies_data['replies'] = 0
                            replies_data['parent_reply'] = ' '
                            replies_data['likes'] = int(_['node']['edge_liked_by']['count'])
                            replies.append(replies_data)
                except Exception as e:
                    pass

                images = ''
                str(images)
                if str(posts['is_video']) == "True":
                    posts['display_url'] = str(posts['display_url']) + str(posts['video_url'])
                    try:
                        attach_data['owner_id'] = str(posts['owner'])
                        attach_data['post_id'] = str(posts['shortcode'])
                        attach_data['attachment_id'] = 1
                        attach_data['header'] = ''
                        attach_data['link'] = str(posts['video_url'])
                        attach_data['type'] = 'video'
                        attach.append(attach_data)
                    except:
                        print('Ошибка добавления данных в базу post_attachments')
                else:
                    try:
                        if posts['edge_sidecar_to_children']['edges'] is not None:
                            try:
                                i = 0
                                b = posts['edge_sidecar_to_children']['edges']
                                for _ in b:
                                    i = i + 1
                                    attach_data['owner_id'] = str(posts['owner'])
                                    attach_data['post_id'] = str(posts['shortcode'])
                                    attach_data['attachment_id'] = int(i)
                                    attach_data['header'] = ''
                                    attach_data['link'] = str(_['node']['display_url'])
                                    attach_data['type'] = 'image'
                                    attach.append(attach_data)
                            except:
                                print('Ошибка добавления данных в базу post_attachments')
                    except:
                        try:
                            attach_data['owner_id'] = str(posts['owner'])
                            attach_data['post_id'] = str(posts['shortcode'])
                            attach_data['attachment_id'] = 1
                            attach_data['header'] = ''
                            attach_data['link'] = str(posts['display_url'])
                            attach_data['type'] = 'image'
                            attach.append(attach_data)
                        except:
                            print('Ошибка добавления данных в базу post_attachments')
                try:
                    posts['edge_media_to_caption']['edges'] = posts['edge_media_to_caption']['edges'][0]['node']['text']
                except:
                    pass
                try:
                    posts['location']['address_json'] = json.loads(posts['location']['address_json'])
                    posts['location'] = posts['location']['address_json']['city_name']
                except:
                    posts['location'] = 'Chelyabinsk, Russia'
                date = datetime.fromtimestamp(int(posts['taken_at_timestamp']))
                posts['taken_at_timestamp'] = date
                try:
                    db_data['owner_id'] = str(posts['owner'])
                    db_data['post_id'] = str(posts['shortcode'])
                    db_data['date'] = posts['taken_at_timestamp']
                    if len(posts['edge_media_to_caption']['edges']) != 0:
                        db_data['text'] = str(posts['edge_media_to_caption']['edges'])
                    else:
                        db_data['text'] = ''
                    db_data['likes'] = int(posts['edge_media_preview_like']['count'])
                    db_data['replies'] = int(posts['edge_media_to_parent_comment']['count'])
                    if len(posts['location']) == 0:
                        db_data['geo'] = 'Chelyabinsk, Russia'
                    else:
                        db_data['geo'] = str(posts['location'])
                    db.append(db_data)
                except:
                    print('Ошибка добавления данных')
                try:
                    date_data['date'] = posts['taken_at_timestamp']
                    date_data['owner_id'] = str(posts['owner'])
                    date_data['post_id'] = str(posts['shortcode'])
                    dates.append(date_data)
                except:
                    print('Ошибка добавления записи в базу inst_date')
                try:
                    print('Успешная обработка записи {0}'.format(self.count))
                except:
                    print('Ошибка добавления записи в базу inst_posts')
                    print(db)
                    self.f.write('kek')
                if (k[g]) == 25:
                    try:
                        # insert_posts(self.db)
                        # insert_dates(self.dates)
                        # insert_photos(self.attach)
                        # insert_replies(self.replies)
                        del db[:]
                        del dates[:]
                        del replies[:]
                        del attach[:]
                        k[g] = 0
                        print('Успешная загрузка в базу данных')
                    except Exception as e:
                        print('Исключение при попытке загрузить в базу данных:')
                        print(e)
                        traceback.print_exc()
                k[g] = k[g] + 1
                posts.clear()

            except Exception as e:
                print('Исключение при обработке записи:')
                print(e)
                time.sleep(2)
                self.f.write('kek')
                k[g] = k[g] + 1
        del linkss[:]

    def get_data_by_date(self, linkss, g, inst_date):

        attach = {}
        post = {}
        k = {g: 1}

        for i in range(len(linkss)):
            attach.clear()
            post.clear()
            try:
                try:
                    headers = {"Connection": "close"}
                    r = requests.Session()
                    s = r.get(linkss[i], headers=headers)
                    page = s.content
                    # r.keep_alive = False
                except Exception as e:
                    print(e)
                    r = ProxyRequests(linkss[i])
                    r.get()
                    page = r.get_raw().decode()
                self.count = self.count + 1
                data = bs(page, 'html.parser')
                body = data.find('body')
                script = body.find('script')
                raw = script.text.strip().replace('window._sharedData =', '').replace(';', '')
                json_data = json.loads(raw)
                posts = json_data['entry_data']['PostPage'][0]['graphql']['shortcode_media']
                posts = json.dumps(posts)
                posts = json.loads(posts)

                # [post['social'],
                #  post['owner_id'],
                #  post['post_id'],
                #  datetime.strptime(
                #      post['date'], '%Y-%m-%d %H:%M:%S'),
                #  post['text'],
                #  post['likes'],
                #  post['shares'],
                #  post['views'],
                #  post['replies'],
                #  post['post_type'],
                #  post['copy_owner_id'],
                #  post['copy_post_id'],
                #  post.get('topic') or -1]

                date = datetime.fromtimestamp(int(posts['taken_at_timestamp']))

                if str(inst_date) > str(date):
                    return 0

                if str(inst_date) <= str(date):
                    # print(inst_date)
                    # print(date.now())
                    # print(date)
                    posts['owner'] = posts['owner']['username']
                    try:
                        posts['edge_media_to_caption']['edges'] = posts['edge_media_to_caption']['edges'][0]['node'][
                            'text']
                    except:
                        pass
                    try:
                        post['social'] = 'inst'
                        post['owner_id'] = str(posts['owner'])
                        post['post_id'] = str(posts['shortcode'])
                        post['date'] = date
                        if len(posts['edge_media_to_caption']['edges']) != 0:
                            stroka = ''
                            v = str(posts['edge_media_to_caption']['edges']).split()
                            for b in range(len(v)):
                                if len(v[b]) > 20:
                                    v[b] = str(v[b]).replace('#', ' #')
                                    v[b] = str(v[b]).replace('_', ' _')
                                stroka = stroka + v[b] + ' '
                            post['text'] = stroka
                        else:
                            post['text'] = ''
                        post['likes'] = int(posts['edge_media_preview_like']['count'])
                        post['shares'] = 0
                        post['views'] = 0
                        post['post_type'] = 'post'
                        post['replies'] = int(posts['edge_media_to_parent_comment']['count'])
                        post['copy_owner_id'] = str(posts['owner'])
                        post['copy_post_id'] = str(posts['shortcode'])
                        self.post_data.append(post)
                    except Exception as e:
                        print(e)
                        self.LOGGER.warning(f'Failed to prepare inst post')

                    # try:
                    #     date = datetime.fromtimestamp(
                    #         int(posts['edge_media_to_comment']['edges'][0]['node']['created_at']))
                    #     if posts['edge_media_to_comment']['edges'][0]['node']['text'] is not None:
                    #         a = posts['edge_media_to_comment']['edges']
                    #         for _ in a:
                    #             replies_data['owner_id'] = str(posts['owner'])
                    #             replies_data['post_id'] = str(posts['shortcode'])
                    #             replies_data['reply_id'] = str(_['node']['id'])
                    #             replies_data['author'] = str(_['node']['owner']['username'])
                    #             replies_data['date'] = date
                    #             replies_data['text'] = str(_['node']['text'])
                    #             replies_data['sticker'] = ' '
                    #             replies_data['replies'] = 0
                    #             replies_data['parent_reply'] = ' '
                    #             replies_data['likes'] = int(_['node']['edge_liked_by']['count'])
                    #             replies.append(replies_data)
                    # except Exception as e:
                    #     pass
                    #
                    images = ''
                    str(images)
                    if str(posts['is_video']) == "True":
                        posts['display_url'] = str(posts['display_url']) + str(posts['video_url'])
                        try:
                            attach['social'] = 'inst'
                            attach['owner_id'] = str(posts['owner'])
                            attach['post_id'] = str(posts['shortcode'])
                            attach['attachment_id'] = 1
                            attach['header'] = ''
                            attach['link'] = str(posts['video_url'])
                            attach['type'] = 'video'
                            self.attach_data.append(attach)
                        except Exception as e:
                            print(e)
                            self.LOGGER.warning(f'Failed to prepare inst post')
                    else:
                        try:
                            if posts['edge_sidecar_to_children']['edges'] is not None:
                                try:
                                    i = 0
                                    b = posts['edge_sidecar_to_children']['edges']
                                    for _ in b:
                                        i = i + 1
                                        attach['social'] = 'inst'
                                        attach['owner_id'] = str(posts['owner'])
                                        attach['post_id'] = str(posts['shortcode'])
                                        attach['attachment_id'] = int(i)
                                        attach['header'] = ''
                                        attach['link'] = str(_['node']['display_url'])
                                        attach['type'] = 'image'
                                        self.attach_data.append(attach)
                                except Exception as e:
                                    print(e)
                                    self.LOGGER.warning(f'Failed to prepare inst post')
                        except:
                            try:
                                attach['social'] = 'inst'
                                attach['owner_id'] = str(posts['owner'])
                                attach['post_id'] = str(posts['shortcode'])
                                attach['attachment_id'] = 1
                                attach['header'] = ''
                                attach['link'] = str(posts['display_url'])
                                attach['type'] = 'image'
                                self.attach_data.append(attach)
                            except Exception as e:
                                print(e)
                                self.LOGGER.warning(f'Failed to prepare inst post')
            except Exception as e:
                self.LOGGER.info(f"Error while processing the record")
                print('Исключение при обработке записи:')
                print(e)
                traceback.print_exc()
        del linkss[:]
    # result.index = range(len(result.index))
    # directory="./images/"
    # for i in range(len(result)):
    #     r = requests.get(result['display_url'][i])
    #     with open(directory+result['shortcode'][i]+".jpg", 'wb') as f:
    #                     f.write(r.content)
