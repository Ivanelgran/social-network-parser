from datetime import datetime, timedelta
import pickle

import pandas as pd

from common_lib.queue_manager import RpcClient
from common_lib.cass_worker import (
    CassandraTables, insert_data, update_data, select_data, setup_connection)
from common_lib.src.neo4j_driver import (
    Models, update_node, select_with_filters)
from common_lib.src.rabbit import Server, RpcServer
from ok_parser import OkParser
from vk_parser import Parser
from inst_parser import InstParser


class ParseHandler():

    social_parsers = {'vk': Parser,
                      'ok': OkParser,
                      'inst': InstParser}

    def __init__(self, logger=None):
        self.logger = logger
        self.conn = setup_connection()

        self.tasks = {'replies': self.__parse_replies,
                      'last_posts': self.__update_last_posts,
                      'stat': self.__update_post_stat,
                      'group_info': self.__parse_group_info,
                      'post': self.__get_post_data}

    def __save_text_vectors(self, posts):
        task = {'task_name': 'get_vectors_for_posts',
                'args': {'posts': posts}}
        posts_vectors = RpcClient('rel_model_tasks').call(task)
        if posts_vectors != 0:
            insert_data(CassandraTables.posts_vectors, posts_vectors,
                        self.conn)

    def __parse_replies(self, social, owner_id, post_id,
                        return_replies=False):
        if social not in ['vk', 'ok']:
            self.logger.warning(f"Can't parser replies for social '{social}'!")
            return 0

        parser = self.social_parsers[social]()
        replies = parser.get_post_replies(owner_id, post_id) or []
        del parser

        if len(replies):
            insert_data(CassandraTables.replies, replies, self.conn)

        if return_replies:
            return replies

        return len(replies)

    def __parse_posts_replies(self, data):
        for post in data:
            self.__parse_replies(post['social'],
                                 post['owner_id'],
                                 post['post_id'])

    def __prepare_suggestions(self, social, owner_id):
        if social == 'vk' and owner_id.startswith("-"):
            owner_id = owner_id[1:]

        suggestions = select_data(table=CassandraTables.suggestion_groups,
                                  conn=self.conn)

        if len(suggestions) == 0:
            return None

        suggestions = pd.DataFrame(suggestions)
        suggestions = suggestions[(suggestions.social == social)
                                  & (suggestions.owner_id == owner_id)
                                  & (suggestions.state == "completed")]

        if suggestions.shape[0] == 0:
            return None

        dates = list(suggestions.date.values)
        dates = [pd.Timestamp(date).to_pydatetime() for date in dates]

        suggestion_tasks = select_data(table=CassandraTables.suggestions,
                                       filters={'date__in': dates},
                                       conn=self.conn)
        suggestion_tasks = pd.DataFrame(suggestion_tasks)
        suggestions = pd.merge(suggestions,
                               suggestion_tasks,
                               on='date')

        if suggestions.shape[0] == 0:
            return None

        return suggestions.to_dict('records')

    def __detect_suggestions(self, social, owner_id, data):
        suggestions = self.__prepare_suggestions(social, owner_id)

        if suggestions is None:
            return

        for row in suggestions:
            for post in data:
                if row['text'] == post['text']:
                    keys = {'date': row['date'],
                            'social': row['social'],
                            'owner_id': row['owner_id']}
                    items = {'state': 'posted',
                             'post_id': post['post_id']}
                    update_data(table=CassandraTables.suggestion_groups,
                                keys=keys,
                                update_items=items,
                                conn=self.conn)

    def __get_group_last_update(self, social, group_id):
        if social == 'vk' and group_id.startswith('-'):
            group_id = group_id[1:]
        group = select_with_filters(
            Models.group,
            {'social': social, 'group_id': group_id})

        if not len(group):
            return None

        date = group[0].last_update
        if date is None:
            return None

        return datetime(date.year, date.month, date.day,
                        date.hour, date.minute, date.second)

    def __get_group_last_date(self, social, group_id):
        if social == 'vk' and group_id.startswith('-'):
            group_id = group_id[1:]
        group = select_with_filters(
            Models.group,
            {'social': social, 'group_id': group_id})

        if not len(group):
            return None

        date = group[0].last_date
        if date is None or date.timestamp() == 0:
            return None

        return datetime(date.year, date.month, date.day,
                        date.hour, date.minute, date.second)

    def __update_group_last_date(self, social, group_id, date):
        if social == 'vk' and group_id.startswith('-'):
            group_id = group_id[1:]
        keys = {'social': social, 'group_id': group_id}
        items = {'last_date': date, 'last_update': datetime.now()}
        update_node(Models.group, keys, items)

    def __update_last_posts(self, social, owner_id, days=1):
        if social not in self.social_parsers.keys():
            self.logger.warning(f"Social '{social}' does not supported!")
            return

        parser = self.social_parsers[social](logger=self.logger)
        date_limit = self.__get_group_last_update(social, owner_id)
        date_limit = date_limit or datetime.today() - timedelta(days=days)
        result = parser.get_posts(group=owner_id, date_limit=date_limit)

        if result is None:
            self.logger.warning(f"Group {owner_id} is not readed")
            return

        data, att = result
        for post in data:
            post['social'] = social
        for post in att:
            post['social'] = social

        last_date = self.__get_group_last_date(social, owner_id)
        if len(data):
            self.__save_text_vectors(data)
            insert_data(CassandraTables.all_posts, data, self.conn)
            insert_data(CassandraTables.posts_dates, data, self.conn)

            for post in data:
                post['ts'] = datetime.now()
                insert_data(CassandraTables.post_stats, post, self.conn)

            self.__parse_posts_replies(data)

            dates = [*map(lambda x: x['date'], data)]
            last_date = sorted(dates, reverse=True)[0]
        elif last_date is None:
            parser = self.social_parsers[social](logger=self.logger)
            last_post = parser.get_last_post(owner_id)
            if last_post is not None:
                last_date = last_post['date']

        if len(att):
            insert_data(CassandraTables.post_attachments, att, self.conn)

        self.__update_group_last_date(social, owner_id, last_date)
        self.__detect_suggestions(social, owner_id, data)

    def __check_replies_increase(self, social, owner_id, post_id):
        filters = {'social': social,
                   'owner_id': owner_id,
                   'post_id': post_id}
        post_stats = select_data(table=CassandraTables.post_stats,
                                 filters=filters,
                                 conn=self.conn)

        if len(post_stats) < 2:
            return False

        post_stats = sorted(post_stats, key=lambda x: x['ts'])
        replies_increase = post_stats[0]['replies'] - post_stats[1]['replies']

        return replies_increase > 0

    def __update_post_stat(self, social, owner_id, post_id,
                           with_replies=False):
        if social not in ['vk', 'ok']:
            self.logger.warning(f"Can't update stat for social '{social}'!")
            return 0

        parser = self.social_parsers[social]()
        stat = parser.get_post_stat(owner_id, post_id)
        del parser

        if stat is None:
            self.logger.warning(
                f"Stat for {social} {owner_id} {post_id} is None")
            return

        stat['ts'] = datetime.now()
        stat['social'] = social

        insert_data(CassandraTables.post_stats, stat)

        if not with_replies:
            return

        need_parse_replies = self.__check_replies_increase(social,
                                                           owner_id,
                                                           post_id)
        if need_parse_replies:
            self.__parse_replies(social, owner_id, post_id)

    def __parse_group_info(self, social, url):
        if social not in self.social_parsers.keys():
            self.logger.warning(f"Cant parse group info for social '{social}'")
            return None

        parser = self.social_parsers[social]()
        group_info = parser.get_group_info(url)
        del parser

        return group_info

    def __get_post_data(self, social, owner_id, post_id):
        if social not in ['vk']:
            self.logger.warning(f"Cant get post data for social '{social}'")
            return None

        parser = self.social_parsers[social](self.logger)
        data, att = parser.get_post_data_by_ids(
            owner_id=owner_id, post_id=post_id)

        if data.get('error'):
            return 'bad_link'

        insert_data(CassandraTables.all_posts, data, self.conn)
        insert_data(CassandraTables.post_attachments, att, self.conn)

        return 'ok'

    def __handle_task(self, ch, method, properties, body):
        task = pickle.loads(body)

        task_name = task.get('task_name')
        self.logger.info(f"New task: {task_name}, Args: {task.get('args')}")

        if task_name in self.tasks.keys():
            args = task.get('args') or {}
            try:
                result = self.tasks[task_name](**args)
            except:
                self.logger.exception('what happened?')
                result = None

            task_date = task.get('task_date')
            if task_date is not None:
                update_data(table=CassandraTables.rabbit_tasks,
                            keys={'date': task_date},
                            update_items={'completed': True},
                            conn=self.conn)
        else:
            self.logger.warning(f"Task {task_name} doesn't supported")

        ch.basic_ack(delivery_tag=method.delivery_tag)

        if properties.reply_to is not None:
            ch.basic_publish(exchange='',
                             routing_key=properties.reply_to,
                             body=pickle.dumps(result))

    def run(self, queue, rpc=False):
        if rpc:
            server = RpcServer()
        else:
            server = Server()
        server.start(queue, self.__handle_task)
