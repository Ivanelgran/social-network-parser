import logging
import datetime
import pytz
import re
import random
import time
import requests
import numpy as np
import json
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from ip_controller import get_page
import vk_api
from vk_api import VkApiError
from selenium import webdriver

from functools import partial

from common_lib.src.retry import retry


class Parser():
    """
    """

    LOGGER = logging.getLogger('parser.vk_parser')

    vk_months = {'янв': 1,
                 'фев': 2,
                 'мар': 3,
                 'апр': 4,
                 'мая': 5,
                 'июн': 6,
                 'июл': 7,
                 'авг': 8,
                 'сен': 9,
                 'окт': 10,
                 'ноя': 11,
                 'дек': 12}

    token = "c31fed1ec31fed1ec31fed1e9fc375389dcc31fc31fed1e9ff1a58a7ee64e8d02d4f154"

    def __init__(self, logger=None):
        self.ua = UserAgent()
        self.limit = False
        self.LOGGER = logger or logging.getLogger("parser.vk_parser")

    # def get_group_info(self, group_url, ua=None):
    #     self.LOGGER.info(f"Parse info for {group_url}")
    #     result = {}
    #     if ua is None:
    #         ua = self.ua.random
    #     html = get_page(group_url, user_agent=ua)
    #     bs_obj = BeautifulSoup(html, "lxml")

    #     try:
        #         owner_id = bs_obj.find('a', {'href': re.compile(r'/search\?c\[section\]=people')})['href']
    #         owner_id = re.findall(r"([\d-]+)+", owner_id)[0]
    #         owner_id = str(-int(owner_id))
    #     except Exception:
    #         self.LOGGER.exception(f"cant find owner_id for {group_url}")
    #         return
    #     try:
    #         name = bs_obj.find('h2', {'class': 'page_name'}).get_text()
    #     except Exception:
    #         self.LOGGER.exception(f"cant find name for {group_url}")
    #         return

    #     try:
    #         members = bs_obj.find('div', {'class': "header_top"})\
    #             .find_all('span', recursive=False)[1]
    #         members_count = members.get_text().replace(" ", "")
    #     except Exception:
    #         self.LOGGER.exception(f"cant find members_count for {group_url}")
    #         return

    #     result['owner_id'] = owner_id
    #     result['name'] = name
    #     result['members_count'] = int(members_count)
    #     self.LOGGER.info(f"Info is ready")

    #     return result
    def get_group_info(self, group_url):
        vk_session = vk_api.VkApi(token=self.token)
        vk = vk_session.get_api()

        public_pattern = r"https://vk.com/public([\w]+)"
        club_pattern = r"https://vk.com/club([\w]+)"
        name_pattern = r"https://vk.com/([\w]+)"

        is_public = re.findall(public_pattern, group_url)
        is_club = re.findall(club_pattern, group_url)
        name = re.findall(name_pattern, group_url)

        if len(is_public):
            group_id = is_public[0]
        elif len(is_club):
            group_id = is_club[0]
        elif len(name):
            group_id = name[0]
        else:
            self.LOGGER.warning(f"Cant define group_id for {group_url}")
            return

        try:
            info = vk.groups.getById(group_id=group_id,
                                     fields=["members_count"])
        except VkApiError:
            self.LOGGER.exception(f"Cant get info for {group_id}")
            return

        user_info = info[0]

        result = {'social': 'vk',
                  'owner_id': str(user_info['id']),
                  'name': user_info['name'],
                  'members_count': user_info['members_count'],
                  'type': user_info['type']}

        return result

    def get_groups_by_ids(self, group_ids):
        vk_session = vk_api.VkApi(token=self.token)
        vk = vk_session.get_api()

        cur_ids = [id_[1] for id_ in group_ids]
        try:
            info = vk.groups.getById(group_ids=cur_ids,
                                     fields=["members_count"])
        except VkApiError:
            self.LOGGER.exception(f"Cant get info for {cur_ids}")
            return

        result = []
        for user_info in info:
            result.append({'social': 'vk',
                           'owner_id': str(user_info['id']),
                           'name': user_info['name'],
                           'members_count': user_info['members_count'],
                           'type': user_info['type']})

        return result

    def get_persons_info(self, user_ids):
        vk_session = vk_api.VkApi(token=self.token)
        vk = vk_session.get_api()
        try:
            info = vk.users.get(user_ids=user_ids,
                                fields=["city", "country", "bdate", "sex"])
        except VkApiError:
            self.LOGGER.exception(f"Cant get info for {user_ids}")
            return

        result = []
        for user_info in info:
            cur_info = dict()
            cur_info['social'] = 'vk'
            cur_info['person_id'] = str(user_info['id'])
            cur_info['first_name'] = user_info['first_name']
            cur_info['last_name'] = user_info['last_name']
            if user_info.get("sex") is not None:
                cur_info['sex'] = user_info['sex']
            if user_info.get("city") is not None:
                cur_info['city_id'] = user_info['city']['id']
            if user_info.get("country") is not None:
                cur_info['country_id'] = user_info['country']['id']
            if user_info.get("bdate") is not None:
                cur_info['birthday'] = user_info['bdate']
            result.append(cur_info)

        return result

    def get_person_groups(self, user_id):
        vk_session = vk_api.VkApi(token=self.token)
        vk = vk_session.get_api()
        try:
            info = vk.users.getSubscriptions(user_id=user_id)
        except VkApiError:
            return
        except Exception:
            self.LOGGER.exception(f"Cant get groups for {user_id}")
            return

        return info

    def get_person_friends(self, user_id):
        vk_session = vk_api.VkApi(token=self.token)
        vk = vk_session.get_api()
        try:
            info = vk.friends.get(user_id=user_id)
        except VkApiError:
            self.LOGGER.exception(f"Cant get friends for {user_id}")
            return

        return info

    def get_group_members(self, group_id):
        members_count = None
        offset = 0
        members = []

        vk_session = vk_api.VkApi(token=self.token)
        vk = vk_session.get_api()

        while True:
            if members_count is not None and offset > members_count:
                break
            try:
                info = vk.groups.getMembers(group_id=group_id, offset=offset)
            except VkApiError:
                self.LOGGER.exception(f"Cant get members for {group_id}")
                return
            if members_count is None:
                members_count = info['count']
            members.extend(info['items'])
            offset += 1000
            time.sleep(0.5)

    def get_post_data_by_ids(self, owner_id, post_id):
        ua = self.ua.random
        url = f"https://vk.com/wall{owner_id}_{post_id}"
        html = get_page(url, user_agent=ua)
        bs_obj = BeautifulSoup(html, "lxml")
        error = bs_obj.find('div', {'class': 'message_page_title'})
        if error is not None and error.getText() == "Ошибка":
            self.LOGGER.info(error)
        post = bs_obj.find('div', {'id': f'post{owner_id}_{post_id}'})
        if post is not None:
            post_data = self.get_post_data(post, None, None, None, with_limit=False)
            return post_data
        else:
            return {'error': 'error'}, {'error': 'error'}

    def can_comment_in_post(self, owner_id, post_id):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        driver = webdriver.Chrome(executable_path='data/chromedriver',
                                  chrome_options=options)
        driver.implicitly_wait(5)

        group_id = owner_id if not owner_id.startswith('-') else owner_id[1:]
        url = f"https://vk.com/club{group_id}"
        driver.get(url)

        correct_url = driver.current_url + f"?w=wall{owner_id}_{post_id}"
        driver.get(correct_url)

        try:
            driver.find_element_by_css_selector('a.ui_actions_menu_more')
            return True
        except Exception:
            return False

    def get_post_data(self, post, date_limit=None, last_saved_post=None,
                      replies_delay=0, with_limit=True):

        if with_limit and self.limit:
            return

        is_pinned = "post_fixed" in post['class']

        url = post['data-post-id'].split('_')
        owner_id = url[0]
        post_id = url[1]

        time_span = post.find("span", {"class": "rel_date"})
        if time_span:
            if 'time' in time_span.attrs:
                date = datetime.datetime.fromtimestamp(int(time_span['time']))
            else:
                date = post.find("div", {"class": "post_date"}).getText()
                date = self.parse_to_datetime(date)
        else:
            date = datetime.datetime.fromtimestamp(int('0000000001'))

        if not is_pinned:
            if last_saved_post is not None and int(post_id) <= int(last_saved_post) \
                    or date_limit is not None and date < np.datetime64(date_limit):
                self.limit = True
                return

        copy_post = post.find('a', {'data-parent-post-id': post['data-post-id']})
        if copy_post is None:
            post_type = 'post'
            copy_owner_id = owner_id
            copy_post_id = post_id
        else:
            post_type = 'repost'
            copy_url = copy_post['data-post-id'].split('_')
            copy_owner_id = copy_url[0]
            copy_post_id = copy_url[1]

        stat = self.get_post_stat(owner_id, post_id) or {}

        all_text = post.findAll("div", {'class': 'wall_post_text'})
        text = [t.getText(separator='\n').replace("Показать полностью…", " ") for t in all_text]
        text = ' '.join(text)
        text = re.sub(r" {2,}", " ", text)
        if text is None:
            text = ''

        semi_posts = {'social': 'vk',
                      'owner_id': owner_id,
                      'post_id': post_id,
                      'date': date,
                      'text': text.replace("'", ""),
                      'likes': stat.get('likes'),
                      'shares': stat.get('shares'),
                      'replies': stat.get('replies'),
                      'views': stat.get('views'),
                      'post_type': post_type,
                      'copy_owner_id': copy_owner_id,
                      'copy_post_id': copy_post_id}

        post_attachments = self.get_attachments(owner_id, post_id, post)

        return semi_posts, post_attachments

    def get_attachments(self, owner_id, post_id, post):
        all_attachments = []

        infos = [
            post.find('div', {'class': 'wall_text'}).find(
                'div', recursive=False),
            post.find('div', {'class': 'wall_text'}).find(
                'div', {'class': 'copy_quote'}, recursive=False)]

        for info in infos:
            if info is None:
                continue
            drop_content = info.find("div", {'class': 'wall_post_text'})
            if drop_content is not None:
                drop_content.extract()
            drop_content = info.find("div", {'class': 'wall_signed'})
            if drop_content is not None:
                drop_content.extract()
            drop_content = info.find("div", {'class': 'published_comment'})
            if drop_content is not None:
                drop_content.extract()
            drop_content = info.find("div", {'class': 'copy_post_header'})
            if drop_content is not None:
                drop_content.extract()

            attachments = info.find_all('div', recursive=False)
            att_counter = 1
            for attachment in attachments:
                att_types = {'image': self.find_images,
                             'video': self.find_videos,
                             'gif': self.find_gifs,
                             'audio': self.find_music,
                             'doc': self.find_docs}

                # get headers and links
                for att_type, att_func in att_types.items():
                    try:
                        content = att_func(attachment)
                    except Exception:
                        self.LOGGER.exception(f'GROUP {owner_id} POST {post_id} CANT FIND {att_type}')
                        content = None
                    if content is not None:
                        for header, link in content:
                            all_attachments.append(
                                {'social': 'vk',
                                 'attachment_id': att_counter,
                                 'owner_id': owner_id,
                                 'post_id': post_id,
                                 'type': att_type,
                                 'header': header,
                                 'link': link})
                            att_counter += 1

            return all_attachments

    def find_images(self, post):
        images = post.find_all('a', {'onclick': re.compile('showPhoto')})
        result = []

        if len(images) == 0:
            return

        for img in images:
            cur_img = str(img).replace("&quot;", '"')
            base_url = re.findall(r'"base":"[\w/:.-]+"', cur_img)
            if len(base_url) > 0:
                base_url = base_url[0][8:-1]
            else:
                base_url = ""
            if '"w_"' in cur_img:
                url = re.findall(r'"w_":\["[\w/\-_:\.\\]+"', cur_img)[0][7:-1]
            elif '"z_"' in cur_img:
                url = re.findall(r'"z_":\["[\w/\-_:\.\\]+"', cur_img)[0][7:-1]
            elif '"y_"' in cur_img:
                url = re.findall(r'"y_":\["[\w/\-_:\.\\]+"', cur_img)[0][7:-1]
            elif '"x_"' in cur_img:
                url = re.findall(r'"x_":\["[\w/\-_:\.\\]+"', cur_img)[0][7:-1]
            else:
                continue
            url = url.replace('\\', '')
            link = base_url + url + '.jpg' if not url.startswith("https://") else url + '.jpg'
            result.append((None, link))

        return result

    def find_videos(self, post):
        videos = post.find_all('a', {'onclick': re.compile('showVideo')})
        result = []

        if len(videos) == 0:
            return

        base_url = 'https://vk.com'
        for video in videos:
            header = video.find('div', {'class': 'a post_video_title'})
            if header is not None:
                header = header.getText()
            url = base_url + video['href']
            result.append((header, url))

        return result

    def find_gifs(self, post):
        gifs = post.find_all('a', {'onclick': re.compile('showGif')})
        result = []

        if len(gifs) == 0:
            return
        base_url = 'https://vk.com'
        for gif in gifs:
            url = base_url + gif['href']
            result.append((None, url))

        return result

    def find_music(self, post):
        audios = post.find_all('div', {'onclick': re.compile('getAudioPlayer')})
        result = []

        if len(audios) == 0:
            return

        for audio in audios:
            performer = audio.find('div', {'class': 'audio_row__performers'})
            title = audio.find('div', {'class': re.compile('audio_row__title')})
            if performer is None or title is None:
                continue
            performer = performer.getText().replace('\n', '')
            title = title.getText().replace('\n', '')
            header = performer + ' -- ' + title
            result.append((header, None))

        return result

    def find_docs(self, post):
        docs = post.find_all('div', {'class': 'media_desc media_desc__doc '})
        result = []

        if len(docs) == 0:
            return

        base_url = 'https://vk.com'
        for doc in docs:
            info = doc.find('a', {'class': 'page_doc_title'})
            header = info.getText()
            url = base_url + info['href']
            result.append((header, url))

        return result

    def check_own_group(self, url, ua):
        headers = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36'}
        r = requests.get(url, headers=headers)

        return r.url.endswith('own=1')

    def get_posts(self,
                  group: str,
                  verbose: bool = False,
                  get_replies: bool = True,
                  date_limit: datetime.datetime = None,
                  last_saved_post: int = None,
                  request_delay: float = 0,
                  replies_delay: float = 0):
        offset = 0
        all_posts = []
        all_attachments = []

        request_limit = 100

        if int(group) > 0:
            group = "-" + group

        while True:
            # get page

            url = f"https://vk.com/wall{group}"
            ua = self.ua.random
            post_data = {'offset': offset}
            if self.check_own_group(url, ua):
                post_data['own'] = 1
            r = requests.get(url, headers={'User-agent': ua}, params=post_data)
            html, status = r.content, r.status_code

            if status != 200:
                return

            bs_obj = BeautifulSoup(html, "lxml")
            end_page = bs_obj.find("div", {"class": "page_block no_rows"})
            closed_page = bs_obj.find("div", {"class": "message_page_title"})

            if end_page is not None or closed_page is not None and closed_page.getText() == 'Ошибка':
                break

            # drop links in texts
            for x in bs_obj.findAll("div", {"class": "wall_text"}):
                for a in x.findAll("a", {"href": re.compile("^(http)|(/away)")}):
                    a.extract()

            try:
                posts = bs_obj.find("div", {"id": "page_wall_posts"}).findAll("div", recursive=False)
            except AttributeError:
                if request_limit > 0:
                    request_limit -= 1
                    continue
                else:
                    self.LOGGER.warning(f'GROUP {group} POSTS NOT FOUND')
                    break

            offset += 20

            get_post_data = partial(self.get_post_data,
                                    date_limit=date_limit,
                                    last_saved_post=last_saved_post,
                                    replies_delay=replies_delay)
            posts_info = [*map(get_post_data, posts)]
            semi_posts = [post_info[0] for post_info in posts_info if post_info is not None]
            semi_att = [post_info[1] for post_info in posts_info if post_info is not None]

            [all_posts.append(post) for post in semi_posts if post is not None]
            [all_attachments.append(post) for sublist in semi_att for post in sublist if posts is not None]

            if self.limit:
                break

            if len(semi_posts) == 0:
                print("Posts not found")
                break

        delay = random.uniform(0.5 * request_delay, 1.5 * request_delay)
        time.sleep(delay)
        self.limit = False

        return all_posts, all_attachments

    @retry(3)
    def get_last_post(self, group: str):
        if int(group) > 0:
            group = "-" + group

        url = f"https://vk.com/wall{group}"
        ua = self.ua.random

        r = requests.get(url, headers={'User-agent': ua})
        html, status = r.content, r.status_code

        if status != 200:
            return

        bs_obj = BeautifulSoup(html, "lxml")
        end_page = bs_obj.find("div", {"class": "page_block no_rows"})
        closed_page = bs_obj.find("div", {"class": "message_page_title"})

        if end_page is not None or closed_page is not None and closed_page.getText() == 'Ошибка':
            return

        posts = bs_obj.find("div", {"id": "page_wall_posts"}) \
            .findAll("div", recursive=False)

        if not len(posts):
            return

        posts_info = [*map(self.get_post_data, posts)]
        posts_info = sorted(posts_info,
                            key=lambda x: x[0]['date'],
                            reverse=True)

        return posts_info[0][0]

    # def get_replies_count(self, owner_id: int, post_id: int, request_delay: float = 0) -> int:

    #     delay = random.uniform(0.5 * request_delay, 1.5 * request_delay)
    #     time.sleep(delay)

    #     replies_count = 0
    #     ua = random.choice(self.mobile_ua)

    #     url = f"https://vk.com/wall{owner_id}_{post_id}"
    #     html = get_page(url, user_agent=ua)
    #     bs_obj = BeautifulSoup(html, "lxml")
    #     try:
    #         replies_count = bs_obj.find("div", {"class": "wi_buttons_wrap"}).find('span', {'class': 'like_wrap'}).find("span").getText().split()[0]
    #         replies_count = int(replies_count)
    #     except AttributeError:
    #         self.LOGGER.exception(f'GROUP {owner_id} POST {post_id} CANT PARSE REPLIES COUNT')

    #     return replies_count

    @retry(1)
    def get_post_stat(self, owner_id, post_id):
        stat = self._get_post_stat(owner_id, post_id)

        return stat

    def _get_post_stat(self, owner_id, post_id):
        ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36"
        url = f"https://vk.com/wall{owner_id}_{post_id}"
        html, status = get_page(url, user_agent=ua, with_status_code=True)

        if status != 200:
            raise BaseException

        bs_obj = BeautifulSoup(html, "lxml")
        if bs_obj.find("div", {"class": "message_page"}) is not None:
            self.LOGGER.warning("Find message_page")
            return

        replies_pattern = r"var opts = ([\w:,\"-\[\]\}\{\\/ ]+), preload = [\w:,\"-\[\]\}\{\\/ ]+"
        all_replies = re.findall(replies_pattern, html.decode('cp1251'))
        replies_count = re.findall(r"\"count\":(\d+)", all_replies[0])[0]
        replies_count = int(replies_count)

        likes = bs_obj.find("a", {"class": 'like_btn'})
        likes = int(likes['data-count']) if likes is not None else 0

        shares = bs_obj.find("a", {"class": 'share'})
        shares = int(shares['data-count']) if shares is not None else 0

        # views_raw = bs_obj.find("div", {"class": "like_views _views"})['title']
        # if views_raw == "":
        #     views = 0
        # elif views_raw.endswith("K"):
        #     views = int(float(views_raw[:-1])*1000)
        # elif views_raw.endswith("M"):
        #     views = int(float(views_raw[:-1])*(10**6))
        # else:
        #     views = int(views_raw)
        url = 'https://vk.com/like.php'
        headers = {'user-agent': ua}
        data = {
            'act': 'a_get_stats',
            'al': '1',
            'al_ad': '0',
            'object': f'wall{owner_id}_{post_id}',
            'views': '1'
        }
        views_raw = requests.post(url, headers=headers, data=data)
        views_raw = json.loads(views_raw.text[4:])['payload'][1][1]
        views = int(views_raw.split(' ')[0])

        return {'owner_id': owner_id,
                'post_id': post_id,
                'likes': likes,
                'shares': shares,
                'replies': replies_count,
                'views': views}

    def parse_to_datetime(self, date: str) -> datetime.datetime:
        tz = pytz.timezone('Europe/Moscow')
        today = datetime.datetime.now(tz)
        solit_date = date.split()

        if "назад" in date:
            if "секунд" in date:
                cur_date = today - datetime.timedelta(seconds=int(solit_date[0]))
            elif "два" in date:
                cur_date = today - datetime.timedelta(hours=2)
            elif "три" in date:
                cur_date = today - datetime.timedelta(hours=3)
            elif "минуту" in date:
                cur_date = today - datetime.timedelta(minutes=1)
            elif "минуты" in date:
                cur_date = today - datetime.timedelta(minutes=2 if solit_date[0] == 'две' else 3)
            elif "минут" in date:
                cur_date = today - datetime.timedelta(minutes=int(solit_date[0]))
            elif "час" in date:
                cur_date = today - datetime.timedelta(hours=1)

            year = cur_date.year
            month = cur_date.month
            day = cur_date.day
            hour = cur_date.hour
            minute = cur_date.minute
            second = cur_date.second
        elif "вчера" in date:
            yesterday = today - datetime.timedelta(1)
            year = yesterday.year
            month = yesterday.month
            day = yesterday.day
            time = re.findall(r'\d{1,2}:\d\d', date)[0].split(":")
            hour = int(time[0])
            minute = int(time[1])
            second = 0
        elif "сегодня" in date:
            year = today.year
            month = today.month
            day = today.day
            time = re.findall(r'\d{1,2}:\d\d', date)[0].split(":")
            hour = int(time[0])
            minute = int(time[1])
            second = 0
        elif "только" in date:
            year = today.year
            month = today.month
            day = today.day
            hour = today.hour
            minute = today.minute
            second = today.second
        elif len(re.findall("[а-я]{3}", date)) > 0:
            month = re.findall("[а-я]{3}", date)[0]
            month = self.vk_months.get(month, 1)
            day = int(solit_date[0])
            year = re.findall(r"\d{4}", date)
            if len(year) > 0:
                year = year[0]
            else:
                find_year = datetime.datetime(today.year, int(month), int(day),
                                              tzinfo=tz)
                year = today.year if find_year <= today else today.year - 1
            try:
                time = re.findall(r'\d{1,2}:\d\d', date)[0].split(":")
                hour = int(time[0])
                minute = int(time[1])
            except IndexError:
                hour = 0
                minute = 0
            second = 0
        else:
            print("Can't parse date")
            return

        result = datetime.datetime(int(year), int(month), int(day), int(hour),
                                   int(minute), int(second))

        result = result + datetime.timedelta(hours=2)

        return result

    @retry(5)
    def get_post(self, owner_id, post_id):
        ua = self.ua.random
        url = f"https://vk.com/wall{owner_id}_{post_id}"
        html = get_page(url, user_agent=ua)
        bs_obj = BeautifulSoup(html, "lxml")

        error = bs_obj.find('div', {'class': 'message_page_title'})
        if error is not None and error.getText() == "Ошибка":
            return []

        post = bs_obj.find('div', {'id': f'post{owner_id}_{post_id}'})

        post_data = self.get_post_data(post, None, None, None,
                                       with_limit=False)

        return post_data[0]

    @retry(5)
    def get_post_replies(self, owner_id, post_id):

        result = []
        all_replies = []
        ua = self.ua.random
        url = f"https://vk.com/wall{owner_id}_{post_id}"
        html = get_page(url, user_agent=ua)
        bs_obj = BeautifulSoup(html, "lxml")

        error = bs_obj.find('div', {'class': 'message_page_title'})
        if error is not None and error.getText() == "Ошибка":
            return [], []

        replies = bs_obj.find('div', {'id': f'replies{owner_id}_{post_id}'}).find_all('div', recursive=False)

        post = bs_obj.find('div', {'id': f'post{owner_id}_{post_id}'})

        all_replies.extend(replies)

        if bs_obj.find('div', {'id': f'replies{owner_id}_{post_id}'}).find('a', recursive=False) is not None:
            prev_id = replies[-1].find('div', {'class': 'reply_text'}).find('div')['id'].split("_")[1]
            offset = 40
            while True:
                data = {'act': 'get_post_replies',
                        'al': 1,
                        'count': 20,
                        'item_id': post_id,
                        'offset': offset,
                        'order': 'smart',
                        'owner_id': owner_id,
                        'prev_id': prev_id}

                html = get_page(url, user_agent=ua, post_data=data).decode('cp1251')
                bs = BeautifulSoup(html[html.find('<div'):], 'lxml')
                replies = bs.find('body').find_all('div', recursive=False)
                if len(replies) == 0:
                    break
                all_replies.extend(replies)

                offset += 20
                prev_id = replies[-1].find('div', {'class': 'reply_text'}) \
                    .find('div')['id'].split("_")[1]

        replies_count = {}
        for reply in all_replies:

            branch = reply.find(
                'div', {'id': f'replies{reply["data-post-id"]}'})
            if branch is not None:
                branch_replies = branch.find_all('div', recursive=False)
                parent_reply = reply["data-post-id"].split("_")[1]
                replies_count[parent_reply] = len(branch_replies)
                for branch_reply in branch_replies:
                    cur_reply = self.get_reply_info(owner_id,
                                                    post_id,
                                                    branch_reply,
                                                    parent_reply)
                    if cur_reply is not None:
                        result.append(cur_reply)
                continue

            cur_reply = self.get_reply_info(owner_id, post_id, reply)

            if cur_reply is not None:
                result.append(cur_reply)

        if len(result) == 0:
            return []

        for reply in result:
            if reply['reply_id'] in replies_count.keys():
                reply['replies'] = replies_count[reply['reply_id']]

        return result

    def get_reply_info(self, owner_id, post_id, reply, parent_reply=None):

        if reply.find('div', {'class': 'reply_text'}).getText() == 'Комментарий удалён пользователем или руководителем страницы':
            return

        cur_reply = {'social': 'vk',
                     'owner_id': owner_id,
                     'post_id': post_id}

        reply_id = reply['data-post-id'].split('_')[1]

        reply_to = reply.find('a', {'class': 'reply_to'})
        if reply_to is not None:
            reply_to = reply_to['onclick']
            reply_to = re.findall(r"\d+_\d+", reply_to)[1]
            reply_to = reply_to.split('_')[1]

        author = reply.find('a', {'class': 'author'})['data-from-id']

        time_span = reply.find("span", {"class": "rel_date"})
        if 'time' in time_span.attrs:
            date = datetime.datetime.fromtimestamp(int(time_span['time']))
        else:
            date = reply.find("div", {"class": "reply_date"}).getText()
            date = self.parse_to_datetime(date)

        likes = reply.find("div", {"class": "like_button_count"}).getText() or 0

        try:
            text = reply.find('div', {'class': 'wall_reply_text'}).getText()
            for i in reply.find('div', {'class': 'wall_reply_text'}).find_all("img"):
                text += f" {i['alt']}"
        except Exception:
            text = ""

        sticker = reply.find('div', {'class': 'reply_text'}).find(
            "img", {'class': 'sticker_img'})
        if sticker is not None:
            sticker = sticker['src']
        else:
            sticker = ""

        cur_reply['reply_id'] = reply_id
        cur_reply['author'] = author
        cur_reply['date'] = date
        cur_reply['likes'] = int(likes)
        cur_reply['text'] = text
        cur_reply['sticker'] = sticker
        cur_reply['replies'] = 0
        cur_reply['parent_reply'] = parent_reply or ""
        cur_reply['reply_to'] = reply_to or ""

        return cur_reply
