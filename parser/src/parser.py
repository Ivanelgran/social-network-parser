import time

from multiprocessing import Process

from common_lib.src import config, log
from parse_handler import ParseHandler
import producers


def run_listener(queue, logger):
    handler = ParseHandler(logger)
    handler.run(queue)


def run_producer(producer, time_period, logger):
    producers_dict = {'last_posts': producers.LastPosts,
                      'last_stat': producers.LastStat,
                      'traces': producers.Traces,
                      'suggestions': producers.Suggestions,
                      'tracked_posts': producers.TrackedPosts,
                      'our_groups': producers.OurGroups}

    cur_producer = producers_dict[producer](time_period=time_period,
                                            logger=logger)
    cur_producer.run()


def run_rpc(queue, logger=None):
    handler = ParseHandler(logger)
    handler.run(queue, rpc=True)


def check_process(process_data, logger):
    if not process_data['process'].is_alive():
        logger.warning(f"DEAD PROCESS {process_data['args']}")
        process_data['process'].terminate()
        process_data['process'].join()
        del process_data['process']
        p = Process(target=process_data['target'], args=process_data['args'])
        process_data['process'] = p
        process_data['process'].start()


if __name__ == "__main__":
    CONFIG = config.get_config('./common_lib/config/parser.yml')
    logger = log.get_logger('parser.main')

    pool = []

    for queue, process_num in CONFIG['queues'].items():
        args = (queue, logger, )
        for _ in range(process_num):
            p = Process(target=run_listener, args=args)
            process_data = {'process': p,
                            'args': args,
                            'target': run_listener}
            pool.append(process_data)

    for producer, time_period in CONFIG['producers'].items():
        if time_period <= 0:
            continue

        args = (producer, time_period, logger, )
        p = Process(target=run_producer, args=args)
        process_data = {'process': p,
                        'args': args,
                        'target': run_producer}
        pool.append(process_data)

    for queue, run in CONFIG['rpc'].items():
        if not run:
            continue

        args = (queue, logger, )
        p = Process(target=run_rpc, args=args)
        process_data = {'process': p,
                        'args': args,
                        'target': run_rpc}
        pool.append(process_data)

    logger.info(f"Start {len(pool)} parser processes")

    [p['process'].start() for p in pool]

    while True:
        for process_data in pool:
            check_process(process_data, logger)
        time.sleep(10)
