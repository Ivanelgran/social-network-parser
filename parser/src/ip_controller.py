import socket
import pycurl
import requests
import time
import logging
from io import BytesIO
from urllib.parse import urlencode

HOST = '127.0.0.1'
PORT = 10525
group = b'ip-search\n'


def get_ip():
    try:
        with socket.socket() as s:
            s.connect((HOST, PORT))
            s.send(group)
            data = s.recv(255).decode("utf-8")
    except ConnectionRefusedError:
        data = None

    return data


def get_page(url, user_agent, post_data=None, use_proxy=None, with_status_code=False):
    LOGGER = logging.getLogger('Parser.ip_controller')

    if use_proxy is not None:
        cur_ip = get_ip()
        buffer = BytesIO()
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        if post_data is not None:
            postfields = urlencode(post_data)
            c.setopt(pycurl.POSTFIELDS, postfields)
        c.setopt(pycurl.HTTPHEADER, [f'user-agent: {user_agent}'])
        c.setopt(pycurl.INTERFACE, cur_ip)
        c.setopt(pycurl.WRITEFUNCTION, buffer.write)
        c.perform()
        c.close()

        body = buffer.getvalue().decode("cp1251", "ignore")
    else:
        headers = {'user-agent': user_agent}
        for i in range(10):
            try:
                if post_data is not None:
                    response = requests.post(url, data=post_data,
                                             headers=headers, timeout=5)
                else:
                    response = requests.get(url, headers=headers, timeout=5)
                break
            except requests.exceptions.ConnectionError:
                LOGGER.exception(f'Connection problem')
                time.sleep(60)
                continue
            except requests.exceptions.Timeout:
                LOGGER.exception(f'timeout reached')
                time.sleep(60)
                continue
        else:
            return None, 404

        body = response.content

    if with_status_code:
        return body, response.status_code

    return body
