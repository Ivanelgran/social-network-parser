from bokeh.models import (HoverTool, FactorRange, LinearAxis, Grid,
                          Range1d)
from bokeh.models.glyphs import VBar
from bokeh.plotting import figure
from bokeh.models.sources import ColumnDataSource
from math import pi
import pandas as pd

from bokeh.palettes import linear_palette, Plasma256
from bokeh.transform import cumsum

TEMPLATE_STRING = """
<html>
 <head>
  <title>Bar charts with Bottle and Bokeh</title>
  <link href="http://cdn.pydata.org/bokeh/release/bokeh-0.12.6.min.css"
        rel="stylesheet">
  <link href="http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.6.min.css"
        rel="stylesheet">
 </head>
 <body>
  <h1>Bugs found over the past {{ bars_count }} days</h1>
  {{ !div }}
  <script src="http://cdn.pydata.org/bokeh/release/bokeh-0.12.6.min.js"></script>
  <script src="http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.6.min.js"></script>
  {{ !script }}
 </body>
</html>
"""


def create_hover_tool():
    return
    hover_html = """
      <div>
        <span class="hover-tooltip">$x</span>
      </div>
      <div>
        <span class="hover-tooltip">@bugs bugs</span>
      </div>
      <div>
        <span class="hover-tooltip">$@costs{0.00}</span>
      </div>
    """
    return HoverTool(tooltips=hover_html)


def create_bar_chart(data, title, x_name, y_name, hover_tool=None,
                     width=1200, height=300):
    """Creates a bar chart plot with the exact styling for the centcom
       dashboard. Pass in data as a dictionary, desired plot title,
       name of x axis, y axis and the hover tool HTML.
    """
    source = ColumnDataSource(data)
    xdr = FactorRange(factors=[str(i) for i in data[x_name]])
    ydr = Range1d(start=0, end=max(data[y_name])*1.5)

    tools = []
    if hover_tool:
        tools = [hover_tool, ]

    plot = figure(title=title, x_range=xdr, y_range=ydr, plot_width=width,
                  plot_height=height, h_symmetry=False, v_symmetry=False,
                  min_border=10, toolbar_location="above", tools=tools,
                  outline_line_color="#666666")

    glyph = VBar(x=x_name, top=y_name, bottom=0, width=.8,
                 fill_color="#6599ed")
    plot.add_glyph(source, glyph)

    xaxis = LinearAxis()
    yaxis = LinearAxis()

    plot.add_layout(Grid(dimension=0, ticker=xaxis.ticker))
    plot.add_layout(Grid(dimension=1, ticker=yaxis.ticker))
    plot.toolbar.logo = None
    plot.min_border_top = 0
    plot.xgrid.grid_line_color = None
    plot.ygrid.grid_line_color = "#999999"
    plot.yaxis.axis_label = "Количество постов"
    plot.ygrid.grid_line_alpha = 0.1
    plot.xaxis.axis_label = "Тема"
    plot.xaxis.major_label_orientation = 1
    return plot


def create_bar_colormapped(title, data, x_name, y_name, hover_tool=None,
                           width=1200, height=300):
    source = ColumnDataSource(data)
    plot = figure(title=title, x_range=[str(i) for i in data[x_name]],
                  plot_width=width,
                  plot_height=height)
    plot.vbar(x=x_name, top=y_name, width=0.9, source=source)

    return plot


def create_pie_chart(x):
    data = pd.Series(x).reset_index(name='value').rename(columns={'index': 'topic'})
    data['angle'] = data['value']/data['value'].sum() * 2*pi
    data['color'] = linear_palette(list(reversed(Plasma256[:236])), len(x))

    p = figure(plot_width=800, plot_height=490, title="Daily Trends",
               toolbar_location=None, tools="hover",
               tooltips="@topic: @value", x_range=(-0.5, 1.0))

    p.wedge(x=0, y=1, radius=0.4,
            start_angle=cumsum('angle', include_zero=True),
            end_angle=cumsum('angle'),
            line_color="white", fill_color='color',
            legend='topic', source=data)

    p.axis.axis_label = None
    p.axis.visible = False
    p.grid.grid_line_color = None

    return p
