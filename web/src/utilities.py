import time
import logging
from bottle import request

# LOGGER = logging.getLogger('ApiService.service_info')


def time_it(fn):
    """Декаратор, служит для измерения времени выполнения функций, выводит результат в логи"""
    def _time(*args, **kwargs):
        LOGGER.info(f'Start "{fn.__name__}" method!')

        start_time = time.time()
        result = fn(*args, **kwargs)
        time_spent = time.time() - start_time

        LOGGER.info(f'Stop "{fn.__name__}" method! --- {time_spent} seconds ---')
        return result

    return _time


def show_ip_address(fn):
    """Декаратор для отображения IP адресса запроса"""
    def _get_ip(*args, **kwargs):
        client_ip = request.environ.get('HTTP_X_FORWARDED_FOR') or request.environ.get('REMOTE_ADDR')
        LOGGER.info(f'REMOTE IP: "{client_ip}"! FUNCTION: {fn.__name__}')
        result = fn(*args, **kwargs)
        return result

    return _get_ip
