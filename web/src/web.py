from gevent import monkey;

monkey.patch_all()

# TODO: разбить на модули (flask + blueprints, например)
import os
import time
import logging
import logging.config
import json
import markdown
import requests
from requests.exceptions import Timeout
from pathlib import Path
from configparser import ConfigParser
from bottle import route, run, request, response, template, static_file, redirect
# from common_lib.utilities import show_ip_address, time_it
from common_lib.queue_manager import RpcClient
from common_lib.cass_worker import CassandraTables, CassQueryGenerator
from common_lib.src import config, log
import pandas as pd
from bokeh.embed import json_item
from visualizations import create_pie_chart
import urllib
from gevent import sleep
from transliterate import translit
from datetime import datetime, timedelta

from common_lib.cass_worker import (
    CassandraTables, insert_data, update_data, select_data, setup_connection)
from common_lib.src.neo4j_driver import (
    Models, select_with_filters)

from functools import partial

SRC_DIR = Path(__name__).resolve().parents[0]

LOGGER = log.get_logger('web.main')

setup_connection()


def time_it(fn):
    """Декаратор, служит для измерения времени выполнения функций, выводит результат в логи"""
    def _time(*args, **kwargs):
        LOGGER.info(f'Start "{fn.__name__}" method!')

        start_time = time.time()
        result = fn(*args, **kwargs)
        time_spent = time.time() - start_time

        LOGGER.info(f'Stop "{fn.__name__}" method! --- {time_spent} seconds ---')
        return result

    return _time


def show_ip_address(fn):
    """Декаратор для отображения IP адресса запроса"""
    def _get_ip(*args, **kwargs):
        client_ip = request.environ.get('HTTP_X_FORWARDED_FOR') or request.environ.get('REMOTE_ADDR')
        LOGGER.info(f'REMOTE IP: "{client_ip}"! FUNCTION: {fn.__name__}')
        result = fn(*args, **kwargs)
        return result

    return _get_ip


""" Работа с API """
# TODO: вынести в конфиг
api_host, api_port = 'api', 8090


def get_from_api(path="/", params={}):
    r = requests.get(f"http://{api_host}:{api_port}{path}",
                     params={'params': json.dumps(params)})
    result = r.json()

    return result


def put_to_api(path="/", data={}, files=None):
    try:
        requests.put(
            f"http://{api_host}:{api_port}{path}",
            json=data,
            files=files,
            timeout=1)
    except Timeout:
        pass


def post_to_api(path="/", data={}):
    requests.post(f"http://{api_host}:{api_port}{path}", json=data)


""" Сервис """
cass = RpcClient('db_api_tasks')
fts_service = RpcClient('fts_service')
users = None


def get_users():
    global users

    if users is None:
        users = read_users()

    return users


def read_users():
    users = {}
    # args = {'table': CassandraTables.users.value}
    # task = {'task_name': 'get_records',
    #         'args': args}
    # result = cass.call(task)
    query = CassQueryGenerator(cass, CassandraTables.users, 'select')
    result = query.run_task()
    for i in result:
        users[i['hash']] = i['username']

    return users


def write_user(user, user_hash):
    # args = {'table': CassandraTables.users.value,
    #         'values': {'user': user,
    #                    'hash': user_hash}}
    # task = {'task_name': 'insert_record',
    #         'args': args}
    query = CassQueryGenerator(cass, CassandraTables.users, 'insert')
    user = CassandraTables.users(username=user, hash=user_hash)
    query.add_record(user)
    query.run_task()
    # cass.send_message(task)


def add_user(user, user_hash):
    global users

    write_user(user, user_hash)
    users = None
    users = read_users()
    if user_hash not in users:
        users[user_hash] = user


def require_uid(fn):
    def check_uid(**kwargs):
        global users
        cookie_uid = request.get_cookie('user')
        # LOGGER.info((request.cookies))
        if cookie_uid:
            if cookie_uid not in get_users().keys():
                users = read_users()
            if cookie_uid not in get_users().keys():
                response.set_cookie('user', '', expires=0)
                redirect("/login")
            return fn(username=get_users()[cookie_uid], **kwargs)
        else:
            redirect("/login")

    return check_uid


def get_topics_states():
    states = request.get_cookie('topics_states', secret='tracked-key') or {}

    return json.dumps(states)


@route('/login')
@show_ip_address
@time_it
def login():
    """
    Авторизация
    """
    if request.query.get('user'):
        user = request.query.user
        # TODO: Elastic
        response.set_cookie('user', user)
        redirect('/')

    if request.query.get('create'):
        user = request.query.create
        user_hash = str(hash(user))
        response.set_cookie('user', user_hash)
        add_user(user, user_hash)
        redirect('/')

    users = get_users()

    return template('login', users=users)


@route('/logout')
@show_ip_address
@time_it
def logout():
    response.set_cookie('user', '', expires=0)
    redirect('/login')


@route('/notification')
def notification():
    username = request.query.username

    response.content_type = 'text/event-stream'
    response.cache_control = 'no-cache'

    yield 'retry: 100\n\n'

    notifyer = RpcClient("notifications_tasks")

    for i in range(60):
        task = {'task_name': 'get_notification',
                'args': {'username': username}}
        notification = notifyer.call(task)

        if notification is not None:
            yield f"data: {json.dumps(notification, default=str)}\n\n"
        else:
            yield f"data: {json.dumps('ping')}\n\n"

        sleep(1)


@route('/')
@require_uid
@show_ip_address
@time_it
def index(username):
    """
    Метод показывающий что сервис работает
    """
    data = f'<h1>Привет, {username}! Как твои дела?</h1>'

    return template('main', data=data, username=username)


@route('/test')
@require_uid
@show_ip_address
@time_it
def test(username):
    return template('test', username=username)


@route('/help')
@require_uid
@show_ip_address
@time_it
def help(username):
    """
    Справочная информация по использованию сервиса
    """
    with open('README.md') as f:
        data = f.read()
    data = markdown.markdown(data)

    return template('main', data=data, username=username)


@route('/groups-tasks', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def groups_tasks(username):
    """Задачи на парсинг групп"""

    is_keyword = False
    groups_to_add = None
    add_task = request.forms.get('add-task')
    if add_task:
        groups_to_add = request.forms.getall('add-to-task')
        social = "vk"
    keyword_task = request.forms.get('keyword_task')
    if keyword_task is not None:
        groups_to_add = request.forms.keyword_task.split()
        social = request.forms.social or "inst"
        LOGGER.info(f"Add keyword task for: {request.forms.social}")
        is_keyword = True
        # if social == 'inst':
        #     groups_ids = []
        #     for keyword in groups_to_add:
        #         group_id = get_from_api("/parse-keyword-info", {'keyword': keyword,
        #                                                       'social': social})
        #         if group_id is not None:
        #             groups_ids.append(group_id)

    add_custom_task = request.forms.get('custom_task')
    if add_custom_task is not None:
        groups_to_add = request.forms.custom_task.split()
        social = request.forms.social or "vk"
        LOGGER.info(f"Add custom task for: {request.forms.social}")
        if social == "ok" or social == "vk" or social == 'inst':
            groups_ids = []
            for group_url in groups_to_add:
                group_id = get_from_api("/parse-group-info",
                                        {'group': group_url,
                                         'social': social})
                if group_id is not None:
                    groups_ids.append(group_id)
            groups_to_add = groups_ids.copy()
    if groups_to_add is not None:
        # if social == "vk":
        #     groups_to_add = [group if int(group) < 0 else str(-int(group)) for group in groups_to_add]

        label = request.forms.label
        post_to_api("/add-task", {'social': social, 'groups': groups_to_add,
                                  'label': label, 'username': username,
                                  'is_keyword': is_keyword})
    tasks = get_from_api("/tasks")

    unique_groups_count = get_from_api("/unique-groups-count")

    return template('groups-tasks', tasks=tasks,
                    unique_groups_count=unique_groups_count, username=username)


@route('/create-rule', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def create_rule(username):
    words = request.forms.get('words')
    start_time = request.forms.get('start_time')
    start_date = request.forms.get('start_date')
    end_time = request.forms.get('end_time')
    end_date = request.forms.get('end_date')
    words = words.encode("iso-8859-1").decode("utf-8")
    social = request.forms.social or "vk"
    label = request.forms.label

    post_to_api("/add-rule", {
        'start_time': str(start_time),
        'start_date': str(start_date),
        'end_time': str(end_time),
        'end_date': str(end_date),
        'words': str(words),
        'social': str(social),
        'label': str(label)
    })
    req = 'ok'
    return json.dumps(req, indent=4, sort_keys=True, default=str)


@route('/track-topic', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def track_topic(username):
    tag = request.forms.get('tag').encode("iso-8859-1").decode("utf-8")
    social = request.forms.get('social')
    resp = get_from_api("/track-topic", {
        'tag': str(tag),
        'social': str(social),
        'username': str(username)
    })
    return json.dumps(resp, indent=4, sort_keys=True, default=str)


@route('/untrack-topic', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def untrack_topic(username):
    tag = request.forms.get('tag').encode("iso-8859-1").decode("utf-8")
    social = request.forms.get('social')
    resp = get_from_api("/untrack-topic", {
        'tag': str(tag),
        'social': str(social),
        'username': str(username)
    })
    return json.dumps(resp, indent=4, sort_keys=True, default=str)


@route('/create-topic', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def create_topic(username):
    words = request.forms.get('words')
    threshold = request.forms.get('threshold')
    words = words.encode("iso-8859-1").decode("utf-8")
    resp = get_from_api("/add-topic", {
        'words': str(words),
        'threshold': float(threshold)
    })
    response = {'ok': 'ok'}
    return json.dumps(response, indent=4, sort_keys=True, default=str)


@route('/check-topic', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def check_topic(username):
    words = request.forms.get('words')
    words = words.encode("iso-8859-1").decode("utf-8")
    resp = get_from_api("/check-topic", {
        'words': str(words)
    })
    response = resp['ignore_words']
    # response = []
    # for word in resp:
    #     # pos_neg_word = word.split('_')[0]
    #     response.append(pos_neg_word)
    return json.dumps(response, indent=4, sort_keys=True, default=str)


@route('/get-tasks', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_tasks(username):
    tasks = get_from_api("/tasks")

    result = []
    for task in tasks:
        if not task['is_keyword'] and task['social'] == 'vk' and task['is_active']:
            result.append(task)

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/get-task-groups', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_task(username):
    task_id = request.forms.get('task_id')
    task = get_from_api("/task-groups",
                        {'task-id': task_id, 'is_keyword': False})
    return json.dumps(task, indent=4, sort_keys=True, default=str)


@route('/task', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def task(username):
    """
    Конкретная задача на парсинг групп
    """
    task = request.query['id']
    is_keyword = request.query['is_keyword']
    LOGGER.info(is_keyword)
    groups = get_from_api("/task-groups",
                          {'task-id': task, 'is_keyword': is_keyword})

    if is_keyword != 'True':
        return template('task', groups=groups, username=username)
    else:
        return template('keyword_task', groups=groups, username=username)


@route('/notifications')
@require_uid
@show_ip_address
@time_it
def notifications(username):
    notifications = get_from_api("/notifications",
                                 {'username': username})

    return template('notifications', notifications=notifications,
                    username=username)


@route('/add-task', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def add_task(username):
    config = None
    groups = None
    new_parsed_groups = None
    label = ""
    set_new_config = request.forms.get('new-config')
    if set_new_config:
        config = {}
        members_count = request.forms.get('members_count')
        if members_count != "":
            config['members_count'] = int(members_count)
        our_prop = request.forms.get('our_prop')
        if our_prop != "":
            config['our_prop'] = float(our_prop)
        posts_per_day = request.forms.get('posts_per_day')
        if posts_per_day != "":
            config['posts_per_day'] = float(posts_per_day)
        views_prop = request.forms.get('views_prop')
        if views_prop != "":
            config['views_prop'] = float(views_prop)
        comments = request.forms.get('comments')
        if comments != "":
            config['comments'] = float(comments)

        cities = request.forms.getall('city')
        cities = [int(i) for i in cities]
        if len(cities) > 0:
            config['cities'] = cities

        groups = get_from_api('/groups', config)
        groups_ids = [group['group_id'] for group in groups]
        parsed_groups = get_from_api("/parsed-groups")
        parsed_groups = [parsed_group['group_id'] for parsed_group in parsed_groups]
        new_parsed_groups = [groups_id for groups_id in groups_ids if groups_id not in parsed_groups]
        new_parsed_groups = len(new_parsed_groups)

    return template('add-task', groups_config=config, groups=groups,
                    label=label, new_parsed_groups=new_parsed_groups,
                    username=username)


@route('/add-custom-task', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def add_custom_task(username):
    return template('add-custom-task', username=username)


@route('/add-keyword-task', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def add_keyword_task(username):
    return template('add-keyword-task', username=username)


@route('/change-task-state', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def change_task_state(username):
    state = request.forms['state'].lower() == 'true'
    task_id = int(request.forms['task_id'])
    post_to_api("/change-task-state", {'task_id': task_id, 'state': state})


@route('/drop-task', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def drop_task(username):
    task_id = int(request.forms['task_id'])
    post_to_api("/drop-task", {'task_id': task_id})


@route('/drop-topic', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def drop_topic(username):
    words = str(request.forms['words'])
    words = words.encode("iso-8859-1").decode("utf-8")
    post_to_api("/drop-topic", {'words': words})


@route('/drop-rule', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def drop_rule(username):
    tag = str(request.forms['tag'])
    social = str(request.forms['social'])
    tag = tag.encode("iso-8859-1").decode("utf-8")
    post_to_api("/drop-rule", {'tag': tag, 'social': social})


@route('/groups', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def groups(username):
    """
    Работа с группами : отслеживание, скрытие
    """

    groups = get_from_api("/user-groups", {'username': username})

    return template('groups', groups=groups, username=username)


@route('/change-user-group-state', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def change_user_group_state(username):
    state = request.forms['state'].lower() == 'true'
    group_id = request.forms['group_id']
    social = request.forms.get("social") or "vk"
    LOGGER.info(f'CHANGE {username} {group_id} TO {state}')
    post_to_api("/change-user-group-state", {'social': social,
                                             'username': username,
                                             'group_id': group_id,
                                             'state': state})


@route('/visual')
@require_uid
@show_ip_address
@time_it
def visual(username):
    """
    FoamTree визуализация тем
    """
    topics_states = get_topics_states()
    visual_data = get_from_api("/visual", {'topics_states': topics_states})

    return template('visual', visual_data=visual_data, username=username)


@route('/topics')
@require_uid
@show_ip_address
@time_it
def topics(username):
    """
    Темы в виде списка
    """
    topics_states = get_topics_states()
    visual_data = get_from_api("/visual", {'topics_states': topics_states})
    visual_data = json.loads(visual_data)

    return template('topics', visual_data=visual_data, username=username)


@route('/topic', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def topic(username):
    """
    Информация по теме и работа с ней
    """
    level = int(request.query['level'])
    topic = int(request.query['topic'])

    topics_states = json.loads(get_topics_states())

    change_trace = request.query.get('change-trace')
    if change_trace is not None:
        new_trace = request.forms.trace
        put_to_api("/add-trace-template",
                   {'level': level, 'topic': topic, 'new_trace': new_trace})

    change_state = request.forms.get('change-state')
    if change_state:
        state = int(request.forms.get('current-state'))

        topics_states[topic] = state

        LOGGER.info(f"STATE FOR {topic} NOW {state}")
        response.set_cookie('topics_states', topics_states,
                            secret='tracked-key')

    top_words = get_from_api('/top-words', {'topic': topic})
    top_words = ', '.join(top_words)

    trace_template = get_from_api("/trace-template", {'topic': topic})['trace']

    state = topics_states.get(str(topic)) or 0

    posts = get_from_api("/top-posts", {'topic': topic})

    return template('topic-posts', level=level, topic=topic,
                    top_words=top_words, posts=posts, trace=trace_template,
                    state=state, username=username)


@route('/post')
@require_uid
@show_ip_address
@time_it
def post(username):
    """
    Информация поста
    """
    owner_id = request.query['owner-id']
    post_id = request.query['post-id']
    social = request.query['social']
    post_info = get_from_api("/post-info", {'social': social,
                                            'owner-id': owner_id,
                                            'post-id': post_id})
    post = post_info['post_data']
    stat = post_info['post_stat']

    if stat is not None:
        stat['likes'] = [int(st) for st in stat['likes']]
        stat['shares'] = [int(st) for st in stat['shares']]
        stat['views'] = [int(st) for st in stat['views']]
        LOGGER.info(stat['replies'])
        stat['replies'] = [int(st) for st in stat['replies']]

    else:
        stat = {'likes': [post['likes']],
                'shares': [post['shares']],
                'views': [post['views']],
                'replies': [post['replies']],
                'len': [str(post['date'])]}

    bots = get_from_api("/bots-info", {'social': social})

    return template('post',
                    owner_id=owner_id,
                    post_id=post_id,
                    post=post,
                    topics=post_info['post_topics'],
                    social=social,
                    stat=stat,
                    # trace=post_info['template'],
                    initial_sort_id=2,
                    initial_sort_up=2,
                    username=username,
                    bots=bots)


@route('/posts-monitor', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def posts_monitor(username):
    """
    Работа с новыми постами: просмотр, фильтрация
    """
    set_filter = request.forms.get('filter-posts') == "True"
    if set_filter:
        likes = request.forms.get('likes')
        likes = int(likes) if likes != "" else None
        replies = request.forms.get('replies')
        replies = int(replies) if replies != "" else None
        shares = request.forms.get('shares')
        shares = int(shares) if shares != "" else None
        views = request.forms.get('views')
        views = int(views) if views != "" else None
        posts_count = request.forms.get('posts_count')
        posts_count = int(posts_count) if posts_count != "" else None
        filter_tracked = request.forms.get('filter_tracked') is not None
        filter_viewed = request.forms.get('filter_viewed') is not None

        cities = request.forms.getall('city')
        cities = [int(i) for i in cities]
        topics = request.forms.getall('topic')
        topics_list = []
        for one_topic in topics:
            one_topic = one_topic.encode("iso-8859-1").decode("utf-8")
            topics_list.append(one_topic)
        sort = request.forms.getall('sort')

        start_time = request.forms.get('start_time')
        start_date = request.forms.get('start_date')

        topics_states = json.loads(get_topics_states())

        socials = request.forms.getall('social')

        limits = {'likes': likes,
                  'replies': replies,
                  'shares': shares,
                  'views': views,
                  'posts_count': posts_count,
                  'filter_tracked': filter_tracked,
                  'filter_viewed': filter_viewed,
                  'checked_cities': cities,
                  'topics_states': topics_states,
                  'checked_topics': topics_list,
                  'sort': sort,
                  'start_time': start_time,
                  'start_date': start_date,
                  'checked_socials': socials}
        response.set_cookie('post_monitor', limits, secret='post_monitor')
    else:
        limits = request.get_cookie('post_monitor',
                                    secret='post_monitor') or {}
        limits['start_time'] = None
        limits['start_date'] = None
    limits['topics_states'] = json.loads(get_topics_states())
    limits['username'] = username

    # task = {'task_name': 'get_all_topics',
    #         'args': {}}
    # all_topics = cass.call(task)

    query = CassQueryGenerator(cass, CassandraTables.topic_vectors, 'select')
    all_topics = query.run_task() or []

    # task = {'task_name': 'get_all_vector_topics',
    #         'args': {}}
    # all_topics = cass.call(task)
    all_topics.insert(0, {'words': '-1'})
    limits['all_topics'] = all_topics
    return template('new-posts', limits, username=username)


@route('/full-text-search', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def full_text_search(username):
    tags = []
    task = {'task_name': 'get_user_tracked_rules',
            'args': {'username': username}}
    tracked_rules = cass.call(task)
    for rule in tracked_rules:
        tags.append(rule['tag'])
    return template('full-text-search', tags=tags, username=username)


@route('/get-rules', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_rules(username):
    task = {'task_name': 'get_rules',
            'args': {}}
    result = cass.call(task)
    task = {
        'track_rule': True
    }
    fts_service.call(task)
    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/get-topics', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_topics(username):
    # task = {'task_name': 'get_all_vector_topics',
    #         'args': {}}
    # result = cass.call(task)
    query = CassQueryGenerator(cass, CassandraTables.topic_vectors, 'select')
    result = query.run_task()
    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/get-rel-posts', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_rel_posts(username):
    data = request.forms.get('text')
    social = request.forms.get('social')
    owner_id = request.forms.get('owner_id')
    post_id = request.forms.get('post_id')
    result = get_from_api("/get-nearest-posts",
                          {'post_text': data,
                           'social': social,
                           'owner_id': owner_id,
                           'post_id': post_id})

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/test-topic', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def test_topic(username):
    words = request.forms.get('words')
    words = words.encode("iso-8859-1").decode("utf-8")
    response = get_from_api("/test-topic", {
        'words': str(words)
    })
    return json.dumps(response, indent=4, sort_keys=True, default=str)


@route('/get-posts', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_posts(username):
    default_date = request.forms.get('default_date') == "true"
    set_filter = request.forms.get('filter-posts') == "True"
    if set_filter:
        likes = request.forms.get('likes')
        likes = int(likes) if likes != "" else None
        replies = request.forms.get('replies')
        replies = int(replies) if replies != "" else None
        shares = request.forms.get('shares')
        shares = int(shares) if shares != "" else None
        views = request.forms.get('views')
        views = int(views) if views != "" else None
        posts_count = request.forms.get('posts_count')
        posts_count = int(posts_count) if posts_count != "" else None
        filter_tracked = request.forms.get('filter_tracked') is not None
        filter_viewed = request.forms.get('filter_viewed') is not None

        cities = request.forms.getall('city')
        cities = [int(i) for i in cities]

        topics = request.forms.getall('topic')
        topics_list = []
        for one_topic in topics:
            one_topic = one_topic.encode("iso-8859-1").decode("utf-8")
            topics_list.append(one_topic)
            LOGGER.info('Вывожу темы')
        LOGGER.info(topics)
        sort = request.forms.getall('sort')
        start_time = request.forms.get('start_time')
        start_date = request.forms.get('start_date')

        topics_states = json.loads(get_topics_states())

        limits = {'likes': likes,
                  'replies': replies,
                  'shares': shares,
                  'views': views,
                  'posts_count': posts_count,
                  'filter_tracked': filter_tracked,
                  'filter_viewed': filter_viewed,
                  'checked_cities': cities,
                  'topics_states': topics_states,
                  'checked_topics': topics_list,
                  'sort': sort,
                  'start_time': start_time,
                  'start_date': start_date}
        response.set_cookie('post_monitor', limits, secret='post_monitor')
    else:
        limits = request.get_cookie('post_monitor',
                                    secret='post_monitor') or {}
        if default_date:
            limits['start_time'] = None
            limits['start_date'] = None
    limits['topics_states'] = json.loads(get_topics_states())
    limits['username'] = username

    posts = get_from_api("/new-posts", limits)

    # limits['posts'] = posts

    result = {'limits': {'posts': posts},
              'username': username}

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/get-posts-by-rule', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_posts_by_rule(username):
    tag = request.forms.get('tag')
    social = request.forms.get('social')
    start_time = request.forms.get('start_time')
    start_date = request.forms.get('start_date')
    start_number = request.forms.get('start_number')
    end_time = request.forms.get('end_time')
    end_date = request.forms.get('end_date')
    default_date = request.forms.get('default_date')
    tag = tag.encode("iso-8859-1").decode("utf-8")
    drop_dupl = request.forms.get('drop_dupl')
    sort_viewed = request.forms.get('sort_viewed') == 'true'
    param = {
        'username': username,
        'tag': tag,
        'social': social,
        'default_date': default_date,
        'start_time': start_time,
        'start_date': start_date,
        'start_number': start_number,
        'end_time': end_time,
        'end_date': end_date,
        'drop_dupl': drop_dupl,
        'sort_viewed': sort_viewed
    }
    posts = get_from_api("/get-posts-by-rule", param)
    return json.dumps(posts, indent=4, sort_keys=True, default=str)


@route('/change-post-track-state', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def change_post_track_state(username):
    state = request.forms['is_tracked'].lower() == 'true'
    # Теперь разделяется по символу "*", а не "_"
    ids = request.forms['id'].split('*')
    LOGGER.info(ids)
    social, owner_id, post_id = ids[0], ids[1], ids[2]
    post_to_api("/change-post-track-state", {'username': username,
                                             'social': social,
                                             'owner_id': owner_id,
                                             'post_id': post_id,
                                             'state': state})


@route('/set-viewed-fts', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def set_viewed_fts(username):
    fts_ids = request.forms['id'].split('*')

    type = fts_ids[0]
    if type == 'post':
        social, owner_id, post_id = fts_ids[1:]
        add_id = ""
    else:
        social, owner_id, post_id, add_id = fts_ids[1:]

    data = {'username': username,
            'type': type,
            'social': social,
            'owner_id': owner_id,
            'post_id': post_id,
            'add_id': add_id}

    insert_data(CassandraTables.users_fts, data=data)


@route('/change-post-monitoring-state', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def change_post_monitoring_state(username):
    state = request.forms['is_tracked'].lower() == 'true'
    # Теперь разделяется по символу "*", а не "_"
    ids = request.forms['id'].split('*')
    LOGGER.info(ids)
    social, owner_id, post_id = ids[0], ids[1], ids[2]
    post_to_api("/change-post-monitoring-state", {'username': username,
                                                  'social': social,
                                                  'owner_id': owner_id,
                                                  'post_id': post_id,
                                                  'state': state})


@route('/check-post', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def check_post(username):
    post_id = request.forms['post_id']
    group_id = request.forms['group_id']
    social = request.forms['social']
    # day_count = request.forms['day_count']
    end_date = request.forms.end_date
    city = request.forms.city
    topic = request.forms.topic

    new_city = request.forms['new_city'] == "true"
    if new_city:
        insert_data(table=CassandraTables.tracked_cities,
                    data={'city': city})
    new_topic = request.forms['new_topic'] == "true"
    if new_topic:
        insert_data(table=CassandraTables.tracked_topics,
                    data={'topic': topic})
    result = get_from_api("/check-post", {'username': username,
                                          'social': social,
                                          'group_id': group_id,
                                          'post_id': post_id,
                                          'end_date': end_date,
                                          'city': city,
                                          'topic': topic})
    # result = "ok"
    # LOGGER.info(end_date)
    # LOGGER.info(new_topic)
    LOGGER.info(result)
    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/get-tracked-posts', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def get_tracked_posts(username):
    posts = get_from_api("/tracked-posts", {'username': username,
                                            'task_name': 'get_posts_monitoring'})

    return json.dumps(posts, indent=4, sort_keys=True, default=str)


def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta


def post_in_date(post, start_date, end_date):
    post_data = select_data(CassandraTables.all_posts,
                            None,
                            {'social': post['social'],
                             'owner_id': post['owner_id'],
                             'post_id': post['post_id']})

    try:
        post_data = post_data[0]
    except IndexError:
        LOGGER.warning(f'Cant find post {post}')
        return False

    post_start_date = post_data['date'].date()

    post_end_date = (post['end_date'] or post_data['date']).date()
    start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
    end_date = datetime.strptime(end_date, '%Y-%m-%d').date()

    if start_date <= post_start_date <= end_date:
        return True

    for result in perdelta(post_start_date, post_end_date, timedelta(days=1)):
        if start_date <= result <= end_date:
            return True

    return False


def get_group_base(social):
    if social == "vk":
        return 'https://vk.com/club'
    elif social == "ok":
        return 'https://ok.ru/group/'
    elif social == "inst":
        return 'https://instagram.com/'


def get_post_base(social):
    if social == "vk":
        return 'https://vk.com/wall'
    elif social == "ok":
        return 'https://ok.ru/group/'
    elif social == "inst":
        return 'https://instagram.com/'


def get_group_link(post):
    social = post['social']
    group_id = post['owner_id']
    if social == "vk" and group_id.startswith('-'):
        group_id = group_id[1:]

    base = get_group_base(social)

    return base + group_id


def get_post_link(post):
    social = post['social']
    group_id = post['owner_id']
    post_id = post['post_id']

    base = get_post_base(social)
    if social == "vk":
        between = "_"
    elif social == "ok":
        between = "/topic/"
    elif social == "inst":
        between = "p/"

    if social == "inst":
        return base + between + post
    else:
        return base + group_id + between + post_id


def get_post_sma_link(post):
    social = post['social']
    owner_id = post['owner_id']
    post_id = post['post_id']

    return "/post?social={}&owner-id={}&post-id={}".format(
        social, owner_id, post_id
    )


def get_post_links(post):
    result = dict()
    result['group_href'] = get_group_link(post)
    result['post_href'] = get_post_link(post)
    result['href'] = get_post_sma_link(post)

    return result


city_dict = {158: 'Челябинск',
             794: 'Златоуст',
             1573: 'Каменск-Уральский',
             5098: 'Копейск',
             5542: 'Касли',
             12126: 'Коркино',
             74: 'Курган',
             6050: 'Кыштым',
             82: 'Магнитогорск',
             7159: 'Миасс',
             941: 'Озерск',
             3034: 'Пласт',
             6820: 'Рощино',
             145: 'Троицк',
             3255: 'Чебаркуль',
             5401: 'Еманжелинск',
             1268: 'Южноуральск'}


def get_post_info(orig_post):
    post_fields = ['social', 'owner_id', 'post_id', 'date', 'text']
    post = select_data(CassandraTables.all_posts,
                       None,
                       {'social': orig_post['social'],
                        'owner_id': orig_post['owner_id'],
                        'post_id': orig_post['post_id']})[0]

    stat_fileds = ['views', 'likes', 'replies', 'shares']
    stat = select_data(CassandraTables.post_stats,
                       None,
                       {'social': post['social'],
                        'owner_id': post['owner_id'],
                        'post_id': post['post_id']})
    try:
        stat = sorted(stat, key=lambda x: x['ts'])[-1]
    except IndexError:
        stat = post.copy()

    group_fields = ['name', 'main_city', 'city']
    group_id = post['owner_id']
    if post['social'] == 'vk' and group_id.startswith('-'):
        group_id = group_id[1:]
    group = select_with_filters(Models.group,
                                {'social': post['social'],
                                 'group_id': group_id})[0]
    group = dict(group)
    group['city'] = city_dict.get(group['main_city'], '')

    tracked_fields = ['post_city', 'post_topic']

    result = dict()
    fields = (post_fields, stat_fileds, group_fields, tracked_fields)
    all_data = (post, stat, group, orig_post)
    for data_field, data in zip(fields, all_data):
        cur_data = {k: v for k, v in data.items() if k in data_field}
        result.update(cur_data)

    links = get_post_links(post)
    result.update(links)

    return result


@route('/download-saved-posts', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def download_saved_posts(username):
    posts = get_saved_posts()

    df = pd.DataFrame(posts)
    df = df[['date', 'post_city', 'name', 'post_topic', 'text', 'post_href',
             'views', 'likes', 'replies', 'shares']]
    df.index += 1
    total = pd.DataFrame({
        'likes': df.likes.sum(),
        'shares': df.shares.sum(),
        'views': df.views.sum(),
        'replies': df.replies.sum()
    }, index=['Всего'])

    total = pd.concat([df, total], sort=False)
    total.rename(columns={
        'date': 'Дата', 'post_city': 'Город', 'name': 'Группа',
        'post_topic': 'Тема', 'text': 'Текст', 'views': 'Просмотры',
        'likes': 'Лайки', 'replies': 'Комментарии', 'shares': 'Репосты',
        'post_href': 'Ссылка на пост'
    }, inplace=True)
    filename = 'download/report.xlsx'
    total.to_excel(filename)

    return static_file(filename, root='./')


def get_saved_posts():
    # user = request.forms.user
    # user = select_data(CassandraTables.users, filters={'username': user})[0]
    # if user.get('boss'):
    #     filters = None
    # else:
    #     filters = {'username': user['username']}
    filters = None
    LOGGER.info(dict(request.forms))
    start_date = request.forms.get('start_date')
    end_date = request.forms.get('end_date')

    cities = request.forms.cities
    cities = json.loads(cities)

    topics = request.forms.topics
    topics = json.loads(topics)

    posts = select_data(CassandraTables.tracked_posts,
                        filters=filters)

    filter_date = partial(post_in_date, start_date=start_date,
                          end_date=end_date)
    posts = [*filter(filter_date, posts)]

    if len(cities):
        posts = [*filter(lambda x: x['post_city'] in cities, posts)]
    if len(topics):
        posts = [*filter(lambda x: x['post_topic'] in topics, posts)]

    posts = [*map(get_post_info, posts)]

    return posts


@route('/get-saved-posts', method=['GET', 'POST'])
# @require_uid
# @show_ip_address
# @time_it
def saved_posts():
    posts = get_saved_posts()

    return json.dumps(posts, indent=4, sort_keys=True, default=str)


@route('/tracked-posts')
@require_uid
@show_ip_address
@time_it
def tracked_posts(username):
    # posts = get_from_api("/tracked-posts", {'username': username,
    #                                         'task_name': 'get_tracked_posts'})

    cities = select_data(table=CassandraTables.tracked_cities)
    cities = sorted(map(lambda x: x['city'], cities))
    topics = select_data(table=CassandraTables.tracked_topics)
    topics = sorted(map(lambda x: x['topic'], topics))

    return template('tracked-posts', username=username, cities=cities,
                    topics=topics)


@route('/traces', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def traces(username):
    """
    Работа со следами: просмотр, добавление
    """

    # traces = get_from_api("/traces")
    # traces = pd.DataFrame(traces)

    # if traces.shape[0] > 0:
    #     traces.author.fillna("", inplace=True)
    #     traces.bot.fillna("", inplace=True)
    #     traces.reply_id.fillna("", inplace=True)
    #     traces['status_class'] = traces.status.apply(set_status_class)
    #     traces.status = traces.status.apply(set_status)
    # traces = traces.to_dict('records')

    users = get_users()
    users = [user for user in users.values()]

    return template('traces', username=username, users=users)


@route('/add-trace', method=['POST'])
@require_uid
@show_ip_address
@time_it
def add_trace(username):
    owner_id = request.forms.owner_id
    post_id = request.forms.post_id
    social = request.forms.social
    trace = request.forms.get('trace').strip()
    bot = request.forms.bot.split("_")
    upload = request.files.upload
    LOGGER.info(f"upload: {upload}")
    if upload != "":
        filename = upload.filename
        if upload.raw_filename != upload.filename:
            filename = translit(upload.raw_filename, reversed=True)
        LOGGER.info(f"upload filename: {filename}")
        name, ext = os.path.splitext(filename)
        if ext.lower() not in ('.png', '.jpg', '.jpeg'):
            LOGGER.warning('File extension not allowed.')
            filename = None
        else:
            save_path = os.path.join(SRC_DIR, 'data')
            upload.filename = filename
            upload.save(save_path, overwrite=True)
            # upload = upload.filename
    else:
        filename = None
    # if upload is not None:
    #     put_to_api(
    #         "/save-image",
    #         files={'media': open(os.path.join(save_path, upload), 'rb')}
    #     )
    LOGGER.info("putting trace to api")
    put_to_api("/add-trace", {'social': social,
                              'owner_id': owner_id,
                              'post_id': post_id,
                              'trace': trace,
                              'login': bot[0],
                              'password': bot[1],
                              'author': username,
                              'filename': filename})
    LOGGER.info("put trace complete")

    redirect("/traces")


# @route('/reload-trace', method=['POST'])
# @require_uid
# @show_ip_address
# @time_it
# def reload_trace(username):
#     trace_id = request.forms.get('trace_id').split("_")

#     social, owner_id, post_id, date = trace_id

#     task = {'task_name': 'get_trace',
#             'args': {"social": social,
#                      "owner_id": owner_id,
#                      "post_id": post_id,
#                      "date": date}}
#     trace = cass.call(task)

#     task = {'task_name': 'get_bot',
#             'args': {"social": social,
#                      "login": trace['bot']}}
#     bot = cass.call(task)

#     # post_id = request.forms.get('post_id')
#     # social = request.forms.get('social')
#     # trace = request.forms.trace.strip()
#     # bot = request.forms.bot.split("_")
#     # LOGGER.info("putting trace to api")
#     post_to_api("/reload-trace", {'social': social,
#                                   'owner_id': owner_id,
#                                   'post_id': post_id,
#                                   'trace': trace['text'],
#                                   'login': bot['login'],
#                                   'password': bot['password'],
#                                   'author': trace['author'],
#                                   'date': date})
#     LOGGER.info("reload trace complete")

#     redirect("/traces")


def set_status(status):
    new_status = ""
    if status is None or status == "completed":
        new_status = "Оставлен"
    elif status == "in progress":
        new_status = 'В процессе добавления'
    elif status == "failed":
        new_status = "Возникла ошибка!"
    elif status == "deleted":
        new_status = "Удален!"

    return new_status


def set_status_class(status):
    if status in ["completed", "failed"]:
        return status
    elif status == "deleted":
        return "failed"
    elif status is None:
        return "completed"
    else:
        return ""


def save_photo(upload):
    # LOGGER.info(upload)

    list_name = []
    for i in upload:
        # LOGGER.info(i)

        if i != "":

            name, ext = os.path.splitext(i.filename)

            # LOGGER.info(name)
            # LOGGER.info(ext)

            if ext not in ('.png', '.jpg', '.jpeg'):
                LOGGER.warning('File extension not allowed.')

            else:
                save_path = os.path.join(SRC_DIR, 'data')
                LOGGER.info(save_path)
                i.save(save_path, overwrite=True)
                list_name.append(save_path + '/' + i.filename)
        else:
            list_name = None
            LOGGER.info('upload = None')

    return list_name


@route('/bots_editing_album', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def bots_editing_album(username):
    list_photo = request.files.getall('album')
    album_lenght = request.forms.get('album_lenght')
    album_name = request.forms.get('names')

    login = request.forms.get('login').encode("iso-8859-1").decode("utf-8")
    password = request.forms.get('password').encode("iso-8859-1").decode("utf-8")

    # LOGGER.info(list_photo)
    # LOGGER.info(album_lenght)
    # LOGGER.info(album_name)
    # LOGGER.info('-----')
    # LOGGER.info(login)
    # LOGGER.info(password)
    # LOGGER.info('========')

    list_name = save_photo(list_photo)
    # LOGGER.info(list_name)

    dict_res = False
    if list_name is not None:
        dict_res = get_from_api("/bots-editing-album",
                                {'photo': list_name,
                                 'album_lenght': album_lenght,
                                 'album_name': album_name,
                                 'login': login,
                                 'password': password})
    if dict_res:
        for i in list_name:
            os.remove(i)

    return json.dumps(dict_res, indent=4, sort_keys=True, default=str)


@route('/bots_editing_avatar', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def bots_editing_avatar(username):
    avatar_photo = request.files.avatar_photo
    login = request.forms.get('login').encode("iso-8859-1").decode("utf-8")
    password = request.forms.get('password').encode("iso-8859-1").decode("utf-8")

    # LOGGER.info(avatar_photo)
    # LOGGER.info('-----')
    # LOGGER.info(login)
    # LOGGER.info(password)
    # LOGGER.info('========')

    if avatar_photo != "":
        # LOGGER.info(avatar_photo.filename)

        name, ext = os.path.splitext(avatar_photo.filename)
        if ext not in ('.png', '.jpg', '.jpeg'):
            LOGGER.warning('File extension not allowed.')
            upload = None
        else:
            save_path = os.path.join(SRC_DIR, 'data')
            avatar_photo.save(save_path, overwrite=True)
            avatar_photo = avatar_photo.filename
            LOGGER.info('save photo')
    else:
        avatar_photo = None

    dict_res = False
    if avatar_photo is not None:
        dict_res = get_from_api("/bots-editing-avatar",
                                {'photo': save_path + '/' + avatar_photo,
                                 'login': login,
                                 'password': password})
    if dict_res:
        os.remove(save_path + '/' + avatar_photo)

    return json.dumps(dict_res, indent=4, sort_keys=True, default=str)


@route('/bots_login/<login>/<password>', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def bots_login(username, login, password):
    # LOGGER.info(login)
    # LOGGER.info(password)

    dict_res = get_from_api("/bots-login",
                            {'login': login, 'password': password})

    return json.dumps(dict_res, indent=4, sort_keys=True, default=str)


@route('/upload_template', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def upload_template(username):
    social = request.forms.get('social').encode("iso-8859-1").decode("utf-8")
    title = request.forms.get('title').encode("iso-8859-1").decode("utf-8")
    new_template = request.forms.get('new_template')

    LOGGER.info(social)
    LOGGER.info(title)
    LOGGER.info(new_template)

    dict_res = get_from_api("/upload-template",
                            {'social': social, 'title': title, 'new_template': new_template})

    return json.dumps(dict_res, indent=4, sort_keys=True, default=str)


@route('/del_template', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def del_template(username):
    social = request.forms.get('social').encode("iso-8859-1").decode("utf-8")
    title = request.forms.get('title').encode("iso-8859-1").decode("utf-8")

    LOGGER.info(social)
    LOGGER.info(title)

    dict_res = get_from_api("/del-template",
                            {'social': social, 'title': title})

    return json.dumps(dict_res, indent=4, sort_keys=True, default=str)


@route('/save_changes_text', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def save_changes_text(username):
    save_button = request.forms.get('save_button')
    src = request.forms.get('src')
    value = request.forms.get('value').encode("iso-8859-1").decode("utf-8")

    LOGGER.info(src)
    LOGGER.info(value)
    LOGGER.info(save_button)

    res = get_from_api("/save-changes-text",
                       {'value': value,
                        'src': src})

    return json.dumps({'res': res, 'save_button': save_button}, indent=4, sort_keys=True, default=str)


@route('/change_bot_template', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def change_bot_comment(username):
    LOGGER.info(dict(request.forms))

    login = request.forms.login
    template_profil = request.forms.template_profil
    social = request.forms.social
    password = request.forms.password

    LOGGER.info(f"CHANGE COMMENT FOR '{login}'' TO '{template_profil}'")

    post_to_api('/change-bot-template',
                {'social': social, 'login': login, 'password': password, 'template_profil': template_profil})

    # return json.dumps(dict_res, indent=4, sort_keys=True, default=str)


@route('/bots_login_editing', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def bots_login_editing(username):
    return template('bot_editing', username=username)


@route('/bots', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def bots(username):
    """
    Работа с ботами: просмотр, добавление, удаление
    """
    bots_to_drop = request.forms.getall('drop')
    if bots_to_drop is not None:
        logins = [bot.split('_')[1] for bot in bots_to_drop]
        socials = [bot.split('_')[0] for bot in bots_to_drop]
        post_to_api("/drop-bots", {'bots': logins, 'socials': socials})

    add_bot = request.forms.get('add_bot') == "True"
    LOGGER.info(add_bot)
    if add_bot:
        bot = {'social': request.forms.social,
               'name': request.forms.name,
               'login': request.forms.login,
               'password': request.forms.password,
               'city': request.forms.city,
               'comment': request.forms.comment,
               'template_profil': "",
               'password': ""}
        put_to_api("/add-bot", bot)

    bots = get_from_api("/bots-info")
    drop_bots = request.forms.get('drop_bots') == 'True'

    return template('bots', bots=bots, drop_bots=drop_bots, username=username)


@route('/trends', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def trends(username):
    return template('trends', username=username)


@route('/suggest-posts', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def suggest_posts(username):
    # suggestions = get_from_api("/suggestions")
    # for suggestion in suggestions:
    #     suggestion["link"] = "/suggestion?date=" \
    #                          + urllib.parse.quote(suggestion['date'])
    #     suggestion['text'] = suggestion['text'] or ""

    users = get_users()
    users = [user for user in users.values()]

    return template('suggest-posts', username=username,
                    users=users)


@route('/suggestion', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def suggestion(username):
    states = {'in progress': 'В процессе добавления',
              'completed': 'Предложен',
              'failed': 'Возникла ошибка!',
              'posted': 'Опубликован'}

    date = request.query.date
    suggestions = get_from_api("/suggestion-groups",
                               {"date": date})
    for post in suggestions:
        if post['post_id'] != "":
            post['social_post_link'] = f"https://vk.com/wall-{post['owner_id']}_{post['post_id']}"
            post['service_post_link'] = f"/post?social=vk&owner-id=-{post['owner_id']}&post-id={post['post_id']}"

        post['state'] = states[post['state']]

    info = get_from_api("/suggestion",
                        {"date": date})

    title = info['title']
    text = info['text']

    return template('suggestion', username=username,
                    suggestions=suggestions, title=title, text=text)


@route('/new-suggest', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def new_suggest(username):
    bots = get_from_api("/bots-info")

    return template('add-suggestion', username=username, bots=bots)


@route('/add-suggest-post', method=['POST'])
@require_uid
@show_ip_address
@time_it
def add_suggest_posts(username):
    title = request.forms.get('title') or "<без названия>"
    text = request.forms.get('text').strip()

    upload = request.files.get('upload') or ""

    if upload != "":
        filename = upload.filename
        if upload.raw_filename != upload.filename:
            filename = translit(upload.raw_filename, reversed=True)
        LOGGER.info(f"upload filename: {filename}")
        name, ext = os.path.splitext(filename)
        if ext.lower() not in ('.png', '.jpg', '.jpeg'):
            LOGGER.warning('File extension not allowed.')
            filename = None
        else:
            save_path = os.path.join(SRC_DIR, 'data')
            upload.filename = filename
            upload.save(save_path, overwrite=True)
    else:
        filename = None

    social = request.forms.social
    bot = request.forms.bot
    groups = request.forms.getall("group")
    groups = list(set(groups))

    suggest = {"title": title,
               "text": text,
               "upload": filename,
               "social": social,
               "bot": bot,
               "groups": groups}

    LOGGER.info(suggest)
    bot = bot.split("_")

    # if upload is not None:
    #     put_to_api(
    #         "/save-image",
    #         files={'media': open(os.path.join(save_path, upload), 'rb')}
    #     )

    put_to_api("/add-suggestion",
               {'groups': groups,
                'trace': text,
                'login': bot[0],
                'password': bot[1],
                'author': username,
                'filename': filename,
                'title': title})

    LOGGER.info("put suggestion complete")

    redirect("/suggest-posts")


@route('/load-trends', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def load_trends(username):
    LOGGER.info("Start load info")
    my_groups = request.forms['my_groups'].lower() == 'true'
    topics_states = json.loads(get_topics_states())
    trends = get_from_api("/trends", {'my_groups': my_groups,
                                      'username': username,
                                      'topics_states': topics_states,
                                      'period': request.forms['period']})

    LOGGER.info("Info received. START creating diagram")

    data = {}
    for info in trends['topics_distribution']:
        data[info['topic']] = info['count']

    plot = create_pie_chart(data)

    LOGGER.info("DIAGRAM RECEIVED")

    script = json_item(plot)

    result = {'script': script,
              'trend_words': trends['top_words']}

    return json.dumps(result, indent=4, sort_keys=True, default=str)


@route('/change-bot-comment', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def change_bot_comment(username):
    LOGGER.info(dict(request.forms))
    login = request.forms.login
    comment = request.forms.comment
    social = request.forms.social
    LOGGER.info(f"CHANGE COMMENT FOR '{login}'' TO '{comment}'")
    post_to_api('/change-bot-comment',
                {'social': social, 'login': login, 'comment': comment})


@route('/reports', method=['GET', 'POST'])
@require_uid
@show_ip_address
@time_it
def reports(username):
    users = get_users()
    users = [user for user in users.values()]

    return template('reports', username=username, users=users)


@route('/get-report', method=['POST'])
@require_uid
@show_ip_address
@time_it
def get_report(username):
    report_type = request.forms.get('report_type')
    user = request.forms.user
    month = int(request.forms.get('month'))
    year = int(request.forms.get('year'))

    report = get_from_api("/report", {'report_type': report_type,
                                      'user': user,
                                      'month': month,
                                      'year': year})

    return json.dumps(report, indent=4, sort_keys=True, default=str)


@route('/get-smm-report', method=['POST'])
@require_uid
@show_ip_address
@time_it
def get_smm_report(username):
    report_type = request.forms.get('report_type')

    args = {'report_type': report_type,
            'operator': request.forms.get('operator')}

    if report_type == 'month':
        args['month'] = int(request.forms.get('month'))
        args['year'] = int(request.forms.get('year'))
    elif report_type == 'day':
        args['date'] = request.forms.get('date')

    report = get_from_api("/smm-report", args)

    return json.dumps(report, indent=4, sort_keys=True, default=str)


@route('/get-traces', method=['POST'])
@require_uid
@show_ip_address
@time_it
def get_traces(username):
    user = request.forms.user
    start_date = request.forms.get('start_date')
    end_date = request.forms.get('end_date')

    traces = get_from_api("/traces", {'user': user,
                                      'start_date': start_date,
                                      'end_date': end_date})

    traces = pd.DataFrame(traces)

    if traces.shape[0] > 0:
        traces.author.fillna("", inplace=True)
        traces.bot.fillna("", inplace=True)
        traces.reply_id.fillna("", inplace=True)
        traces['status_class'] = traces.status.apply(set_status_class)
        traces.status = traces.status.apply(set_status)
    traces = traces.to_dict('records')

    return json.dumps(traces, indent=4, sort_keys=True, default=str)


@route('/get-suggestions', method=['POST'])
@require_uid
@show_ip_address
@time_it
def get_suggestions(username):
    user = request.forms.user
    start_date = request.forms.get('start_date')
    end_date = request.forms.get('end_date')

    suggestions = get_from_api("/suggestions", {'user': user,
                                                'start_date': start_date,
                                                'end_date': end_date})
    for suggestion in suggestions:
        suggestion["link"] = "/suggestion?date=" \
                             + urllib.parse.quote(suggestion['date'])
        suggestion['text'] = suggestion['text'] or ""

    return json.dumps(suggestions, indent=4, sort_keys=True, default=str)


@route(r"/static/js/<filepath:re:.*\.js>")
def js(filepath):
    return static_file(filepath, root="static/js")


@route(r"/static/foamtree-3.4.3/<filepath:re:.*\.js>")
def foamtree(filepath):
    return static_file(filepath, root="static/foamtree-3.4.3")


@route(r"/images/<filepath:re:.*\.png>")
def img_png(filepath):
    return static_file(filepath, root="images")


@route(r"/images/<filepath:re:.*\.svg>")
def img_svg(filepath):
    return static_file(filepath, root="images")


@route(r"/trace_images/<filepath:re:.*\.jpg>")
def trace_img_jpg(filepath):
    return static_file(filepath, root="data")


@route(r"/trace_images/<filepath:re:.*\.png>")
def trace_img_png(filepath):
    return static_file(filepath, root="data")


@route(r"/trace_images/<filepath:re:.*\.jpeg>")
def trace_img_jpeg(filepath):
    return static_file(filepath, root="data")


if __name__ == '__main__':
    # CONFIG = ConfigParser()
    # CONFIG.read("config/config.ini", encoding='utf8')
    CONFIG = config.get_config('./common_lib/config/web.yml')

    API_VERSION = CONFIG['INFO']['API_VERSION']
    NUM_WORKERS = CONFIG['BUILD']['WORKERS']
    DEBUG = CONFIG['BUILD']['DEBUG']
    WITH_SSL = CONFIG['BUILD']['WITH_SSL']
    # DEBUG = CONFIG.getboolean('BUILD', 'DEBUG')
    # WITH_SSL = CONFIG.getboolean('BUILD', 'WITH_SSL')

    LOGGER.info(f"SERVICE CONFIG: API VERSION: {API_VERSION} DEBUG MODE: {DEBUG}")

    if WITH_SSL:
        run(host='0.0.0.0', port=8095, server='gunicorn', workers=NUM_WORKERS,
            timeout=100, reload=True, debug=DEBUG, worker_class='gevent',
            certfile="star_is74_ru.crt", keyfile="star_is74_ru.key")
    else:
        run(host='0.0.0.0', port=8095, server='gunicorn', workers=NUM_WORKERS,
            timeout=100, reload=True, debug=DEBUG, worker_class='gevent')
