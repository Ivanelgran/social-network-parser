% from datetime import datetime, timedelta
% start_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
% end_date = datetime.now().strftime("%Y-%m-%d")
% include('main.tpl')

<style>

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.hidden {
    display: none;
}

td.completed {
  color: green;
}

td.failed {
  color: red;
}

</style>

<form action="/new-suggest" style="margin:20;">
<input value="Добавить вброс" type="submit" />
</form>

<table>
<tr>
<td align="center">Выводить все вбросы 
</td>
<td>
<label class="switch">
  <input type="checkbox" onChange="changeView()" id="filter_switch">
  <span class="slider round"></span>
</label>
</td>
</tr>
</table>

<div id="user_filter" class="hidden">
<p>Пользователь: 
<select id="users" onChange="changeView()">
<option value="all">
Все
</option>
% for user in users:
<option value="{{user}}">
{{user}}
</option>
% end
</select>
</p>
</div>
<p>Дата:<br> 
<input type="date" value="{{start_date}}" id="start_date" onChange="changeView()"> - 
<input type="date" value="{{end_date}}" id="end_date" onChange="changeView()">
</p>

<table class="sort" align="center">
<thead>
<tr>
<td>№</td>
<td>Название</td>
<td>Дата</td>
<td>Автор</td>
<td>Опубликовано</td>
<td><img width="30" src="images/eye_monitor.png"></td>
<td><img width="30" src="images/like_outline_24.svg"></td>
<td><img width="30" src="images/comment_outline_24.svg"></td>
<td><img width="30" src="images/share_outline_24.svg"></td>
</tr>
</thead>
<tbody id="main_body">
</tbody>
</table>

<script>
function changeView() {
    var checkbox = document.getElementById("filter_switch")
    var users = document.getElementById("users");
    var curUser = users.options[users.selectedIndex].value;
    var user = "{{username}}"
    var body = document.getElementById("main_body");
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;

    body.innerHTML = "";
    
    if (checkbox.checked) {
        var getAll = curUser === "all";
        document.getElementById("user_filter").className = ""
    } else {
        var getAll = false
        curUser = user
        document.getElementById("user_filter").className = "hidden"
    }

    console.log(curUser)

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/get-suggestions", true);
    xhttp.send("user=" + encodeURIComponent(curUser)
               + "&start_date=" + start_date.toString()
               + "&end_date=" + end_date.toString());

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("traces GOT");
          var suggestions = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText)
          var suggestions = []
        }

        for (var i = 0, length = suggestions.length; i < length; i++) {
            
            var trace_date = new Date(suggestions[i]['date']);
            var newTr = document.createElement('tr');

            var td = document.createElement('td');
            td.innerHTML = i + 1;
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = '<a href="' + suggestions[i]['link'] + '">' + suggestions[i]['title'] + '</a>';
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = suggestions[i]['date'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = suggestions[i]['author'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = suggestions[i]['posted_count'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = suggestions[i]['views'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = suggestions[i]['likes'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = suggestions[i]['replies'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = suggestions[i]['shares'];
            newTr.appendChild(td);

            body.appendChild(newTr);
        
        }
    }
}
</script>

<script>
window.onload = changeView();
</script>

% include('notification.tpl')