% include('main.tpl')

% suggestions = suggestions if defined('suggestions') else None
% if suggestions is not None:

<p>{{title}}</p>
<p>{{text}}</p>

<table class="sort" align="center">
<thead>
<tr>
<td>№</td>
<td>Группа</td>
<td>Состояние</td>
<td>Пост</td>
</tr>
</thead>
<tbody id="main_body">
% for counter, info in enumerate(suggestions):
    <tr>
        <td>{{counter+1}}</td>
        <td><a href="{{info['link']}}">{{info.get('name') or "<Имя группы>"}}</a></td>
        <td>{{info['state']}}</td>
        % if info['state'] == "Опубликован":
        <td><a href="{{info['social_post_link']}}">ВК</a> <a href="{{info['service_post_link']}}">SMA</a></td>
        % else:
        <td></td>
        % end
    </tr>
    % end
% end
</tbody>
</table>
% end

% include('notification.tpl')