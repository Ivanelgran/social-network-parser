<html>
	<head>

		
	</head>

	<body> 
        % include('main.tpl')

		<div id="visualization" style="width: 100%; height: 100%; float:left;"></div>
		
        <script src="/static/foamtree-3.4.3/carrotsearch.foamtree.js"></script>

        <script>		
            window.addEventListener("load", function() {
                var foamtree = new CarrotSearchFoamTree({
                    groupColorDecorator: function(opts, params, vars) {
                        var is_tracked = params.group.is_tracked;
                        if (params.group.level === 0){
                            vars.labelColor = "#000";
                            vars.groupColor.h = 60;
                            vars.groupColor.s = 80;
                            vars.groupColor.l = 50;
                            vars.groupColor.a = 0.4;
                        } else {
                            vars.labelColor = "#0006";
                            if (params.group.is_tracked) {
                                vars.groupColor.h = 120;
                                vars.groupColor.s = 100;
                                vars.groupColor.l = 50;
                                vars.groupColor.a = 0.8;        
                            } else if (params.group.is_hidden) {
                                vars.groupColor.h = 120;
                                vars.groupColor.s = 120;
                                vars.groupColor.l = 120;
                                vars.groupColor.a = 0.4;
                            } else {
                                vars.groupColor.h = 60;
                                vars.groupColor.s = 100;
                                vars.groupColor.l = 50;
                                vars.groupColor.a = 0.6;
                            }
                        }
                        vars.groupColor.model = "hsla";
                    },
                    id: "visualization",
                    dataObject: {{!visual_data}},
                    rolloutDuration : 0,
                    pullbackDuration : 0,
                    fadeDuration: 500,  
                    groupBorderWidth: 0,
                });
                foamtree.on("GroupClick", function (info) {
                    if (info.group.selectable && !info.secondary) {
                        window.open(`/topic?level=${info.group.level}&topic=${info.group.topic}`);
                    }
                });
            });	    
        </script>
	</body>
</html>

% include('notification.tpl')