% include('main.tpl')

% if social == "vk":
% group_href = f"https://vk.com/club{owner_id[1:]}"
% href = f"https://vk.com/wall{owner_id}_{post_id}"
% elif social == "ok":
% group_href = f"https://ok.ru/group/{owner_id}"
% href = f"https://ok.ru/group/{owner_id}/topic/{post_id}"
% elif social == "inst":
% group_href = f"https://instagram.com/{owner_id}"
% href = f"https://instagram.com/p/{post_id}"
% end

<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<style>
      a.content {
    color: #000000;
    text-decoration: none;
   }
   a.content:visited {
    color: #000000; 
    text-decoration: none;
   }
   .mySlides {display: none}
   .slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}
/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}
#rel_posts{
  background: #E0FFFF;
  padding: 20;
}
.loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.hidden {
  display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

</style>

<div style="padding:10;">
<div style="float:left;width:40%;padding:20;background-color:rgb(252, 247, 243);">

  <div style="float:left">
  <img width="50" src="images/{{social}}.png">
  </div>

  <div style="float:left">
    <div style="margin-left:15;color:rgb(0,100,186);font-weight:bold;">
      <a target="_blank" href="{{group_href}}" class="content">{{post['name']}}</a>
    </div>
    <div style="margin:10 15;color:darkslategrey;">
      {{post['date']}}
    </div>
  </div>

  <div style="clear:left;"><a target="_blank" href="{{href}}" class="content" id="post_text">{{!post['text'].replace('\n', '<br>')}}</a></div>
  <div style="float:left;padding-top:20">
    <div style="float:left;"><img width="30" src="images/like_outline_24.svg"></div> 
    <div style="float:inherit;margin:8 5;">{{stat['likes'][-1]}}</div>
  </div>
  <div style="float:left;padding-top:20">
    <div style="float:left;"><img width="30" src="images/comment_outline_24.svg"></div> 
    <div style="float:inherit;margin:8 5;">{{stat['replies'][-1]}}</div>
  </div>
  % if social != "inst":
    <div style="float:left;padding-top:20">
      <div style="float:left;"><img width="30" src="images/share_outline_24.svg"></div> 
      <div style="float:inherit;margin:8 5;">{{stat['shares'][-1]}}</div>
    </div>
    <div style="float:right;padding-top:30"><img width="30" src="images/eye_monitor.png"> {{stat['views'][-1]}}</div>
  % end
</div>

% topics = topics if defined('topics') else None

% if topics is not None:
<div style="float:left;padding:0 20;width:30%;">
<table class="sort" align="center">
<thead>
<tr>
    <td>Тема</td>
   <!-- <td>Название</td> -->
    <td>Вероятность</td>
</tr>
</thead>
<tbody>
% for topic in topics:
    <tr>
        <!-- % href = f"/topic?level=1&topic={topic['topic']}" -->
        <td><a target="_blank" href={{href}}>{{topic['topic']}}</a></td>
        <!--<td>{{topic['topic']}}</td> -->
        <td>{{topic['probability']}}</td>
    </tr>
% end
</tbody>
</table>
</div>
% end





<div style="float:right;width:25%;">
<p style="
    text-align: center;
    font-size: 20;
    font-weight: bold;
">Похожие посты:</p>
<div class="slideshow-container" id="rel_posts">

<div class="loader" id="loader"></div>


</div>
</div>


% trace = trace if defined('trace') else ""

<div style="clear: left;padding:20;">
<form action="/add-trace" method="post" enctype="multipart/form-data" accept-charset='utf-8'>
<p>Вложения: <input type="file" name="upload" /></p>
<p>Рекомендуемый след</p>
<input name="social" type="hidden" value={{post['social']}}>
<input name="owner_id" type="hidden" value={{owner_id}}>
<input name="post_id" type="hidden" value={{post_id}}>
<p><textarea cols=70 rows=8 name="trace">{{trace}}</textarea></p>
<p>Бот: 
<select name="bot">
% for bot in bots:
<option value="{{bot['login']}}_{{bot['password']}}">
{{bot['name']}} -- {{bot['city']}} -- {{bot['comment']}}
</option>
% end
</select>
</p>
% can_add_trace = "" if len(bots) > 0 else "disabled"
<input value="Оставить след к посту" type="submit" {{can_add_trace}}/>
</form>
</div>

<div id="stat_likes" style="clear:left;width:49%;float:left;"></div>
<div id="stat_replies" style="width:49%;float:right;"></div>
<div id="stat_shares" style="clear:left;width:49%;float:left;"></div>
<div id="stat_views" style="width:49%;float:right;"></div>

<script>
function getRelPosts() {
    console.log("Task: get rel posts");

    var social = "{{!social}}";
    var owner_id = "{{!owner_id}}";
    var post_id = "{{!post_id}}";

    document.getElementById("loader").className = "loader";
    var xhttp = new XMLHttpRequest();
    var text = document.getElementById("post_text").innerHTML.toString()
    console.log(text)
    xhttp.open("POST", "/get-rel-posts", true);
    xhttp.send("text=" + text.toString() + "&social=" + social + "&owner_id=" + owner_id + "&post_id=" + post_id);
    console.log("Getting posts");
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = []
        }
        console.log(response)
        var rel_posts = document.getElementById("rel_posts")
        document.getElementById("loader").className = "hidden";


        for (var i = 0, length = response.length; i < length; i++)
        {
            var Div1 = document.createElement('div');
            Div1.className = "mySlides";

            var newDiv = document.createElement('div');
            newDiv.className = "float:left;padding:20;background-color:rgb(243, 248, 252);border:1px solid black;";
            newDiv.innerHTML = '<div style="float:left">' + '<img width="50" src="images/' + response[i]['social'] + '.png">';

            var Div = document.createElement('div');
            Div.style = "float:left"
            Div.innerHTML = '<div style="margin-left:15;color:rgb(0,100,186);font-weight:bold;">' +
                  '<a target="_blank" href="' + response[i]['group_href'] + '" class="content">' + response[i]['name'] + '</a></div>' +
                  '<div style="margin:10 15;color:darkslategrey;">' + response[i]['date'] + '</div>'
            newDiv.appendChild(Div)

            Div = document.createElement('div');
            Div.style = "clear:left"
            if (response[i]['text'].length < 500) {
                Div.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'] + "</a>"
                + "<p> <strong>Score:</strong> " + response[i]['score'] + "</p>";

            } else {
                Div.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'].slice(0, 500) + "</a>" 
                               + '<a class="wall_post_more" onclick="hide(this); show(this);"> Показать полностью…</a>'
                               + '<span style="display: none">' + response[i]['text'].slice(500) + '</span>' + "<p><strong>Score:</strong> " + response[i]['score'] + "</p>";
            }
            newDiv.appendChild(Div);

            var Div = document.createElement('div');
            Div.style = "float:left;padding-top:20"
            Div.innerHTML = '<div style="float:left;">' + '<img width="30" src="images/like_outline_24.svg">' + '</div>' + 
            '<div style="float:inherit;margin:8 5;">' + response[i]['likes']
            newDiv.appendChild(Div)

            var Div = document.createElement('div');
            Div.style = "float:left;padding-top:20"
            Div.innerHTML = '<div style="float:left;">' + '<img width="30" src="images/comment_outline_24.svg">' + '</div>' + 
            '<div style="float:inherit;margin:8 5;">' + response[i]['replies']
            newDiv.appendChild(Div)

            if (response[i]['social'] != "inst")
            {
              var Div = document.createElement('div');
              Div.style = "float:left;padding-top:20"
              Div.innerHTML = '<div style="float:left;">' + '<img width="30" src="images/share_outline_24.svg">' + '</div>' + 
              '<div style="float:inherit;margin:8 5;">' + response[i]['shares']
              newDiv.appendChild(Div)

              var Div = document.createElement('div');
              Div.style = "float:right;padding-top:30"
              Div.innerHTML = '<img width="30" src="images/eye_monitor.png">' + response[i]['views']
              newDiv.appendChild(Div)
            }
            
            
            Div1.appendChild(newDiv)
            rel_posts.appendChild(Div1)
        }

        var Div3 = document.createElement('div');
        Div3.style = "text-align:center;clear:both;padding-top:10;"
        for (var i = 0, length = response.length; i < length; i++)
        {
          var divv = document.createElement('a')
          divv.innerHTML = '<span class = "dot" onclick="currentSlide(' + i + ')">'
          Div3.appendChild(divv)
        }
        rel_posts.appendChild(Div3)
        showSlides(0);
    }
}
function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var slideIndex;
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[n].style.display = "block";  
  dots[n].className += " active";
}
</script>

<script>
function hide(link) {
    link.style = "display: none;";
}
function show(link) {
    var span = link.nextElementSibling;
    var all_text = link.previousElementSibling;
    all_text.innerHTML = all_text.innerHTML + span.innerHTML
}
</script>

<script type="text/javascript">
window.onload = onPageLoad();

function onPageLoad() {
  getRelPosts()
}

</script>

% if stat is not None:
<script>

var likes = {
  x: {{!stat['len']}},
  y: {{stat['likes']}},
  type: 'scatter',
  name: 'Лайки'
};

var shares = {
  x: {{!stat['len']}},
  y: {{stat['shares']}},
  type: 'scatter',
  name: "Репосты"
};
var replies = {
  x: {{!stat['len']}},
  y: {{stat['replies']}},
  type: 'scatter',
  name: "Комментарии"
};

var views = {
  x: {{!stat['len']}},
  y: {{stat['views']}},
  type: 'scatter',
  name: "Просмотры"
};

Plotly.newPlot('stat_views', [views], {title:'Просмотры'});
Plotly.newPlot('stat_likes', [likes], {title:'Лайки'});
Plotly.newPlot('stat_shares', [shares], {title:'Репосты'});
Plotly.newPlot('stat_replies', [replies], {title:'Комментарии'});
</script>
% end

% include('notification.tpl')