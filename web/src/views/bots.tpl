% include('main.tpl')
% drop_bots = drop_bots if defined('drop_bots') else False

% if not drop_bots:

<p>Добавить бота</p>
<form action="/bots" method="post">
    <table>
        <tr><td>
        <input name="add_bot" type="hidden" value="True">
        </td></tr>
        <tr>
            <td><strong style="line-height: 40px">Социальная сеть</strong></td>
            <td>
                <select id="social_select" name="social" onchange="upload_template_selector()">
                    <option value="vk">ВКонтакте</option>
                    <option value="ok">Одноклассники</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong style="line-height: 40px">Имя </strong></td>
            <td><input type="text" name="name"></p></td>
        </tr>
        <tr>
            <td><strong style="line-height: 40px">Логин </strong></td>
            <td><input type="text" name="login"></td>
        </tr>
        <tr>
            <td><strong style="line-height: 40px">Пароль </strong></td>
            <td><input type="text" name="password"></td>
        </tr>
        <tr>
            <td><strong style="line-height: 40px">Город </strong></td>
            <td> <input type="text" name="city"></td>
        </tr>
        <tr>
            <td><strong style="line-height: 40px">Комментарий </strong></td>
            <td><input type="text" name="comment"></td>
        </tr>
<!--        <tr>-->
<!--            <td><strong style="line-height: 40px">Шаблон профиля </strong></td>-->
<!--            <td><select id="template_profil" name="template_profil"></select></td>-->
<!--        </tr>-->
        <tr><td>
            <input value="Добавить бота" type="submit" /></p>
        </td></tr>
    </table>
</form>

<form id="drop_bots" action="/bots" method="post">
<input name="drop_bots" type="hidden" value="True">
<input value="Удалить ботов" type="submit" />
</form>
% else:
<form id="drop_bots" action="/bots" method="post" />
<input value="Удалить" type="submit" form="drop_bots" />
% end

% bots = bots if defined('bots') else None
% if bots is not None:

<table class="sort" align="center">
<thead>
<tr>
% if drop_bots:
<td>Удалить</td>
% end
<td>№</td>
<td>Соц. сеть</td>
<td>Имя</td>
<td>Город</td>
<td>Комментарий</td>
<td>Шаблон</td>
<td>Подтвердить шаблон</td>
<td>Статус подтверждения</td>
</tr>
</thead>
<tbody>
% for counter, info in enumerate(bots):
    <tr>
        % if drop_bots:
        <td><input name="drop" type="checkbox" value="{{info['social']}}_{{info['login']}}" form="drop_bots"></td>
        % end
        <td>{{counter+1}}</td>
        <td>{{info['social']}}</td>
        <td>{{info['name']}}</td>
        <td>{{info['city']}}</td>
        <td><input type="text" value="{{info['comment']}}" id="{{info['social']}}_{{info['login']}}" onChange="changeBotComment(this)"></td>
        <td>
            <select type="text" value="{{info['template_profil']}}" id="{{info['social']}}_{{info['login']}}_{{info['template_profil']}}" name="table_select_template" onclick="changeBotTemplateStatus(this)"></select>
        </td>
        <td>
            <input type="button" value="Ok" id="{{info['social']}}_{{info['login']}}_{{info['password']}}_{{info['template_profil']}}_verification" name="table_select_template_verification" onclick="changeBotTemplate(this)">
            <input value="{{info['template_profil']}}" id="{{info['social']}}_{{info['login']}}_{{info['template_profil']}}_status" style = "width:20px;height:20px;visibility: hidden "></input>
        </td>
        <td id="{{info['social']}}_{{info['login']}}_{{info['template_profil']}}_img_verification">{{info['status_template']}}</td>
    </tr>
% end
</tbody>
</table>
% end

<script>
function upload_table_template_selector(){

    var xhttp = new XMLHttpRequest();
    xhttp.open('POST', '/upload_template');
    var social = document.getElementById('social_select').value;
    var  body ='social='+ social + '&title=*all*'
    console.log(body);
    xhttp.send(body);

    xhttp.onreadystatechange = function(){
        if (xhttp.readyState != 4) return;
        try {
              console.log("Posts GOT");
              var response = JSON.parse(xhttp.responseText);
              console.log(response);

              var table_select_template = document.getElementsByName('table_select_template')

              for(i=0;i<table_select_template.length;i++){

                  //console.log(table_select_template[i].value);
                  //console.log(table_select_template[i].id);

                   var id = table_select_template[i].id
                   var info = table_select_template[i].id.split("_")
                   var info_social = info[0];
                   var info_value = info[2];
                  // console.log(info_social);


                 td = document.getElementById(id + '_img_verification');

                 parent = td.parentElement
                 inputs = parent.getElementsByTagName('input')
                 for(j=0;j<inputs.length;j++){
                    if (inputs[j].type == "button"){
                         button = inputs[j]
                    }
                 }

                 if(td.innerText == "True"){
                    td.innerHTML = "<img width='30' src='images/positive_face.png'>"
                    button.disabled=false
                 }
                 else if(td.innerText.indexOf('обрабатывается')+1){
                    button.disabled=true
                 }
                 else{
                    button.disabled=false
                 }

                   if (info_social=='vk'){

                        var option = document.createElement('option');
                        option.value = info_value
                        option.innerText = info_value

                        table_select_template[i].appendChild(option);

                        for( var key in response['info']){
                            if (response['info'][key]['title'] != info_value){


                                var option = document.createElement('option');
                                option.value = response['info'][key]['title']
                                option.innerText = response['info'][key]['title']

                                table_select_template[i].appendChild(option);
                            }
                        }
                   }
              }

        }
        catch(e) {
               console.log("Problem in JSON.parse")
               console.log(e);
        }
    }
}
document.addEventListener("DOMContentLoaded", upload_table_template_selector);
</script>

<script>
function changeBotTemplateStatus(select){

    template = document.getElementById(select.id +'_status')
    //console.log(template.value);

    //console.log('template = ' + template.value);
    //console.log("select.value = " + select.value);

    var info = select.id.split("_");
    var social = info[0];
    var login = info[1];

    td = document.getElementById(select.id+'_img_verification')

    if(td.innerText != "" ){
        if(td.innerText == "Нет шаблона"){

            while (td.firstChild) {
                td.removeChild(td.firstChild);
            }
             td.innerHTML = "<img width='30' src='images/negative_face.png'>"
        }
        return
    }

    while (td.firstChild) {
            td.removeChild(td.firstChild);
    }

    if(select.value != template.value){
        td.innerHTML = "<img width='30' src='images/negative_face.png'>"
        //console.log('Не равно');

    }
    else{
        td.innerHTML = "<img width='30' src='images/positive_face.png'>"
    }

}
</script>


<script>
function changeBotTemplate(button){

    var info = button.id.split("_");
    var social = info[0];
    var login = info[1];
    var password = info[2];
    var tm = info[3];

    var template = document.getElementById(social + "_" + login + "_" + tm + '_status').value;
    var template_profil =  document.getElementById(social + "_" + login + "_" + tm).value;


    //console.log('template_profil = ' + template_profil);
    //console.log("template = " + template);

    if(social=="ok"){
        return
    }

    if(template == template_profil){
        console.log("Уже применен");
        return
    }


    var xhttp = new XMLHttpRequest();
    var body = "login=" + encodeURIComponent(login) + "&template_profil=" + encodeURIComponent(template_profil) + "&social=" + encodeURIComponent(social)+ "&password=" + encodeURIComponent(password);
    xhttp.open("POST", "/change_bot_template", true);
    xhttp.send(body);
    console.log("Загрузка шаблона ...");

    td = document.getElementById(social + "_" + login + "_" + tm +'_img_verification')
    td.innerText = 'Запрос отправлен'
    button.disabled=true

    //xhttp.onreadystatechange = function(){
    //    if (xhttp.readyState != 4) return;
    //    try {
    //          console.log("Posts GOT");
    //          var response = JSON.parse(xhttp.responseText);
    //          console.log(response);
    //
    //          if(response == true){
    //            template_status = document.getElementById(social + "_" + login + "_" + tm+'_status')
    //            template_status.value = template_profil
    //
    //            td = document.getElementById(social + "_" + login + "_" + tm +'_img_verification')
    //
    //            while (td.firstChild) {
    //                td.removeChild(td.firstChild);
    //            }
    //
    //            //td.innerHTML = "<img width='30' src='images/positive_face.png'>"
    //            td.innerText = 'Готово'
    //            button.disabled=false
    //
    //          }
    //          else{
    //              td = document.getElementById(social + "_" + login + "_" + tm +'_img_verification')
    //              while (td.firstChild) {
    //                  td.removeChild(td.firstChild);
    //              }
    //
    //              //td.innerHTML = "<img width='30' src='images/negative_face.png'>"
    //              td.innerText = response
    //              button.disabled=false
    //          }
    //    }
    //    catch(e) {
    //           console.log("Problem in JSON.parse")
    //           console.log(e);
    //           td.innerText = 'Ошибка'
    //           button.disabled=false
    //    }
    //}
}
</script>

<script>
function changeBotComment(input) {
    var xhttp = new XMLHttpRequest();
    var info = input.id.split("_");
    var social = info[0];
    var login = info[1];
    var comment = input.value;
    var body = "login=" + encodeURIComponent(login) + "&comment=" + encodeURIComponent(comment) + "&social=" + encodeURIComponent(social);
    xhttp.open("POST", "/change-bot-comment", true);
    xhttp.send(body);
}
</script>

% include('notification.tpl')