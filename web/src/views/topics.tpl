% include('main.tpl')
<style>
a.content {
    color: #000000;
    text-decoration: none;
}
a.content:visited {
    color: #000000; 
    text-decoration: none;
}
.mySlides {
    display: none
}
.slideshow-container {
  width: 750px;
  padding-left: 100px;
  position: relative;
  margin: auto;
}
.active, .dot:hover {
  background-color: #717171;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}
/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}
#rel_posts{
  background: #E0FFFF;
  padding: 20;
}
.inline {
   display:inline-block;
   margin-right:5px;
}
.modalDialog {
	position: fixed;
	font-family: Arial, Helvetica, sans-serif;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: rgba(0,0,0,0.8);
	z-index: 99999;
	-webkit-transition: opacity 400ms ease-in;
	-moz-transition: opacity 400ms ease-in;
	transition: opacity 400ms ease-in;
	display: none;
	pointer-events: none;
}
.modalDialog:target {
	display: block;
	pointer-events: auto;
}
.loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.hidden {
  display: none;
}

.modalDialog > div {
	width: 400px;
	position: relative;
	margin: 10% auto;
	padding: 5px 20px 13px 20px;
	border-radius: 10px;
	background: #fff;
}
.close {
	background: #606061;
	color: #FFFFFF;
	line-height: 25px;
	position: absolute;
	right: -12px;
	text-align: center;
	top: -10px;
	width: 24px;
	text-decoration: none;
	font-weight: bold;
	-webkit-border-radius: 12px;
	-moz-border-radius: 12px;
	border-radius: 12px;
	-moz-box-shadow: 1px 1px 3px #000;
	-webkit-box-shadow: 1px 1px 3px #000;
	box-shadow: 1px 1px 3px #000;
}
.close_button {
    border-radius: 10px;
    width: 120px; 
    height: 40px; 
    display: inline-block;
    outline: none;
    font-size: 11pt;
    text-align: center;
    margin-left:50px;
    margin-bottom:10px;
}
.theme_button {
    border-radius: 10px;
    width: 150px; 
    height: 30px; 
    display: inline-block;
    outline: none;
    text-align: center;
    margin-left: 30px;
    margin-bottom:10px;
}
.ignore_word {
    text-align: center;
    font-weight: bold;
}
.close:hover { background: #00d9ff; }
</style>

<!-- <a target="_blank" href="/visual">Визуализация</a>
<br>
<ul>
    % for topic in visual_data['groups']:
        % topic_href = f"/topic?level={topic['level']}&topic={topic['topic']}"
        <li>{{topic['label']}}</li>
        % if len(topic['groups']) > 0:
            <ul>
                % for child in topic['groups']:
                    % child_href = f"/topic?level={child['level']}&topic={child['topic']}"
                    % is_tracked = child.get('is_tracked')
                    % if is_tracked is not None:
                    <li><a target="_blank" href={{child_href}}>{{child['label']}} (Отслеживается)</a></li>
                    % else:
                    <li><a target="_blank" href={{child_href}}>{{child['label']}}</a></li>
                    % end
                    
                % end
            </ul>
        % end
    % end
</ul> -->
<br>
<div class="container">
    <div class="box">
        <div style="display:inline-block; padding-top: 18px; vertical-align:top; padding-left: 20px">
            <form name="AddTopics" method="post">
                <div>
                    <textarea cols=50 rows=3 name="topic_words" placeholder="Слова"></textarea>
                </div>
                <div class="box">
                        <div style="display:inline-block; vertical-align:top;">
                            <p><input value="Добавить тему" type="button" class="btn theme_button" id="add_topic" onClick="checkWords(this);"/></p>
                        </div>
                        <div style="display:none; vertical-align:top;" id="add_thres">
                            <p><input value="Установить порог" type="button" class="btn theme_button" onClick="AddTopic();"/></p>
                        </div>
                        <div style="display:inline-block;">
                            <p><input value="Опробовать тему" type="button" class="btn theme_button" id="try_topic" onClick="checkWords(this);"/></p>
                        </div>
                        <div style="display: none;" id="thres_selector">
                            <input type="range" step="0.05" name="thres" min="0.1" max="1" value="0.6" list="rangeList" onchange="document.getElementById('rangeValue').innerHTML = this.value;">
                              <datalist id="rangeList">
                              <option value="0" label="0">
                              <option value="5" label="5">
                              <option value="10" label="10">
                              </datalist>
                              <span id="rangeValue">0.6</span>
                        </div>
                </div>
            </form>
        </div>
        <div style="display:inline-block; vertical-align:top; padding-left: 100px" >
            <table class="sort topicTable">
                <thead>
                <tr>
                <td>Тема</td>
                <td>Порог</td>
                <td>Управление</td>
                </tr>
                </thead>    
                <div class="topicTable">
                    <tbody id="topic_body" style="height: 150px">
                    </tbody>
                </div>
            </table>
        </div>
        <div style="display:inline-block;" ><br />
            <div class="loader" id="loader"></div> 
            <div class="slideshow-container" id="rel_topic_posts"></div>
        </div>

    </div>
</div>
<a href="#openModal" id="kek"></a>

<div id="openModal" class="modalDialog">
	<div>
		<a href="#close" title="Закрыть" id="close_window" class="close"></a>
		<h2 align="center">Плохие новости :(</h2>
		<p align="center">Похоже, что наша модель ещё не знает эти слова:</p>
        <div id="ignore_words"></div>
		<p align="center">Желаете продолжить без них?</p>
        <div id="confirm_buttons"></div>
	</div>
</div>

<script>
function testTopic() {
    console.log("Task: get rel topic posts");
    document.getElementById("close_window").click()
    document.getElementById("loader").className = "loader";
    document.getElementById("rel_topic_posts").innerHTML = "" 
    var xhttp = new XMLHttpRequest();
    var words = document.AddTopics.topic_words.value
    xhttp.open("POST", "/test-topic", true);
    xhttp.send("words=" + words.toString());
    console.log("Testing topics");
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = []
        }
        console.log(response)
        var rel_posts = document.getElementById("rel_topic_posts")
        document.getElementById("loader").className = "hidden";


        for (var i = 0, length = response.length; i < length; i++)
        {
            var Div1 = document.createElement('div');
            Div1.className = "mySlides";

            var newDiv = document.createElement('div');
            newDiv.className = "float:left;padding:20;background-color:rgb(243, 248, 252);border:1px solid black;";
            newDiv.innerHTML = '<div style="float:left">' + '<img width="50" src="images/' + response[i]['social'] + '.png">';

            var Div = document.createElement('div');
            Div.style = "float:left"
            Div.innerHTML = '<div style="margin-left:15;color:rgb(0,100,186);font-weight:bold;">' +
                  '<a target="_blank" href="' + response[i]['group_href'] + '" class="content">' + response[i]['name'] + '</a></div>' +
                  '<div style="margin:10 15;color:darkslategrey;">' + response[i]['date'] + '</div>'
            newDiv.appendChild(Div)

            Div = document.createElement('div');
            Div.style = "clear:left"
            if (response[i]['text'].length < 500) {
                Div.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'] + "</a>"
                + "<p> <strong>Score:</strong> " + response[i]['score'] + "</p>";

            } else {
                Div.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'].slice(0, 500) + "</a>" 
                               + '<a class="wall_post_more" onclick="hide(this); show(this);"> Показать полностью…</a>'
                               + '<span style="display: none">' + response[i]['text'].slice(500) + '</span>' + "<p><strong>Score:</strong> " + response[i]['score'] + "</p>";
            }
            newDiv.appendChild(Div);

            var Div = document.createElement('div');
            Div.style = "float:left;padding-top:20"
            Div.innerHTML = '<div style="float:left;">' + '<img width="30" src="images/like_outline_24.svg">' + '</div>' + 
            '<div style="float:inherit;margin:8 5;">' + response[i]['likes']
            newDiv.appendChild(Div)

            var Div = document.createElement('div');
            Div.style = "float:left;padding-top:20"
            Div.innerHTML = '<div style="float:left;">' + '<img width="30" src="images/comment_outline_24.svg">' + '</div>' + 
            '<div style="float:inherit;margin:8 5;">' + response[i]['replies']
            newDiv.appendChild(Div)

            if (response[i]['social'] != "inst")
            {
              var Div = document.createElement('div');
              Div.style = "float:left;padding-top:20"
              Div.innerHTML = '<div style="float:left;">' + '<img width="30" src="images/share_outline_24.svg">' + '</div>' + 
              '<div style="float:inherit;margin:8 5;">' + response[i]['shares']
              newDiv.appendChild(Div)

              var Div = document.createElement('div');
              Div.style = "float:right;padding-top:30"
              Div.innerHTML = '<img width="30" src="images/eye_monitor.png">' + response[i]['views']
              newDiv.appendChild(Div)
            }
            
            
            Div1.appendChild(newDiv)
            rel_posts.appendChild(Div1)
        }

        var Div3 = document.createElement('div');
        Div3.style = "text-align:center;clear:both;padding-top:10;"
        for (var i = 0, length = response.length; i < length; i++)
        {
          var divv = document.createElement('a')
          divv.innerHTML = '<span class = "dot" onclick="currentSlide(' + i + ')">'
          Div3.appendChild(divv)
        }
        rel_posts.appendChild(Div3)
        showSlides(0);
    }
}
function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var slideIndex;
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[n].style.display = "block";  
  dots[n].className += " active";
}
</script>

<script>
function changeAndAddTopic(ign_words) {
    var words = ign_words.id.split('__');
    changeTopic(words[0])
    if (words[1] == 'add_topic') {
        addThres()
    } else {
        testTopic()
    }
}
function changeTopic(ign_words) {
    document.getElementById("close_window").click()
    start_topic = document.AddTopics.topic_words.value.split(' ')
    final_topic = ign_words
    try {
        result = start_topic.filter(el => !final_topic.includes(el));
    } catch(e) {
        console.log(e)
        result = start_topic
    }
    result = result.join(' ')
    document.AddTopics.topic_words.value = result
}
</script>
<script>
function hide(link) {
    link.style = "display: none;";
}
function show(link) {
    var span = link.nextElementSibling;
    var all_text = link.previousElementSibling;
    all_text.innerHTML = all_text.innerHTML + span.innerHTML
}
</script>

<script>
function checkWords(form_button) {
  var buttons = document.getElementById("confirm_buttons")
  buttons.innerHTML = ""
  var words = document.getElementById("ignore_words")
  words.innerHTML = ""
  var xhttp = new XMLHttpRequest();
  var body = "words=" + document.AddTopics.topic_words.value;
  xhttp.open("POST", "/check-topic", true);
  xhttp.send(body);
  xhttp.onreadystatechange = function() {
      if (xhttp.readyState != 4) return;
      try {
        console.log("Posts GOT");
        var response = JSON.parse(xhttp.responseText);
      } catch(e) {
        console.log("Problem in JSON.parse")
        console.log(xhttp.responseText);
        var response = []
      }
      console.log(response)
      if (response.length == 0) {
          if (form_button.id == 'try_topic') {
              testTopic()
          } else {
              addThres()
          }
      } else {
          var words = document.getElementById("ignore_words")
            for (var i = 0, length = response.length; i < length; i++){
                var textField = document.createElement('p');
                textField.innerHTML = response[i]
                textField.className = "ignore_word"
                words.appendChild(textField);
            }
            var buttons = document.getElementById("confirm_buttons")
         /*   <input value="Да" type="button" class="close_button" id="b3"/>
        <input value="Нет" type="button" class="close_button" id="b4"/>*/
            var button_id = response.join(' ');
            var buttons_div = document.createElement('div');
            buttons_div.innerHTML = '<input value="Да" type="button" onclick="changeAndAddTopic(this)" class="close_button" id="' + button_id + '__' + form_button.id +'"/> <input value="Нет" onclick="changeTopic(this)" type="button" class="close_button" id="' + 
            button_id + '"/>';
            buttons.appendChild(buttons_div)

        document.getElementById("kek").click();
      } 
  }
}
</script>
<script>
function getTopics() {
  var body = document.getElementById("topic_body")
    body.innerHTML = ""
    var xhttp = new XMLHttpRequest();
    var body = "Get rules"
    xhttp.open("POST", "/get-topics", true);
    xhttp.send(body);
    console.log("Getting topics");
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Topics GOT");
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = []
        }
        console.log(response)
        var body = document.getElementById("topic_body")
        var tr_class = "default";
        var g = 1
        for (var i = 0, length = response.length; i < length; i++)
        {
            var newTr = document.createElement('tr');
            newTr.id = "tr_" + response[i]['words'];
            newTr.className = tr_class;

            var td = document.createElement('td');
            td.setAttribute("width", "120")
            td.innerHTML = response[i]['words']
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = response[i]['threshold'].toFixed(2)
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("align", "center")
            td.innerHTML = '<img width="30" src="/images/bin.png" onClick="dropTopic(this)" id="' + response[i]['words'] + '" style="cursor:hand;" align="middle" title="Удалить тему">'
            newTr.appendChild(td);
            body.appendChild(newTr);
        }        
        console.log("Topics loaded")
        g = g + 1
    }
}
</script>

<script>
function dropTopic(source) {
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("tr_" + source.id);
    var body = "words=" + source.id;
    xhttp.open("POST", "/drop-topic", true);
    xhttp.send(body);
    removeElement(tr)
}

function removeElement(element) {
    element.parentNode.removeChild(element);
}
</script>

<script type="text/javascript">
window.onload = onPageLoad();

function onPageLoad() {
  document.getElementById("close_window").click()
  document.getElementById("loader").className = "hidden";
  getTopics()
}

</script>

<script>
function addThres() {
  document.getElementById('add_topic').style = "display: none";
  document.getElementById('add_thres').style = "display: inline-block; vertical-align: top;";
  document.getElementById('try_topic').style = "display: none";
  document.getElementById("thres_selector").style = "display: inline-block; padding-top: 18px; padding-left: 30px;"
  document.getElementById('add_topic').value = "Установить порог"
  document.getElementById('add_topic').onсlick = AddTopic;
}
</script>


<script>
function AddTopic() {
  var xhttp = new XMLHttpRequest();
  var body = "words=" + document.AddTopics.topic_words.value + "&threshold=" + document.AddTopics.thres.value;
  xhttp.open("POST", "/create-topic", true);
  xhttp.send(body);
  document.getElementById('add_topic').disabled = true;
  document.getElementById('add_topic').value = "Добавление темы..."

  xhttp.onreadystatechange = function() {
      if (xhttp.readyState != 4) return;

      try {
        console.log("Posts GOT");
        var response = JSON.parse(xhttp.responseText);
      } catch(e) {
        console.log("Problem in JSON.parse")
        console.log(xhttp.responseText);
        var response = []
      }
      document.AddTopics.reset();
      document.getElementById('add_topic').style = "display:inline-block; vertical-align:top;"
      document.getElementById('add_thres').style = "display: none;";
      document.getElementById('add_topic').disabled = false;
      document.getElementById('add_topic').value = "Добавить тему"
      document.getElementById('thres_selector').style = "display: none;";
      document.getElementById('try_topic').style = "display: inline-block;";
      document.getElementById("rel_topic_posts").innerHTML = ""
      getTopics()
  }
}
</script>
</ul>

% include('notification.tpl')
