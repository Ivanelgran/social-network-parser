% include('main.tpl')
% from datetime import datetime, timedelta
% start_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
% end_date = datetime.now().strftime("%Y-%m-%d")

<script type="text/javascript" src="static/js/jspdf.debug.js"></script>

<style>
a.content {
    color: #000000;
    text-decoration: none;
}
a.content:visited {
    color: #000000; 
    text-decoration: none;
}
.mySlides {
    display: none
}
.slideshow-container {
  width: 750px;
  padding-left: 100px;
  position: relative;
  margin: auto;
}
.active, .dot:hover {
  background-color: #717171;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}
/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}
#rel_posts{
  background: #E0FFFF;
  padding: 20;
}
.inline {
   display:inline-block;
   margin-right:5px;
}
.modalDialog {
	position: fixed;
	font-family: Arial, Helvetica, sans-serif;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: rgba(0,0,0,0.8);
	z-index: 99999;
	-webkit-transition: opacity 400ms ease-in;
	-moz-transition: opacity 400ms ease-in;
	transition: opacity 400ms ease-in;
	display: none;
	pointer-events: none;
}
.modalDialog:target {
	display: block;
	pointer-events: auto;
}
.loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.hidden {
  display: none;
}

.modalDialog > div {
	width: 500px;
	position: relative;
	margin: 10% auto;
	padding: 5px 20px 13px 20px;
	border-radius: 10px;
	background: #fff;
}
.close {
	background: #606061;
	color: #FFFFFF;
	line-height: 25px;
	position: absolute;
	right: -12px;
	text-align: center;
	top: -10px;
	width: 24px;
	text-decoration: none;
	font-weight: bold;
	-webkit-border-radius: 12px;
	-moz-border-radius: 12px;
	border-radius: 12px;
	-moz-box-shadow: 1px 1px 3px #000;
	-webkit-box-shadow: 1px 1px 3px #000;
	box-shadow: 1px 1px 3px #000;
}
.close_button {
    border-radius: 10px;
    width: 120px; 
    height: 40px; 
    display: inline-block;
    outline: none;
    font-size: 11pt;
    text-align: center;
    margin-left:50px;
    margin-bottom:10px;
}
.theme_button {
    border-radius: 10px;
    width: 150px; 
    height: 30px; 
    display: inline-block;
    outline: none;
    text-align: center;
    margin-left: 10px;
    margin-bottom:10px;
}
.ignore_word {
    text-align: center;
    font-weight: bold;
}
.close:hover { background: #00d9ff; }
/* Базовый контейнер табов */
.tabs {
	min-width: 320px;
	padding: 0px;
	margin: 0 auto;
}
/* Стили секций с содержанием */
.tabs>section {
	display: none;
	padding: 15px;
	background: #fff;
	border: 1px solid #ddd;
}
.tabs>section>p {
	margin: 0 0 5px;
	line-height: 1.5;
	color: #383838;
	/* прикрутим анимацию */
	-webkit-animation-duration: 1s;
	animation-duration: 1s;
	-webkit-animation-fill-mode: both;
	animation-fill-mode: both;
	-webkit-animation-name: fadeIn;
	animation-name: fadeIn;
}
/* Описываем анимацию свойства opacity */
 
@-webkit-keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
@keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
/* Прячем чекбоксы */
.tabs>input {
	display: none;
	position: absolute;
}
/* Стили переключателей вкладок (табов) */
.tabs>label {
	display: inline-block;
	margin: 0 0 -1px;
	padding: 15px 25px;
	font-weight: 600;
	text-align: center;
	color: #aaa;
	border: 0px solid #ddd;
	border-width: 1px 1px 1px 1px;
	background: #f1f1f1;
	border-radius: 3px 3px 0 0;
}

/* Изменения стиля переключателей вкладок при наведении */
 
.tabs>label:hover {
	color: #888;
	cursor: pointer;
}
/* Стили для активной вкладки */
.tabs>input:checked+label {
	color: #555;
	border-top: 1px solid #009933;
	border-bottom: 1px solid #fff;
	background: #fff;
}

/* Активация секций с помощью псевдокласса :checked */
#tab1:checked~#content-tab1, #tab2:checked~#content-tab2, #tab3:checked~#content-tab3, #tab4:checked~#content-tab4 {
	display: block;
}
/* Убираем текст с переключателей 
* и оставляем иконки на малых экранах
*/
 
@media screen and (max-width: 680px) {
	.tabs>label {
		font-size: 0;
	}
	.tabs>label:before {
		margin: 0;
		font-size: 18px;
	}
}
/* Изменяем внутренние отступы 
*  переключателей для малых экранов
*/
@media screen and (max-width: 400px) {
	.tabs>label {
		padding: 15px;
	}
}

.plot {
    width: 49%;
    display: inline-block;
}

input[type="date"] {
    position: relative;
}

/* create a new arrow, because we are going to mess up the native one
see "List of symbols" below if you want another, you could also try to add a font-awesome icon.. */
input[type="date"]:after {
    content: "\25BC"; 
    color: #555;
    padding: 0 5px;
}

/* change color of symbol on hover */
input[type="date"]:hover:after {
    color: #bf1400;
}

/* make the native arrow invisible and stretch it over the whole field so you can click anywhere in the input field to trigger the native datepicker*/
input[type="date"]::-webkit-calendar-picker-indicator {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: auto;
    height: auto;
    color: transparent;
    background: transparent;
}

/* adjust increase/decrease button */
input[type="date"]::-webkit-inner-spin-button {
    z-index: 1;
}

 /* adjust clear button */
 input[type="date"]::-webkit-clear-button {
     z-index: 1;
 }
 .loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.hidden {
  display: none;
}   
.container { 
    border:2px solid #ccc; 
    width:300px; 
    overflow-y: scroll; 
    font-size:18;
    max-height:100px;
}
</style>
<div class="tabs">
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1" title="Отслеживаемые посты">Отслеживаемые посты</label>
 
    <input id="tab2" type="radio" name="tabs">
    <label for="tab2" title="Сохраненные посты">Сохраненные посты</label>
 
    <section id="content-tab1">
    <a href="#openModal" id="kek"></a>

    <div id="openModal" class="modalDialog">
        <div style="text-align: center;">
            <a href="#close" title="Закрыть" id="close_window" class="close">✖</a>
            <h3 align="center">Добавьте ссылку на пост</h2>
            <br />
            <textarea cols=40 rows=3 id="post_link" placeholder="Ссылка на пост"></textarea>
            <br />
            <br />
            Город: 
            <select id="new-post-city" onChange="newField(this)" new-field="new_city">
              % for city in cities:
                <option value="{{city}}">{{city}}</option>
              % end
              <option value="other">Другой</option>
            </select>
            <input id="new_city" type="hidden" value="">
            <br />
            <br />
            Тема: 
            <select id="new-post-topic" onChange="newField(this)" new-field="new_topic">
              % for topic in topics:
                <option value="{{topic}}">{{topic}}</option>
              % end
              <option value="other">Другая</option>
            </select>
            <input id="new_topic" type="hidden" value="">
            <div id="alert_message"></div>
            <p>До какой даты отслеживать:
            <input type="date" value = "{{end_date}}" id="tracked_end_date"></p>
            <p><input value="Добавить пост" class="btn theme_button" type="button" id="b1" onClick="createPost();"/></p>
        </div>
    </div>
    <div style="text-align: center;">
    <img width="50" src="images/plus.png" onClick="addPost()" style="cursor:hand; text-align: center;" title="Добавить пост">
    <br />

    </div>
    <table class="sort" align="center">
    <thead>
    <tr>
    <td>№</td>
    <td>Группа</td>
    <td>Дата</td>
    <td>Текст</td>
    <td><img width="30" src="images/eye_monitor.png"></td>
    <td><img width="30" src="images/like_outline_24.svg"></td>
    <td><img width="30" src="images/comment_outline_24.svg"></td>
    <td><img width="30" src="images/share_outline_24.svg"></td>
    <td>Отслеживать</td>
    </tr>
    </thead>
    <tbody id="main_body">
    </tbody>
    </table>
    <br />
    
    </section>  
    <section id="content-tab2">
    <p>Дата:<br> 
    <input type="date" value="{{start_date}}" id="start_date"> - 
    <input type="date" value="{{end_date}}" id="end_date">
    </p>
    <div>
    <p>Города:</p>
    <input id="-1" type="checkbox" onClick="toggle(this)"> <label id="citiesToggle" for="-1">Выбрать все</label><br/>
    <div class="container" id="cities">
        % for i, city in enumerate(cities):
          <input id="{{i}}" type="checkbox" name="city" value="{{city}}"><label for="{{i}}">{{city}}</label><br>
        % end
    </div>
    </div>
    <div>
    <p>Темы:</p>
    <input id="99" type="checkbox" onClick="toggle(this)"> <label id="topicsToggle" for="99">Выбрать все</label><br/>
    <div class="container" id="topics">
        % for i, topic in enumerate(topics):
          <input id="{{100+i}}" type="checkbox" name="topic" value="{{topic}}"><label for="{{100+i}}">{{topic}}</label><br>
        % end
    </div>
    </div>
    <br>
    <button onClick="loadTrackedPosts()" id="trackedPostsButton">Вывести</button>
    <button onclick="javascript:downloadReport()" id="downloadReport" disabled>Скачать</button>

    <div class="hidden" id="loader"></div>
    <table class="sort hidden" align="center" id="main_table">
    <thead>
    <tr>
    <td>№</td>
    <td>Дата</td>
    <td>Город</td>
    <td>Группа</td>
    <td>Тема</td>
    <td>Текст</td>
    <td><img width="30" src="images/eye_monitor.png"></td>
    <td><img width="30" src="images/like_outline_24.svg"></td>
    <td><img width="30" src="images/comment_outline_24.svg"></td>
    <td><img width="30" src="images/share_outline_24.svg"></td>
    </tr>
    </thead>
    <tbody  id="tracked_body">
    </tbody>
    </table>

    <table class="hidden" id="result_table">
    <thead>
    <tr>
    <td>Всего:</td>
    <td>Постов</td>
    <td><img width="30" src="images/eye_monitor.png"></td>
    <td><img width="30" src="images/like_outline_24.svg"></td>
    <td><img width="30" src="images/comment_outline_24.svg"></td>
    <td><img width="30" src="images/share_outline_24.svg"></td>
    </tr>
    </thead>
    <tbody  id="result_tracked_body">
    </tbody>
    </table>

    </section>  

  
<script>
function newField(source) {
  console.log(source.selectedIndex);
  console.log(source.length);
  var newField = document.getElementById(source.getAttribute('new-field'));
  if (source.selectedIndex + 1 === source.length) {
    newField.type = "text";
  } else {
    newField.type = "hidden";
  }
}
</script>

<script language="JavaScript">
function toggle(source) {
  checkboxes = source.parentElement.getElementsByTagName('input');
  if (source.checked){
      var newText = "Снять выделение";
  } else {
      var newText = "Выбрать все";
  }
  source.parentElement.getElementsByTagName("label")[0].textContent = newText;
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>
<script>
function loadTrackedPosts() {
    document.getElementById("loader").className = "loader";
    document.getElementById("main_table").className = "hidden";
    document.getElementById("result_table").className = "hidden";
    document.getElementById("trackedPostsButton").disabled = true;

    var body = document.getElementById("tracked_body")
    body.innerHTML = ""
    var result_body = document.getElementById("result_tracked_body")
    result_body.innerHTML = ""

    checkboxes = document.getElementById("cities").getElementsByTagName('input');
    var cities = [];
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if (checkboxes[i].checked) {
        cities.push(checkboxes[i].value);
      }
    }

    checkboxes = document.getElementById("topics").getElementsByTagName('input');
    var topics = [];
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if (checkboxes[i].checked) {
        topics.push(checkboxes[i].value);
      }
    }

    var xhttp = new XMLHttpRequest();
    var user = "{{!username}}";
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;
    xhttp.open("POST", "/get-saved-posts", true);
    xhttp.send("user=" + encodeURIComponent(user)
               + "&start_date=" + start_date.toString()
               + "&end_date=" + end_date.toString()
               + "&cities=" + JSON.stringify(cities)
               + "&topics=" + JSON.stringify(topics));
    console.log("Getting saved posts");

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText.replace(/\bNaN\b/g, "null"));
          console.log(response);

        } catch(e) {
          console.log(e)
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText)
          var response = []
        }

        var posts_count = response.length;
        var views_counter = 0;
        var likes_counter = 0;
        var replies_counter = 0;
        var shares_counter = 0;

        if (posts_count > 0) {
          document.getElementById('downloadReport').disabled = false;
        } else {
          document.getElementById('downloadReport').disabled = true;
        }

        for (var i = 0, length = response.length; i < length; i++)
        {
            views_counter += response[i]['views'];
            likes_counter += response[i]['likes'];
            replies_counter += response[i]['replies'];
            shares_counter += response[i]['shares'];

            var newTr = document.createElement('tr');
            newTr.id = "trr*" + response[i]['social'] + "*" + response[i]['owner_id'] + "*" + response[i]['post_id'];

            var td = document.createElement('td');
            td.innerHTML = i + 1;
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = '<a target="_blank" href="' + response[i]['post_href'] + '">' + response[i]['date'].replace(' ', '<br>') + '</a>';
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            var city = response[i]['post_city'];
            td.innerHTML = (typeof city !== "undefined") ? city : '';
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = '<a target="_blank" href="' + response[i]['group_href'] + '">' + response[i]['name'] + '</a>';
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            var topic = response[i]['post_topic'];
            td.innerHTML = (typeof topic !== "undefined") ? topic : '';
            newTr.appendChild(td);

            var td = document.createElement('td');
            if (response[i]['text'] === "") {
                td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content"><Отсутствует текст></a>';
            } else {
                td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'] + '</a>';
            }
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['views'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['likes'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['replies'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['shares'];
            newTr.appendChild(td);

            body.appendChild(newTr);
        }

        var newTr = document.createElement('tr');

        var td = document.createElement('td');
        newTr.appendChild(td);

        var td = document.createElement('td');
        td.setAttribute("nowrap", "nowrap")
        td.innerHTML = posts_count;
        newTr.appendChild(td);

        var td = document.createElement('td');
        td.setAttribute("nowrap", "nowrap")
        td.innerHTML = views_counter;
        newTr.appendChild(td);

        var td = document.createElement('td');
        td.setAttribute("nowrap", "nowrap")
        td.innerHTML = likes_counter;
        newTr.appendChild(td);

        var td = document.createElement('td');
        td.setAttribute("nowrap", "nowrap")
        td.innerHTML = replies_counter;
        newTr.appendChild(td);

        var td = document.createElement('td');
        td.setAttribute("nowrap", "nowrap")
        td.innerHTML = shares_counter;
        newTr.appendChild(td);

        result_body.appendChild(newTr);


        document.getElementById("result_table").className = "sort";
        document.getElementById("main_table").className = "sort";
        document.getElementById("loader").className = "hidden";
        document.getElementById("trackedPostsButton").disabled = false;
    }
}
</script>
<script>
function getPosts() {
    var body = document.getElementById("main_body")
    body.innerHTML = ""
    var xhttp = new XMLHttpRequest();
    var users = "{{!username}}"
    var body = "username=" + users
    xhttp.open("POST", "/get-tracked-posts", true);
    xhttp.send(body);
    console.log("Getting posts");
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText.replace(/\bNaN\b/g, "null"));

        } catch(e) {
          console.log(e)
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText)
          var response = []
        }
        var body = document.getElementById("main_body")

        var posts_count = response.length;
        var views_counter = 0;
        var likes_counter = 0;
        var replies_counter = 0;
        var shares_counter = 0;
        
        for (var i = 0, length = response.length; i < length; i++)
        {

            views_counter += response[i]['views'];
            likes_counter += response[i]['likes'];
            replies_counter += response[i]['replies'];
            shares_counter += response[i]['shares'];

            var newTr = document.createElement('tr');
            newTr.id = "trr*" + response[i]['social'] + "*" + response[i]['owner_id'] + "*" + response[i]['post_id'];

            var td = document.createElement('td');
            td.innerHTML = i + 1;
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = '<a target="_blank" href="' + response[i]['group_href'] + '">' + response[i]['name'] + '</a>';
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = '<a target="_blank" href="' + response[i]['post_href'] + '">' + response[i]['date'] + '</a>';
            newTr.appendChild(td);

            var td = document.createElement('td');
            if (response[i]['text'] === "") {
                td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content"><Отсутствует текст></a>';
            } else {
                td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'] + '</a>';
            }
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['views'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['likes'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['replies'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("nowrap", "nowrap")
            td.innerHTML = response[i]['shares'];
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("align", "center")
            td.innerHTML = '<img width="30" src="images/minus.png" onClick="changeMonitoringPostState(this)" id="' + response[i]['social'] + '*' + response[i]['owner_id'] + '*' + response[i]['post_id'] + '">';
            newTr.appendChild(td);
            

            body.appendChild(newTr);
        }
        console.log("Posts loaded")
    }
}
</script>
<script>
function changePostState(source) {
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("tr*"+source.id);
    var body = "is_tracked=False" + "&id=" + source.id;
    xhttp.open("POST", "/change-post-track-state", true);
    xhttp.send(body);
    removeElement(tr)
}

function changeMonitoringPostState(source) {
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("trr*"+source.id);
    var body = "is_tracked=False" + "&id=" + source.id;
    xhttp.open("POST", "/change-post-monitoring-state", true);
    xhttp.send(body);
    removeElement(tr)
}

function removeElement(element) {
    element.parentNode.removeChild(element);
}
function addPost() {
    document.getElementById("kek").click();
}
function createPost() {
    link = document.getElementById("post_link").value
    end_date = document.getElementById("tracked_end_date").value
    if (/wall-\d+_\d+/i.test(link)) {
        console.log('Ссылка введена верно');
        link = link.split('wall')[1];
        link = link.split('_');
        group_id = link[0];
        post_id = link[1];
        social = 'vk';
        city = document.getElementById("new-post-city").value;
        new_city = false;
        if (city === "other") {
          city = document.getElementById("new_city").value;
          new_city = true;
        }
        topic = document.getElementById("new-post-topic").value;
        new_topic = false;
        if (topic === "other") {
          topic = document.getElementById("new_topic").value;
          new_topic = true
        }
        var xhttp = new XMLHttpRequest();
        var body = "group_id=" + group_id + "&post_id=" + post_id + "&social=" 
          + social + "&end_date=" + encodeURIComponent(end_date)
          + "&city=" + encodeURIComponent(city) 
          + "&topic=" + encodeURIComponent(topic) + "&new_city=" + new_city
          + "&new_topic=" + new_topic;
        console.log(body);
        xhttp.open("POST", "/check-post", true);
        xhttp.send(body);
         xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText);

        } catch(e) {
          console.log(e)
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText)
          var response = []
        }
        if (response == 'ok') {
            document.getElementById("close_window").click()
            document.getElementById("post_link").value = ""
            getPosts()
        } else if (response == 'bad_link'){ 
            console.log('Плохая ссылка')
            var alert_mess = document.getElementById("alert_message")
            alert_mess.style.color = "#FF3333"
            alert_mess.innerHTML = "Плохая ссылка!"
            setTimeout(clear, 5000);

        } else {
            var alert_mess = document.getElementById("alert_message")
            alert_mess.style.color = "#FF3333"
            alert_mess.innerHTML = "Не могу перейти на пост!"
            setTimeout(clear, 5000);
        }
        
         }
    } else {
        var alert_mess = document.getElementById("alert_message")
        alert_mess.style.color = "#FF3333"
        alert_mess.innerHTML = "Ссылка введена неверно!"
        setTimeout(clear, 5000);
    }
}
function clear() {
    var alert_mess = document.getElementById("alert_message")
    alert_mess.innerHTML = ""
}
</script>

<script type="text/javascript">
window.onload = onPageLoad();

function onPageLoad() {
  getPosts()
  document.getElementById("close_window").click()
}

</script>

<script type="text/javascript">
function saveData(blob, fileName) // does the same as FileSaver.js
{
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";

    var url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
}

function downloadReport() {
    checkboxes = document.getElementById("cities").getElementsByTagName('input');
    var cities = [];
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if (checkboxes[i].checked) {
        cities.push(checkboxes[i].value);
      }
    }

    checkboxes = document.getElementById("topics").getElementsByTagName('input');
    var topics = [];
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if (checkboxes[i].checked) {
        topics.push(checkboxes[i].value);
      }
    }

    var xhttp = new XMLHttpRequest();
    var user = "{{!username}}";
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;
    xhttp.open("POST", "/download-saved-posts", true);
    xhttp.responseType = "blob";
    xhttp.onload = function () {
        saveData(this.response, 'отчет.xlsx'); // saveAs is now your function
    };

    xhttp.send("user=" + encodeURIComponent(user)
               + "&start_date=" + start_date.toString()
               + "&end_date=" + end_date.toString()
               + "&cities=" + JSON.stringify(cities)
               + "&topics=" + JSON.stringify(topics));
}
</script>

% include('notification.tpl')
