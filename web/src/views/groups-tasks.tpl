% include('main.tpl')

% tasks = tasks if defined('tasks') else None
% unique_groups_count = unique_groups_count if defined('unique_groups_count') else 0

<style>
    tr.hidden {
        background-color: rgba(242, 241, 239, 0.5);
        opacity: 0.5;
    }
</style>

<form id="add-task" action="/add-task" method="get"></form>
<form id="add-custom-task" action="/add-custom-task" method="get"></form>
<form id="add-keyword-task" action="/add-keyword-task" method="get"></form>
<p>
<button form="add-task">Добавить группы по критериям</button>
<button form="add-custom-task">Добавить свои группы</button>
<!--<button form="add-keyword-task">Добавить группы по ключевым словам</button>-->
</p>

% if tasks is not None:
<table class="sort" align="center">
<thead>
<tr>
    <td>№</td>
    <td>Название задачи</td>
    <td>Дата создания</td>
    <td>Количество групп</td>
    <td>Автор</td>
    <td>Управление</td>
</tr>
</thead>
<tbody>
% for i, task in enumerate(tasks):
    % tr_class = "default" if task['is_active'] else "hidden"
    % state_title = "Перестать парсить" if task['is_active'] else "Начать парсить"

    <tr id="tr_{{task['task_id']}}" class="{{tr_class}}">
        % href = f"/task?id={task['task_id']}&is_keyword={task['is_keyword']}"
        <td><a target="_blank" href={{href}}>{{i+1}}</a></td>
        <td>{{task['label']}}</td>
        <td>{{task['date']}}</td>
        <td>{{task['groups_count']}}</td>
        <td>{{task['author']}}</td>
        <td align="center">
            <a href="{{href}}"><img width="25" src="/images/eye.png" style="cursor:hand;" align="middle" title="Подробнее"></a>
            <img width="25" src="/images/magnifier.png" onClick="changeTaskState(this)" id="{{task['task_id']}}" style="cursor:hand;" align="middle" title="{{state_title}}">
            <img width="30" src="/images/bin.png" onClick="dropTask(this)" id="{{task['task_id']}}" style="cursor:hand;" align="middle" title="Удалить из парсинга">
        </td>
    </tr>
% end
</tbody>
</table>
% end

<br>

<p>Число уникальных групп в парсинге: <b>{{unique_groups_count}}</b></p>

<script>
function dropTask(source) {
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("tr_"+source.id);
    var body = "task_id=" + source.id;
    xhttp.open("POST", "/drop-task", true);
    xhttp.send(body);
    removeElement(tr)
}

function changeTaskState(source) {
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("tr_"+source.id);
    var state = tr.className !== "hidden";
    if (state) {
        tr.className = "hidden";
        source.title = "Начать парсить";
    } else {
        tr.className = "default";
        source.title = "Перестать парсить";
    }
    var body = "state=" + (tr.className !== "hidden").toString() + "&task_id=" + source.id;
    xhttp.open("POST", "/change-task-state", true);
    xhttp.send(body);
    
}

function removeElement(element) {
    element.parentNode.removeChild(element);
}
</script>

% include('notification.tpl')