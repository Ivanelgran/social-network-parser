% include('main.tpl')

<table class="sort" align="center">
<thead>
<tr>
<td>№</td>
<td>Группа</td>
<td>Количество участников</td>
<td>Доля подписчиков из наших городов</td>
<td>Среднее количество постов в день</td>
<td>Среднее количество просмотров постов</td>
<td>Среднее количество комментариев на пост</td>
</tr>
</thead>
<tbody>

% for counter, group in enumerate(groups):
    <tr>
        <td>{{counter+1}}</td>
        <td><a target="_blank" href="{{group['href']}}">{{group['name']}}</a></td>
        <td>{{group['members_count']}}</td>
        <td>{{round(group.get('our_prop', 0), 2)}}</td>
        <td>{{round(group.get('post_per_day', 0), 2)}}</td>
        <td>{{round(group.get('views_per_post', 0), 2)}}</td>
        <td>{{round(group.get('comments', 0), 2)}}</td>
    </tr>
% end

</tbody>
</table>

% include('notification.tpl')