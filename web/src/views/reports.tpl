% include('main.tpl')
% from datetime import datetime
% cur_year, cur_month = datetime.now().year, datetime.now().month
% start_date = datetime.now().strftime("%Y-%m-%d")

<style>
.hidden {
  display: none;
}
<style>
.visible {
  display: block;
}
.loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Базовый контейнер табов */
.tabs {
	min-width: 320px;
	padding: 0px;
	margin: 0 auto;
}
/* Стили секций с содержанием */
.tabs>section {
	display: none;
	padding: 15px;
	background: #fff;
	border: 1px solid #ddd;
}
.tabs>section>p {
	margin: 0 0 5px;
	line-height: 1.5;
	color: #383838;
	/* прикрутим анимацию */
	-webkit-animation-duration: 1s;
	animation-duration: 1s;
	-webkit-animation-fill-mode: both;
	animation-fill-mode: both;
	-webkit-animation-name: fadeIn;
	animation-name: fadeIn;
}
/* Описываем анимацию свойства opacity */
 
@-webkit-keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
@keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
/* Прячем чекбоксы */
.tabs>input {
	display: none;
	position: absolute;
}
/* Стили переключателей вкладок (табов) */
.tabs>label {
	display: inline-block;
	margin: 0 0 -1px;
	padding: 15px 25px;
	font-weight: 600;
	text-align: center;
	color: #aaa;
	border: 0px solid #ddd;
	border-width: 1px 1px 1px 1px;
	background: #f1f1f1;
	border-radius: 3px 3px 0 0;
}

/* Изменения стиля переключателей вкладок при наведении */
 
.tabs>label:hover {
	color: #888;
	cursor: pointer;
}
/* Стили для активной вкладки */
.tabs>input:checked+label {
	color: #555;
	border-top: 1px solid #009933;
	border-bottom: 1px solid #fff;
	background: #fff;
}

/* Активация секций с помощью псевдокласса :checked */
#tab1:checked~#content-tab1, #tab2:checked~#content-tab2, #tab3:checked~#content-tab3, #tab4:checked~#content-tab4 {
	display: block;
}
/* Убираем текст с переключателей 
* и оставляем иконки на малых экранах
*/
 
@media screen and (max-width: 680px) {
	.tabs>label {
		font-size: 0;
	}
	.tabs>label:before {
		margin: 0;
		font-size: 18px;
	}
}
/* Изменяем внутренние отступы 
*  переключателей для малых экранов
*/
@media screen and (max-width: 400px) {
	.tabs>label {
		padding: 15px;
	}
}

.plot {
    width: 49%;
    display: inline-block;
}
</style>

<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<div class="tabs">
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1" title="Следы и вбросы">Следы и вбросы</label>
 
    <input id="tab2" type="radio" name="tabs">
    <label for="tab2" title="SMM SL">SMM SL</label>
 
    <section id="content-tab1">
        <p>Пользователь: 
            <select id="users">
                <option value="all">
                Все
                </option>
                % for user in users:
                    <option value="{{user}}">
                    {{user}}
                    </option>
                % end
            </select>
        </p>

        Месяц <input type="number" id="month" value="{{cur_month}}">
        Год <input type="number" id="year" value="{{cur_year}}">

        <button onClick="getReport()">Построить отчет</button>

        <div class="hidden" id="loader"></div>

        <table class="sort" align="center" id="main_table">
        </table>

        <div id="sugg_count" class="plot"></div>
        <div id="traces_count" class="plot"></div>
        <div id="views_count" class="plot"></div>
        <div id="inv_count" class="plot"></div>
    </section>  
    <section id="content-tab2">
        <p>
          Тип:
          <select id="smm_type" onChange="setSmmReportFields()">
            <option value="day">День</option>
            <option value="month">Месяц</option>
          </select>
        </p>
        <p>
          Сотрудник:
          <select id="smm_operator">
            <option value="all">Все</option>
            <option value="486">Козлов А.С.</option>
            <option value="407">Зайков А.С.</option>
            <option value="642">Тараненко И.В.</option>
          </select>
        </p>

        <div id="smm_day_fields">
            Дата: <input type="date" id="smm_date" value="{{start_date}}">
        </div>

        <div id="smm_month_fields" class='hidden'>
            Месяц <input type="number" id="smm_month" value="{{cur_month}}">
            Год <input type="number" id="smm_year" value="{{cur_year}}">
        </div>
        <br>
        <button onClick="getSmmReport()">Построить отчет</button>

        <div class="hidden" id="smm_loader"></div>

        <br>

        <div id="smm_day_result" class='hidden'>
            <h2 id="smm_day_label" style="text-align: center;"></h2>
            <table style="font-size: 18;">
                <tr>
                    <td>Общее количество ответов</td>
                    <td id="smm_day_count"></td>
                </tr>
                <tr>
                    <td>Количество ответов вовремя</td>
                    <td id="smm_day_in_time"></td>
                </tr>
                <tr>
                    <td>Среднее время ответа</td>
                    <td id="smm_day_mean_time"></td>
                </tr>
                <tr>
                    <td>Медианное время ответа</td>
                    <td id="smm_day_median_time"></td>
                </tr>
                <tr>
                    <td>SL</td>
                    <td id="smm_day_sl"></td>
                </tr>
            </table>

            <div id="smm_day_plot" style="width: 90%;margin: auto;"></div>
        </div>

        <div id="smm_month_result" class='hidden'>
            <h2 id="smm_month_label" style="text-align: center;"></h2>
            <table style="font-size: 18;">
                <tr>
                    <td>Общее количество ответов</td>
                    <td id="smm_month_count"></td>
                </tr>
                <tr>
                    <td>Количество ответов вовремя</td>
                    <td id="smm_month_in_time"></td>
                </tr>
                <tr>
                    <td>Среднее время ответа</td>
                    <td id="smm_month_mean_time"></td>
                </tr>
                <tr>
                    <td>Медианное время ответа</td>
                    <td id="smm_month_median_time"></td>
                </tr>
                <tr>
                    <td>SL</td>
                    <td id="smm_month_sl"></td>
                </tr>
            </table>

            <div id="smm_month_plot_count" class="plot"></div>
            <div id="smm_month_plot_time" class="plot"></div>
        </div>

        <div id="smm_replies" class="hidden">
        <h3>Ответы:</h3>
        <table style="font-size: 18; border-collapse: collapse; padding: 15px;" border="1">
            <thead>
                <tr>
                    <td style="padding: 10px;">№</td>
                    <td style="padding: 10px;">Дата</td>
                    <td style="padding: 10px;">Текст</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
    </section> 
</div>

<script>
function setSmmReportFields() {
    var type = document.getElementById("smm_type").value;

    if (type === "day") {
        document.getElementById("smm_day_fields").className = "visible";
        document.getElementById("smm_month_fields").className = "hidden";
    } else if (type === "month") {
        document.getElementById("smm_day_fields").className = "hidden";
        document.getElementById("smm_month_fields").className = "visible";
    }
}
</script>

<script>
function getSmmReport() {
    var type = document.getElementById("smm_type").value;

    document.getElementById("smm_day_result").className = "hidden";
    document.getElementById("smm_month_result").className = "hidden";
    document.getElementById("smm_replies").className = "hidden";

    document.getElementById("smm_loader").className = "loader";

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/get-smm-report", true);
    var month = document.getElementById("smm_month").value;
    var year = document.getElementById("smm_year").value;
    var date = document.getElementById("smm_date").value;
    var operator = document.getElementById("smm_operator").value;
    var params = "report_type=" + type + "&date=" + encodeURIComponent(date) 
                 + "&month=" + month + "&year=" + year+ "&operator=" + operator;
    console.log(params)
    xhttp.send(params);

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("report GOT");
          var response = JSON.parse(xhttp.responseText);
          console.log(response)
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText)
          var response = [];
        }

        if (type === "day") {
            var mean_time = getSecondsString(response['mean_time']);
            var median_time = getSecondsString(response['median_time']);

            document.getElementById("smm_day_label").innerText = "Отчет за " + response['info']['date'] + " с 10:00 до 22:00";

            document.getElementById("smm_day_count").innerText = response['count'];
            document.getElementById("smm_day_in_time").innerText = response['in_time'];
            document.getElementById("smm_day_mean_time").innerText = mean_time;
            document.getElementById("smm_day_median_time").innerText = median_time;
            document.getElementById("smm_day_sl").innerText = Math.round(response['sl']*100) + ' %';

            document.getElementById("smm_day_result").className = "visible";

            var smm_activity = {
                x: response['info']['dates'],
                y: response['info']['times'],
                name: 'Ответы',
                mode: 'markers',
                marker: {
                    color: 'rgb(219, 64, 82)',
                    size: 12
                }
            };

            var layout = {
                title:'Ответы за день',
                yaxis: {
                    title: {
                        text: 'Время ответа (мин)'
                    }
                }
            }

            Plotly.newPlot('smm_day_plot', [smm_activity], layout);
        } else if (type === "month") {
            var mean_time = getSecondsString(response['all']['mean_time']);
            var median_time = getSecondsString(response['all']['median_time']);

            document.getElementById("smm_month_label").innerText = "Отчет за " + response['info']['date'] + " с 10:00 до 22:00";

            document.getElementById("smm_month_count").innerText = response['all']['count'];
            document.getElementById("smm_month_in_time").innerText = response['all']['in_time'];
            document.getElementById("smm_month_mean_time").innerText = mean_time;
            document.getElementById("smm_month_median_time").innerText = median_time;
            document.getElementById("smm_month_sl").innerText = Math.round(response['all']['sl']*100) + ' %';

            document.getElementById("smm_month_result").className = "visible";

            var smm_counts = {
                x: response['info']['dates'],
                y: response['info']['counts'],
                type: 'scatter',
                name: 'Количество ответов'
            };

            var smm_in_time = {
                x: response['info']['dates'],
                y: response['info']['in_time'],
                name: 'Количество ответов вовремя',
                line: {
                    dash: 'dot',
                    width: 4
                }
            };

            var smm_mean_times = {
                x: response['info']['dates'],
                y: response['info']['mean_times'],
                type: 'scatter',
                name: 'Среднее время ответа'
            };

            var smm_median_times = {
                x: response['info']['dates'],
                y: response['info']['median_times'],
                type: 'scatter',
                name: 'Медианное время ответа'
            };

            Plotly.newPlot('smm_month_plot_count', [smm_counts, smm_in_time], {title:'Количество ответов'});
            Plotly.newPlot('smm_month_plot_time', [smm_mean_times, smm_median_times], {title:'Время ответа (минут)'});

        }

        var replies = response['replies'];
        if (replies.length > 0) {

            var body = document.getElementById("smm_replies").getElementsByTagName("tbody")[0];
            body.innerHTML = ""

            for (var i = 0, length = replies.length; i < length; i++)
            {
                var newTr = document.createElement('tr');

                var td = document.createElement('td');
                td.innerHTML = i + 1;
                td.style.padding = "10px";
                newTr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = '<a target="_blank" href="' + replies[i]['link'] + '">' + replies[i]['date'] + '</a>';
                td.style.padding = "10px";
                newTr.appendChild(td);

                var td = document.createElement('td');
                if (replies[i]['text'] === "") {
                    td.innerHTML = '<Отсутствует текст>';
                } else {
                    td.innerHTML = replies[i]['text'].slice(0, 300);
                }
                td.style.padding = "10px";
                newTr.appendChild(td);
                body.appendChild(newTr);
            }

            document.getElementById("smm_replies").className = "";
        }

        document.getElementById("smm_loader").className = "hidden";
    }
}

function getSecondsString(seconds) {
    var time = new Date(Math.round(seconds * 1000));
    var result = "";

    var hour = time.getUTCHours();
    if (hour === 1) {
        result += "1 час ";
    } else if ([2, 3, 4].includes(hour)) {
        result += hour + " часа ";
    } else if (hour > 0) {
        result += hour + " часов ";
    }

    var minutes = time.getUTCMinutes();
    if (minutes === 1) {
        result += "1 минута ";
    } else if ([2, 3, 4].includes(minutes)) {
        result += minutes + " минуты ";
    } else if (minutes > 0) {
        result += minutes + " минут ";
    }

    var seconds = time.getUTCSeconds();
    if (seconds === 1) {
        result += "1 секунда ";
    } else if ([2, 3, 4].includes(seconds)) {
        result += seconds + " секунды ";
    } else if (seconds > 0) {
        result += seconds + " секунд";
    }

    return result
}
</script>

<script>
function getReport() {

    document.getElementById("loader").className = "loader";

    var body = document.getElementById("main_table")
    body.innerHTML = ""
    document.getElementById("sugg_count").innerHTML = ""
    document.getElementById("traces_count").innerHTML = ""
    document.getElementById("views_count").innerHTML = ""
    document.getElementById("inv_count").innerHTML = ""

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/get-report", true);
    var month = document.getElementById("month").value;
    var year = document.getElementById("year").value;
    var params = "report_type=month&user=" + encodeURIComponent(document.getElementById("users").value) 
                 + "&month=" + month + "&year=" + year;
    console.log(params)
    xhttp.send(params);

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("report GOT");
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          var response = []
        }

        document.getElementById("loader").className = "hidden";

        var newTr = document.createElement('tr');
        var td = document.createElement('td');
        newTr.appendChild(td);
        for (var i = 0, length = response['week_labels'].length; i < length; i++)
        {
            var td = document.createElement('td');
            td.innerHTML = response['week_labels'][i];
            newTr.appendChild(td);
        }
        var td = document.createElement('td');
        td.innerHTML = "Итого за месяц";
        newTr.appendChild(td);
        body.appendChild(newTr);

        var newTr = document.createElement('tr');
        var td = document.createElement('td');
        td.innerHTML = "Вбросы";
        newTr.appendChild(td);
        for (var i = 0, length = response['suggestions_count'].length; i < length; i++)
        {
            var td = document.createElement('td');
            td.innerHTML = response['suggestions_count'][i];
            newTr.appendChild(td);
        }
        var td = document.createElement('td');
        td.innerHTML = response['suggestions_sum'];
        newTr.appendChild(td);
        body.appendChild(newTr);

        var newTr = document.createElement('tr');
        var td = document.createElement('td');
        td.innerHTML = "Комментарии";
        newTr.appendChild(td);
        for (var i = 0, length = response['traces_count'].length; i < length; i++)
        {
            var td = document.createElement('td');
            td.innerHTML = response['traces_count'][i];
            newTr.appendChild(td);
        }
        var td = document.createElement('td');
        td.innerHTML = response['traces_sum'];
        newTr.appendChild(td);
        body.appendChild(newTr);

        var newTr = document.createElement('tr');
        var td = document.createElement('td');
        td.innerHTML = "Охваты";
        newTr.appendChild(td);
        for (var i = 0, length = response['views'].length; i < length; i++)
        {
            var td = document.createElement('td');
            td.innerHTML = response['views'][i];
            newTr.appendChild(td);
        }
        var td = document.createElement('td');
        td.innerHTML = response['views_sum'];
        newTr.appendChild(td);
        body.appendChild(newTr);

        var newTr = document.createElement('tr');
        var td = document.createElement('td');
        td.innerHTML = "Вовлеченность";
        newTr.appendChild(td);
        for (var i = 0, length = response['involvement'].length; i < length; i++)
        {
            var td = document.createElement('td');
            td.innerHTML = response['involvement'][i];
            newTr.appendChild(td);
        }
        var td = document.createElement('td');
        td.innerHTML = response['involvement_sum'];
        newTr.appendChild(td);
        body.appendChild(newTr);

        console.log(response)

        var traces_count = {
            x: response['traces_by_date_range'],
            y: response['traces_by_date'],
            type: 'scatter',
            name: 'Комментарии'
        };

        var sugg_count = {
            x: response['traces_by_date_range'],
            y: response['sugg_by_date'],
            type: 'scatter',
            name: 'Вбросы'
        };

        var views_count = {
            x: response['traces_by_date_range'],
            y: response['views_by_date'],
            type: 'scatter',
            name: 'Охваты'
        };

        var inv_count = {
            x: response['traces_by_date_range'],
            y: response['inv_by_date'],
            type: 'scatter',
            name: 'Вовлеченность'
        };

        Plotly.newPlot('sugg_count', [sugg_count], {title:'Вбросы'});
        Plotly.newPlot('traces_count', [traces_count], {title:'Комментарии'});
        Plotly.newPlot('views_count', [views_count], {title:'Охваты'});
        Plotly.newPlot('inv_count', [inv_count], {title:'Вовлеченность'});

    }
}
</script>

% include('notification.tpl')