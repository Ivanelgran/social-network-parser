% include('main.tpl')

<table class="sort" align="center">
<tbody>
% for info in notifications:
    <tr>
        <td>{{info['date']}}</td>
        <td>{{info['info']['title']}}</td>
        <td><a href="{{info['info']['link']}}">{{info['info']['body']}}</a></td>
    </tr>
% end
</tbody>
</table>

% include('notification.tpl')