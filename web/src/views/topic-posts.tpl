% include('main.tpl')

<p>Тема {{topic}}</p>
<b>{{top_words}}</b>

% trace = trace if defined('trace') else None

% if trace is not None:
<p>Шаблон следа для данной тематики:</p>
<form action="/topic?level={{level}}&topic={{topic}}&change-trace=1" method="post">
<textarea cols=50 rows=3 name="trace">{{trace}}</textarea>
<p><input value="Сохранить новый след" type="submit" /></p>
</form>
% end

% state = state if defined('state') else None
% if state is not None:
<p>Состояние темы:
<form action="/topic?level={{level}}&topic={{topic}}" method="post">
<input name="change-state" type="hidden" value="True">

<input id="0" name="current-state" type="radio" value="0" {{'checked' if state == 0 else ''}}><label for="0">Не отслеживается</label><br>
<input id="1" name="current-state" type="radio" value="1" {{'checked' if state == 1 else ''}}><label for="1">Отслеживается</label><br>
<input id="2" name="current-state" type="radio" value="-1" {{'checked' if state == -1 else ''}}><label for="2">Скрыта</label><br>
<input value="Изменить" type="submit" />
</form>
</p>
% end

% posts = posts if defined('posts') else None

% if posts is not None:
<table class="sort" align="center">
<thead>
<tr>
    <td>Пост</td>
    <td>Вероятность</td>
    <td>Текст</td>
</tr>
</thead>
<tbody>
% for post in posts:
    <tr>
        <%
            own, p, soc = post['owner_id'], post['post_id'], post['social']
            href = f"/post?social={soc}&owner-id={own}&post-id={p}"
            label = f"{own}_{p}"
            prob = post['probability']
            text = post['text']
        %>
        <td><a target="_blank" href={{href}}>{{label}}</a></td>
        <td>{{prob}}</td>
        <td>{{text}}</td>
    </tr>
% end
</tbody>
</table>
% end

% include('notification.tpl')