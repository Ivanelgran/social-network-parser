<script>
window.onload = onPageLoad();

function onPageLoad() {
    var username = encodeURI("{{username}}");
    var es = new EventSource("/notification?username="+username);
    es.onmessage = function (e) {
        info = JSON.parse(e.data)
        if (info !== "ping") {
          notifyMe(info);
        }
    };
}
</script>
<script>
function notifyMe(info) {
  console.log("new notification")
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  else if (Notification.permission === "granted") {
    console.log("1 " + info['title'])
    var notification = new Notification(info['title'], {body: info['body'], data:info['link']});
    notification.onclick = function(event) {
      event.preventDefault();
      window.open(event.target.data, '_blank');
    }
  }

  else {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        console.log("2 " + info['title'])
        var notification = new Notification(info['title'], {body: info['body'], data:info['link']});
        notification.onclick = function(event) {
          event.preventDefault();
          window.open(event.target.data, '_blank');
        }
      }
    });
  }
}
</script>