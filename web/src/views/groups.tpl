% include('main.tpl')

<style>
    tr.hidden {
        background-color: rgba(242, 241, 239, 0.5);
        opacity: 0.5;
    }

    img.active {
        cursor:hand;
        width: 30px;
        content: url("images/eye.png");
    }
    img.active:hover {
        content: url("images/redline_eye.png");
    }

    img.inactive {
        cursor:hand;
        width: 30px;
        content: url("images/redline_eye.png");
    }
    img.inactive:hover {
        content: url("images/eye.png");
    }
</style>

<table class="sort" align="center">
<thead>
<tr>
<td>№</td>
<td>Группа</td>
<td>Количество участников</td>
<td>Город</td>
<td>Доля подписчиков из главенствующего города</td>
<td>Доля подписчиков из наших городов</td>
<td>Среднее количество постов в день</td>
<td>Среднее количество просмотров постов</td>
<td>Среднее количество комментариев на пост</td>
<td>Управление</td>
</tr>
</thead>
<tbody>

% for counter, group in enumerate(groups):
    % tr_class = "hidden" if group.get('is_hidden') else "default"
    % img_class = "inactive" if group.get('is_hidden') else "active"
    % state_title = "Показывать посты из этой группы" if group.get('is_hidden') else "Не показывать посты из этой группы"
    <tr id="tr_{{group['social']}}_{{group['group_id']}}" class="{{tr_class}}">
        <td>{{counter+1}}</td>
        % if group['social'] == "vk":
        % href = f"https://vk.com/club{group['group_id']}"
        % elif group['social'] == "ok":
        % href = f"https://ok.ru/group/{group['group_id']}"
        % elif group['social'] == "inst":
        % href = f"https://instagram.com/{group['group_id']}"
        % end
        <input name="add-to-task" type="hidden" value={{group['group_id']}} form="save-task">
        <td><a target="_blank" href="{{href}}">{{group.get('name') or "<Имя группы>"}}</a></td>
        <td>{{group.get('members_count') or ""}}</td>
        <td>{{group.get('main_city') or ""}}</td>
        <td>{{round(group.get('main_prop') or 0, 2)}}</td>
        <td>{{round(group.get('our_prop') or 0, 2)}}</td>
        <td>{{round(group.get('post_per_day') or 0, 2)}}</td>
        <td>{{round(group.get('views_per_post') or 0, 2)}}</td>
        <td>{{round(group.get('comments') or 0, 2)}}</td>
        <td align="center"><img src="images/eye.png" onClick="changeUserGroupState(this)" id="{{group['social']}}_{{group['group_id']}}" class="{{img_class}}" title="{{state_title}}"></td>
    </tr>
% end

</tbody>
</table>

<script>
function changeUserGroupState(source) {
    var xhttp = new XMLHttpRequest();
    var group_id = source.id.split("_")
    var tr = document.getElementById("tr_"+group_id[0]+"_"+group_id[1]);
    var state = tr.className !== "hidden";
    if (state) {
        tr.className = "hidden";
        source.title = "Показывать посты из этой группы";
        source.className = "inactive";
    } else {
        tr.className = "default";
        source.title = "Не показывать посты из этой группы";
        source.className = "active";
    }
    var body = "state=" + (tr.className !== "hidden").toString() + "&group_id=" + group_id[1] + "&social=" + group_id[0];
    xhttp.open("POST", "/change-user-group-state", true);
    xhttp.send(body);
    
}
</script>

% include('notification.tpl')