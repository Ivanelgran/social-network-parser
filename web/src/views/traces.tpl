% from datetime import datetime, timedelta
% start_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
% end_date = datetime.now().strftime("%Y-%m-%d")
% include('main.tpl')

<style>

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.hidden {
    display: none;
}

td.completed {
  color: green;
}

td.failed {
  color: red;
}

</style>

<form action="/reload-trace" method="post" id="reload_trace">
<input type="hidden" name="trace_id" id="reload_trace_id">
</form>

<table>
<tr>
<td align="center">Выводить все следы 
</td>
<td>
<label class="switch">
  <input type="checkbox" onChange="changeView()" id="filter_switch">
  <span class="slider round"></span>
</label>
</td>
</tr>
</table>

<div id="user_filter" class="hidden">
<p>Пользователь: 
<select id="users" onChange="changeView()">
<option value="all">
Все
</option>
% for user in users:
<option value="{{user}}">
{{user}}
</option>
% end
</select>
</p>
</div>
<p>Дата:<br> 
<input type="date" value="{{start_date}}" id="start_date" onChange="changeView()"> - 
<input type="date" value="{{end_date}}" id="end_date" onChange="changeView()">
</p>

<table class="sort" align="center">
<thead>
<tr>
<td>№</td>
<td>Дата</td>
<td>След</td>
<td>Автор</td>
<td>Состояние</td>
<td>Количество лайков</td>
<td>Количество ответов</td>
</tr>
</thead>
<tbody id="main_body">
</tbody>
</table>

<!--
<script>
function reloadTrace(img) {
  var trace_id = document.getElementById("reload_trace_id");
  trace_id.value = img.id;

  document.getElementById("reload_trace").submit();
}

</script>
-->

<script>
function changeView() {
    var checkbox = document.getElementById("filter_switch")
    var users = document.getElementById("users");
    var curUser = users.options[users.selectedIndex].value;
    var user = "{{username}}"
    var body = document.getElementById("main_body");
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;

    body.innerHTML = "";
    
    if (checkbox.checked) {
        var getAll = curUser === "all";
        document.getElementById("user_filter").className = ""
    } else {
        var getAll = false
        curUser = user
        document.getElementById("user_filter").className = "hidden"
    }

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/get-traces", true);
    xhttp.send("user=" + encodeURIComponent(curUser)
               + "&start_date=" + start_date.toString()
               + "&end_date=" + end_date.toString());

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("traces GOT");
          var traces = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText)
          var traces = []
        }

        for (var i = 0, length = traces.length; i < length; i++) {
            
            var trace_date = new Date(traces[i]['date']);
                var newTr = document.createElement('tr');

                var td = document.createElement('td');
                td.innerHTML = i + 1;
                newTr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = '<a href="' + traces[i]['post'] + '">' + traces[i]['date'] + '</a>';
                newTr.appendChild(td);

                var td = document.createElement('td');
                if (traces[i]['status_class'] === "completed" && traces[i]['social'] === 'vk') {
                  td.innerHTML = '<a href="' + traces[i]['post'] + "?reply=" + traces[i]['reply_id'] + '">' + traces[i]['text'] + '</a>';
                } else {
                  td.innerHTML = traces[i]['text'];
                }
                newTr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = traces[i]['author'];
                newTr.appendChild(td);

                var td = document.createElement('td');
                td.className = traces[i]['status_class']
                td.innerHTML = traces[i]['status'];
                newTr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = traces[i]['likes'];
                newTr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = traces[i]['replies'];
                newTr.appendChild(td);

                /*if (traces[i]['status_class'] === "failed") {
                  var td = document.createElement('td');
                  var img = document.createElement('img');
                  img.setAttribute("width", "25");
                  img.setAttribute("src", "/images/reload.png");
                  img.setAttribute("id", traces[i]['social'] + "_" + traces[i]['owner_id'] + "_" 
                    + traces[i]['post_id'] + "_" + traces[i]['date']);
                  img.setAttribute("style", "cursor:hand;");
                  img.setAttribute("align", "middle");
                  img.setAttribute("title", "Оставить заново");
                  img.onclick = function() { reloadTrace(this) };
                  td.appendChild(img);
                  newTr.appendChild(td);
                }*/

                body.appendChild(newTr);
            
        }
    }
}
</script>

<script>
window.onload = changeView();
</script>

% include('notification.tpl')