% include('main.tpl')

<style>
    div.suggest {
        float: left;
        margin: 20
    }
    div.add_groups {
        float: right;
        margin: 20
    }
    div.container { 
       border:2px solid #ccc; 
       width:600px; 
       height: 250px; 
       overflow-y: scroll; 
       font-size:18;
    }
    div.group_panel {
        width: 97%;
        border-bottom: 1px solid;
        font-size: 18;
        padding: 5;
        display: flex;
    }
    div.select_task:hover {
        background: RGB(231, 108, 97);
    }
    div.group_title {
        width: 80%;
        float: left;
        margin: auto 5;
    }
    div.group_in_list {
        width: 80%;
    }
    div.list {
        width: 40%;
    }
    div.group_add {
        width: 20%;
    }
    div.group_remove {
        float: right;
        width: 20%;
    }
    img.group_add {
        margin: 5;
        float: right;
    }
    .hidden {
        display: none;
    }
</style>

<div class="suggest">
<form id="main" action="/add-suggest-post" method="post" enctype="multipart/form-data" accept-charset='utf-8'>
<p>Название вброса: 
<textarea cols=30 rows=1 name="title"></textarea></p>
<p>Текст: 
<textarea cols=30 rows=5 name="text"></textarea></p>
<p>Вложения: <input type="file" name="upload" /></p>
<p>Социальная сеть:
<select name="social" onChange="changeBots()">
    <option value="vk">Вконтакте</option>
    <!-- <option value="ok">Одноклассники</option> -->
    <!-- <option value="inst">Инстаграм</option> -->
</select>
<p>Бот: 
<select name="bot">
</select>
</p>
</form>
</div>

<div class="add_groups">
<div class="container" id="poss_groups">
</div>
</div>

<br>
<div style="clear: both;margin:20;">
<button style="clear: both;" onClick="loadTasks()">Добавить группы из парсинга</button>
</div>

<div class="hidden container" id="tasks">
</div>

<br>
<h2>Группы</h2>
<div style="margin-left:20;" id="groups">
</div>
<p><input value="Добавить вброс" form="main" type="submit" id="submit_suggestion" disabled/></p>

<script>
function changeGroup(elem){
    if (elem.parentNode.className === "group_add") {
        addGroup(elem);
    } else if (elem.parentNode.className === "group_remove"){
        removeGroup(elem);
    }
}

function removeGroup(elem) {
    var element = elem.parentNode.parentNode;
    element.parentNode.removeChild(element);
    checkGroupsCount();
}

function addGroup(elem) {
    var element = elem.parentNode.parentNode.cloneNode(true);
    element.className = "list group_panel";
    element.childNodes[0].className = "group_in_list group_title";
    element.childNodes[1].className = "group_remove";
    element.childNodes[1].firstChild.src = "images/minus.png"

    var input = document.createElement('input');
    input.setAttribute("type", "hidden");
    input.setAttribute("form", "main");
    input.setAttribute("name", "group");
    input.setAttribute("value", element.id);
    element.appendChild(input);

    document.getElementById("groups").appendChild(element);

    removeGroup(elem);
    checkGroupsCount();
}

function checkGroupsCount() {
    var button = document.getElementById("submit_suggestion");
    var groups = document.getElementById("groups").childElementCount;

    if (groups > 0) {
        button.disabled = false;
    } else {
        button.disabled = true;
    }
}
</script>

<script>
function loadTasks(){
    var container = document.getElementById("tasks");
    container.innerHTML = ""

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/get-tasks", true);
    xhttp.send("");
    console.log("Getting tasks");
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = [];
        }

        var body = document.getElementById("tasks");
        for (var i = 0, length = response.length; i < length; i++)
        {
            var main_div = document.createElement('div');
            main_div.className = "select_task group_panel";
            var id = response[i]["task_id"];
            main_div.id = id;
            main_div.setAttribute( "onclick", "javascript: loadTaskGroups(this);" );

            var title = document.createElement('div');
            title.className = "group_title";
            title.innerHTML = response[i]["label"];
            title.setAttribute("title", "Количество групп: " + response[i]["groups_count"]);
            main_div.appendChild(title);

            body.appendChild(main_div);
        }

        if (response.length > 0) {
            body.className = "container";
        }
    }
}

function loadTaskGroups(elem){

    var id = elem.id;

    var container = document.getElementById("tasks");
    container.className = "hidden container";
    var body = document.getElementById("poss_groups");
    body.innerHTML = ""

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/get-task-groups", true);
    xhttp.send("task_id=" + id);
    console.log("Getting groups");
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Groups GOT");
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = []
        }

        for (var i = 0, length = response.length; i < length; i++)
        {
            var main_div = document.createElement('div');
            main_div.className = "group_panel";
            var id = response[i]["social"] + "_" + response[i]["group_id"];
            main_div.id = id;

            var title = document.createElement('div');
            title.className = "group_title";
            title.setAttribute("title", "Количество участников: " + response[i]["members_count"]);
            var a = document.createElement('a');
            a.setAttribute("src", response[i]["href"])
            a.innerHTML = response[i]["name"];
            title.appendChild(a);
            main_div.appendChild(title);

            var add = document.createElement('div');
            add.className = "group_add";
            var img = document.createElement('img');
            img.className = "group_add";
            img.setAttribute("src", "images/plus.png");
            img.setAttribute("width", "30");
            img.setAttribute("onclick", "javascript: changeGroup(this);");
            add.appendChild(img);
            main_div.appendChild(add);

            body.appendChild(main_div);
        }
    }
}
</script>

<script>
function changeBots() {
    var None = "";
    var bots = {{!bots}};
    var form = document.forms[0];
    var select = form.elements.social;
    var curSocial = select.value

    var botsForm = form.elements.bot
    botsForm.innerHTML = ""
    for (var i = 0; i < bots.length; i++) {
        if (bots[i]['social'] === curSocial) {
            var option = document.createElement('option');
            option.value = bots[i]['login'] + "_" + bots[i]['password']
            option.innerHTML = bots[i]['name'] + " -- " + bots[i]['city'] + " -- " + bots[i]['comment']
            botsForm.appendChild(option);
        }
    }
}
</script>

<script>
window.onload = changeBots();
</script>

% include('notification.tpl')