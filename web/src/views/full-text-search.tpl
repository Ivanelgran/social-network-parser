% include('main.tpl')

% from datetime import datetime, timedelta
% start_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
% start_time = (datetime.now() - timedelta(days=1)).strftime("%H:%M")
% end_date = (datetime.now()).strftime("%Y-%m-%d")
% end_time = (datetime.now()).strftime("%H:%M")
% default_date = True


<% 
tags = tags if defined('tags') and tags is not None else ""
%>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<style>
.checkselect {
    position: relative;
    display: inline-block;
    min-width: 150px;
    text-align: left;
}
.checkselect-control {
    position: relative;
    padding: 0 !important;
}        
.checkselect-control select {
    position: relative;
    display: inline-block;
    width: 100%;
    margin: 0;
    padding-left: 5px;
    height: 30px;
}
.checkselect-over {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;             
    cursor: pointer;
}
.checkselect-popup {
    display: none;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    width: 100%;
    height: auto;
    max-height: 200px;
    position: absolute;
    top: 100%;
    left: 0px; 
    border: 1px solid #dadada;
    border-top: none;
    background: #fff;
    z-index: 9999;
    overflow: auto;
    user-select: none;
}    
.checkselect label {
    position: relative;
    display: block;
    margin: 0;
    padding: 4px 6px 4px 25px;
    font-weight: normal;
    font-size: 1em;
    line-height: 1.1;
    cursor: pointer;
}            
.checkselect-popup input {
    position: absolute;
    top: 5px; 
    left: 8px;
    margin: 0 !important;
    padding: 0;
}
.checkselect-popup label:hover {
    background: #03a2ff;
    color: #fff;
}
.checkselect-popup fieldset {
    display: block;
    margin:  0;
    padding: 0;
    border: none;
}
.checkselect-popup fieldset input {
    left: 15px;
}        
.checkselect-popup fieldset label {
    padding-left: 32px;
}        
.checkselect-popup legend {
    display: block;
    margin: 0;
    padding: 5px 8px;
    font-weight: 700;
    font-size: 1em;
    line-height: 1.1;
}
   a.content {
    color: #000000;
    text-decoration: none;
   }
   a.content:visited {
    color: #000000; 
    text-decoration: none;
   }
   .container { 
       border:2px solid #ccc; 
       width:300px; 
       height: 100px; 
       overflow-y: scroll; 
    }
    tr.tracked {
        background-color: rgba(33, 150, 243, 0.4);
    }
    tr.trace {
        background-color: rgba(231, 108, 97, 0.4);
    }
    tr.topic_tracked {
        background-color: rgba(243, 126, 33, 0.4);
    }
    tr.viewed {
        background-color: rgb(242, 241, 239);
    }
    .scrollup{
        width:40px;
        height:40px;
        opacity:0.3;
        position:fixed;
        bottom:50px;
        right:100px;
        display:none;
        text-indent:-9999px;
        background: url('images/icon_top.png') no-repeat;
    }
    a.wall_post_more {
        color: rgb(42, 88, 133);
        cursor:hand;
    }
    img.social {
      margin-left: auto;
      margin-right: auto;
}
.loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.hidden {
  display: none;
}

tbody {
    overflow: auto;
    height: 200px;
}
[draggable] {
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  user-select: none;
  /* Required to make elements draggable in old WebKit */
  -khtml-user-drag: element;
  -webkit-user-drag: element;
}

#columns {
  list-style-type: none;
}

.column {
  width: 162px;
  padding-bottom: 5px;
  padding-top: 5px;
  text-align: center;
  cursor: move;
}
.column header {
  height: 20px;
  width: 150px;
  color: black;
  background-color: #ccc;
  padding: 5px;
  border-bottom: 1px solid #ddd;
  border-radius: 10px;
  border: 2px solid #666666;
}

.column.over {
  //border: 2px dashed #000;
  border-top: 2px solid blue;
}
.rulesTable {
  overflow: auto;
  height: 10px;
}

.settings {
  padding: 10 20;
  float: left;
}
   a.content {
    color: #000000;
    text-decoration: none;
   }
   a.content:visited {
    color: #000000; 
    text-decoration: none;
   }
   .mySlides {display: none}
   .slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}
/* Style the Image Used to Trigger the Modal */
#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption { 
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.popUpImg {
  cursor: hand;
  width: 100;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
% label = label if defined('label') else ""

<form name="AddRule" method="post">
  <div style="overflow: hidden;">
    <div style="width: 1000%;">
      <div style="float: left; padding-right: 20; display: inline-block;">
        <p style="color: black;"><strong>Построение правил</strong>:<br>
        Например, правилу <strong>кот</strong> будут соответствовать объекты с текстом "кот", "кОт", "Котик".<br> 
        Правилу <strong>"кот"</strong> из вышеперечисленных будет соответствовать только объект с текстом "кот". <br>
        Правилу <strong>-"кот"</strong> будут соответствовать объекты, которые не содержат точную словоформу «кот». <br>
        Правилу <strong>-собака</strong> будут соответствовать объекты, которые не содержат слово «собака» в любой форме.<br> 
        Чтобы искать записи с хэштегом, используйте правило вида <strong>"#хэштег"</strong>. <br> 
        Для создания правил, где тексты включают хотя бы одно из слов, используйте оператор ИЛИ - <strong>"|"</strong>.<br>  
        <strong>Пример:</strong> интерсвязь | is74 | intersvyaz</p>
      </div>
      <div style="float: left; display: inline-block;">
        <div style="display: inline-block; padding-top: 20; vertical-align:top">
          <div>
            <div style="float: left; padding-right:10;">
              <textarea cols=30 rows=1 name="label" placeholder="Название правила">{{label}}</textarea></p>     
            </div>
            <div class="checkselect" style="float: left">
                <label><input type="checkbox" name="brands[]" value="vk" checked> Вконтакте</label>
                <label><input type="checkbox" name="brands[]" value="ok"> Одноклассники</label>
                <label><input type="checkbox" name="brands[]" value="inst"> Инстаграм</label>
            </div>
          </div>
          <div>
            <textarea cols=50 rows=3 name="rules_words" placeholder="Слова"></textarea>
            <p><input value="Создать правило" type="button" id="b1" onClick="AddRules();"/></p>
          </div>
        </div>
        <div style="padding-left: 25; display: inline-block; vertical-align:top; overflow: auto;">
          <table class="sort rulesTable">
          <thead>
          <tr>
          <td>Соцсеть</td>
          <td>Правило</td>
          <td>Слова</td>
          <td>Управление</td>
          </tr>
          </thead>    
          <div class="rulesTable">
            <tbody id="rule_body" style="height: 150px">
            </tbody>
            </div>
          </table>
        </div>
      </div>
    </div>
  </div>

<div style="clear: left; display: flex">
<p>Начальная дата:<br> 
<input type="time" value="{{start_time}}" name="start_time"> - 
<input type="date" value="{{start_date}}" name="start_date">
<p style="padding-left: 20">Конечная дата:<br> 
<input type="time" value="{{end_time}}" name="end_time"> - 
<input type="date" value="{{end_date}}" name="end_date">
<p style="padding-left: 20">Вывести:<br> 
<input type="number" value = "100" name="start_number" style="height: 24px">
</p>
<div style="padding-top: 13; padding-left: 20; font-size: 18">
<p><label for="dupl">Убрать дубликаты по тексту</label>
<input type="checkbox" id="dupl" name="drop_text_duplicates" style="width:20px;height:20px;" value="check"></p>
</div>
<div style="padding-top: 13; padding-left: 20; font-size: 18">
<p><label for="sort_viewed">Не выводить просмотренное наверх</label>
<input type="checkbox" id="sort_viewed" name="sort_viewed" style="width:20px;height:20px;" value="check"></p>
</div>
<div id="totCount">
</div>
</form>
</div>

<div class="loader hidden" id="loader"></div>
<div id="warning_div" style="text-align:  center; font-size: 200%"></div>

<table class="sort hidden" align="center" id="main_table">
<thead>
<tr>
<td>№</td>
<td>Соцсеть</td>
<td>Тип</td>
<td>Дата</td>
<td>Текст</td>
<td>Вложения</td>
<td><img width="30" src="images/eye_monitor.png"></td>
<td><img width="30" src="images/like_outline_24.svg"></td>
<td><img width="30" src="images/comment_outline_24.svg"></td>
<td><img width="30" src="images/share_outline_24.svg"></td>
<td>Прочитано</td>
</tr>
</thead>
<tbody id="main_body">
</tbody>
</table>

<script>
function showTotalCount(count){
  var NewDiv = document.getElementById("totCount");
  NewDiv.innerHTML = '<p style="padding-left: 40; padding-top: 20; font-size: 18">Общее количество постов за этот период: <strong>' + count + '</strong><br></p>';
}
function clearTotalCount(){
  var NewDiv = document.getElementById("totCount")
  NewDiv.innerHTML = ""
}
</script>
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
<script>
document.onkeydown = function(evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc");
    } else {
        isEscape = (evt.keyCode === 27);
    }
    if (isEscape) {
        var modal = document.getElementById("myModal");
        modal.style.display = "none";
    }
};
</script>
<script>
function getPostByRule(source) {
    id = source.id.split('_')
    var newWar = document.getElementById("warning_div")
    newWar.innerHTML = "";
    document.getElementById("main_table").className = "sort hidden";
    var body = document.getElementById("main_body")
    body.innerHTML = ""
    document.getElementById("loader").className = "unhidden";
    console.log("Task: get posts by rule");
    document.getElementById("loader").className = "loader";
    if (document.getElementById("dupl").checked) {
      var drop_dupl = true;
    } else {
      var drop_dupl = false;
    }
    if (document.getElementById("sort_viewed").checked) {
      var sort_viewed = true;
    } else {
      var sort_viewed = false;
    }
    var xhttp = new XMLHttpRequest();
    var body = "tag=" + id[0] + "&social=" + id[1] + "&start_time=" + document.AddRule.start_time.value + "&start_date=" + document.AddRule.start_date.value + "&start_number=" + document.AddRule.start_number.value + "&end_time=" + document.AddRule.end_time.value + "&end_date=" + document.AddRule.end_date.value + "&drop_dupl=" + drop_dupl + "&sort_viewed=" + sort_viewed;
    xhttp.open("POST", "/get-posts-by-rule", true);
    xhttp.send(body);
    console.log("Getting posts by rule");

    var modal = document.getElementById("myModal");
    var modalImg = document.getElementById("img01");
    var span = document.getElementsByClassName("close")[0];

    modal.onclick = function() { 
      modal.style.display = "none";
    }
    span.onclick = function() { 
      modal.style.display = "none";
    }
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText.replace(/\bNaN\b/g, "null"));
          count = response[1]
          showTotalCount(count)
          response = response[0]

        } catch(e) {
          console.log(e)
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText)
          var response = []
        }
        var body = document.getElementById("main_body")
        if (typeof response == 'undefined') {
          var newWar = document.getElementById("warning_div")
          var newA = document.createElement("div");
          newA.innerHTML = "<br /><br /><br /><strong>Посты не найдены</strong>";
          newWar.appendChild(newA);
          document.getElementById("loader").className = "hidden";
          console.log('Post data is empty')
          clearTotalCount()
          return
        }
        for (var i = 0, length = response.length; i < length; i++)
        {
            var tr_tracked_image = "images/plus.png";
            var tr_class = "default";
            if (response[i]['has_trace']) {
                tr_class = "trace";
            } else if (response[i]['is_tracked']) {
                tr_class = "tracked";
                tr_tracked_image = "images/minus.png";
            } else if (response[i]['topic_tracked']) {
                tr_class = "topic_tracked";
            } else if (response[i]['is_viewed']) {
                tr_class = "viewed";
            }

            var newTr = document.createElement('tr');
            newTr.id = "tr*" + response[i]['social'] + "*" + response[i]['owner_id'] + "*" + response[i]['post_id'];
            newTr.className = tr_class;

            var td = document.createElement('td');
            td.innerHTML = i + 1;
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = '<img class="social" width="40" src="images/' + response[i]['social'] + '.png">';
            newTr.appendChild(td);

            td = document.createElement('td');
            td.setAttribute("align","center")
            if (response[i]['post_type'] == 'post') {
              td.innerHTML = 'Пост';
            } else if (response[i]['post_type'] == 'topic_post') {
              td.innerHTML = 'Обсуждение';
            } else if (response[i]['post_type'] == 'comment') {
              td.innerHTML = 'Комментарий';
            } else {
              td.innerHTML = 'Репост';
            }
            newTr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = '<a target="_blank" href="' + response[i]['post_href'] + '">' + response[i]['date'] + "</a>";
            newTr.appendChild(td);

            
            td = document.createElement('td');
            if (response[i]['text'] === "" || typeof response[i]['text'] === 'undefined') {
              td.innerHTML = "";
            } else {
              if (response[i]['post_type'] == 'post') {
                  if (response[i]['text'].length < 500) {
                    td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'] + "</a>";
                } else {
                    td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'].slice(0, 500) + "</a>" 
                                  + '<a class="wall_post_more" onclick="hide(this); show(this);"> Показать полностью…</a>'
                                  + '<span style="display: none">' + response[i]['text'].slice(500) + '</span>';
                }
              } else {
                if (response[i]['text'].length < 500) {
                    td.innerHTML = '<a class="content">' + response[i]['text'] + "</a>";
                } else {
                    td.innerHTML = '<a class="content">' + response[i]['text'].slice(0, 500) + "</a>" 
                                  + '<a class="wall_post_more" onclick="hide(this); show(this);"> Показать полностью…</a>'
                                  + '<span style="display: none">' + response[i]['text'].slice(500) + '</span>';
                }
              }
            }
            
            newTr.appendChild(td);

            td = document.createElement('td');
            href = response[i]['link']
            if (href === "" || typeof href === 'undefined') {
                td.innerHTML = "<a> Нет вложений </a>";
                } else {
                  if (response[i]['show_link'] && response[i]['show_link'].length > 10 && href.includes('video') == false) {
                  var img = document.createElement("img");
                  img.src = href;
                  img.className = "popUpImg";
                  img.onclick = function(){
                    modal.style.display = "block";
                    modalImg.src = this.src;
                  }
                  td.appendChild(img);
                  } else if (href.includes('video') == true) {
                    td.innerHTML = '<a target="_blank" href="' + href + '">' + '<img width="100" src="' + response[i]['show_link'] + '">' + "</a>";
                  } else {
                    td.align = 'center'
                    td.innerHTML = '<a target="_blank" align="center" href="' + href + '">' + response[i]['show_link'] + "</a>";
                  }
            }
            newTr.appendChild(td);


            td = document.createElement('td');
            if (typeof response[i]['views'] === 'undefined') {
              td.innerHTML = ''
            } else {
              td.innerHTML = response[i]['views'];
            }
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            if (typeof response[i]['likes'] === 'undefined') {
              td.innerHTML = ''
            } else {
              td.innerHTML = response[i]['likes'];
            }            
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            if (typeof response[i]['replies'] === 'undefined') {
              td.innerHTML = ''
            } else {
              td.innerHTML = response[i]['replies'];
            }             
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            if (typeof response[i]['shares'] === 'undefined') {
              td.innerHTML = ''
            } else {
              td.innerHTML = response[i]['shares'];
            }             
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            //if (response[i]['post_type'] == 'post') {
            //  td.innerHTML = '<img width="30" src="' + tr_tracked_image + '" onClick="changePostState(this)" id="' + response[i]['social'] + "*" + response[i]['owner_id'] + "*" + response[i]['post_id'] + '"' + ' style="cursor:hand;">';
            //} else {
            //   td.innerHTML = '<img width="30" src="' + tr_tracked_image + '" id="' + response[i]['social'] + "*" + response[i]['owner_id'] + "*" + response[i]['post_id'] + '"' + '>';
            //}
            if (!response[i]['is_viewed']) {
                var id = response[i]['type'] + "*" + response[i]['social'] + "*" + response[i]['owner_id'];
                if (response[i]['type'] === 'topic_post'){
                  id += "*" + response[i]['topic_id'];
                } else {
                  id += "*" + response[i]['post_id'];
                }
                if (response[i]['type'] !== 'post') {
                  id += "*" + response[i]['add_id'];
                }
                td.innerHTML = '<img width="30" src="images/check.png" id="' + id + '" onClick="setViewed(this)" style="cursor:hand;">';
            }
            td.setAttribute("align", "center")
            newTr.appendChild(td);

            body.appendChild(newTr);
        }
            
        document.getElementById("main_table").className = "sort";
        document.getElementById("loader").className = "hidden";
        console.log("Posts loaded")
    }
}
</script>

<script>
function trackTopic(source) {
    id = source.id.split('_')
    var xhttp = new XMLHttpRequest();
    var body = "tag=" + id[0] + "&social=" + id[1];
    xhttp.open("POST", "/track-topic", true);
    xhttp.send(body);
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log(e)
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = []
        }
        if (response === 'ok') {
          var ring_bell = document.getElementById(source.id)
          ring_bell.src = '/images/bell2.png'
          ring_bell.onclick = function(event) {
            untrackTopic(event.currentTarget)
          }
        } else {
          console.log(response)
        }
    }
}
function untrackTopic(source) {
    id = source.id.split('_')
    var xhttp = new XMLHttpRequest();
    var body = "tag=" + id[0] + "&social=" + id[1];
    xhttp.open("POST", "/untrack-topic", true);
    xhttp.send(body);
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log(e)
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = []
        }
        if (response === 'ok') {
            red_ring_bell = document.getElementById(source.id)
            red_ring_bell.src = '/images/bell.png'
            red_ring_bell.onclick = function(event) {
              trackTopic(event.currentTarget)
             }
        } else {
          console.log(response)
        }
    }
}
</script>

<script>
function getRules() {
  var body = document.getElementById("rule_body")
    body.innerHTML = ""
    var xhttp = new XMLHttpRequest();
    var body = "Get rules"
    xhttp.open("POST", "/get-rules", true);
    xhttp.send(body);
    console.log("Getting rules");
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Rules GOT");
          var response = JSON.parse(xhttp.responseText);
        } catch(e) {
          console.log(e)
          console.log("Problem in JSON.parse")
          console.log(xhttp.responseText);
          var response = []
        }
        var body = document.getElementById("rule_body")
        var tr_class = "default";
        for (var i = 0, length = response.length; i < length; i++)
        {
            var newTr = document.createElement('tr');
            newTr.id = "tr_" + response[i]['tag'];
            newTr.className = tr_class;

            var td = document.createElement('td');
            td.align = "center";
            var socials = response[i]['social'].split(" ")
            var socialPictures = ''
            for (var n = 0, len = socials.length; n < len; n++) {
              var picture = '<img class="social" width="30" src="images/' + socials[n] + '.png">';
              socialPictures = socialPictures + picture
            }
            td.innerHTML = socialPictures
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("width", "120")
            td.innerHTML = response[i]['tag']
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("width", "200")
                if (response[i]['value'].length < 180) {
                  td.innerHTML = response[i]['value']
              } else {
                  td.innerHTML = response[i]['value'].slice(0, 180)
                                + '<a class="wall_post_more" onclick="hide(this); show(this);"> …</a>'
                                + '<span style="display: none">' + response[i]['value'].slice(180) + '</span>';
              }
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.setAttribute("align", "center")
            var rules_dict = {{!tags}}
            if (response[i]['social'].includes('vk')) {
              if (rules_dict.includes(response[i]['tag']))
              {
                td.innerHTML = '<img width="35" src="/images/eye.png" onClick="getPostByRule(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] +
            '" style="cursor:hand;" align="middle" title="Показать посты">' + '<img width="25" hspace="5" src="/images/bell2.png" onClick="untrackTopic(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] + '_bell' + '" style="cursor:hand;" align="middle" title="Отслеживать тему">' + '<img width="30" src="/images/bin.png" onClick="dropTask(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] + '" style="cursor:hand;" align="middle" title="Удалить из поиска">'
              } else {
                td.innerHTML = '<img width="35" src="/images/eye.png" onClick="getPostByRule(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] +
            '" style="cursor:hand;" align="middle" title="Показать посты">' + '<img width="25" hspace="5" src="/images/bell.png" onClick="trackTopic(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] + '_bell' + '" style="cursor:hand;" align="middle" title="Отслеживать тему">' + '<img width="30" src="/images/bin.png" onClick="dropTask(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] + '" style="cursor:hand;" align="middle" title="Удалить из поиска">'
              }
                } else {
              td.innerHTML = '<img width="35" src="/images/eye.png" onClick="getPostByRule(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] +
            '" style="cursor:hand;" align="middle" title="Показать посты">' + '<img width="25" hspace="5" src="/images/bell3.png" id="' + response[i]['tag'] + '_' + response[i]['social'] + '_bell' + '" style="cursor:hand;" align="middle" title="Отслеживать тему">' + '<img width="30" src="/images/bin.png" onClick="dropTask(this)" id="' + response[i]['tag'] + '_' + response[i]['social'] + '" style="cursor:hand;" align="middle" title="Удалить из поиска">'
            }
            newTr.appendChild(td);
            body.appendChild(newTr);
        }        
        console.log("Rules loaded")
        document.getElementById('b1').disabled = false;
        document.getElementById('b1').value = "Создать правило"
    }
}
</script>

<script>
function AddRules() {
  var socialValues = getCheckedCheckBoxes()
  var xhttp = new XMLHttpRequest();
  var body = "social=" + socialValues + "&label=" + document.AddRule.label.value + "&words=" 
  + document.AddRule.rules_words.value + "&start_time=" + document.AddRule.start_time.value + "&start_date=" + document.AddRule.start_date.value 
  + "&end_time=" + document.AddRule.end_time.value + "&end_date=" + document.AddRule.end_date.value
  xhttp.open("POST", "/create-rule", true);
  xhttp.send(body);
  console.log("Getting posts by rule");
  document.getElementById('b1').disabled = true;
  document.getElementById('b1').value = "Добавление правила..."

  xhttp.onreadystatechange = function() {
      if (xhttp.readyState != 4) return;

      try {
        console.log("Posts GOT");
        var response = JSON.parse(xhttp.responseText);
      } catch(e) {
        console.log("Problem in JSON.parse")
        console.log(xhttp.responseText);
        var response = []
      }
      document.AddRule.reset();
      vk_input = document.querySelector("label>input[value='vk']");
      vk_input.click()
      vk_input.click()
      getRules();
  }
}
</script>


<script>
var dragSrcEl = null;

function handleDragStart(e) {
  // Target (this) element is the source node.
  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.outerHTML);

  this.classList.add('dragElem');
}
function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault(); // Necessary. Allows us to drop.
  }
  this.classList.add('over');

  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnter(e) {
  // this / e.target is the current hover target.
}

function handleDragLeave(e) {
  this.classList.remove('over');  // this / e.target is previous target element.
}

function handleDrop(e) {
  // this/e.target is current target element.

  if (e.stopPropagation) {
    e.stopPropagation(); // Stops some browsers from redirecting.
  }

  // Don't do anything if dropping the same column we're dragging.
  if (dragSrcEl != this) {
    // Set the source column's HTML to the HTML of the column we dropped on.
    //alert(this.outerHTML);
    //dragSrcEl.innerHTML = this.innerHTML;
    //this.innerHTML = e.dataTransfer.getData('text/html');
    this.parentNode.removeChild(dragSrcEl);
    var dropHTML = e.dataTransfer.getData('text/html');
    this.insertAdjacentHTML('beforebegin',dropHTML);
    var dropElem = this.previousSibling;
    addDnDHandlers(dropElem);
    
  }
  this.classList.remove('over');
  return false;
}

function handleDragEnd(e) {
  // this/e.target is the source node.
  this.classList.remove('over');

  sort_cols = document.getElementById("columns")

  cols = sort_cols.getElementsByTagName('li')

  for (var i = 0; i < cols.length; i++) {
      //alert( cols[i].attributes["name"].value );
  }

  /*[].forEach.call(cols, function (col) {
    col.classList.remove('over');
  });*/
}

function addDnDHandlers(elem) {
  elem.addEventListener('dragstart', handleDragStart, false);
  elem.addEventListener('dragenter', handleDragEnter, false)
  elem.addEventListener('dragover', handleDragOver, false);
  elem.addEventListener('dragleave', handleDragLeave, false);
  elem.addEventListener('drop', handleDrop, false);
  elem.addEventListener('dragend', handleDragEnd, false);

}

var cols = document.querySelectorAll('#columns .column');
[].forEach.call(cols, addDnDHandlers);
</script>

<script language="JavaScript">
function toggle(source) {
  checkboxes = source.parentElement.getElementsByTagName('input');
  if (source.checked){
      var newText = "Снять выделение";
  } else {
      var newText = "Выбрать все";
  }
  source.parentElement.getElementsByTagName("label")[0].textContent = newText;
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<script>
function hide(link) {
    link.style = "display: none;";
}
function show(link) {
    var span = link.nextElementSibling;
    var all_text = link.previousElementSibling;
    all_text.innerHTML = all_text.innerHTML + span.innerHTML
}
</script>



<script>
function changePostState(source) {
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("tr*"+source.id);
    var to_tracked = tr.className !== "tracked";
    if (to_tracked) {
        source.src = "images/minus.png";
        tr.className = "tracked";
    } else {
        source.src = "images/plus.png";
        tr.className = "default";
    }
    var body = "is_tracked=" + (tr.className === "tracked").toString() + "&id=" + source.id;
    xhttp.open("POST", "/change-post-track-state", true);
    xhttp.send(body);
    
}
</script>
<script>
function setViewed(source) {
    var tr = source.parentElement.parentElement;
    tr.className = "viewed";
    source.parentElement.innerHTML = "";

    var xhttp = new XMLHttpRequest();
    var body = "id=" + source.id;
    xhttp.open("POST", "/set-viewed-fts", true);
    xhttp.send(body);
}
</script>
<script type="text/javascript">
$(document).ready(function(){
 
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
        
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
    
});
</script>

<script>
function changeUserGroupState(source) {
    var xhttp = new XMLHttpRequest();
    var group_id = source.id.split("_")
    var tr = document.getElementById("tr_"+group_id[0]+"_"+group_id[1]);
    var state = tr.className !== "hidden";
    if (state) {
        tr.className = "hidden";
        source.title = "Показывать посты из этой группы";
        source.className = "inactive";
    } else {
        tr.className = "default";
        source.title = "Не показывать посты из этой группы";
        source.className = "active";
    }
    var body = "state=" + (tr.className !== "hidden").toString() + "&group_id=" + group_id[1] + "&social=" + group_id[0];
    xhttp.open("POST", "/change-user-group-state", true);
    xhttp.send(body);
    
}
</script>

<script type="text/javascript">
window.onload = onPageLoad();


function onPageLoad() {
  getRules()
}
</script>


<script>
function getCheckedCheckBoxes() {
  var selectedCheckBoxes = document.querySelectorAll("input[type='checkbox']:checked");
  var checkedValues = Array.from(selectedCheckBoxes).map(cb => cb.value);
  var socialValues = checkedValues.join(' ')
  return socialValues;
}

(function($) {
    function setChecked(target) {
        var selectedCheckBoxes = document.querySelectorAll("input[type='checkbox']:checked");
        var checkedValues = Array.from(selectedCheckBoxes).map(cb => cb.value);
        if (checkedValues) {
            $(target).find('select option:first').html('Выбрано: ' + checkedValues);
        } else {
            $(target).find('select option:first').html('Выберите соц.сети');
        }
    }
    getCheckedCheckBoxes()
    $.fn.checkselect = function() {
        this.wrapInner('<div class="checkselect-popup"></div>');
        this.prepend(
            '<div class="checkselect-control">' +
                '<select class="form-control"><option></option></select>' +
                '<div class="checkselect-over"></div>' +
            '</div>'
        );    

        this.each(function(){
            setChecked(this);
        });        
        this.find('input[type="checkbox"]').click(function(){
            setChecked($(this).parents('.checkselect'));
        });

        this.parent().find('.checkselect-control').on('click', function(){
            $popup = $(this).next();
            $('.checkselect-popup').not($popup).css('display', 'none');
            if ($popup.is(':hidden')) {
                $popup.css('display', 'block');
                $(this).find('select').focus();
            } else {
                $popup.css('display', 'none');
            }
        });

        $('html, body').on('click', function(e){
            if ($(e.target).closest('.checkselect').length == 0){
                $('.checkselect-popup').css('display', 'none');
            }
        });
    };
})(jQuery);    

$('.checkselect').checkselect();
</script>

<script>
function dropTask(source) {
    id = source.id.split('_')
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("tr_"+id[0]);
    var body = "tag=" + id[0] + "&social=" + id[1];
    xhttp.open("POST", "/drop-rule", true);
    xhttp.send(body);
    removeElement(tr)
}

function removeElement(element) {
    element.parentNode.removeChild(element);
}
</script>

<a href="#" class="scrollup">Наверх</a>

% include('notification.tpl')