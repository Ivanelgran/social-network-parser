% include('main.tpl')

% groups = groups if defined('groups') and groups is not None else []

<p><b>Критерии отбора групп</b></p>
<% 
main_prop = groups_config.get('main_prop') or "" if defined('groups_config') and groups_config is not None else ""
members_count = groups_config.get('members_count') or "" if defined('groups_config') and groups_config is not None else ""
our_prop = groups_config.get('our_prop') or "" if defined('groups_config') and groups_config is not None else ""
posts_per_day = groups_config.get('posts_per_day') or "" if defined('groups_config') and groups_config is not None else ""
views_prop = groups_config.get('views_prop') or "" if defined('groups_config') and groups_config is not None else ""
comments = groups_config.get('comments') or "" if defined('groups_config') and groups_config is not None else ""
label = label if defined('label') else ""
all_cities_checked = not defined('groups_config') or groups_config is None
new_parsed_groups = new_parsed_groups if new_parsed_groups is not None else len(groups)
%>

% checked_values = {}
% if defined('groups_config') and groups_config is not None:
% for i in range(1, 18):
% checked_values[i] = "checked" if i in groups_config.get('cities') else ""
% end
% end

<style>
   .container { 
       border:2px solid #ccc; 
       width:300px; 
       height: 100px; 
       overflow-y: scroll; 
    }
</style>

<form action="/add-task" method="post">
<input name="new-config" type="hidden" value="True">
<p>Города:</p>
<input id="0" type="checkbox" onClick="toggle(this)"> <label id="citiesToggle" for="0">Выбрать все</label><br/>
<div class="container">
    <input id="1" type="checkbox" name="city" value="1" {{checked_values.get(1) or ""}}> <label for="1">Челябинск</label> <br />
    <input id="2" type="checkbox" name="city" value="2" {{checked_values.get(2) or ""}}> <label for="2">Златоуст</label> <br />
    <input id="3" type="checkbox" name="city" value="3" {{checked_values.get(3) or ""}}> <label for="3">Каменск-Уральский</label> <br />
    <input id="4" type="checkbox" name="city" value="4" {{checked_values.get(4) or ""}}> <label for="4">Копейск</label> <br />
    <input id="5" type="checkbox" name="city" value="5" {{checked_values.get(5) or ""}}> <label for="5">Касли</label> <br />
    <input id="6" type="checkbox" name="city" value="6" {{checked_values.get(6) or ""}}> <label for="6">Коркино </label><br />
    <input id="7" type="checkbox" name="city" value="7" {{checked_values.get(7) or ""}}> <label for="7">Курган</label> <br />
    <input id="8" type="checkbox" name="city" value="8" {{checked_values.get(8) or ""}}> <label for="8">Кыштым</label> <br />
    <input id="9" type="checkbox" name="city" value="9" {{checked_values.get(9) or ""}}> <label for="9">Магнитогорск</label> <br />
    <input id="10" type="checkbox" name="city" value="10" {{checked_values.get(10) or ""}}> <label for="10">Миасс</label> <br />
    <input id="11" type="checkbox" name="city" value="11" {{checked_values.get(11) or ""}}> <label for="11">Озерск </label><br />
    <input id="12" type="checkbox" name="city" value="12" {{checked_values.get(12) or ""}}> <label for="12">Пласт</label> <br />
    <input id="13" type="checkbox" name="city" value="13" {{checked_values.get(13) or ""}}> <label for="13">Рощино</label> <br />
    <input id="14" type="checkbox" name="city" value="14" {{checked_values.get(14) or ""}}> <label for="14">Троицк </label><br />
    <input id="15" type="checkbox" name="city" value="15" {{checked_values.get(15) or ""}}> <label for="15">Чебаркуль </label><br />
    <input id="16" type="checkbox" name="city" value="16" {{checked_values.get(16) or ""}}> <label for="16">Еманжелинск </label><br />
    <input id="17" type="checkbox" name="city" value="17" {{checked_values.get(17) or ""}}> <label for="17">Южноуральск </label><br />
</div>
<p>Минимальное количество подписчиков
<input name="members_count" type="text" size="5" value="{{members_count}}"></p>
<p>Минимальная доля подписчиков из главенствующего городов
<input name="main_prop" type="text" size="5" value="{{main_prop}}"></p>
<p>Минимальная доля подписчиков из наших городов
<input name="our_prop" type="text" size="5" value="{{our_prop}}"></p>
<p>Минимальное количество постов в сутки
<input name="posts_per_day" type="text" size="5" value="{{posts_per_day}}"></p>
<p>Минимальная доля просмотров постов от количества участников
<input name="views_prop" type="text" size="5" value="{{views_prop}}"></p>
<p>Минимальное количество комментариев
<input name="comments" type="text" size="5" value="{{comments}}"></p>
<p><input value="Отобразить" type="submit" /></p>
</form>

<form id="save-task" action="/groups-tasks" method="post">
<input name="add-task" type="hidden" value="True">
<input name="social" type="hidden" value="vk">
<input name="label" type="hidden" value="{{label}}">
<p>Название: 
<input type="text" size="30" name="label" value="{{label}}"></p>
<input value="Добавить группы на парсинг" type="submit" />
</form>

<br>
<p>Количество новых групп для парсинга: <b>{{new_parsed_groups}}</b></p>

<table class="sort" align="center">
<thead>
<tr>
<td>№</td>
<td>Группа</td>
<td>Количество участников</td>
<td>Город</td>
<td>Доля подписчиков из главенствующего города</td>
<td>Доля подписчиков из наших городов</td>
<td>Среднее количество постов в день</td>
<td>Среднее количество просмотров постов</td>
<td>Среднее количество комментариев на пост</td>
</tr>
</thead>
<tbody>

% for counter, group in enumerate(groups):
    <tr>
        <td>{{counter+1}}</td>
        % href = 'https://vk.com/club' + group['group_id']
        <input name="add-to-task" type="hidden" value={{group['group_id']}} form="save-task">
        <td><a target="_blank" href="{{href}}">{{group['name']}}</a></td>
        <td>{{group.get('members_count') or ""}}</td>
        <td>{{group.get('main_city') or ""}}</td>
        <td>{{round(group.get('main_prop') or 0, 2)}}</td>
        <td>{{round(group.get('our_prop') or 0, 2)}}</td>
        <td>{{round(group.get('post_per_day') or 0, 2)}}</td>
        <td>{{round(group.get('views_per_post') or 0, 2)}}</td>
        <td>{{round(group.get('comments') or 0, 2)}}</td>
    </tr>
% end

</tbody>
</table>

<script language="JavaScript">
function toggle(source) {
  checkboxes = document.getElementsByName('city');
  if (source.checked){
      var newText = "Снять выделение";
  } else {
      var newText = "Выбрать все";
  }
  document.getElementById("citiesToggle").textContent = newText;
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

% if all_cities_checked:
<script type="text/javascript">
window.onload = onPageLoad();

function onPageLoad() {
  document.getElementById("0").checked = true;
  toggle(document.getElementById("0"));
}
</script>
% end

% include('notification.tpl')