% include('main.tpl')

% if start_date is None:
% from datetime import datetime, timedelta
% start_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
% start_time = '00:00'
% default_date = True
% else:
% default_date = False
% end

<% 
likes = likes if defined('likes') and likes is not None else ""
replies = replies if defined('replies') and replies is not None else ""
shares = shares if defined('shares') and shares is not None else ""
views = views if defined('views') and views is not None else ""
posts_count = posts_count if defined('posts_count') and posts_count is not None else "100"
filter_tracked = filter_tracked if defined('filter_tracked') else False
filter_viewed = filter_viewed if defined('filter_viewed') else False
all_cities_checked = not defined('checked_cities')
sort = sort if defined('sort') and sort is not None else ['date', 'views', 'replies', 'likes', 'shares']
%>

% checked_values = {}
% if defined('checked_cities'):
% for i in range(1, 18):
% checked_values[i] = "checked" if i in checked_cities else ""
% end
% end

% cur_checked_topics = {}
% if defined('checked_topics'):
% for i in range(len(all_topics)):
% cur_checked_topics[i] = "checked" if str(all_topics[i]['words']) in checked_topics else ""
% end
% end

% cur_checked_socials = {}
% if defined('checked_socials'):
% for i in ['vk', 'ok', 'inst']:
% cur_checked_socials[i] = "checked" if i in checked_socials else ""
% end
% end


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<style>
   a.content {
    color: #000000;
    text-decoration: none;
   }
   a.content:visited {
    color: #000000; 
    text-decoration: none;
   }
   .container { 
       border:2px solid #ccc; 
       width:300px; 
       height: 200px; 
       overflow-y: scroll; 
       font-size:18;
    }
    tr.tracked {
        background-color: rgba(33, 150, 243, 0.4);
    }
    tr.trace {
        background-color: rgba(231, 108, 97, 0.4);
    }
    tr.topic_tracked {
        background-color: rgba(243, 126, 33, 0.4);
    }
    tr.viewed {
        background-color: rgb(242, 241, 239);
    }
    .scrollup{
        width:40px;
        height:40px;
        opacity:0.3;
        position:fixed;
        bottom:50px;
        right:100px;
        display:none;
        text-indent:-9999px;
        background: url('images/icon_top.png') no-repeat;
    }
    a.wall_post_more {
        color: rgb(42, 88, 133);
        cursor:hand;
    }
    img.social {
      margin-left: auto;
      margin-right: auto;
      display: block;
    }
.loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.hidden {
  display: none;
}

[draggable] {
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  user-select: none;
  /* Required to make elements draggable in old WebKit */
  -khtml-user-drag: element;
  -webkit-user-drag: element;
}

#columns {
  list-style-type: none;
}

.column {
  width: 162px;
  padding-bottom: 5px;
  padding-top: 5px;
  text-align: center;
  cursor: move;
}
.column header {
  height: 20px;
  width: 150px;
  color: black;
  background-color: #ccc;
  padding: 5px;
  border-bottom: 1px solid #ddd;
  border-radius: 10px;
  border: 2px solid #666666;
}

.column.over {
  //border: 2px dashed #000;
  border-top: 2px solid blue;
}

.settings {
  padding: 10 20;
  float: left;
  font-size: 18;
}
   a.content {
    color: #000000;
    text-decoration: none;
   }
   a.content:visited {
    color: #000000; 
    text-decoration: none;
   }
   .mySlides {display: none}
   .slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Style the Image Used to Trigger the Modal */
#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption { 
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.popUpImg {
  cursor: hand;
  width: 100;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>

<form id="filter" action="/posts-monitor" method="post">
<div class="settings">
<input name="filter-posts" type="hidden" value="True">
<p>Минимальное количество лайков
<input name="likes" type="text" size="5" value="{{likes}}"></p>
<p>Минимальное количество комментариев
<input name="replies" type="text" size="5" value="{{replies}}"></p>
<p>Минимальное количество репостов
<input name="shares" type="text" size="5" value="{{shares}}"></p>
<p>Минимальное количество просмотров
<input name="views" type="text" size="5" value="{{views}}"></p>
<p>Количество выводимых постов
<input name="posts_count" type="text" size="5" value="{{posts_count}}"></p>
<p><label for="tracked">Выводить отслеживаемые темы наверх </label>
% filter_tracked = "checked" if filter_tracked else ""
<input id="tracked" name="filter_tracked" type="checkbox" {{filter_tracked}}></p>
<p><label for="viewed">Не выводить наверх просмотренные посты</label>
% filter_viewed = "checked" if filter_viewed else ""
<input id="viewed" name="filter_viewed" type="checkbox" {{filter_viewed}}></p>

</div>

<div class="settings">
<p>Соцсети:</p>
<input id="socials" type="checkbox" onClick="toggle(this)"> <label id="topicsToggle" for="socials">Выбрать все</label><br/>
<div class="container">
    <input id="social_vk" type="checkbox" name="social" value="vk" {{cur_checked_socials.get('vk') or ""}}> <label for="social_vk">Вконтакте</label> <br />
    <input id="social_ok" type="checkbox" name="social" value="ok" {{cur_checked_socials.get('ok') or ""}}> <label for="social_ok">Одноклассники</label> <br />
    <input id="social_inst" type="checkbox" name="social" value="inst" {{cur_checked_socials.get('inst') or ""}}> <label for="social_inst">Инстаграм</label> <br />
</div>
</div>

<div class="settings">
<p>Выводимые города:</p>
<input id="0" type="checkbox" onClick="toggle(this)"> <label id="citiesToggle" for="0">Выбрать все</label><br/>
<div class="container">
    <input id="1" type="checkbox" name="city" value="1" {{checked_values.get(1) or ""}}> <label for="1">Челябинск</label> <br />
    <input id="2" type="checkbox" name="city" value="2" {{checked_values.get(2) or ""}}> <label for="2">Златоуст</label> <br />
    <input id="3" type="checkbox" name="city" value="3" {{checked_values.get(3) or ""}}> <label for="3">Каменск-Уральский</label> <br />
    <input id="4" type="checkbox" name="city" value="4" {{checked_values.get(4) or ""}}> <label for="4">Копейск</label> <br />
    <input id="5" type="checkbox" name="city" value="5" {{checked_values.get(5) or ""}}> <label for="5">Касли</label> <br />
    <input id="6" type="checkbox" name="city" value="6" {{checked_values.get(6) or ""}}> <label for="6">Коркино </label><br />
    <input id="7" type="checkbox" name="city" value="7" {{checked_values.get(7) or ""}}> <label for="7">Курган</label> <br />
    <input id="8" type="checkbox" name="city" value="8" {{checked_values.get(8) or ""}}> <label for="8">Кыштым</label> <br />
    <input id="9" type="checkbox" name="city" value="9" {{checked_values.get(9) or ""}}> <label for="9">Магнитогорск</label> <br />
    <input id="10" type="checkbox" name="city" value="10" {{checked_values.get(10) or ""}}> <label for="10">Миасс</label> <br />
    <input id="11" type="checkbox" name="city" value="11" {{checked_values.get(11) or ""}}> <label for="11">Озерск </label><br />
    <input id="12" type="checkbox" name="city" value="12" {{checked_values.get(12) or ""}}> <label for="12">Пласт</label> <br />
    <input id="13" type="checkbox" name="city" value="13" {{checked_values.get(13) or ""}}> <label for="13">Рощино</label> <br />
    <input id="14" type="checkbox" name="city" value="14" {{checked_values.get(14) or ""}}> <label for="14">Троицк </label><br />
    <input id="15" type="checkbox" name="city" value="15" {{checked_values.get(15) or ""}}> <label for="15">Чебаркуль </label><br />
    <input id="16" type="checkbox" name="city" value="16" {{checked_values.get(16) or ""}}> <label for="16">Еманжелинск </label><br />
    <input id="17" type="checkbox" name="city" value="17" {{checked_values.get(17) or ""}}> <label for="17">Южноуральск </label><br />
</div>
</div>

<div class="settings">
<p>Выводимые темы:</p>
<input id="topic" type="checkbox" onClick="toggle(this)"> <label id="topicsToggle" for="topic">Выбрать все</label><br/>
<div class="container">

    % for i, topic in enumerate(all_topics):
    % topic_id = f"topic_{topic['words']}"
    <input id="{{topic_id}}" type="checkbox" name="topic" value="{{topic['words']}}" {{cur_checked_topics.get(i) or ""}}> <label for="{{topic_id}}">{{topic['words'] if topic['words'] != '-1' else '-1 тема не определена'}}</label> <br />
    % end
</div>
</div>

<div class="settings">
<p>Сортировка:</p>
<ul id="columns">
% sort_cols = {'date': 'Дата', 'views': 'Просмотры', 'replies': 'Комментарии', 'likes': 'Лайки', 'shares': 'Репосты'}
% for sort_col in sort:
  <li class="column" draggable="true" name="{{sort_col}}"><header>{{sort_cols[sort_col]}}</header> <input type="hidden" name="sort" value="{{sort_col}}"></li>
% end
</ul> 
</div>

<div style="clear: left;">
<div style="display: inline-block;">
<p>Начальная дата:<br> 
<input type="time" value="{{start_time}}" name="start_time"> - 
<input type="date" value="{{start_date}}" name="start_date">
</p>
<p><input value="Применить" type="submit"></p>
</div>
</form>
</div>

<div class="loader" id="loader"></div>

<table class="sort hidden" align="center" id="main_table">
<thead>
<tr>
<td>№</td>
<td>Соцсеть</td>
<td>Группа</td>
<td>Дата</td>
<td>Текст</td>
<td>Вложения</td>
<td><img width="30" src="images/eye_monitor.png"></td>
<td><img width="30" src="images/like_outline_24.svg"></td>
<td><img width="30" src="images/comment_outline_24.svg"></td>
<td><img width="30" src="images/share_outline_24.svg"></td>
<td>Тема</td>
<td>Тональность</td>
<td>Отслеживать пост</td>
</tr>
</thead>
<tbody id="main_body">
</tbody>
</table>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>

<script>
document.onkeydown = function(evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc");
    } else {
        isEscape = (evt.keyCode === 27);
    }
    if (isEscape) {
        var modal = document.getElementById("myModal");
        modal.style.display = "none";
    }
};
</script>
<script>
function getPosts() {
    console.log("Task: get posts");
    document.getElementById("loader").className = "loader";
    var xhttp = new XMLHttpRequest();
    var default_date = '{{default_date}}' === 'True';
    xhttp.open("POST", "/get-posts", true);
    xhttp.send("default_date=" + default_date.toString());
    console.log("Getting posts");

    var modal = document.getElementById("myModal");
    var modalImg = document.getElementById("img01");
    var span = document.getElementsByClassName("close")[0];

    modal.onclick = function() { 
      modal.style.display = "none";
    }
    span.onclick = function() { 
      modal.style.display = "none";
    }
    
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState != 4) return;

        try {
          console.log("Posts GOT");
          var response = JSON.parse(xhttp.responseText)['limits']['posts'];
        } catch(e) {
          console.log("Problem in JSON.parse")
          console.log(e)
          console.log(xhttp.responseText);
          var response = []
        }
        console.log(response)
        var body = document.getElementById("main_body")

        for (var i = 0, length = response.length; i < length; i++)
        {
            var tr_tracked_image = "images/plus.png";
            var tr_class = "default";
            if (response[i]['has_trace']) {
                tr_class = "trace";
            } else if (response[i]['is_tracked']) {
                tr_class = "tracked";
                tr_tracked_image = "images/minus.png";
            } else if (response[i]['topic_tracked']) {
                tr_class = "topic_tracked";
            } else if (response[i]['is_viewed']) {
                tr_class = "viewed";
            }

            var newTr = document.createElement('tr');
            newTr.id = "tr*" + response[i]['social'] + "*" + response[i]['owner_id'] + "*" + response[i]['post_id'];
            newTr.className = tr_class;

            var td = document.createElement('td');
            td.innerHTML = i + 1;
            newTr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = '<img class="social" width="40" src="images/' + response[i]['social'] + '.png">';
            newTr.appendChild(td);

            td = document.createElement('td');
            if (response[i]['name']) {
                td.innerHTML = '<a target="_blank" href="' + response[i]['group_href'] + '"'
                               + " title='Подписчиков: " + response[i]['members_count']
                               + " Город: " + response[i]['main_city']
                               + " Доля города: " + (Math.round(response[i]['main_prop'] * 100) / 100).toString()
                               + "'>" + response[i]['name'] + "</a>";
            } else {
                td.innerHTML = '<a target="_blank" href="' + response[i]['group_href'] + '"'
                               + " title='Подписчиков: " + response[i]['members_count']
                               + " Город: " + response[i]['main_city']
                               + " Доля города: " + (Math.round(response[i]['main_prop'] * 100) / 100).toString()
                               + "'>" + " Ссылка на группу " + "</a>";
            }
            newTr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = '<a target="_blank" href="' + response[i]['post_href'] + '">' + response[i]['date'] + "</a>";
            newTr.appendChild(td);

            td = document.createElement('td');
            if (response[i]['text'] === "") {
                td.innerHTML = '<div style="text-align: center;"><a target="_blank" href="' + response[i]['href'] + '" class="content"><Отсутствует текст></a></div>';
            } else {
              if (response[i]['text'].length < 500) {
                td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'] + "</a>";
            } else {
                td.innerHTML = '<a target="_blank" href="' + response[i]['href'] + '" class="content">' + response[i]['text'].slice(0, 500) + "</a>" 
                               + '<a class="wall_post_more" onclick="hide(this); show(this);"> Показать полностью…</a>'
                               + '<span style="display: none">' + response[i]['text'].slice(500) + '</span>';
              }
            }
            newTr.appendChild(td);

            td = document.createElement('td');
            href = response[i]['link']
            if (href !== "") {
                var img = document.createElement("img");
                img.src = href;
                img.className = "popUpImg";
                img.onclick = function(){
                  modal.style.display = "block";
                  modalImg.src = this.src;
                }
                td.appendChild(img);
            }
            newTr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = response[i]['views'] + ' (+' + response[i]['views_increase'] + ')';
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = response[i]['likes'] + ' (+' + response[i]['likes_increase'] + ')';
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = response[i]['replies'] + ' (+' + response[i]['replies_increase'] + ')';
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = response[i]['shares'] + ' (+' + response[i]['shares_increase'] + ')';
            td.setAttribute("nowrap", "nowrap")
            newTr.appendChild(td);

            td = document.createElement('td');
            if (response[i]['topic'] !== -1) {
                var href = '/topic?level=1&topic=' + response[i]['topic']
             /*   td.innerHTML = '<a target="_blank" href="' + href + '">' + response[i]['topic'] + " " + response[i]['topic_title'] + "</a>"; */
                td.innerHTML = '<a target="_blank">' + response[i]['topic'] + "</a>";
            } else {
                td.innerHTML = "-1";
            }
            newTr.appendChild(td);

            td = document.createElement('td');
            if (response[i]['sentiment'] === -1) {
              var sentiment = "images/negative_face.png";
            } else if (response[i]['sentiment'] === 1) {
              var sentiment = "images/positive_face.png";
            } else if (response[i]['sentiment'] === 0) {
              var sentiment = "images/neutral_face.png";
            }
            td.innerHTML = '<img width="30" src="' + sentiment + '">';
            td.setAttribute("align", "center")
            newTr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = '<img width="30" src="' + tr_tracked_image + '" onClick="changePostState(this)" id="' + response[i]['social'] + "*" + response[i]['owner_id'] + "*" + response[i]['post_id'] + '"' + ' style="cursor:hand;">';
            td.setAttribute("align", "center")
            newTr.appendChild(td);

            body.appendChild(newTr);
        }
            
        document.getElementById("main_table").className = "sort";
        document.getElementById("loader").className = "hidden";
        console.log("Posts loaded")
    }
}
</script>

<script>
var dragSrcEl = null;

function handleDragStart(e) {
  // Target (this) element is the source node.
  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.outerHTML);

  this.classList.add('dragElem');
}
function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault(); // Necessary. Allows us to drop.
  }
  this.classList.add('over');

  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnter(e) {
  // this / e.target is the current hover target.
}

function handleDragLeave(e) {
  this.classList.remove('over');  // this / e.target is previous target element.
}

function handleDrop(e) {
  // this/e.target is current target element.

  if (e.stopPropagation) {
    e.stopPropagation(); // Stops some browsers from redirecting.
  }

  // Don't do anything if dropping the same column we're dragging.
  if (dragSrcEl != this) {
    // Set the source column's HTML to the HTML of the column we dropped on.
    //alert(this.outerHTML);
    //dragSrcEl.innerHTML = this.innerHTML;
    //this.innerHTML = e.dataTransfer.getData('text/html');
    this.parentNode.removeChild(dragSrcEl);
    var dropHTML = e.dataTransfer.getData('text/html');
    this.insertAdjacentHTML('beforebegin',dropHTML);
    var dropElem = this.previousSibling;
    addDnDHandlers(dropElem);
    
  }
  this.classList.remove('over');
  return false;
}

function handleDragEnd(e) {
  // this/e.target is the source node.
  this.classList.remove('over');

  sort_cols = document.getElementById("columns")

  cols = sort_cols.getElementsByTagName('li')

  for (var i = 0; i < cols.length; i++) {
      //alert( cols[i].attributes["name"].value );
  }

  /*[].forEach.call(cols, function (col) {
    col.classList.remove('over');
  });*/
}

function addDnDHandlers(elem) {
  elem.addEventListener('dragstart', handleDragStart, false);
  elem.addEventListener('dragenter', handleDragEnter, false)
  elem.addEventListener('dragover', handleDragOver, false);
  elem.addEventListener('dragleave', handleDragLeave, false);
  elem.addEventListener('drop', handleDrop, false);
  elem.addEventListener('dragend', handleDragEnd, false);

}

var cols = document.querySelectorAll('#columns .column');
[].forEach.call(cols, addDnDHandlers);
</script>

<script language="JavaScript">
function toggle(source) {
  checkboxes = source.parentElement.getElementsByTagName('input');
  if (source.checked){
      var newText = "Снять выделение";
  } else {
      var newText = "Выбрать все";
  }
  source.parentElement.getElementsByTagName("label")[0].textContent = newText;
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<script>
function hide(link) {
    link.style = "display: none;";
}
function show(link) {
    var span = link.nextElementSibling;
    var all_text = link.previousElementSibling;
    all_text.innerHTML = all_text.innerHTML + span.innerHTML
}
</script>

% if all_cities_checked:
<script type="text/javascript">
window.onload = onPageLoad();

function onPageLoad() {
  document.getElementById("0").checked = true;
  toggle(document.getElementById("0"))
  getPosts()
}
</script>
% else:
<script type="text/javascript">
window.onload = onPageLoad();

function onPageLoad() {
  
  getPosts()
}
</script>
% end

<script>
function changePostState(source) {
    var xhttp = new XMLHttpRequest();
    var tr = document.getElementById("tr*"+source.id);
    var to_tracked = tr.className !== "tracked";
    if (to_tracked) {
        source.src = "images/minus.png";
        tr.className = "tracked";
    } else {
        source.src = "images/plus.png";
        tr.className = "default";
    }
    var body = "is_tracked=" + (tr.className === "tracked").toString() + "&id=" + source.id;
    xhttp.open("POST", "/change-post-track-state", true);
    xhttp.send(body);
    
}
</script>
<script type="text/javascript">
$(document).ready(function(){
 
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
        
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
    
});
</script>

<a href="#" class="scrollup">Наверх</a>

% include('notification.tpl')