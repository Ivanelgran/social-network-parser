% include('main.tpl')

<table class="sort" align="center">
<thead>
<tr>
<td>№</td>
<td>Ключевое слово</td>
</tr>
</thead>
<tbody>

% for counter, group in enumerate(groups):
    <tr>
        <td>{{counter+1}}</td>
        <td>{{group['keyword']}}</td>
    </tr>
% end

</tbody>
</table>

% include('notification.tpl')