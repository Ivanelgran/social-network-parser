% include('main.tpl')

% label = label if defined('label') else ""

<form action="/groups-tasks" method="post">
<p>Название задачи: 
<textarea cols=30 rows=1 name="label">{{label}}</textarea></p>
<p>Социальная сеть:
<select name="social">
    <option value="vk">Вконтакте</option>
    <option value="ok">Одноклассники</option>
    <option value="inst">Инстаграм</option>
</select>
<p>Добавить группы:</p>
<p style="color: gray;font-style: italic;">Примеры:<br>
- Вконтакте (ссылки на группы): https://vk.com/intersvyaz_vk https://vk.com/rostelecom https://vk.com/domru_ru<br>
- Одноклассники (ссылки на группы): https://ok.ru/intersvyaz https://ok.ru/rostelecom.official https://ok.ru/domru<br>
- Инстаграм (ссылки на группы): https://www.instagram.com/chelyabinsk.life https://www.instagram.com/zlatoust2015</p>
<textarea cols=50 rows=3 name="custom_task"></textarea>
<p><input value="Добавить группы для парсинга" type="submit" /></p>
</form>

% include('notification.tpl')