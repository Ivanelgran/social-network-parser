% include('main.tpl')

<style type="text/css">
.tabs { width: 100%; padding: 0px; margin: 0 auto; }
.tabs>input { display:none; }
.tabs>div {
    display: none;
    padding: 12px;
    border: 1px solid #C0C0C0;
    background: #FFFFFF;
}
.tabs>label {
    display: inline-block;
    padding: 7px;
    margin: 0 -5px -1px 0;
    text-align: center;
    color: #666666;
    border: 1px solid #C0C0C0;
    background: #E0E0E0;
    cursor: pointer;
}
.tabs>input:checked + label {
    color: #000000;
    border: 1px solid #C0C0C0;
    border-bottom: 1px solid #FFFFFF;
    background: #FFFFFF;
}
.loader {
    margin: auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 47%;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.buttonget {
  background-color: #2196F3; /* Green background */
  border: none; /* Remove borders */
  color: white; /* White text */
  padding: 12px 24px; /* Some padding */
  font-size: 16px; /* Set a font-size */
  border-radius: 4px;
  cursor: pointer;
}

/* Add a right margin to each icon */
.fa {
  margin-left: -12px;
  margin-right: 8px;
}

.hidden {
  display: none;
}
.trend_block{
    width: 25%;
    text-align: center;
    border-right: 1px solid #ddd;
}
#tab_1:checked ~ #main_txt_1,
#tab_2:checked ~ #main_txt_2 { display: block; }
</style>

<link href="http://cdn.pydata.org/bokeh/release/bokeh-1.0.4.min.css" 
        rel="stylesheet">
<link href="http://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.4.min.css" 
        rel="stylesheet">
<link href="http://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.4.min.css" 
        rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="http://cdn.pydata.org/bokeh/release/bokeh-1.0.4.min.js"></script>
<script src="http://cdn.pydata.org/bokeh/release/bokeh-api-1.0.4.min.js"></script>
<script src="http://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.4.min.js"></script>
<script src="http://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.4.min.js"></script>

<div class="tabs">
    <input type="radio" name="inset" value="" id="tab_1" checked>
    <label for="tab_1">Мои группы</label>

    <input type="radio" name="inset" value="" id="tab_2">
    <label for="tab_2">Все группы</label>

    <div id="main_txt_1">
        
        <div class="loader" id="txt_1_loader" style="display:none;"></div>
        <table width="100%" id="txt_1_table">
        <tr>
        <td align="center" colspan="3" style="border-bottom: 1px solid #ddd;padding: 10;">
            <label class="container" checked>День
                <input type="radio" value="day" name="txt_1" checked>
                <span class="checkmark"></span>
            </label>
            <label class="container">Неделя
                <input type="radio" value="week" name="txt_1">
                <span class="checkmark"></span>
            </label>
            <button class="buttonget" onClick="loadTrends(this)" id="txt_1">
                <i class="hidden"></i>Получить
            </button>
        </td>
        </tr>
        <tr id="tr_txt_1">
        <td class="trend_block hidden">
            <h2>Популярные слова:</h2>
            <font size=5 id="txt_1_words">
            </font>
        </td>
        <td class="hidden">
            <div id="txt_1_wedge"></div>
        </td>
        </tr>
        </table>
    </div>
    <div id="main_txt_2">
        <table width="100%" id="txt_2_table">
        <tr>
        <td align="center" colspan="3" style="border-bottom: 1px solid #ddd;padding: 10;">
            <label class="container" checked>День
                <input type="radio" value="day" name="txt_2" checked>
                <span class="checkmark"></span>
            </label>
            <label class="container">Неделя
                <input type="radio" value="week" name="txt_2">
                <span class="checkmark"></span>
            </label>
            <button class="buttonget" onClick="loadTrends(this)" id="txt_2">
                <i class="hidden"></i>Получить
            </button>
        </td>
        </tr>
        <tr id="tr_txt_2">
        <td class="trend_block hidden">
            <h2>Популярные слова:</h2>
            <font size=5 id="txt_2_words">
            </font>
        </td>
        <td class="hidden">
            <div id="txt_2_wedge"></div>
        </td>
        </tr>
        </table>
    </div>
</div>

<script>
function loadTrends(source) {
    if (source.className === "buttonget") {
        console.log("start load trends for " + source.id)
    	source.className = "buttonget load"
        source.children[0].className = "fa fa-spinner fa-spin"
        source.childNodes[2].nodeValue = "Загрузка"
            
        var tr = document.getElementById("tr_" + source.id);

        tr.children[0].className = "hidden";
        tr.children[1].className = "hidden";

        document.getElementById(source.id + "_words").innerHTML = "";
        document.getElementById(source.id + "_wedge").innerHTML = "";

        var radios = document.getElementsByName(source.id);

        var period = "day"

        for (var i = 0, length = radios.length; i < length; i++)
        {
            if (radios[i].checked)
            {
            // do whatever you want with the checked radio
                period = radios[i].value;

                // only one radio can be logically checked, don't check the rest
                break;
            }
        }
        
        var xhttp = new XMLHttpRequest();
        var body = "my_groups=" + (source.id === "txt_1").toString() + "&period=" + period;
        xhttp.open("POST", "/load-trends", true);
        xhttp.send(body);
        
        xhttp.onreadystatechange = function() {

            if (xhttp.readyState != 4) return;

            var response = JSON.parse(xhttp.responseText);

            Bokeh.embed.embed_item(response['script'], source.id + "_wedge");

            var words = document.getElementById(source.id + "_words");
            response["trend_words"].forEach(function(element) {
                words.innerHTML += element + "<br> ";
            });

            tr.children[0].className = "trend_block";
            tr.children[1].className = "";

            source.className = "buttonget"
            source.children[0].className = "hidden"
            source.childNodes[1].nodeValue = "Получить"
        }
    }
}
</script>