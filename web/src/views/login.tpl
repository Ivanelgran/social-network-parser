% users = users if defined('users') and users is not None else []

<h2>Выбор пользователя</h2>

<form action="/login">
  <select name="user">
    % for user_hash, user in users.items():
  <option value="{{user_hash}}">{{user}}</option>
    % end
  </select>
  <input type="submit" value="Выбрать">
</form>

<h2>Создание пользователя</h2>

<form action="/login">
  <input type="text" name="create">
  <input type="submit" value="Создать">
</form>