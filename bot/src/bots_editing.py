import ast
import re
import time
import pickle
import os
import vk_api
import requests
import logging
from bs4 import BeautifulSoup
from pathlib import Path

from pip._vendor.retrying import retry
from selenium import webdriver
from common_lib.queue_manager import RpcClient, make_server
from multiprocessing import Process
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from pika.exceptions import StreamLostError, ConnectionWrongStateError

LOGGER = logging.getLogger('Bots_edit')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
SRC_DIR = Path(__name__).resolve().parents[0]

fh = logging.FileHandler(os.path.join(SRC_DIR, 'logs/logs.log'))
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)

cass = RpcClient('db_tasks')

class Captcha(Exception):
    def __init__(self, text):
        self.txt = text

class Bots:
    count_try = 5

    personal_alcohol_and_smoking = {
        0: "Не указано",
        1: "Резко негативное",
        2: "Негативное",
        3: "Компромиссное",
        4: "Нейтральное",
        5: "Положительное"
    }

    personal_people_main = {
        0: "Не указано",
        1: "Ум и креативность",
        2: "Доброта и честность",
        3: "Красота и здоровье",
        4: "Власть и богатство",
        5: "Смелость и упорство",
        6: "Юмор и жизнелюбие"
    }

    personal_life_main = {
        0: "Не указано",
        1: "Семья и дети",
        2: "Карьера и деньги",
        3: "Развлечения и отдых",
        4: "Наука и исследования",
        5: "Совершенствование мира",
        6: "Саморазвитие",
        7: "Красота и искусство",
        8: "Слава и влияние",
    }

    personal_political = {
        0: "Не указано",
        8: "Индифферентные",
        1: "Коммунистические",
        2: "Социалистичеcкие",
        3: "Умеренные",
        4: "Либеральные",
        5: "Консервативные",
        6: "Монархические",
        7: "Ультраконсервативные",
        9: "Либертарианские",
    }

    education_form = {
        0: "Не выбрана",
        'Очное отделение': "Очная",
        'Очно-заочное отделение': "Очно-заочная",
        'Заочное отделение': "Заочная",
        'Экстернат': "Экстернат",
        'Дистанционное обучение': "Дистанционная",
    }

    education_status = {

        "Не выбран": 0,

        "Абитуриентка": 1,
        "Абитуриент": 1,

        "Студентка (специалист)": 2,
        "Студент (специалист)": 2,

        "Студентка (магистр)": 3,
        "Студент (магистр)": 3,

        "Выпускница (специалист)": 4,
        "Выпускник (специалист)": 4,

        "Выпускник(бакалавр)": 5,
        "Выпускница (бакалавр)": 5,

        "Выпускница (магистр)": 6,
        "Выпускник(магистр)": 6,

        "Аспирант": 7,
        "Аспирантка": 7,

        "Кандидат наук": 8,

        "Доктор наук": 9,

        "Интерн": 10,

        "Клинический ординатор": 11,

        "Соискательница": 12,

        "Соискатель": 13,

        "Ассистент-стажёр": 14,
        "Докторант": 15,

        "Адъюнкт": 16,
    }

    def link_personal_information(self, login, password):
        """
            Авторизация через sisenium и переход на страницу редактирования персональной информации

            После успешного выполнения возвращает объект driver
        """
        try:
            url = "https://vk.com/"
            user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'

            options = webdriver.ChromeOptions()
            options.add_argument(f'user-agent={user_agent}')
            options.add_argument('--no-sandbox')
            options.add_argument('--headless')
            options.add_argument("--disable-setuid-sandbox")
            options.add_argument("--window-size=1920,1080")

            driver = webdriver.Chrome("data/chromedriver", options=options)
            driver.implicitly_wait(10)
            driver.get(url=url)
            driver.find_element_by_xpath("//input[@id='index_email']").send_keys(login)
            driver.find_element_by_xpath("//input[@id='index_pass']").send_keys(password)
            driver.find_element_by_xpath("//button[@id='index_login_button']").click()
            time.sleep(1)

            if self.captcha_check(driver):
                raise Captcha('Ошибка: "Captcha" при сохранении изменений')

            driver.find_element_by_xpath("//a[@id='top_profile_link']").click()
            prof_dit = driver.find_element_by_id('top_edit_link')
            url = prof_dit.get_attribute('href')
            driver.get(url=url)

        except Exception as error:
            LOGGER.exception(error)
            raise error

        return driver

    def captcha_check(self, driver):
        try:
            driver.find_element_by_xpath("//div[@class='box_layout']")
            return True
        except:
            return False

    def save_personal_changes(self, driver):
        """
            Нажатие на кнопку "Сохранить" изменения
        """
        # LOGGER.info(driver)
        try:
            driver.find_element_by_xpath("//button[@class='flat_button button_big_width']").click()

            if self.captcha_check(driver):
                raise Captcha('Ошибка: "Captcha" при сохранении изменений')
            else:
                driver.find_element_by_xpath("//div[@class='msg_text']")

            # LOGGER.info(driver.find_element_by_xpath("//div[@class='msg_text']").get_attribute('outerHTML'))
        except Captcha as ex:
            raise ex
        except Exception as ex:
            time.sleep(3)
            driver.find_element_by_xpath("//button[@class='flat_button button_big_width']").click()

        return True
        time.sleep(1)

    def insert_text(self, driver, text, enter=True, tag=''):
        """
            Вставка текста в поля с селектором и вводом текста

            driver  -
            text    - текст, который нужно ввести
            enter   - нажатие клавиши enter после ввода
            tag     - название переменной, хранящей значение text (необходимо для отслеживания неудачных попыток ввода)

            После успешного выполнения возвращает True иначе False
        """
        # LOGGER.info(text)

        if text is None:
            text = " "

        p_g = driver.find_element_by_xpath('..')
        p_g.find_element_by_tag_name('input').click()
        pole = p_g.find_element_by_tag_name('input')
        # time.sleep(1)

        if enter:
            pole.clear()
            pole.send_keys(text)
            time.sleep(1)
            pole.send_keys(u'\ue007')
            pole.send_keys(Keys.ESCAPE)

            # LOGGER.info(pole.get_attribute('value'))

            if pole.get_attribute('value') == '':
                LOGGER.warning('Неправильные данные {0}: {1}'.format(tag, text))
                return False

            return True

        pole.clear()
        pole.send_keys(text, Keys.ESCAPE)
        time.sleep(1)
        return True

    def pedit_del(self, driver, class_name):
        """
            Удаление старых записей

            class_name - название класса элемента, который нужно удалить
        """
        time.sleep(2)

        # LOGGER.info('Пытаемся удалить ')
        field = driver.find_elements_by_xpath("//a[@class='{0}']".format(class_name))

        # LOGGER.info('Нужно удалить')
        # LOGGER.info(len(field))

        if len(field) > 0:
            try:
                field[0].click()
            except:
                LOGGER.warning('Неудачна попытка удаления')

            for i in field:
                time.sleep(1)
                try:
                    i.click()
                except:
                    LOGGER.warning('Неудачна попытка удаления')

            # Сохранение
            self.save_personal_changes(driver)

            driver.refresh()

    def encoding_dict_in_special_str(self, social, title, login, password, field_table):
        """
            Приведение словаря к строке

            field_table = {
                'variable_1': '',
                'variable_2': '',
                'variable_3': '',
                'variable_4': '',
                ...
            }

            После успешного выполнения возвращает cnhjre:
            s ='login=#?#password=#?#social=#?#title=#?#variable_1=#?#variable_2=#?#... '
        """
        s = 'login#=#{0}#?#password#=#{1}#?#social#=#{2}#?#title#=#{3}'.format(login, password, social, title)
        arr_tag = ['grandparent', 'parent', 'sibling', 'child', 'grandchild']
        #
        for key in field_table.keys():
            if key == 'grandparent':
                for i in arr_tag:
                    s = '{0}#?#{1}#=#{2}'.format(s, i, field_table[i].replace(';', '[]'))

        for key in field_table.keys():
            if key != 'social' and key != 'title' and arr_tag.count(key) == 0:
                s = '{0}#?#{1}#=#{2}'.format(s, key, field_table[key])

        # LOGGER.info('s=')
        # LOGGER.info(s)
        return s

    def pars_fields(self, dic_fields):
        """
            Парсинг строки с данными

            dic_fields='login=#?#password=#?#
                variable_1=#?#
                variable_2=#?#
                variable_3=#?#
                variable_4=#?#
                ... '

            После успешного выполнения возвращает словарь:
            dic_value = {
                'login': '',
                'password': '',
                'variable_1': '',
                'variable_2': '',
                ...
            }
        """

        # LOGGER.info('bot____pars_fields____bot')
        # LOGGER.info(dic_fields)

        dic_value = {}
        arr_tag = ['grandparent', 'parent', 'sibling', 'child', 'grandchild']
        list_fields = dic_fields.split('#?#')

        # LOGGER.info(list_fields)

        for i in list_fields:
            spl = i.split('#=#')

            tag = spl[0]
            value = spl[1]

            if arr_tag.count(tag) != 0:
                list_value = value.split('[]')
                list_value = list(filter(None, list_value))
                dic_value[tag] = list_value

            else:
                if tag in dic_value and tag == 'title' and dic_value[tag] != value:
                    dic_value['new_title'] = value

                elif tag in dic_value and tag == 'social' and dic_value[tag] != value:
                    dic_value['new_social'] = value

                else:
                    if value == '':
                        dic_value[tag] = None
                    else:
                        dic_value[tag] = value

        # LOGGER.info(dic_value)
        return dic_value

    def select_data(self, driver, text, field, index):
        """
            Метод для поиска элемента на странице с selector_dropdown

            driver  -
            text    - текс, который нужно выбрать
            field   - имя поля, с которым нужно взаимодействовать
            id      - индекс элемента на странице

            После успешного выполнения возвращает True иначе False
        """
        # LOGGER.info(text)

        f = driver.find_elements_by_css_selector('[id^="{0}"]'.format(field))
        p_g = f[index].find_element_by_xpath('..')
        p_g = p_g.find_element_by_xpath('..')
        p_g.find_element_by_class_name('selector_dropdown').click()

        if text is None or text == ' ':
            f = driver.find_elements_by_xpath("//li[@val='0']")
            # LOGGER.info('Не выбран "{0}"'.format(field))
            f[-1].click()
            return False
        else:
            f = driver.find_elements_by_xpath("//li[@title='{0}']".format(text))
            # LOGGER.info('Ищем "{0}"'.format(text))

            if len(f) < 1:
                driver.find_elements_by_xpath("//li[@val='0']")
                # LOGGER.info('"{0}" не существует'.format(text))
                return False
        # LOGGER.info(f)
        f[-1].click()
        return True

    def save_to_db(self, dict_value, task_name, name_fields, new_title=None, new_social=None):
        try:

            titel = dict_value['title']
            social = dict_value['social']

            if 'login' in dict_value:
                dict_value.pop('login')
                dict_value.pop('password')

            if 'social' in dict_value:
                dict_value.pop('social')
                dict_value.pop('title')

            if 'description' in dict_value:
                description = dict_value['description']
                dict_value.pop('description')
            else:
                description = None

            for i in dict_value.keys():
                if dict_value[i] is None:
                    dict_value[i] = ''
                dict_value[i] = str(dict_value[i])

            LOGGER.info('--save_to_db--')

            task = {'task_name': task_name,
                    'args': {'social': social, 'title': titel, 'description': description, 'new_title': new_title,
                             'new_social': new_social, name_fields: dict_value}}

            res = cass.call(task)
            LOGGER.info(res)
            return res

        except Exception as error:
            raise error

    def login_vk_api(self, login, password):
        """
            Авторизация через vk_api

            Возвращает объект <class 'vk_api.vk_api.VkApiMethod'>, доступ к методам vk_api
        """
        try:

            vk_session = vk_api.VkApi(login, password, scope='wall, photos, secure, audio, video, groups , notes')
            vk_session.auth(reauth=True)
            vk = vk_session.get_api()

        except Exception as error:
            LOGGER.exception(error)
            # if str(error) == 'Captcha needed':
            # return 'Captcha needed'
            raise error

        return vk

    def login_vk_api_upload(self, login, password):
        """
            Авторизация через vk_api

            Возвращает объект <class 'vk_api.upload.VkUpload'>, доступ к методам vk_api для загрузки файлов
        """
        try:
            vk_session = vk_api.VkApi(login, password, scope='wall, photos, secure, audio, video, groups , notes')
            vk_session.auth(reauth=True)
            upload = vk_api.VkUpload(vk_session)

        except Exception as error:
            LOGGER.exception(error)
            if str(error) == 'Captcha needed':
                return 'Captcha needed'
            raise error

        return upload

    def spt_url(self, url):
        """
            Разбор url

            Возвращает словарь dict_url = {'nik_id': '', 'owner_id': '', 'object_id': '', 'type': ''}
        """

        dict_url = {'nik_id': '', 'owner_id': '', 'object_id': '', 'type': ''}

        spl = url.split('/')
        spl = spl[-1].split('?')
        nik_id = spl[0]

        if len(spl) == 1:
            r = re.findall(r"[+-]?\d+", spl[0])
            name_type = re.findall(r"[a-z]+", spl[0])[0]

            if len(r) > 1:

                dict_url['owner_id'] = r[0]
                dict_url['object_id'] = r[1]
                dict_url['type'] = name_type
                return dict_url

            elif not r:
                dict_url['nik_id'] = nik_id
                return dict_url

            else:
                dict_url['nik_id'] = name_type
                dict_url['owner_id'] = r[0]
                return dict_url

        spl = spl[-1].split('%')
        r = re.findall(r"[+-]?\d+", spl[0])
        type_id = re.findall(r"\w+[a-z]", spl[0])[0]
        dict_url = {'nik_id': nik_id, 'owner_id': r[0], 'object_id': r[1], 'type': type_id}

        return dict_url

    def put_like(self, vk, url):
        """
            like (пост, фото, видео)

            Возвращает В случае успеха возвращает объект с полем likes, в котором находится текущее количество пользователей,
            которые добавили данный объект в свой список Мне нравится.
        """
        try:
            data_url = self.spt_url(url)
            if data_url['type'] == 'wall':
                data_url['type'] = 'post'
            result = vk.likes.add(item_id=data_url['object_id'], type=data_url['type'], owner_id=data_url['owner_id'])
        except Exception as error:
            LOGGER.exception(error)
            return False

        return result

    def put_like_comment(self, vk, url, text_comment):
        """
            like (комметрарий)

            Вслучае успеха возвращает объект с полем likes, в котором находится текущее количество пользователей,
            которые добавили данный объект в свой список Мне нравится.
        """
        global comment
        try:
            data_url = self.spt_url(url)

            r = requests.get(url)

            soup = BeautifulSoup(r.text, 'html.parser')
            comments = soup.body.findAll('div', "pi_body")
            # print('comments = ', comments)
            comments = str(comments).split('pi_body')

            for i in comments:
                if i.find(text_comment) > 0:
                    comment = i

            if data_url['type'] == 'topic':
                target = 'post'
            else:
                target = 'reply'

            comment_id = comment.split(target)
            comment_id = re.findall(r"\w+", comment_id[1])
            comment_id = comment_id[0]

            result = vk.likes.add(item_id=comment_id, type='comment', owner_id=data_url['owner_id'])

        except Exception as error:
            LOGGER.exception(error)
            return False

        return result

    def put_repost(self, vk, url, message):
        """
            Репост

            После успешного выполнения возвращает объект со следующими полями:
                success — всегда содержит 1;
                post_id — идентификатор созданной записи;
                reposts_count — количество репостов объекта с учетом осуществленного;
                likes_count — число отметок «Мне нравится» у объекта.
        """
        try:
            data_url = self.spt_url(url)
            s = data_url['type'] + data_url['owner_id'] + '_' + data_url['object_id']
            print(s)
            result = vk.wall.repost(object=s, message=message, v='5.100')
        except Exception as error:
            LOGGER.exception(error)
            return error
        return result

    def put_comment(self, vk, url, message):
        """
            Написать комментарий

            После успешного выполнения возвращает идентификатор созданного комментария.
            Но в зависимости от объекта, которому оставляется коментарий результат может менять.
            Более подробно узнать об результатах конкретной функции можно в документации vk
        """

        data_url = self.spt_url(url)
        print(data_url)
        if not data_url['object_id']:
            return 'Ошибка: Неправильная ссылка'
        try:
            if data_url['type'] == 'photo':
                result = vk.photos.createComment(owner_id=data_url['owner_id'], photo_id=data_url['object_id'],
                                                 message=message, v='5.100')

            elif data_url['type'] == 'wall':
                result = vk.wall.createComment(owner_id=data_url['owner_id'], post_id=data_url['object_id'],
                                               message=message, v='5.100')

            elif data_url['type'] == 'video':
                result = vk.video.createComment(owner_id=data_url['owner_id'], video_id=data_url['object_id'],
                                                message=message, v='5.100')

            elif data_url['type'] == 'topic':
                result = vk.board.createComment(group_id=data_url['owner_id'], topic_id=data_url['object_id'],
                                                message=message, v='5.100')
            #
            # elif data_url['type'] == 'topic':
            #     result = vk.notes.createComment(owner_id=data_url['owner_id'], note_id=data_url['object_id'],
            #                                     message=message, v='5.100')

            elif data_url['type'] == 'product':
                result = vk.market.createComment(owner_id=data_url['owner_id'], item_id=data_url['object_id'],
                                                 message=message, v='5.100')

            else:
                return url, 'Что-то новенькое'

        except Exception as error:
            LOGGER.exception(error)
            return error

        return result

    def album_create(self, login, password, list_name, album_lenght, album_name):
        """
            Создание альбома и добавляет в него фото

            list_name    - список имен фотографий
            album_lenght - список с количеством фото в каждом альбоме
            album_name   - список с именами альбомов

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        album_name = album_name.split(',')
        album_lenght = album_lenght.split(',')

        LOGGER.info(album_name)
        LOGGER.info(album_lenght)

        try:
            vk = self.login_vk_api(login, password)
            upload = self.login_vk_api_upload(login, password)
            album_id = []
            if vk and upload:

                count_album = 0
                count_photo = 0

                for i in album_name:

                    result = vk.photos.createAlbum(title=i)
                    album_id.append(result['id'])

                    LOGGER.info(result)
                    for j in range(int(album_lenght[count_album])):
                        upload.photo(list_name[count_photo], album_id=result['id'], )
                        LOGGER.info(list_name[count_photo])

                        count_photo += 1
                    count_album += 1

                return True

        except Exception as error:

            try:
                for i in album_id:
                    vk.photos.deleteAlbum(i)

            except Exception as err:
                LOGGER.exception(err)

            LOGGER.exception(error)
            return False

        return False

    def add_photos_into_album(self, upload, url, list_photo):
        """
            Добавление фотографий в альбом

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        global result
        try:
            data_url = self.spt_url(url)

            if data_url['object_id'] == '':
                album_id = data_url['owner_id']

            else:
                album_id = data_url['object_id']

            for i in list_photo:
                result = upload.photo(i, album_id=album_id)

        except Exception as error:
            LOGGER.exception(error)
            return error

        return result

    def add_avatar(self, login, password, photo):
        """
            Добавление аватарки

            photo - путь до фото

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        LOGGER.info(photo)
        try:
            upload = self.login_vk_api_upload(login, password)

            upload.photo_profile(photo=photo)

        except Exception as error:
            LOGGER.exception(error)
            return False

        return True

    def add_video(self, vk, url):
        """
            Добавление видео в "мои видеозаписи"

            url - ссылка на видео

            После успешного выполнения возвращает True иначе False
        """
        try:
            data_url = self.spt_url(url)
            if vk.video.add(video_id=data_url['object_id'], owner_id=data_url['owner_id']) == 1:
                return True
            else:
                return False
        except Exception as error:
            LOGGER.exception(error)
            return False

    @retry(stop_max_attempt_number=count_try)
    def profile_editing(self, dic_fields, sava_db=True):
        """
            Редактирование инф. аккаунта

            dic_fields='login=#?#password=#?#bdate=#?#sex=#?#maiden_name=#?#home_town=#?#relation=#?#status='

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        dict_value = self.pars_fields(dic_fields)
        LOGGER.info('------- profile_editing -------')

        try:
            vk = self.login_vk_api(dict_value['login'], dict_value['password'])

            if 'new_title' in dict_value:
                new_title = dict_value['new_title']
                dict_value.pop('new_title')

            else:
                new_title = None

            if 'new_social' in dict_value:
                new_social = dict_value['new_social']
                dict_value.pop('new_social')
            else:
                new_social = None

            # LOGGER.info(dict_value)

            if not 'maiden_name' is dict_value:
                dict_value['maiden_name'] = None

            if dict_value['maiden_name'] == 'undefined' or dict_value['maiden_name'] is None:
                vk.account.saveProfileInfo(bdate=dict_value['bdate'], relation=int(dict_value['relation'])
                                           , status=dict_value['status'], home_town=dict_value['home_town']
                                           , sex=int(dict_value['sex']))
                dict_value.pop('maiden_name')

            else:
                # LOGGER.info(dict_value['maiden_name'])
                vk.account.saveProfileInfo(maiden_name=dict_value['maiden_name'], bdate=dict_value['bdate']
                                           , relation=int(dict_value['relation']), status=dict_value['status']
                                           , home_town=dict_value['home_town'], sex=int(dict_value['sex']))
            if sava_db:
                if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='main',
                                   new_social=new_social,
                                   new_title=new_title):
                    return True
            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    @retry(stop_max_attempt_number=count_try)
    def set_interests(self, dic_fields, sava_db=True):
        """
            Запонение "Интересы"

            dic_fields='login=#?#password=#?#
                pedit_interests_activities=#?#
                pedit_interests_interests=#?#
                ... '

            (в пременной list перечислены все поля dic_fields)

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        # LOGGER.info('G1')

        LOGGER.info('------- set_interests -------')
        try:
            dict_value = self.pars_fields(dic_fields)

            driver = self.link_personal_information(dict_value['login'], dict_value['password'])

            prof_dit = driver.find_element_by_id('ui_rmenu_interests')
            url = prof_dit.get_attribute('href')
            driver.get(url=url)

            # LOGGER.info('G2')
            list = ["pedit_interests_activities", "pedit_interests_interests", "pedit_interests_music",
                    "pedit_interests_movies", "pedit_interests_tv", "pedit_interests_books",
                    "pedit_interests_games", "pedit_interests_quotes", "pedit_interests_about"]

            for i in list:
                driver.find_element_by_id(i).clear()

                KASTIL = i.split('pedit_interests_')[1]
                if dict_value[KASTIL] is None:
                    dict_value[KASTIL] = ' '

                # LOGGER.info('G3-3')
                driver.find_element_by_id(i).send_keys(dict_value[KASTIL])

            self.save_personal_changes(driver)
            # cookies = driver.get_cookies()
            driver.close()

            if sava_db:
                if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='interests'):
                    return True
                return '"Ошибка базы данных"'
            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    @retry(stop_max_attempt_number=count_try)
    def set_kindred(self, dic_fields, sava_db=True):
        """
            Запонение "Родственики"

            dic_fields='login=#?#password=#?#
                grandparent=**[]**[]**[]#?#
                parent=**[]**[]#?#
                sibling=**[]**[]#?#
                child=**[]**[]#?#
                grandchild=**[]**[]**[]'

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        LOGGER.info('------- set_kindred -------')

        try:
            # LOGGER.info(dic_fields)
            dict_value = self.pars_fields(dic_fields)

            driver = self.link_personal_information(dict_value['login'], dict_value['password'])
            index = 0
            index_dropdown = 0

            # Удаление старых записей
            self.pedit_del(driver, "pedit_del_icon _del_icon")

            dict_value.pop('login')
            dict_value.pop('password')

            social = dict_value['social']
            title = dict_value['title']
            dict_value.pop('social')
            dict_value.pop('title')

            for i in dict_value.keys():
                index_child = 0

                # Модуль для дат рождения внуков и детей
                # if i == 'child' or i == 'grandchild':
                #     list_name = []
                #     for j in dic_value[i]:
                #         list_name.append(j['name'])
                # else:
                #     list_name = dic_value[i]

                # print(fields[i])

                for j in dict_value[i]:
                    if j != ' ' and j != '':
                        pole = driver.find_element_by_id("pedit_add_{0}_link".format(i))
                        pole.find_element_by_tag_name('a').click()
                        time.sleep(1)

                        self.insert_text(driver.find_element_by_id("{0}{1}_custom".format(i, index)), j)

                        g = driver.find_element_by_id("{0}{1}_custom".format(i, index))
                        print(i, index, g.get_attribute('value'))

                        index += 1
                        index_dropdown += 1

                    # Модуль для дат рождения внуков и детей
                    # try:
                    #     if i == 'child' or i == 'grandchild':
                    #
                    #
                    #         f = driver.find_elements_by_class_name('selector_dropdown')
                    #
                    #         list_birthday = {'day':f[-3] , 'month': f[-2], 'year': f[-1]}
                    #
                    #         for k in list_birthday.keys():
                    #             # print(index_dropdown)
                    #             print(i, k)
                    #
                    #             list_birthday[k].click()
                    #
                    #             cl = driver.find_elements_by_xpath("//li[@title='{0}']".format(fields[i][index_child][k]))
                    #
                    #             driver.execute_script("arguments[0].click();", cl[-1])
                    #             index_dropdown += 1
                    #
                    #             time.sleep(1)
                    #
                    #         index_child += 1
                    #
                    # except Exception as error:
                    #     index_dropdown -= 1
                    #     LOGGER.info('Нашел чел. в друзьях')
                    #     LOGGER.warning(error)

            # Сохранение
            self.save_personal_changes(driver)

            # cookies = driver.get_cookies()
            driver.close()

            arr_tag = ['grandparent', 'parent', 'sibling', 'child', 'grandchild']
            if dict_value == {}:
                for i in arr_tag:
                    dict_value[i] = " "
            else:
                for i in dict_value.keys():
                    s = ''
                    for j in dict_value[i]:
                        s = s + j + ';'
                    dict_value[i] = s[:-1]

            dict_value['social'] = social
            dict_value['title'] = title

            if sava_db:
                if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='relatives'):
                    return True
                return '"Ошибка базы данных"'
            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    @retry(stop_max_attempt_number=count_try)
    def set_school_education(self, dic_fields, sava_db=True):
        """
            Заполнение образования "Школа"

            dic_fields='login=#?#password=#?#
                schoole_country=#?#
                schoole_city=#?#
                schools_name#?#
                schools_speciality#?#
                schools_year_from=#?#
                schools_year_to=#?#
                schools_year_graduated=#?#
                schools_class='

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        LOGGER.info('------- set_school_education -------')

        # LOGGER.info(dic_fields)
        try:
            dict_value = self.pars_fields(dic_fields)

            driver = self.link_personal_information(dict_value['login'], dict_value['password'])

            prof_dit = driver.find_element_by_id("ui_rmenu_education")
            url = prof_dit.get_attribute('href')
            driver.get(url=url)

            index = 1
            index_dropdown = 0

            # Удаление старых записей
            try:
                self.pedit_del(driver, 'pedit_del_icon _del_icon')
            except:
                LOGGER.info('Нет записей для удаления')

            # LOGGER.info(dict_value['schools_name'])
            if dict_value['schools_name'] is None or dict_value['schools_name'] == ' ':
                dict_value['schools_name'] = 'Не выбрана'
                self.insert_text(driver.find_element_by_xpath("//td[@id='dropdown{0}']".format(5 + index_dropdown)),
                                 dict_value['schools_name'])

                self.save_personal_changes(driver)
                if sava_db:
                    if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='school'):
                        return True
                    return '"Ошибка базы данных"'
                return True

            else:
                if dict_value['schools_speciality'] is None:
                    dict_value['schools_speciality'] = " "
                if dict_value['schools_class'] is None or dict_value['schools_class'] == " ":
                    dict_value['schools_class'] = "Не выбран"

            # for i in list:

            i = dict_value

            if index_dropdown != 0:
                add = driver.find_elements_by_class_name('pedit_add_row')
                add[-1].click()

            if not self.select_data(driver, i['schoole_country'], 's_country', index):
                LOGGER.warning('Неправильное название страны: {0}'.format(i['schoole_country']))
                return 'Неправильное название страны: {0}'.format(i['schoole_country'])

            if not self.select_data(driver, i['schoole_city'], 's_city', index):
                LOGGER.warning('Неправильное название города:  {0}'.format(i['schoole_city']))
                return 'Неправильное название города:  {0}'.format(i['schoole_city'])

            if not self.insert_text(driver.find_element_by_xpath("//td[@id='dropdown{0}']".format(5 + index_dropdown)),
                                    i['schools_name']):
                LOGGER.warning('Неправильное название школы:  {0}'.format(i['schools_name']))
                return 'Неправильное название школы:  {0}'.format(i['schools_name'])

            self.select_data(driver, i['schools_year_from'], 's_start', index)

            self.select_data(driver, i['schools_year_to'], 's_finish', index)

            self.select_data(driver, i['schools_year_graduated'], 's_graduation', index)

            if not self.select_data(driver, i['schools_class'], 's_class', index):
                LOGGER.warning('Неправильное буква класса:  {0}'.format(i['schools_class']))
                return 'Неправильное буква класса:  {0}'.format(i['schools_class'])

            driver.find_element_by_xpath("//input[@id='s_spec-{0}_custom']".format(index)).send_keys(
                i['schools_speciality'])

            index_dropdown += 7
            index += 1

            # Сохранение
            self.save_personal_changes(driver)

            # cookies = driver.get_cookies()
            driver.close()

            if sava_db:
                if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='school'):
                    return True
                return '"Ошибка базы данных"'
            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    @retry(stop_max_attempt_number=count_try)
    def set_higher_education(self, dic_fields, sava_db=True):
        """
            Заполнение образования "ВУЗ"

            dic_fields='login=#?#password=#?#
                university_country=#?#
                university_city=#?#
                university_name#?#
                education_form#?#
                schools_year_from=#?#
                education_status=#?#
                university_graduation='

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        LOGGER.info('------- set_higher_education -------')
        try:
            dict_value = self.pars_fields(dic_fields)

            driver = self.link_personal_information(dict_value['login'], dict_value['password'])

            prof_dit = driver.find_element_by_id("ui_rmenu_education")
            url = prof_dit.get_attribute('href')
            driver.get(url=url)

            prof_dit = driver.find_elements_by_class_name("ui_tab")
            url = prof_dit[-1].get_attribute('href')
            driver.get(url=url)

            index = 0

            # Удаление старых записей
            try:
                self.pedit_del(driver, 'pedit_del_icon _del_icon')
            except:
                LOGGER.info('Нет записей')

            # time.sleep(2)
            # for i in list:
            i = dict_value

            if dict_value['university_name'] is None or dict_value['university_name'] == ' ':
                dict_value['university_name'] = 'Не выбран'
                self.insert_text(driver.find_elements_by_css_selector('[id^="{0}"]'.format('u_university'))[index],
                                 i['university_name'])
                self.save_personal_changes(driver)
                if sava_db:
                    if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='high_education'):
                        return True
                    return '"Ошибка базы данных"'
                return True

            # if index != 0:
            #     driver.find_element_by_class_name('pedit_add_row').click()

            if not self.select_data(driver, i['university_country'], 'u_country', index):
                LOGGER.warning('Неправильное название страны: {0}'.format(i['university_country']))
                return 'Неправильное название страны: {0}'.format(i['university_country'])

            if not self.select_data(driver, i['university_city'], 'u_city', index):
                LOGGER.warning('Неправильное название города:  {0}'.format(i['university_city']))
                return 'Неправильное название города:  {0}'.format(i['university_city'])

            if not self.insert_text(driver.find_elements_by_css_selector('[id^="{0}"]'.format('u_university'))[index],
                                    i['university_name']):
                LOGGER.warning('Неправильное название института:  {0}'.format(i['university_name']))
                return 'Неправильное название института:  {0}'.format(i['university_name'])

            self.select_data(driver, i['education_form'], 'u_edu_form', index)

            self.select_data(driver, i['education_status'], 'u_edu_status', index)

            self.select_data(driver, i['university_graduation'], 'u_graduation', index)

            index += 2

            # Сохранение
            self.save_personal_changes(driver)
            # cookies = driver.get_cookies()
            driver.close()

            if sava_db:
                if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='high_education'):
                    return True
                return '"Ошибка базы данных"'
            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    @retry(stop_max_attempt_number=count_try)
    def set_career(self, dic_fields, sava_db=True):
        """
            Заполнение "Карьера"

            dic_fields='login=#?#password=#?#
                career_company=#?#
                work_country=#?#
                work_city#?#
                career_from#?#
                career_until=#?#
                career_position='

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        LOGGER.info('------- set_career -------')
        try:
            dict_value = self.pars_fields(dic_fields)

            driver = self.link_personal_information(dict_value['login'], dict_value['password'])

            prof_dit = driver.find_element_by_id("ui_rmenu_career")
            url = prof_dit.get_attribute('href')
            driver.get(url=url)

            try:
                g = driver.find_element_by_id("group-1")
            except:
                self.pedit_del(driver, 'pedit_del_icon _del_icon')

            # LOGGER.info(dict_value['career_company'])
            if dict_value['career_company'] is None or dict_value['career_company'] == ' ':
                self.save_personal_changes(driver)

                if sava_db:
                    if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='career'):
                        return True
                    return '"Ошибка базы данных"'
                return True

            index = 1
            index_dropdown = 0

            # for i in list:
            i = dict_value

            if index != 1:
                driver.find_element_by_class_name('pedit_add_row').click()

            self.insert_text(driver.find_element_by_id("group-{0}".format(index)),
                             i['career_company'], tag='career_company')

            if not self.select_data(driver, i['work_country'], 'country', index):
                LOGGER.warning('Неправильное название страны: {0}'.format(i['work_country']))
                return 'Неправильное название страны: {0}'.format(i['work_country'])

            if not self.select_data(driver, i['work_city'], 'city', index):
                LOGGER.warning('Неправильное название города:  {0}'.format(i['work_city']))
                return 'Неправильное название города:  {0}'.format(i['work_city'])

            self.insert_text(driver.find_element_by_id('dropdown{0}'.format(1 + index_dropdown)),
                             i['career_from'], tag='career_from')

            self.insert_text(driver.find_element_by_id('dropdown{0}'.format(2 + index_dropdown)),
                             i['career_until'], tag='career_until')

            self.insert_text(driver.find_element_by_id('position-{0}_custom'.format(index)),
                             i['career_position'], tag='career_position')

            # index_dropdown += 6
            # index += 1

            # Сохранение
            self.save_personal_changes(driver)
            # cookies = driver.get_cookies()
            driver.close()

            if sava_db:
                if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='career'):
                    return True
                return '"Ошибка базы данных"'
            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    @retry(stop_max_attempt_number=count_try)
    def set_rmenu_military(self, dic_fields, sava_db=True):
        """
            Заполнение "Военная служба"

            dic_fields='login=#?#password=#?#
                military_unit=#?#
                military_country=#?#
                military_from#?#
                career_from#?#
                military_until='

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        LOGGER.info('------- set_rmenu_military -------')
        try:
            dict_value = self.pars_fields(dic_fields)

            driver = self.link_personal_information(dict_value['login'], dict_value['password'])

            prof_dit = driver.find_element_by_id("ui_rmenu_military")
            url = prof_dit.get_attribute('href')
            driver.get(url=url)

            index = 1
            index_dropdown = 0

            try:
                driver.find_element_by_id("unit-1")
                index = 1
            except:
                self.pedit_del(driver, 'pedit_del_icon _del_icon')

            # LOGGER.info(dict_value['military_unit'])
            if dict_value['military_unit'] is None or dict_value['military_unit'] == ' ':
                self.save_personal_changes(driver)

                if sava_db:
                    if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='military'):
                        return True
                    return '"Ошибка базы данных"'
                return True

            # for i in list:
            i = dict_value

            if index != 1:
                driver.find_element_by_class_name('pedit_add_row').click()

            if not self.select_data(driver, i['military_country'], 'country', index):
                LOGGER.warning('Неправильное название страны: {0}'.format(i['military_country']))
                return 'Неправильное название страны: {0}'.format(i['military_country'])

            # driver.find_element_by_xpath("//td[@id='dropdown{0}']".format(4 + index_dropdown)).click()
            # f = driver.find_elements_by_xpath("//li[@title='{0}']".format(i['military_country']))
            # f[-1].click()

            self.insert_text(driver.find_element_by_id("unit-{0}".format(index)), i['military_unit'],
                             tag='military_unit')

            self.insert_text(driver.find_element_by_id('dropdown{0}'.format(1 + index_dropdown)), i['military_from'],
                             tag='military_from')

            self.insert_text(driver.find_element_by_id('dropdown{0}'.format(2 + index_dropdown)), i['military_until'],
                             tag='military_until')

            # index_dropdown += 4
            # index += 1

            # Сохранение
            self.save_personal_changes(driver)
            driver.close()

            if sava_db:
                if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='military'):
                    return True
                return '"Ошибка базы данных"'
            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    @retry(stop_max_attempt_number=count_try)
    def set_social(self, dic_fields, sava_db=True):
        """
            Заполнение образования "Жизненная позиция"

            dic_fields='login=#?#password=#?#
                personal_political=#?#
                personal_religion=#?#
                personal_life_main#?#
                personal_people_main#?#
                personal_smoking=#?#
                personal_alcohol=#?#
                personal_inspired_by='

            После успешного выполнения возвращает True иначе ***(False, Ошибку)***
        """
        LOGGER.info('------- set_social -------')
        try:
            dict_value = self.pars_fields(dic_fields)

            driver = self.link_personal_information(dict_value['login'], dict_value['password'])

            prof_dit = driver.find_element_by_id("ui_rmenu_personal")
            url = prof_dit.get_attribute('href')
            driver.get(url=url)

            index = 0

            self.select_data(driver, dict_value['personal_political'], 'pedit_political_custom', index)

            if dict_value['personal_religion'] is None:
                dict_value['personal_religion'] = ' '

            self.insert_text(driver.find_element_by_id("pedit_religion_custom"), dict_value['personal_religion'])

            self.select_data(driver, dict_value['personal_life_main'], 'pedit_life_custom', index)

            self.select_data(driver, dict_value['personal_people_main'], 'pedit_people_custom', index)

            self.select_data(driver, dict_value['personal_smoking'], 'pedit_smoking_custom', index)

            self.select_data(driver, dict_value['personal_alcohol'], 'pedit_alcohol_custom', index)

            self.insert_text(driver.find_element_by_id("pedit_inspired_by"), dict_value['personal_inspired_by'])

            # Сохранение
            self.save_personal_changes(driver)

        except Exception as error:
            LOGGER.exception(error)
            return False

        # cookies = driver.get_cookies()
        driver.close()

        if sava_db:
            if self.save_to_db(dict_value, task_name='insert_bot_patterns', name_fields='lifestyle'):
                return True
            return '"Ошибка базы данных"'
        return True

    def apply_template_to_account(self, social, login, password, template_profil):
        try:
            task = {'task_name': 'select_bot_patterns',
                    'args': {'social': social, 'title': template_profil}}
            table = cass.call(task)
            # table = cass.call(task)[0]
            if table is False:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': "Ошибка: Данный шаблон удален из сервиса"}}
                cass.send_message(task)
                return "Ошибка: Данный шаблон удален из сервиса"
            else:
                table = table[0]

            try:
                field_table = dict(table['main'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.profile_editing(dic_fields, sava_db=False)
            except Exception as error:
                if str(error) == 'Captcha needed':
                    status_template = "Проблемы с авторизацией бота"
                else:
                    status_template = 'Ошибка записи: "Основное"'

                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': status_template}}
                cass.send_message(task)
                return status_template

            try:
                field_table = dict(table['relatives'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.set_kindred(dic_fields, sava_db=False)
            except Captcha as ex:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': ex}}
                cass.send_message(task)
                return ex
            except Exception as error:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': 'Ошибка записи: "Родственники"'}}
                cass.send_message(task)
                return 'Ошибка записи: "Родственники"'

            try:
                field_table = dict(table['interests'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.set_interests(dic_fields, sava_db=False)
            except Captcha as ex:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': ex}}
                cass.send_message(task)
                return ex
            except Exception as error:
                    LOGGER.warning(error)
                    if error == 'Ошибка: "CAPTCHA" при сохранении изменений':
                        return error

                    task = {'task_name': 'change_bot_template',
                            'args': {'social': social,
                                     'login': login,
                                     'template_profil': '',
                                     'status_template': 'Ошибка записи: "Интересы"'}}
                    cass.send_message(task)
                    return 'Ошибка записи: "Интересы"'

            try:
                field_table = dict(table['school'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.set_school_education(dic_fields, sava_db=False)
            except Captcha as ex:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': ex}}
                cass.send_message(task)
                return ex
            except Exception as error:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': 'Ошибка записи: "Школьное образование"'}}
                cass.send_message(task)
                return 'Ошибка записи: "Школьное образование"'

            try:
                field_table = dict(table['high_education'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.set_higher_education(dic_fields, sava_db=False)
            except Captcha as ex:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': ex}}
                cass.send_message(task)
                return ex
            except Exception as error:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': 'Ошибка записи: "Высшее образование"'}}
                cass.send_message(task)
                return 'Ошибка записи: "Высшее образование"'

            try:
                field_table = dict(table['career'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.set_career(dic_fields, sava_db=False)
            except Captcha as ex:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': ex}}
                cass.send_message(task)
                return ex
            except Exception as error:

                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': 'Ошибка записи: "Карьера"'}}
                cass.send_message(task)
                return 'Ошибка записи: "Карьера"'

            try:
                field_table = dict(table['military'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.set_rmenu_military(dic_fields, sava_db=False)
            except Captcha as ex:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': ex}}
                cass.send_message(task)
                return ex
            except Exception as error:

                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': 'Ошибка записи: "Военная служба"'}}
                cass.send_message(task)
                return 'Ошибка записи: "Военная служба"'

            try:
                field_table = dict(table['lifestyle'])
                dic_fields = self.encoding_dict_in_special_str(social, template_profil, login, password, field_table)
                self.set_social(dic_fields, sava_db=False)
            except Captcha as ex:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': ex}}
                cass.send_message(task)
                return ex
            except Exception as error:
                task = {'task_name': 'change_bot_template',
                        'args': {'social': social,
                                 'login': login,
                                 'template_profil': '',
                                 'status_template': 'Ошибка записи: "Жизненная позиция"'}}
                cass.send_message(task)
                return 'Ошибка записи: "Жизненная позиция"'

            LOGGER.info('======== ALL CHANGES SAVE TO VK ========')

            # Изменение в базк шаблона профиля
            task = {'task_name': 'change_bot_template',
                    'args': {'social': social,
                             'login': login,
                             'template_profil': template_profil,
                             'status_template': True}}
            cass.send_message(task)

            LOGGER.info('======== CHANGES template_profil IN DB ========')
            LOGGER.info('======== END apply_template_to_account ========')

            return True

        except Exception as error:
            LOGGER.exception(error)
            raise error

    def authentication_bot(self, login, password):
        """
            Авторизация и подтягивание данных профиля

            После успешного выполнения возвращает словарь dict_res с полем результа операции (True or False) и с полями
            из списка fields_user в случае успешного выполнения
        """

        # LOGGER.info('authentication_bot')

        try:
            vk = self.login_vk_api(login, password)

            if not vk is False:

                if vk == 'Captcha needed':
                    dict_res = {'result': ' "Captcha needed" необходимо авторизоваться вручную и пройти капчу'}
                    return dict_res

                # LOGGER.info('Все ок')
                # LOGGER.info(vk)
                fields_user = ['first_name', 'maiden_name', 'last_name', 'screen_name', 'can_access_closed', 'about',
                               'activities', 'bdate', 'books', 'can_post', 'can_see_all_posts', 'can_see_audio',
                               'can_send_friend_request', 'can_write_private_message', 'career', 'city', 'common_count',
                               'connections', 'counters', 'education', 'exports', 'followers_count', 'games',
                               'has_photo',
                               'home_town', 'interests', 'last_seen', 'military', 'movies', 'music', 'occupation',
                               'personal', 'quotes', 'relation', 'schools', 'sex', 'status', 'tv', 'universities']

                # info = vk.account.getProfileInfo()
                info = vk.users.get(fields=fields_user)[0]

                # LOGGER.info(info)
                info_res = {}

                for i in info.keys():
                    if type(info[i]) == dict:
                        for j in info[i]:
                            s = i + '_' + j
                            info_res[s] = info[i][j]

                    elif type(info[i]) == list and len(info[i]) != 0:
                        # LOGGER.info(info[i])
                        for j in info[i][0]:
                            s = i + '_' + j
                            info_res[s] = info[i][0][j]
                    else:
                        info_res[i] = info[i]

                if 'personal_alcohol' in info_res:
                    info_res['personal_alcohol'] = self.personal_alcohol_and_smoking[info_res['personal_alcohol']]

                if 'personal_smoking' in info_res:
                    info_res['personal_smoking'] = self.personal_alcohol_and_smoking[info_res['personal_smoking']]

                if 'personal_life_main' in info_res:
                    info_res['personal_life_main'] = self.personal_life_main[info_res['personal_life_main']]

                if 'personal_people_main' in info_res:
                    info_res['personal_people_main'] = self.personal_people_main[info_res['personal_people_main']]

                if 'personal_political' in info_res:
                    info_res['personal_political'] = self.personal_political[info_res['personal_political']]

                if 'education_form' in info_res:
                    info_res['education_form'] = self.education_form[info_res['education_form']]
                # info_res['universities_education_status'] = education_status[info_res['universities_education_status']]

                # LOGGER.info(info)

                dict_res = {'result': 'true', 'info': info_res}

                # LOGGER.info(dict_res)
                return dict_res

            else:
                dict_res = {'result': False}
                return dict_res

        except Exception as error:
            LOGGER.exception(error)
            raise error


def on_request(ch, method, props, body):
    task = pickle.loads(body)

    if task.get('task_name') in bots_method_list:
        task_method = getattr(bots, task.get('task_name'))
        task_args = task['args']
        try:
            result = task_method(**task_args)
        except Exception:
            LOGGER.exception(f"Error occured in {task.get('task_name')}")

            result = 'Ошибка сервиса'
    else:
        result = "Current method is not supported"

    ch.basic_ack(delivery_tag=method.delivery_tag)
    if props.reply_to is not None:
        try:
            ch.basic_publish(exchange='',
                             routing_key=props.reply_to,
                             body=pickle.dumps(result))

        except (StreamLostError, ConnectionWrongStateError) as ex:
            LOGGER.exception(ex)


if __name__ == "__main__":
    bots = Bots()
    bots_method_list = [
        func for func in dir(bots) if callable(getattr(bots, func)) and not func.startswith("__")
    ]
    queue_name = 'bots_tasks'

    # TODO: Process Pools
    process = Process(target=make_server,
                      args=("bots_edit_tasks", on_request,))

    process.start()
    LOGGER.info('SUCCESSFUL START BOTS SERVICE')
    process.join()

# login#=#89227064082#?#password#=#IS4082#?#social#=#vk#?#title#=#TEST#?#grandparent#=#1[]2[]3[]#?#parent#=#1[]2[]#?#sibling#=#4[]5[]#?#child#=##?#grandchild#=#
# login#=#89193003140#?#password#=#Qweasdzxc#?#social#=#vk#?#title#=#gg#?#child#=#1[]2[]3[]4[]5#?#grandchild#=#1[]2[]3[]4[]5[]6#?#grandparent#=#1[]2[]3[]6#?#parent#=#452[]3963#?#sibling#=#7777[]88888[]66666[]552525252[]34231423412[]32412341234123[] кцука[]смфкмфкум
