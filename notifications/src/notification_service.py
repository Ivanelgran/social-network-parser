from common_lib.queue_manager import make_server, RpcClient
from common_lib.cass_worker import CassandraTables, CassQueryGenerator
from threading import Thread
from collections import defaultdict
import pickle
import logging
import urllib.parse
from datetime import datetime

import requests

LOGGER = logging.getLogger('Notifications')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')

fh = logging.FileHandler('logs/logs.log')
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)


class Notifyer():

    def __init__(self):
        self.LOGGER = logging.getLogger('Notifications.Controller')
        self.notify_queue = "notification"
        self.notifications = defaultdict(list)
        self.method_list = [
            func for func in dir(self) if callable(getattr(self, func)) and not func.startswith("__")
        ]
        self.cass = RpcClient('db_tasks')

    def add_notification(self, ch, method, properties, body):
        self.LOGGER.info(f"new notification")
        info = pickle.loads(body)
        self.LOGGER.info(f"new notification {info}")

        ntf_info = self.prepare_notification(info)
        self.LOGGER.info(f"notification prepared")
        query = CassQueryGenerator(self.cass, CassandraTables.notifications,
                                   'insert')
        ntf = CassandraTables.notifications(username=info['author'],
                                            date=datetime.now(),
                                            info=ntf_info)
        query.add_record(ntf)
        query.run_task()
        self.LOGGER.info(f"notification saved")

        self.notifications[info['author']].append(ntf_info)
        self.LOGGER.info(f"notification complete")
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def on_request(self, ch, method, props, body):
        task = pickle.loads(body)

        if task.get('task_name') in self.method_list:
            task_method = getattr(self, task.get('task_name'))
            task_args = task['args']
            try:
                result = task_method(**task_args)
            except Exception:
                result = None
        else:
            result = "Current method is not supported"

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=pickle.dumps(result))
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def get_notification(self, username):
        if len(self.notifications[username]) == 0:
            return None
        else:
            result = self.notifications[username].pop(0)
            left_ntf = len(self.notifications[username])
            self.LOGGER.info(
                f"send notification to  {username}, left: {left_ntf}")
            return result

    def send_ntf_to_django(self, rule: str, data: dict):
        ntf_info = self.prepare_notification(data)
        ntf_info['rule'] = rule

        url = 'https://web-django:8000/send_push'
        r = requests.post(url=url, json=ntf_info, verify=False)
        self.LOGGER.info(r.content)

    def prepare_notification(self, notification):
        result = {'title': "",
                  'body': "",
                  'link': "/"}
        if notification['type'] == 'parsing':
            result['title'] = f'Группа {notification["owner_id"]} обновлена'
            result['body'] = f'Количество новых постов: ' \
                + str(notification["posts"])
        elif notification['type'] == 'suggestion_posted':
            result['title'] = 'Опубликован вброс'
            result['body'] = notification['title']
            result['link'] = "/suggestion?date=" \
                + urllib.parse.quote(notification['date'])
        elif notification['type'] == 'trace_answer':
            result['title'] = 'Вам ответили'
            result['body'] = notification['trace']
            result['link'] = "https://vk.com/wall" +\
                f"{notification['owner_id']}_{notification['post_id']}" + \
                f"?reply={notification['reply_id']}"
        elif notification['type'] == 'stp_notificator':
            result['title'] = 'Новое упоминание'
            result['body'] = notification['body']
            result['link'] = notification['link']

        return result


if __name__ == "__main__":
    notifyer = Notifyer()

    notifications_consumer = Thread(target=make_server,
                                    args=('notification',
                                          notifyer.add_notification,))
    notifications_tasks = Thread(target=make_server,
                                 args=('notifications_tasks',
                                       notifyer.on_request,))

    notifications_consumer.start()
    notifications_tasks.start()

    LOGGER.info('notifications ready')

    notifications_consumer.join()
    notifications_tasks.join()
