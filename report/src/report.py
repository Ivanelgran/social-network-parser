import pickle

from common_lib.queue_manager import make_server
from common_lib.src import config, log
from smm_reports import SmmDayReport, SmmMonthReport


class ReportManager():
    """Генератор отчетов"""

    resports = {'day': SmmDayReport,
                'month': SmmMonthReport}

    def generate_report(self, report_type, **kwargs):
        if report_type not in self.resports:
            return

        try:
            report = self.resports[report_type](**kwargs)
            info = report.build_report()
        except Exception:
            LOGGER.exception(
                f"Problem in generate report {report_type} {kwargs}")
            return

        return info


class ReportHandler():
    """Обработчик сообщений из RabbitMQ"""

    def __init__(self, seconds_limit):
        self.seconds_limit = seconds_limit
        self.report_manager = ReportManager()

    def on_request(self, ch, method, props, body):
        LOGGER.info("New Task")
        task = pickle.loads(body)
        LOGGER.info(f'Task args: {task}')

        report_type = task['report_type']
        report_args = task['args']
        report_args['seconds_limit'] = self.seconds_limit
        result = self.report_manager.generate_report(
            report_type, **report_args)

        if result is not None:
            LOGGER.info("Report is ready")
        else:
            result = {'error': "Current task is not supported"}
            LOGGER.warning(result)

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=pickle.dumps(result))
        ch.basic_ack(delivery_tag=method.delivery_tag)
        LOGGER.info("Task completed!")


if __name__ == "__main__":
    LOGGER = log.get_logger('report.main')
    CONFIG = config.get_config('./common_lib/config/report.yml')

    seconds_limit = CONFIG['main']['seconds_limit']
    LOGGER.info(f"Current seconds limit: {seconds_limit}")

    handler = ReportHandler(seconds_limit=seconds_limit)

    queue_name = 'report'
    make_server(queue_name, handler.on_request)
