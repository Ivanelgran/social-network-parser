from datetime import datetime, timedelta

import pandas as pd
import numpy as np

from common_lib.cass_worker import (
    CassandraTables, select_data, setup_connection
)


class BaseReport():

    def __init__(self, seconds_limit):
        self.seconds_limit = seconds_limit
        self.responses = []

        setup_connection()

    def drop_duplicates(self, responses):
        ids = set()
        result = []
        for response in responses:
            response_id = "{}*{}*{}*{}".format(
                response['social'],
                response['owner_id'],
                response['post_id'],
                response['reply_id'],
            )
            if response_id not in ids:
                ids.add(response_id)
                result.append(response.copy())

        return result

    def __get_responses(self):
        filters = {'date__gte': self.start_date,
                   'date__lt': self.end_date}
        if self.operator != "all":
            filters['operator'] = int(self.operator)

        responses = select_data(CassandraTables.smm_responses,
                                filters=filters)
        self.responses = self.drop_duplicates(responses)

    def __filter_responses(self):
        filtered = []
        for response in self.responses:
            if not response['has_incident']:
                continue

            parent_date = response['date'] - timedelta(
                seconds=response['response_time'])

            response_at_day = 10 <= response['date'].hour < 22
            parent_at_day = 10 <= parent_date.hour < 22
            same_date = response['date'].date() == parent_date.date()
            tonight = (response['date'].date() - parent_date.date()).days <= 1

            if response_at_day and parent_at_day and same_date:
                filtered.append(response)
            elif response_at_day and not parent_at_day and tonight:
                morning_response = 10 <= response['date'].hour <= 11
                if morning_response:
                    response['response_time'] = self.seconds_limit
                else:
                    start_datetime = datetime(
                        year=response['date'].year,
                        month=response['date'].month,
                        day=response['date'].day,
                        hour=11)
                    response['response_time'] = self.seconds_limit + \
                        (response['date'] - start_datetime).seconds
                filtered.append(response)

        self.responses = filtered

    def __get_reply(self, reply):
        fields = ['social', 'owner_id', 'post_id', 'reply_id']
        filters = {field: reply[field] for field in fields}
        reply = select_data(CassandraTables.replies, filters=filters)[0]

        return reply

    def __get_reply_link(self, reply):
        link = "https://vk.com/wall{}_{}?reply={}".format(
            reply['owner_id'], reply['post_id'], reply['reply_id']
        )

        if reply['parent_reply']:
            link += f"&thread={reply['parent_reply']}"

        return link

    def __get_reply_info(self, reply):
        fields = ['date', 'text', 'parent_reply']
        info = {field: reply[field] for field in fields}
        info['link'] = self.__get_reply_link(reply)

        return info

    def __get_replies(self):
        replies = [*map(self.__get_reply, self.responses)]
        infos = [*map(self.__get_reply_info, replies)]
        infos = sorted(infos, key=lambda x: x['date'])

        return infos

    def _get_responses_stat(self, times):
        """Строит статистику по времени ответов"""

        count = len(times)

        if not count:
            stat = {'sl': 0,
                    'mean_time': 0,
                    'median_time': 0,
                    'count': 0,
                    'in_time': 0}
            return stat

        response_in_time = 0
        for t in times:
            if t <= self.seconds_limit:
                response_in_time += 1

        sl = response_in_time / count
        mean = np.mean(times)
        median = np.median(times)

        stat = {'sl': sl,
                'mean_time': mean,
                'median_time': median,
                'count': count,
                'in_time': response_in_time}

        return stat

    def build_report(self):
        self.__get_responses()

        self.__filter_responses()

        stat = self.get_all_stat()
        stat['replies'] = self.__get_replies()

        return stat


class SmmDayReport(BaseReport):

    def __init__(self, operator: str, date: str, *args, **kwargs):
        self.operator = operator
        self.date = datetime.strptime(date, "%Y-%m-%d")

        self.start_date = datetime.date(self.date)
        self.end_date = self.start_date + timedelta(days=1)

        super().__init__(*args, **kwargs)

    def get_all_stat(self):
        times = list(map(lambda x: x['response_time'], self.responses))
        stat = self._get_responses_stat(times)

        stat['info'] = self.get_info()

        return stat

    def get_info(self):
        dates = list(map(lambda x: str(x['date']), self.responses))
        times = list(map(lambda x: x['response_time'] / 60, self.responses))

        start_date = str(datetime.date(self.date)) + " 10:00:00"
        end_date = str(datetime.date(self.date)) + " 22:00:00"
        dates.insert(0, start_date)
        dates.append(end_date)

        times.insert(0, 0)
        times.append(0)

        return {'dates': dates,
                'times': times,
                'date': self.date.strftime("%d.%m.%Y")}


class SmmMonthReport(BaseReport):

    def __init__(self, operator: str, year: int, month: int, *args, **kwargs):
        self.operator = operator
        self.year = year
        self.month = month

        self.start_date = datetime(year, month, 1)
        self.end_date = datetime(year, month+1, 1)

        super().__init__(*args, **kwargs)

    def get_all_stat(self):
        result = dict()

        all_count = 0
        all_in_time = 0
        all_times = []

        df = pd.DataFrame(self.responses)
        if df.shape[0]:
            df['day'] = [d.day for d in df['date']]
        elif 'day' not in df.columns:
            df.insert(0, 'day', 0)

        end_day = datetime(self.year, self.month+1, 1) - timedelta(days=1)
        end_day = end_day.day
        month_info = {'dates': [],
                      'counts': [],
                      'in_time': [],
                      'mean_times': [],
                      'median_times': [],
                      'date': f'{self.month}.{self.year}'}
        for day in range(1, end_day+1):
            cur_date = str(datetime(self.year, self.month, day).date())
            month_info['dates'].append(cur_date)
            day_responses = df[df.day == day]
            if not day_responses.shape[0]:
                info = {'sl': 0,
                        'mean_time': 0,
                        'median_time': 0,
                        'count': 0,
                        'in_time': 0}
                result[cur_date] = info

                month_info['counts'].append(0)
                month_info['in_time'].append(0)
                month_info['mean_times'].append(0)
                month_info['median_times'].append(0)
                continue

            times = list(day_responses.response_time.values)
            stat = self._get_responses_stat(times)
            result[cur_date] = stat

            all_times.extend(times)
            all_count += stat['count']
            all_in_time += stat['in_time']

            month_info['counts'].append(stat['count'])
            month_info['in_time'].append(stat['in_time'])
            month_info['mean_times'].append(stat['mean_time']/60)
            month_info['median_times'].append(stat['median_time']/60)

        result['info'] = month_info

        sl = all_in_time/all_count if all_count > 0 else 0
        if not len(all_times):
            all_times.append(0)
        result['all'] = {'count': all_count,
                         'in_time': all_in_time,
                         'sl': sl,
                         'mean_time': np.mean(all_times),
                         'median_time': np.median(all_times)}

        return result
