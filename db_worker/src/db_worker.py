# TODO: ORM and migration
# TODO: pandas -> api
import pandas as pd
import logging
import time
import json
from datetime import datetime, timedelta
from cassandra.cluster import Cluster, NoHostAvailable
from cassandra.query import dict_factory, SimpleStatement


class CassandraWorker():
    posts_monitoring_table = 'posts_monitoring'
    tracked_rules_table = 'tracked_rules'
    tg_ignore_periods_table = 'tg_ignore_periods'
    tg_ignore_posts_table = "tg_ignore_posts"
    tg_users_table = "tg_users"
    keywords_table = "keyword_posts"
    keyspace = 'sma_service'
    posts_table = "all_posts"
    new_posts_table = "newPosts2"
    top_post_table = 'top_posts'
    posts_stats_table = 'post_stats'
    att_table = 'post_attachments'
    posts_dates_table = "posts_dates"
    groups_table = 'groups'
    task_table = 'tasks'
    task_groups_table = 'task_groups'
    task_keywords_table = 'task_keywords'
    topics_table = 'topics'
    topic_in_post_table = 'topic_in_post'
    trace_templates_table = 'trace_templates'
    traces_table = "traces"
    traces_by_date_table = "traces_by_date"
    users_groups_table = 'user_groups'
    users_posts_table = 'user_posts'
    bots_table = "bots"
    users_table = 'users'
    replies_table = "replies"
    tracked_posts_table = 'tracked_posts'
    posts_rules_table = 'posts_rules'
    rules_table = 'rules'
    topic_posts_table = 'topic_posts'
    suggestions_table = "suggestions"
    suggestion_groups_table = "suggestion_groups"
    persons = "persons"
    ignore_groups_table = 'ignore_groups'
    posts_vectors_table = "posts_vectors"
    topic_vectors_table = "topic_vectors"
    vector_topics_prob_table = "vector_topics_prob"
    notifications_table = "notifications"
    bots_patterns = "bots_patterns"

    cassandra_address = "cassandra"

    def __init__(self):
        self.LOGGER = logging.getLogger('DB_worker.db')

        while True:
            conn = self.test_connection()
            if conn:
                self.LOGGER.info("Cassandra is ready")
                break
            time.sleep(1)

    def test_connection(self):
        try:
            with Cluster(contact_points=[self.cassandra_address]) as cluster:
                with cluster.connect() as session:
                    query = "SELECT now() FROM system.local"
                    session.execute(query)
            return True
        except NoHostAvailable:
            return False

    def select_data(self, table, select_cols=None, conditions=None,
                    order_col=None, order_asc=False, limit=None,
                    print_query=False, simple=False):
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                cols = ','.join(
                    select_cols) if select_cols is not None else '*'
                query = f"""SELECT {cols}
                            FROM {table}"""
                if conditions is not None:
                    query += f"\nWHERE {conditions[0]}"
                    for cond in conditions[1:]:
                        query += f" AND {cond}"
                if order_col is not None:
                    ascending = "ASC" if order_asc else "DESC"
                    query += f"\nORDER BY {order_col} {ascending}"
                if limit is not None:
                    query += f"\nLIMIT {limit}"
                query += "\nALLOW FILTERING"

                if print_query:
                    self.LOGGER.info(query)

                session.row_factory = dict_factory
                session.default_timeout = 60.0

                if simple:
                    rows = []
                    statement = SimpleStatement(query, fetch_size=10)
                    for user_row in session.execute(statement):
                        rows.append(user_row)
                else:
                    rows = list(session.execute(query))

        return rows

    def select_count_data(self, table, conditions=None,
                          order_col=None, order_asc=False,
                          print_query=False):
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""SELECT count(*)
                            FROM {table}"""
                if conditions is not None:
                    query += f"\nWHERE {conditions[0]}"
                    for cond in conditions[1:]:
                        query += f" AND {cond}"
                if order_col is not None:
                    ascending = "ASC" if order_asc else "DESC"
                    query += f"\nORDER BY {order_col} {ascending}"
                query += "\nALLOW FILTERING"

                if print_query:
                    self.LOGGER.info(query)

                session.row_factory = dict_factory
                session.default_timeout = 60.0

                count = list(session.execute(query))
        return count[0]['count']

    def select_by_post_data(self, table, df=None, conditions=None, limit=None,
                            print_query=False, date_count=None,
                            date_column='ts', reply=False, post_ids=None):
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                futures = []
                posts = []
                session.row_factory = dict_factory
                session.default_timeout = 60.0

                if post_ids is None:
                    for own_id in set(df.owner_id.values):
                        cur_posts = list(df[df.owner_id == own_id].post_id.values)
                        socials = list(df[df.owner_id == own_id].social.values)
                        if reply:
                            replies = list(df[df.owner_id == own_id].reply_id.values)
                        for post in range(len(cur_posts)):
                            if reply:
                                query = f"""SELECT * FROM {table}
                                            WHERE social = '{socials[post]}'
                                            AND owner_id = '{own_id}'
                                            AND post_id = '{cur_posts[post]}'
                                            AND reply_id = '{replies[post]}'"""

                            else:
                                query = f"""SELECT * FROM {table}
                                        WHERE social = '{socials[post]}'
                                        AND owner_id = '{own_id}'
                                        AND post_id = '{cur_posts[post]}'"""
                            if conditions is not None:
                                for cond in conditions:
                                    query += f" AND {cond}"
                            if limit is not None:
                                query += f"\nLIMIT {limit}"
                            query += "\nALLOW FILTERING"
                            if print_query:
                                print_query = False
                                self.LOGGER.info(query)
                            futures.append(session.execute_async(query))
                    for future in futures:
                        rows = list(future.result())
                        if date_count is not None:
                            rows = pd.DataFrame(rows).sort_values(
                                date_column, ascending=False) \
                                       .iloc[:date_count].to_dict('records')
                        posts.extend(rows)
                    if print_query:
                        self.LOGGER.info(query)
                else:
                    query = f"""SELECT * FROM {table}
                                WHERE social = ?
                                AND owner_id = ?
                                AND post_id = ?"""
                    get = session.prepare(query)
                    for cur_values in post_ids:
                        futures.append(session.execute_async(get, cur_values))
                    for future in futures:
                        rows = list(future.result())
                        posts.extend(rows)

        return posts

    def insert_data(self, table, insert_cols, insert_values, use_ttl=0):
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                cols = ', '.join(insert_cols)
                values = ', '.join(['?'] * len(insert_cols))
                query = f"""INSERT INTO {table}({cols})
                            VALUES ({values})"""
                if use_ttl > 0:
                    query += f" USING TTL {use_ttl}"

                insert = session.prepare(query)
                for cur_values in insert_values:
                    session.execute(insert, cur_values)

    def delete_data(self, table, pr_cols, pr_values):
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""DELETE FROM {table}
                            WHERE """
                cols = [col + " = ?" for col in pr_cols]
                cols = ' AND '.join(cols)
                query += cols
                delete = session.prepare(query)
                for cur_values in pr_values:
                    session.execute(delete, cur_values)

    # to get_records
    def get_last_post(self, social, owner_id):
        data = self.select_data(table=self.posts_table,
                                select_cols=['post_id'],
                                conditions=[
                                    f"social = '{social}'",
                                    f"owner_id = '{owner_id}'"],
                                order_col='post_id',
                                limit=1)

        if len(data) == 0:
            return

        return data[0]['post_id']

    # to get_records
    def get_post(self, social, owner_id, post_id):
        data = self.select_data(table=self.posts_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'"])

        if len(data) == 0:
            self.LOGGER.error(f"POST {owner_id} {post_id} NOT FOUND")
            return

        return data[0]

    # to get_records
    def get_notifications(self, username):
        data = self.select_data(table=self.notifications_table,
                                conditions=[f"username = '{username}'"])

        if len(data) == 0:
            self.LOGGER.warning(f"NOTIFICATIONS FOR {username} NOT FOUND")
            return []

        return data

    # to get_records
    def get_notification(self, username, date):
        data = self.select_data(table=self.notifications_table,
                                conditions=[f"username = '{username}'",
                                            f"date = '{date}'"])

        if len(data) == 0:
            self.LOGGER.warning(f"NOTIFICATIONS FOR {username} NOT FOUND")
            return []

        return data[0]

    # to get_records
    def get_users(self):
        data = self.select_data(table=self.users_table)

        return data

    # to get_records
    def get_top_posts(self, level, topic):
        data = self.select_data(table=self.top_post_table,
                                conditions=[f'level = {level}',
                                            f'topic = {topic}'])

        if len(data) == 0:
            self.LOGGER.error(f"TOPIC {level} {topic} NOT FOUND")
            return

        return data

    # to get_records
    def get_post_replies(self, social, owner_id, post_id):
        data = self.select_data(table=self.replies_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'"])

        return data

    # to get_records
    def get_reply(self, social, owner_id, post_id, reply_id):
        data = self.select_data(table=self.replies_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'",
                                            f"reply_id = '{reply_id}'"])

        return data[0] if len(data) > 0 else {}

    def get_post_topics(self, social, owner_id, post_id):
        data = self.select_data(table=self.topic_in_post_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'"])

        if len(data) == 0:
            return
        posts = []
        for row in data:
            title = self.get_topic_label(level=1, topic=row['topic'])
            info = row.copy()
            info['title'] = title
            posts.append(info)

        return posts

    def get_posts_topics(self, posts):
        data = self.select_by_post_data(table=self.topic_in_post_table,
                                        df=pd.DataFrame(posts))
        if len(data) == 0:
            return []
        data = pd.DataFrame(data)
        data = data.sort_values('probability', ascending=False) \
            .drop_duplicates(['social', 'owner_id', 'post_id']) \
            .to_dict('records')

        return data

    # to get_records
    def get_vector_post_topics(self, social, owner_id, post_id):
        topic_info = self.get_all_vector_topics()
        topics = []
        for topic in topic_info:
            topics.append(topic['words'])

        data = self.select_data(table=self.vector_topics_prob_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'"])

        data = pd.DataFrame(data)
        data = data[data.topic.isin(topics)]
        data = data.to_dict('records')

        if len(data) == 0:
            return

        return data

    def get_vector_posts_topics(self, posts):
        topic_info = self.get_all_vector_topics()
        topics = []
        for topic in topic_info:
            topics.append(topic['words'])

        data = self.select_by_post_data(table=self.vector_topics_prob_table,
                                        df=pd.DataFrame(posts))
        if len(data) == 0:
            return []
        data = pd.DataFrame(data)
        data = data[data.topic.isin(topics)]
        data = data.groupby(['social', 'owner_id', 'post_id'])['topic'].apply(lambda x: ';\n'.join(x)).reset_index()
        # data = data.sort_values('probability', ascending=False) \
        #     .drop_duplicates(['social', 'owner_id', 'post_id']) \
        #     .to_dict('records')

        return data.to_dict('records')

    # to get_records
    def get_topic_label(self, level, topic):
        data = self.select_data(table=self.topics_table,
                                select_cols=['title'],
                                conditions=[f'level = {level}',
                                            f'topic = {topic}'])

        if len(data) == 0:
            self.LOGGER.error(f"TOPIC {level} {topic} NOT FOUND")
            return

        return data[0]['title']

    # to get_records
    def get_all_topics(self, level=1):
        data = self.select_data(table=self.topics_table,
                                conditions=[f'level = {level}'])

        return data

    def get_last_posts(self, date=None, cond=None):

        start_time = time.time()

        posts_ids = self.select_data(table=self.posts_dates_table,
                                     conditions=date,
                                     simple=True)

        self.LOGGER.info(f"Posts for date get in {time.time() - start_time}")
        if len(posts_ids) == 0:
            self.LOGGER.warning("LAST POSTS NOT FOUND")
            return
        posts_ids = pd.DataFrame(posts_ids)

        cur_t = time.time()
        data = self.select_by_post_data(table=self.posts_table,
                                        df=posts_ids[
                                            ['social', 'owner_id', 'post_id']],
                                        conditions=cond)
        self.LOGGER.info(f"Posts data get in {time.time() - cur_t}")
        self.LOGGER.info(f"get_last_posts in {time.time() - start_time}")

        if len(data) == 0:
            self.LOGGER.warning("LAST POSTS NOT FOUND")
            return

        return data

    def get_posts_by_rule(self, tag, date, limit):
        post_data = list()
        topic_post_data = list()
        replies_data = list()
        start_time = time.time()
        count = self.select_count_data(table=self.posts_rules_table,
                                       conditions=[f"rule = '{tag}'", date[0]])
        posts_ids = self.select_data(table=self.posts_rules_table,
                                     conditions=[f"rule = '{tag}'", date[0]],
                                     limit=limit)
        ign_groups = self.select_data(table=self.ignore_groups_table,
                                      select_cols=['group_id'])
        # Игнорирование групп
        for group in ign_groups:
            posts_ids[:] = [d for d in posts_ids if d.get('owner_id') != group['group_id']]

        self.LOGGER.info(f"Posts for rule get in {time.time() - start_time}")
        if len(posts_ids) == 0:
            self.LOGGER.warning(f"POSTS FOR RULE {tag} NOT FOUND")
            return [], [], [], 0
        for post in posts_ids:
            if str(post['type']) == 'post' or str(post['type']) == 'repost':
                post_data.append(post)
            elif str(post['type']) == 'topic_post':
                topic_post_data.append(post)
            elif str(post['type']) == 'comment' or str(post['type']) == 'reply':
                replies_data.append(post)
            else:
                self.LOGGER.warning('Не могу обработать пост:')
                self.LOGGER.warning(post)

        posts_ids = pd.DataFrame(post_data)
        topic_posts_ids = pd.DataFrame(topic_post_data)
        replies_ids = pd.DataFrame(replies_data)

        cur_t = time.time()
        try:
            post_data = self.select_by_post_data(table=self.posts_table,
                                                 df=posts_ids[['social', 'owner_id', 'post_id']]
                                                 )
        except Exception:
            self.LOGGER.warning("POSTS NOT FOUND")
        self.LOGGER.info(post_data)

        try:
            topic_post_data = self.select_by_post_data(table=self.topic_posts_table,
                                                       df=topic_posts_ids[['social', 'owner_id', 'post_id']])
        except Exception:
            self.LOGGER.warning("TOPIC POSTS NOT FOUND")

        try:
            replies_data = self.select_by_post_data(table=self.replies_table,
                                                    df=replies_ids[['social', 'owner_id', 'post_id', 'reply_id']],
                                                    reply=True)
        except Exception:
            self.LOGGER.warning("REPLIES BY RULE NOT FOUND")

        self.LOGGER.info(f"Posts data get in {time.time() - cur_t}")
        self.LOGGER.info(f"get_posts_by_rule in {time.time() - start_time}")

        return post_data, topic_post_data, replies_data, count

    # to get_records
    def get_trace_template(self, level, topic):
        data = self.select_data(table=self.trace_templates_table,
                                conditions=[
                                    f'level = {level}', f'topic = {topic}'],
                                limit=1)

        if len(data) == 0:
            self.LOGGER.info(f"NO TRACE TEMPLATE FOR {level} {topic}")
            return

        return data

    # to get_records
    def get_traces(self):
        data = self.select_data(table=self.traces_table)

        return data

    # to get_records
    def get_traces_by_date(self, date, end_date=None):
        conditions = [f"date >= '{date}'"]
        if end_date is not None:
            conditions.append(f"date <= '{end_date}'")
        data = self.select_data(table=self.traces_by_date_table,
                                conditions=conditions)

        return data

    # to get_records
    def get_trace(self, social, owner_id, post_id, date):
        data = self.select_data(table=self.traces_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'",
                                            f"date = '{date}'"])

        return data[0]

    def get_tracked_topics(self):
        data = self.select_data(table=self.topics_table,
                                select_cols=['topic'],
                                conditions=['state = 1'])

        topics = []
        for row in data:
            topics.append(row['topic'])

        return topics

    # to get_records
    def get_topic(self, level, topic):
        data = self.select_data(table=self.topics_table,
                                conditions=[f'level = {level}',
                                            f'topic = {topic}'])

        if len(data) == 0:
            self.LOGGER.error(f"TOPIC {level} {topic} NOT FOUND")
            return

        return data[0]

    # to get_records
    # def get_group(self, social, group_id):
    #     data = self.select_data(table=self.groups_table,
    #                             conditions=[f"social = '{social}'",
    #                                         f"group_id = '{group_id}'"])

    #     if len(data) == 0:
    #         self.LOGGER.warning(f"GROUP {group_id} NOT FOUND")
    #         return

    #     return data[0]

    # to get_records
    def get_groups_name(self, groups_id):
        data = []
        for group in groups_id:
            result = self.select_data(table=self.groups_table,
                                      select_cols=['name'],
                                      conditions=[f"group_id = '{group}'"])
            data.append(result)
        if len(data) == 0:
            self.LOGGER.warning(f"GROUP {group} NOT FOUND")
            return

        return data

    # to get_records
    def get_tasks(self):
        tasks = self.select_data(table=self.task_table)

        return tasks

    # to get_records
    def get_active_tasks(self):
        tasks = self.select_data(table=self.task_table,
                                 select_cols=['task_id', 'is_keyword'],
                                 conditions=['is_active = True'])

        return tasks

    # to get_records
    def get_active_rules(self):
        tasks = self.select_data(table=self.rules_table,
                                 select_cols=['tag', 'value', 'social'],
                                 conditions=['is_active = True'])

        return tasks

    def get_rules_values(self, tag):
        tasks = self.select_data(table=self.rules_table,
                                 select_cols=['value'],
                                 conditions=[f"tag = '{tag}'"])

        return tasks

    def get_all_groups(self):
        data = self.select_data(table=self.groups_table)

        return data

    # to get_records
    def get_task(self, task_id):
        task = self.select_data(table=self.task_table,
                                conditions=[f'task_id = {task_id}'])

        if len(task) == 0:
            self.LOGGER.warning(f'TASK {task_id} IS NOT FOUND')
            return

        return task[0]

    def get_parsed_groups(self):
        active_tasks = self.get_active_tasks()
        active_tasks_array = list()
        for task in active_tasks:
            if not task['is_keyword']:
                active_tasks_array.append(str(task['task_id']))
        active_tasks = ','.join(active_tasks_array)
        data = self.select_data(table=self.task_groups_table,
                                select_cols=['social', 'group_id'],
                                conditions=[f'task_id IN ({active_tasks})'])

        return data

    def get_parsed_keywords(self):
        active_tasks_array = list()
        active_tasks = self.get_active_tasks()
        for task in active_tasks:
            if task['is_keyword']:
                active_tasks_array.append(str(task['task_id']))
        active_tasks = ','.join(active_tasks_array)
        data = self.select_data(table=self.task_keywords_table,
                                select_cols=['social', 'keyword'],
                                conditions=[f'task_id IN ({active_tasks})'])

        return data

    # to get_records
    def get_groups(self, limits):
        data = self.select_data(table=self.groups_table,
                                conditions=limits)

        return data

    # to get_records
    def get_all_posts(self):
        data = self.select_data(table=self.posts_table,
                                select_cols=['social', 'owner_id',
                                             'post_id', 'text'])

        return pd.DataFrame(data)

    def get_post_to_predict(self):
        posts_date = (
                datetime.now() - timedelta(days=1)
        ).strftime("%Y-%m-%d %H:%M:%S")
        data = self.select_data(table=self.posts_table,
                                select_cols=['social', 'owner_id', 'post_id',
                                             'text', 'likes', 'replies',
                                             'views', 'shares'],
                                conditions=[f"date >= '{posts_date}'"])

        return data

    # to get_records
    def get_task_groups(self, task_id):
        data = self.select_data(table=self.task_groups_table,
                                select_cols=['social', 'group_id'],
                                conditions=[f'task_id = {task_id}'])

        return data

    # to get_records
    def get_suggestions(self):
        data = self.select_data(table=self.suggestions_table)

        return data

    # to get_records
    def get_some_suggestions(self, dates):
        data = self.select_data(table=self.suggestions_table,
                                conditions=[f"date in ({dates})"])

        return data

    # to get_records
    def get_suggestion(self, date):
        data = self.select_data(table=self.suggestions_table,
                                conditions=[f"date = '{date[:-3]}'"])

        return data

    # to get_records
    def get_suggestions_by_date(self, date, end_date=None):
        conditions = [f"date >= '{date}'"]
        if end_date is not None:
            conditions.append(f"date <= '{end_date}'")
        data = self.select_data(table=self.suggestions_table,
                                conditions=conditions)

        return data

    # to get_records
    def get_suggestion_groups(self, date=None):
        if date is not None:
            condition = [f"date = '{str(date)[:-3]}'"]
        else:
            condition = None
        data = self.select_data(table=self.suggestion_groups_table,
                                conditions=condition)

        return data

    # to get_records
    def get_task_keywords(self, task_id):
        data = self.select_data(table=self.task_keywords_table,
                                select_cols=['social', 'keyword'],
                                conditions=[f'task_id = {task_id}'])

        return data

    # to get_records
    def get_keyword_groups(self, keyword):
        data = self.select_data(table=self.keywords_table,
                                select_cols=['social', 'owner_id'],
                                conditions=[f"hashtag = '{keyword}'"])

        return data

    # to get_records
    def get_groups_info(self, social, groups):
        if isinstance(social, str):
            social_filter = f"social = '{social}'"
        else:
            socials = ','.join([f"'{soc}'" for soc in social])
            social_filter = f"social IN ({socials})"
        data = self.select_data(table=self.groups_table,
                                conditions=[social_filter,
                                            f'group_id IN ({groups})'])

        return data

    # to get_records
    def get_tracked_posts(self, username):
        data = self.select_data(table=self.tracked_posts_table,
                                conditions=[f"username = '{username}'"])

        return data

    def get_tracked_from_date(self, social, owner_id, post_id):
        data = self.select_data(
            table=self.tracked_posts_table,
            select_cols=["writetime(end_date) as from_date"],
            conditions=[f"social = '{social}'",
                        f"owner_id = '{owner_id}'",
                        f"post_id = '{post_id}'"])

        result = data[0]['from_date']
        self.LOGGER.info(f"get_tracked_from_date {result}")

        return result

    def get_posts_monitoring(self, username):
        data = self.select_data(table=self.posts_monitoring_table,
                                conditions=[f"username = '{username}'"])

        return data

    def get_user_tracked_rules(self, username):
        data = self.select_data(table=self.tracked_rules_table,
                                conditions=[f"user = '{username}'"])

        return data

    def get_tracked_rules(self):
        data = self.select_data(table=self.tracked_rules_table)

        return data

    def get_user_groups(self, username):
        data = self.select_data(table=self.users_groups_table,
                                conditions=[f"username = '{username}'"])

        return data

    # to get_records
    def get_viewed_posts(self, username):
        data = self.select_data(table=self.users_posts_table,
                                conditions=[f"username = '{username}'"])

        return data

    # to get_records
    def get_bots(self, social=None):
        condition = None
        if social is not None:
            condition = [f"social = '{social}'"]
        bots = self.select_data(table=self.bots_table,
                                conditions=condition)

        return bots

    # to get_records
    def get_bot(self, social, login):
        bot = self.select_data(table=self.bots_table,
                               conditions=[f"social = '{social}'",
                                           f"login = '{login}'"])

        return bot[0]

    # to get_records
    def get_post_attachments(self, social, owner_id, post_id):
        att = self.select_data(table=self.att_table,
                               conditions=[f"social = '{social}'",
                                           f"owner_id = '{owner_id}'",
                                           f"post_id = '{post_id}'"])

        return att

    # to get_records
    def get_rules(self):
        rules = self.select_data(table=self.rules_table,
                                 conditions=[f"is_active = True"])

        return rules

    def get_tg_ignore_posts(self):
        data = self.select_data(table=self.tg_ignore_posts_table)

        return data

    def get_tg_ignore_periods(self):
        data = self.select_data(table=self.tg_ignore_periods_table)

        return data

    def get_tg_users(self):
        data = self.select_data(table=self.tg_users_table)

        return data

    def delete_tg_users(self, user_id):
        delete_values = [[str(user_id)]]

        self.delete_data(table=self.tg_users_table,
                         pr_cols=['user_id'],
                         pr_values=delete_values)

    def insert_tg_ignore_table(self, user_id, post_id):
        try:
            self.insert_data(table=self.tg_ignore_posts_table,
                             insert_cols=['user_id', 'post_id'],
                             insert_values=[[str(user_id), str(post_id)]])
            return True
        except:
            return False

    def insert_tg_ignore_periods_table(self, user_id, period_id):
        try:
            self.insert_data(table=self.tg_ignore_periods_table,
                             insert_cols=['user_id', 'period_id'],
                             insert_values=[[str(user_id), str(period_id)]])
            return True
        except:
            return False

    def insert_tg_users(self, user_id, name, username):
        self.insert_data(table=self.tg_users_table,
                         insert_cols=['user_id', 'name', 'username'],
                         insert_values=[[str(user_id), str(name), str(username)]])

    def get_posts_attachments(self, posts):

        data = self.select_by_post_data(table=self.att_table,
                                        df=pd.DataFrame(posts))

        return data

    def get_posts_vectors(self, date):

        # data = self.select_by_post_data(table=self.posts_vectors_table,
        #                                 df=pd.DataFrame(posts))
        start_time = time.time()
        posts_ids = self.select_data(table=self.posts_dates_table,
                                     conditions=date,
                                     simple=True)
        self.LOGGER.info(f"Posts for date get in {time.time() - start_time}")
        if len(posts_ids) == 0:
            self.LOGGER.warning("LAST POSTS NOT FOUND")
            return
        posts_ids = pd.DataFrame(posts_ids)

        posts_ids = posts_ids.drop_duplicates(subset=['social', 'owner_id', 'post_id'])
        posts_ids = posts_ids.to_dict("records")

        cur_t = time.time()
        get_post_ids = []
        for post in posts_ids:
            get_post_ids.append([post['social'],
                                 post['owner_id'],
                                 post['post_id']])

        data = self.select_by_post_data(table=self.posts_vectors_table,
                                        post_ids=get_post_ids)
        self.LOGGER.info(f"Posts data get in {time.time() - cur_t}")
        self.LOGGER.info(f"get_last_posts in {time.time() - start_time}")

        return data

    def get_posts_vectors_by_ids(self, posts_ids):
        get_post_ids = []
        for post in posts_ids:
            get_post_ids.append([post['social'],
                                 post['owner_id'],
                                 post['post_id']])

        data = self.select_by_post_data(table=self.posts_vectors_table,
                                        post_ids=get_post_ids)

        return data

    def get_posts_vectors_for_count(self):

        start_time = time.time()
        posts_ids = self.select_data(table=self.posts_dates_table,
                                     limit=2000,
                                     simple=True)
        self.LOGGER.info(f"Posts for count get in {time.time() - start_time}")
        if len(posts_ids) == 0:
            self.LOGGER.warning("LAST POSTS NOT FOUND")
            return
        # posts_ids = pd.DataFrame(posts_ids)

        cur_t = time.time()
        get_post_ids = []
        for post in posts_ids:
            get_post_ids.append([post['social'],
                                 post['owner_id'],
                                 post['post_id']])

        data = self.select_by_post_data(table=self.posts_vectors_table,
                                        post_ids=get_post_ids)
        self.LOGGER.info(f"Posts data get in {time.time() - cur_t}")
        self.LOGGER.info(f"get_last_posts in {time.time() - start_time}")

        return data

    # to get_records
    def get_post_vector(self, social, owner_id, post_id):

        data = self.select_data(table=self.posts_vectors_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'"])

        return data[0]

    # to get_records
    def get_all_vector_topics(self):
        data = self.select_data(table=self.topic_vectors_table)

        return data

    def get_post_stat(self, social, owner_id, post_id, count=2):
        stat = self.select_data(table=self.posts_stats_table,
                                conditions=[f"social = '{social}'",
                                            f"owner_id = '{owner_id}'",
                                            f"post_id = '{post_id}'"])

        if not len(stat):
            return []

        if count is None:
            return stat or []

        stat = pd.DataFrame(stat).sort_values('ts', ascending=False) \
            .to_dict('records')

        return stat[:count] or []

    def get_posts_stat(self, posts, count=2):

        data = self.select_by_post_data(table=self.posts_stats_table,
                                        df=pd.DataFrame(posts))

        if not len(data):
            self.LOGGER.info(posts)
            return []

        data = pd.DataFrame(data).sort_values('ts', ascending=False).groupby(
            ['social', 'owner_id', 'post_id']).head(count).to_dict('records')

        return data

    def get_posts(self, posts):

        data = self.select_by_post_data(table=self.posts_table,
                                        df=pd.DataFrame(posts)
                                        )

        return data

    def drop_bots(self, socials, logins):
        delete_values = []
        for i in range(len(logins)):
            delete_values.append([socials[i], logins[i]])

        self.delete_data(table=self.bots_table,
                         pr_cols=['social', 'login'],
                         pr_values=delete_values)

    def drop_old_posts(self, posts):
        delete_values = []
        for post in posts:
            delete_values.append([post['owner_id'],
                                  post['post_id']])

        self.delete_data(table=self.new_posts_table,
                         pr_cols=['owner_id', 'post_id'],
                         pr_values=delete_values)

    def drop_task_and_groups(self, task_id):
        delete_values = [[task_id]]

        self.delete_data(table=self.task_table,
                         pr_cols=['task_id'],
                         pr_values=delete_values)

        self.delete_data(table=self.task_groups_table,
                         pr_cols=['task_id'],
                         pr_values=delete_values)

        self.delete_data(table=self.task_keywords_table,
                         pr_cols=['task_id'],
                         pr_values=delete_values)

    def drop_rule(self, tag, social):
        delete_values = [[tag, social]]
        self.delete_data(table=self.rules_table,
                         pr_cols=['tag', 'social'],
                         pr_values=delete_values)

    def drop_posts_rules(self, tag):
        delete_values = [[tag]]
        self.delete_data(table=self.posts_rules_table,
                         pr_cols=['rule'],
                         pr_values=delete_values)

    def drop_topic(self, words):
        delete_values = [[words]]
        self.LOGGER.info(delete_values)
        self.delete_data(table=self.topic_vectors_table,
                         pr_cols=['words'],
                         pr_values=delete_values)
        self.delete_data(table=self.vector_topics_prob_table,
                         pr_cols=['topic'],
                         pr_values=delete_values)

    def insert_task(self, task):
        max_task = self.select_data(table=self.task_table,
                                    select_cols=['max(task_id) AS task_id'])

        if max_task[0]['task_id'] is None:
            task_id = 1
        else:
            task_id = max_task[0]['task_id'] + 1
        insert_values = [
            [task_id, task['date'],
             task['groups_count'], task['label'], True, task['author'],
             task['social'], task['is_keyword']]]

        self.insert_data(table=self.task_table,
                         insert_cols=['task_id', 'date', 'groups_count',
                                      'label', 'is_active', 'author', 'social',
                                      'is_keyword'],
                         insert_values=insert_values)

        return task_id

    def insert_bot(self, bot):
        self.insert_data(table=self.bots_table,
                         insert_cols=['social', 'name', 'login',
                                      'password', 'city', 'comment', 'template_profil'],
                         insert_values=[bot])

    def insert_notification(self, notification):
        data = [notification['username'],
                notification['date'],
                notification['info'],
                False]
        self.insert_data(table=self.notifications_table,
                         insert_cols=['username', 'date',
                                      'info', 'is_old'],
                         insert_values=[data])

    def insert_user(self, user):
        self.insert_data(table=self.users_table,
                         insert_cols=['username', 'hash'],
                         insert_values=[user])

    def insert_posts_vectors(self, posts):
        insert_values = []
        for post in posts:
            vector = [float(i) for i in post['vector']]
            insert_values.append(
                [post['social'],
                 post['owner_id'],
                 post['post_id'],
                 vector])

        self.insert_data(table=self.posts_vectors_table,
                         insert_cols=['social', 'owner_id',
                                      'post_id', 'vector'],
                         insert_values=insert_values)

    def insert_vector_topics_prob(self, data):
        insert_values = []
        for post in data:
            insert_values.append(
                [post['social'],
                 post['owner_id'],
                 post['post_id'],
                 post['probability'],
                 post['topic']])

        self.insert_data(table=self.vector_topics_prob_table,
                         insert_cols=['social', 'owner_id',
                                      'post_id', 'probability',
                                      'topic'],
                         insert_values=insert_values)

    def insert_topics_vector(self, words, vector, threshold):
        threshold = float('{:.2f}'.format(threshold))
        vector = [float(i) for i in vector]
        try:
            self.insert_data(table=self.topic_vectors_table,
                             insert_cols=['words', 'vector', 'threshold'],
                             insert_values=[[words, vector, threshold]])
            self.LOGGER.info('SUCCESS INSERT TOPIC VECTOR')
        except Exception:
            self.LOGGER.exception('ERROR WHILE INSERT TOPIC VECTOR')

    def insert_suggestion(self, suggestion):
        self.insert_data(table=self.suggestions_table,
                         insert_cols=['date', 'title', 'author', 'text'],
                         insert_values=[suggestion])

    def insert_suggestion_groups(self, date, groups):
        insert_values = []
        for group in groups:
            insert_values.append(
                [date,
                 group[0],
                 group[1],
                 "",
                 'in progress'])

        self.insert_data(table=self.suggestion_groups_table,
                         insert_cols=['date', 'social', 'owner_id',
                                      'post_id', 'state'],
                         insert_values=insert_values)

    def insert_task_groups(self, task_id, social, groups):
        insert_values = []
        for group in groups:
            insert_values.append([task_id, social, group])

        self.insert_data(table=self.task_groups_table,
                         insert_cols=['task_id', 'social', 'group_id'],
                         insert_values=insert_values)

    def insert_task_keywords(self, task_id, social, keywords):
        insert_values = []
        for keyword in keywords:
            insert_values.append([task_id, social, keyword])

        self.insert_data(table=self.task_keywords_table,
                         insert_cols=['task_id', 'social', 'keyword'],
                         insert_values=insert_values)

    def insert_viewed_posts(self, username, posts):
        insert_values = []
        for post in posts:
            insert_values.append([username,
                                  post['social'],
                                  post['owner_id'],
                                  post['post_id']])

        self.insert_data(table=self.users_posts_table,
                         insert_cols=['username', 'social',
                                      'owner_id', 'post_id'],
                         insert_values=insert_values,
                         use_ttl=86400)

    def insert_keyword_data(self, posts):
        self.LOGGER.info('INSERT DATA IN "KEYWORD_POSTS" TABLE')
        insert_values = []
        for post in posts:
            insert_values.append([post['hashtag'],
                                  post['social'],
                                  post['owner_id'],
                                  post['post_id']])
        self.insert_data(table=self.keywords_table,
                         insert_cols=['hashtag', 'social',
                                      'owner_id', 'post_id'],
                         insert_values=insert_values)

        self.insert_posts_stats(posts)

    def insert_posts_data(self, posts, rules_posts=True):
        insert_values = []
        for post in posts:
            insert_values.append([post['social'],
                                  post['owner_id'],
                                  post['post_id'],
                                  post['date'],
                                  post['text'],
                                  post['likes'],
                                  post['shares'],
                                  post['views'],
                                  post['replies'],
                                  post['post_type'],
                                  post['copy_owner_id'],
                                  post['copy_post_id'],
                                  post.get('topic') or -1,
                                  post.get('sentiment') or 0])
        self.insert_data(table=self.posts_table,
                         insert_cols=['social', 'owner_id', 'post_id',
                                      'date', 'text', 'likes', 'shares',
                                      'views', 'replies', 'post_type',
                                      'copy_owner_id', 'copy_post_id',
                                      'topic', 'sentiment'],
                         insert_values=insert_values)

        self.insert_posts_stats(posts)

        if rules_posts:
            insert_values = []
            for post in posts:
                insert_values.append([post['owner_id'],
                                      post['post_id'],
                                      post['social'],
                                      post['date']])
            self.insert_data(table=self.posts_dates_table,
                             insert_cols=['owner_id', 'post_id',
                                          'social', 'date'],
                             insert_values=insert_values)

    def insert_topic_posts_table(self, posts):
        insert_values = []
        for post in posts:
            insert_values.append([post['social'],
                                  post['owner_id'],
                                  post['topic_id'],
                                  post['post_id'],
                                  post['date'],
                                  post['text'],
                                  post['likes'],
                                  post.get('topic') or -1,
                                  post.get('sentiment') or 0])
        self.insert_data(table=self.topic_posts_table,
                         insert_cols=['social', 'owner_id', 'topic_id',
                                      'post_id', 'date', 'text', 'likes',
                                      'topic', 'sentiment'],
                         insert_values=insert_values)

    def insert_posts_rules(self, posts):
        insert_values = []
        for post in posts:
            try:
                insert_values.append([post['rule'],
                                      post['social'],
                                      post['owner_id'],
                                      post['post_id'],
                                      post['type'],
                                      post.get('reply_id') or '',
                                      post['date']])
            except Exception as e:
                self.LOGGER.warning(e)
                self.LOGGER.info(post)
                self.LOGGER.info(type(posts))
                self.LOGGER.info(posts)
        self.insert_data(table=self.posts_rules_table,
                         insert_cols=['rule', 'social', 'owner_id', 'post_id',
                                      'type', 'reply_id', 'date'],
                         insert_values=insert_values)

    def insert_rules(self, rule, words, is_active, social):
        self.insert_data(table=self.rules_table,
                         insert_cols=['tag', 'value', 'is_active', 'social'],
                         insert_values=[[rule, words, is_active, social]])

    def save_new_trace(self, level, topic, text):
        self.insert_data(table=self.trace_templates_table,
                         insert_cols=['level', 'topic', 'date', 'trace'],
                         insert_values=[[level, topic, datetime.now(), text]])

    def insert_trace(self, data):
        self.insert_data(table=self.traces_table,
                         insert_cols=['social', 'owner_id', 'post_id',
                                      'date', 'text', 'author',
                                      'bot', 'status'],
                         insert_values=[[data['social'], data['owner_id'],
                                         data['post_id'],
                                         data['date'],
                                         data['text'],
                                         data.get('author') or "",
                                         data['bot'], 'in progress']])

    def insert_posts_att(self, posts):
        insert_values = []
        for post in posts:
            try:
                insert_values.append([post['social'],
                                      post['owner_id'],
                                      post['post_id'],
                                      post['attachment_id'],
                                      post['type'],
                                      post['header'],
                                      post['link'],
                                      post.get('show_link') or post['link']])
            except Exception as e:
                self.LOGGER.exception(e)
                self.LOGGER.info(post)
        self.insert_data(table=self.att_table,
                         insert_cols=['social', 'owner_id', 'post_id',
                                      'attachment_id', 'type', 'header',
                                      'link', 'show_link'],
                         insert_values=insert_values)

    def insert_topics(self, topics):
        insert_values = []
        for data in topics:
            insert_values.append([data['level'],
                                  data['topic'],
                                  data['title'],
                                  0])

        self.insert_data(table=self.topics_table,
                         insert_cols=['level', 'topic', 'title', 'state'],
                         insert_values=insert_values)

    def insert_topics_in_posts(self, posts):
        insert_values = []
        for data in posts:
            insert_values.append([data['social'],
                                  data['topic'],
                                  data['owner_id'],
                                  data['post_id'],
                                  data['probability']])

        self.insert_data(table=self.topic_in_post_table,
                         insert_cols=['social', 'topic',
                                      'owner_id', 'post_id', 'probability'],
                         insert_values=insert_values)

    def insert_top_posts(self, posts):
        insert_values = []
        for data in posts:
            insert_values.append([data['social'],
                                  data['owner_id'],
                                  data['post_id'],
                                  data['probability'],
                                  data['level'],
                                  data['topic'],
                                  data['text']])

        self.insert_data(table=self.top_post_table,
                         insert_cols=['social', 'owner_id', 'post_id',
                                      'probability', 'level', 'topic', 'text'],
                         insert_values=insert_values)

    def insert_persons(self, persons):
        insert_values = []
        for person in persons:
            insert_values.append([person['social'],
                                  person['person_id'],
                                  person['birthday'],
                                  person['city_id'],
                                  person['country_id'],
                                  person['first_name'],
                                  person['last_name'],
                                  person['sex']])

        self.insert_data(table=self.persons,
                         insert_cols=['social', 'person_id', 'birthday',
                                      'city_id', 'country_id', 'first_name',
                                      'last_name', 'sex'],
                         insert_values=insert_values)

    def insert_groups(self, groups):
        insert_values = []
        for group, data in groups.items():
            insert_values.append([group,
                                  data['name'],
                                  data['members_count'],
                                  data['is_closed'],
                                  data['our_prop'],
                                  data['post_per_day'],
                                  data['views_per_post'],
                                  data['comments'],
                                  data['social'],
                                  data['main_city'],
                                  data['main_prop']])

        self.insert_data(table=self.groups_table,
                         insert_cols=['group_id', 'name', 'members_count',
                                      'is_closed', 'our_prop', 'post_per_day',
                                      'views_per_post', 'comments', 'social',
                                      'main_city', 'main_prop'],
                         insert_values=insert_values)

    def insert_posts_stats(self, posts):
        insert_values = []
        for data in posts:
            insert_values.append([data['social'],
                                  data['owner_id'],
                                  data['post_id'],
                                  datetime.now(),
                                  data['likes'],
                                  data['shares'],
                                  data['views'],
                                  data['replies']])

        self.insert_data(table=self.posts_stats_table,
                         insert_cols=['social', 'owner_id', 'post_id', 'ts',
                                      'likes', 'shares', 'views', 'replies'],
                         insert_values=insert_values)

    def insert_replies(self, replies):
        insert_values = []
        for reply in replies:
            insert_values.append([reply['social'],
                                  reply['owner_id'],
                                  reply['post_id'],
                                  reply['reply_id'],
                                  reply['author'],
                                  reply['date'],
                                  reply['likes'],
                                  reply['parent_reply'],
                                  reply['replies'],
                                  reply['sticker'],
                                  reply.get('reply_to') or "",
                                  reply['text']])

        self.insert_data(table=self.replies_table,
                         insert_cols=['social', 'owner_id', 'post_id',
                                      'reply_id', 'author', 'date',
                                      'likes', 'parent_reply', 'replies',
                                      'sticker', 'reply_to', 'text'],
                         insert_values=insert_values)

    # TODO: CHANGE UPDATE METHODS!!!

    def set_trace_state(self, owner_id, post_id, state=True):
        post_data = self.select_data(table=self.new_posts_table,
                                     conditions=[f"owner_id = '{owner_id}'",
                                                 f"post_id = '{post_id}'"])
        if post_data is None or len(post_data) == 0:
            time.sleep(10)
            post_data = self.select_data(table=self.new_posts_table,
                                         conditions=[
                                             f"owner_id = '{owner_id}'",
                                             f"post_id = '{post_id}'"])
            if post_data is None or len(post_data) == 0:
                self.LOGGER.error(
                    f'TOPIC FOR POST {owner_id} {post_id} NOT SAVED')
                return
            else:
                post_data = post_data[0]
        else:
            post_data = post_data[0]
        date = post_data['date'].strftime("%Y-%m-%d %H:%M:%S")
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                text = post_data['text'].replace("'", "\"")
                query = f"""UPDATE {self.new_posts_table}
                            SET text = '{text}',
                                likes = {post_data['likes']},
                                shares = {post_data['shares']},
                                views = {post_data['views']},
                                replies = {post_data['replies']},
                                post_type = '{post_data['post_type']}',
                                copy_owner_id = {post_data['copy_owner_id']},
                                copy_post_id = {post_data['copy_post_id']},
                                topic = {post_data['topic']},
                                is_tracked = {post_data['is_tracked']},
                                has_trace = {state}
                            WHERE owner_id = {owner_id}
                            AND post_id = {post_id}
                            AND date = '{date}'"""
                session.execute(query)

    def set_tracked_state(self, username, social, owner_id, post_id,
                          state=True, post_city=None, post_topic=None,
                          end_date=None):

        values = [[username, social, owner_id, post_id]]
        cols = ['username', 'social', 'owner_id', 'post_id']
        if state:
            values[0].extend([end_date, post_city, post_topic])
            cols.extend(['end_date', 'post_city', 'post_topic'])
            self.insert_data(table=self.tracked_posts_table,
                             insert_cols=cols,
                             insert_values=values)
        else:
            self.delete_data(table=self.tracked_posts_table,
                             pr_cols=cols,
                             pr_values=values)

    def set_posts_monitoring(self, username, social,
                             owner_id, post_id, ttl=None, state=True):

        values = [[username, social, owner_id, post_id]]
        cols = ['username', 'social', 'owner_id', 'post_id']
        if state:
            if ttl is not None:
                self.insert_data(table=self.posts_monitoring_table,
                                 insert_cols=cols,
                                 insert_values=values,
                                 use_ttl=ttl)
            else:
                self.LOGGER.warning('YOU MUST SPECIFY "TTL", OR USE "STATE=FALSE"')
        else:
            self.delete_data(table=self.posts_monitoring_table,
                             pr_cols=cols,
                             pr_values=values)
    def set_tracked_rules(self, username, tag,
                          social, state=True):

        values = [[username, tag, social]]
        cols = ['user', 'tag', 'social']
        try:
            if state:
                self.insert_data(table=self.tracked_rules_table,
                                 insert_cols=cols,
                                 insert_values=values)
            else:
                self.delete_data(table=self.tracked_rules_table,
                                 pr_cols=cols,
                                 pr_values=values)
            return True
        except:
            self.LOGGER.exception('ERROR WHILE SET TRACKED RULES')
            return False

    def set_user_group_state(self, social, username, group_id, state):
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""UPDATE {self.users_groups_table}
                            SET state = {-1 if not state else 0}
                            WHERE social = '{social}'
                            AND username = '{username}'
                            AND group_id = '{group_id}'"""

                session.execute(query)

    def set_viewed_notification(self, username, date):
        notification = self.get_notification(username, date)
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""UPDATE {self.notifications_table}
                            SET info = {notification['info']},
                                is_old = true
                            WHERE username = '{username}'
                            AND date = '{date}'"""

                session.execute(query)

    def change_post_topic(self, social, owner_id, post_id, topic):
        post_data = self.select_data(table=self.posts_table,
                                     conditions=[f"social = '{social}'",
                                                 f"owner_id = '{owner_id}'",
                                                 f"post_id = '{post_id}'"])
        if post_data is None or len(post_data) == 0:
            time.sleep(10)
            post_data = self.select_data(table=self.new_posts_table,
                                         conditions=[
                                             f"owner_id = '{owner_id}'",
                                             f"post_id = '{post_id}'"])
            # self.LOGGER.info(post_data)
            if post_data is None or len(post_data) == 0:
                self.LOGGER.error(
                    f'TOPIC FOR POST {owner_id} {post_id} NOT SAVED')
                return
            else:
                post_data = post_data[0]
        else:
            post_data = post_data[0]
        date = post_data['date'].strftime("%Y-%m-%d %H:%M:%S")
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                text = post_data['text'].replace("'", "\"")
                query = f"""UPDATE {self.posts_table}
                            SET text = '{text}',
                                likes = {post_data['likes']},
                                shares = {post_data['shares']},
                                views = {post_data['views']},
                                replies = {post_data['replies']},
                                post_type = '{post_data['post_type']}',
                                copy_owner_id = '{post_data['copy_owner_id']}',
                                copy_post_id = '{post_data['copy_post_id']}',
                                topic = {topic},
                                date = '{date}'
                            WHERE social = '{social}'
                            AND owner_id = '{owner_id}'
                            AND post_id = '{post_id}'"""
                session.execute(query)

    def change_topic_tracking(self, level, topic, state):
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""UPDATE {self.topics_table}
                            SET state = {state}
                            WHERE level = {level}
                            AND topic = {topic}"""
                session.execute(query)

    def change_trace_status(self, social, owner_id, post_id,
                            date, status, reply_id=""):
        trace_data = []
        for _ in range(5):
            trace_data = self.select_data(table=self.traces_table,
                                          conditions=[
                                              f"social = '{social}'",
                                              f"owner_id = '{owner_id}'",
                                              f"post_id = '{post_id}'",
                                              f"date = '{date}'"])
            if len(trace_data) > 0:
                break
            time.sleep(1)
        else:
            raise BaseException
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                self.LOGGER.info(f"date: {date} {type(date)}")
                query = f"""UPDATE {self.traces_table}
                            SET status = '{status}',
                                author = '{trace_data[0]["author"]}',
                                bot = '{trace_data[0]["bot"]}',
                                text = '{trace_data[0]["text"]}',
                                reply_id = '{reply_id}'
                            WHERE social = '{social}'
                            AND owner_id = '{owner_id}'
                            AND post_id = '{post_id}'
                            AND date = '{date}'"""
                session.execute(query)

    def change_suggestion_group(self, date, social, owner_id,
                                post_id=None, state=None):
        suggestion_data = []
        for _ in range(5):
            suggestion_data = self.select_data(
                table=self.suggestion_groups_table,
                conditions=[
                    f"date = '{date[:-3]}'",
                    f"social = '{social}'",
                    f"owner_id = '{owner_id}'"
                ])
            if len(suggestion_data) > 0:
                break
            time.sleep(1)
        else:
            raise BaseException
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                self.LOGGER.info(f"date: {date} {type(date)}")
                query = f"""UPDATE {self.suggestion_groups_table}
                            SET post_id = '{post_id or suggestion_data[0]['post_id']}',
                                state = '{state or suggestion_data[0]['state']}'
                            WHERE date = '{date[:-3]}'
                            AND social = '{social}'
                            AND owner_id = '{owner_id}'"""
                session.execute(query)

    def change_task_state(self, task_id, state):
        task = self.get_task(task_id)
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""UPDATE {self.task_table}
                            SET date = '{task['date'].strftime("%Y-%m-%d %H:%M:%S")}',
                                groups_count = {task['groups_count']},
                                is_active = {state},
                                label = '{task['label']}',
                                author = '{task['author']}',
                                social = '{task['social']}'
                            WHERE task_id = {task_id}"""
                session.execute(query)

    def del_template_bot_patterns(self, social, title):
        try:
            with Cluster(contact_points=[self.cassandra_address]) as cluster:
                with cluster.connect(self.keyspace) as session:
                    query = f"""DELETE FROM {self.bots_patterns}
                                WHERE title = '{title}' AND social = '{social}'"""
                    # self.LOGGER.info(query)
                    session.execute(query)
            return True

        except Exception as error:
            self.LOGGER.exception(error)
            return False

    def insert_bot_patterns(self, social, title, description=None, new_social=None, new_title=None, main=None,
                            relatives=None, interests=None, school=None, high_education=None, career=None,
                            military=None, lifestyle=None):

        for _ in range(5):
            table_bots_patterns = self.select_data(
                table=self.bots_patterns,
                conditions=[
                    f"title = '{title}'",
                    f"social = '{social}'"
                ], print_query=True)
            if len(table_bots_patterns) > 0:
                break
            time.sleep(1)
        else:
            self.LOGGER.warning('Не удалось найти запись')
            return False
            # raise BaseException

        self.LOGGER.info(table_bots_patterns)
        try:
            with Cluster(contact_points=[self.cassandra_address]) as cluster:
                with cluster.connect(self.keyspace) as session:

                    # self.LOGGER.info('-----new_title----')
                    # self.LOGGER.info(new_title)
                    # self.LOGGER.info('-----new_social---')
                    # self.LOGGER.info(new_social)

                    if not new_title is None or not new_social is None:

                        if new_social != social and new_social is None:
                            new_social = social

                        if self.del_template_bot_patterns(social, title) is False:
                            return False

                        query = f"""INSERT INTO {self.bots_patterns}
                                            (social, title, description) 
                                    VALUES
                                    ('{new_social}', '{new_title}', '')"""

                        # self.LOGGER.info(query)
                        session.execute(query)
                        title = new_title
                        social = new_social

                    query = f"""UPDATE {self.bots_patterns}
                                SET main = {main or table_bots_patterns[0]['main'] or {}},
                                    relatives = {relatives or table_bots_patterns[0]['relatives'] or {}},
                                    interests = {interests or table_bots_patterns[0]['interests'] or {}},
                                    school = {school or table_bots_patterns[0]['school'] or {}},
                                    high_education = {high_education or table_bots_patterns[0]['high_education'] or {}},
                                    career = {career or table_bots_patterns[0]['career'] or {}},
                                    military = {military or table_bots_patterns[0]['military'] or {}},
                                    lifestyle = {lifestyle or table_bots_patterns[0]['lifestyle'] or {}},
                                    description = '{description or table_bots_patterns[0]['description']}'
                                WHERE title = '{title}'
                                AND social = '{social}'"""

                    # self.LOGGER.info(query)
                    session.execute(query)

                    return True

        except Exception as error:
            self.LOGGER.exception(error)
            return False

    def select_bot_patterns(self, social, title):

        if social == '*all*' and title == '*all*':
            # for _ in range(5):
            #     table_bots_patterns = self.select_data(
            #         table=self.bots_patterns, print_query=True)
            #     if len(table_bots_patterns) > 0:
            #         break
            #     time.sleep(1)
            # else:
            #     self.LOGGER.warning('Не удалось найти запись')
            #     return False
            table_bots_patterns = self.select_data(
                table=self.bots_patterns, print_query=True)

            return table_bots_patterns

        elif social != '*all*' and title == '*all*':
            for _ in range(5):
                table_bots_patterns = self.select_data(
                    table=self.bots_patterns,
                    conditions=[
                        f"social = '{social}'"
                    ], print_query=True)
                if len(table_bots_patterns) > 0:
                    break
                time.sleep(1)
            else:
                self.LOGGER.warning('Не удалось найти запись')
                return False

            return table_bots_patterns

        elif social == '*all*' and title != '*all*':
            for _ in range(5):
                table_bots_patterns = self.select_data(
                    table=self.bots_patterns,
                    conditions=[
                        f"title = '{title}'"
                    ], print_query=True)
                if len(table_bots_patterns) > 0:
                    break
                time.sleep(1)
            else:
                self.LOGGER.warning('Не удалось найти запись')
                return False
            return table_bots_patterns

        else:
            for _ in range(5):
                table_bots_patterns = self.select_data(
                    table=self.bots_patterns,
                    conditions=[
                        f"title = '{title}'",
                        f"social = '{social}'"
                    ], print_query=True)
                if len(table_bots_patterns) > 0:
                    break
                time.sleep(1)
            else:
                self.LOGGER.warning('Не удалось найти запись')
                return False
                # raise BaseException

            self.LOGGER.info(table_bots_patterns)

            return table_bots_patterns

    def create_new_template_bot_patterns(self, social, title):

        count = self.select_bot_patterns(social, title)

        try:
            with Cluster(contact_points=[self.cassandra_address]) as cluster:
                with cluster.connect(self.keyspace) as session:
                    if not count is False:
                        info = 'Шаблон с таким названием и соц. сетью уже существует'
                        self.LOGGER.warning(info)
                        return info

                    main = "{'bdate': '01.01.2001', 'home_town': '', 'relation': '0', 'sex': '1', 'status': ''}"
                    relatives = "{'child': ' ', 'grandchild': ' ', 'grandparent': ' ', 'parent': ' ', 'sibling': ' '}"
                    interests = "{'about': ' ', 'activities': ' ', 'books': ' ', 'games': ' ', 'interests': ' ', 'movies': ' ', 'music': ' ', 'quotes': ' ', 'tv': ' '}"
                    school = "{'schoole_city': 'Челябинск', 'schoole_country': 'Россия', 'schools_class': '', 'schools_name': '', 'schools_speciality': ' ', 'schools_year_from': ' ', 'schools_year_graduated': ' ', 'schools_year_to': ' '}"
                    high_education = "{'education_form': 'Не выбрана', 'education_status': 'Не выбран', 'university_city': 'Челябинск', 'university_country': 'Россия', 'university_graduation': ' ', 'university_name': ''}"
                    career = "{'career_company': ' ', 'career_from': ' ', 'career_position': ' ', 'career_until': ' ', 'work_city': 'Челябинск', 'work_country': 'Россия'}"
                    military = "{'military_country': 'Россия', 'military_from': ' ', 'military_unit': ' ', 'military_until': ' '}"
                    lifestyle = "{'personal_alcohol': 'Не указано', 'personal_inspired_by': ' ', 'personal_life_main': 'Не указано', 'personal_people_main': 'Не указано', 'personal_political': 'Не выбраны', 'personal_religion': '', 'personal_smoking': 'Не указано'}"

                    query = f"""INSERT INTO {self.bots_patterns}
                                        (social, title, description, main, relatives, interests,
                                         school,high_education, career, military, lifestyle )
                                VALUES
                                ('{social}', '{title}', '',
                                 {main},{relatives},{interests},{school},{high_education},
                                  {career}, {military}, {lifestyle})"""

                    # self.LOGGER.info(query)
                    session.execute(query)

                    template = self.select_bot_patterns(social, title)
                    return template

        except Exception as error:
            self.LOGGER.exception(error)
            return False

    def change_bot_comment(self, social, login, comment):
        bot = self.get_bot(social, login)
        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""UPDATE {self.bots_table}
                            SET comment = '{comment}',
                                name = '{bot['name']}',
                                password = '{bot['password']}',
                                city = '{bot['city']}'
                            WHERE social = '{social}'
                            AND login = '{login}'"""
                session.execute(query)

    def change_bot_template(self, social, login=None, template_profil='', status_template="Нет шаблона"):

        bot = self.get_bot(social, login)

        with Cluster(contact_points=[self.cassandra_address]) as cluster:
            with cluster.connect(self.keyspace) as session:
                query = f"""UPDATE {self.bots_table}
                            SET template_profil = '{template_profil}',
                                status_template = '{status_template}',
                                name = '{bot['name']}',
                                password = '{bot['password']}',
                                city = '{bot['city']}'"""
                if login is None:
                    query += f"\n WHERE social = '{social}'"
                    self.LOGGER.warning('NET')

                else:
                    self.LOGGER.warning('OO DAA')

                    query += f"\n WHERE social = '{social}' AND login = '{login}'"

                session.execute(query)
