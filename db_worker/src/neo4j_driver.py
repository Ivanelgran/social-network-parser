import json

from neo4j import GraphDatabase


class Neo4jWorker():
    """Методы для работы с neo4j"""

    def __init__(self, host, port, login, password, logger):
        self.driver = GraphDatabase.driver(f"bolt://{host}:{port}",
                                           auth=(login, password))
        self.logger = logger

        self.queries = {'select': Selecter,
                        'insert': Inserter,
                        'update': Updater,
                        'delete': Deleter,
                        'insert_relation': RelationInserter,
                        'delete_relation': RelationDeleter}

    def run_query(self, data):
        result = {'completed': False,
                  'message': "",
                  'result': None}

        query_type = data['type']
        if query_type not in self.queries:
            result['message'] = f"error: '{query_type}' does not supported"
            return result

        try:
            query = self.queries[query_type](data, self.driver)
            query_result = query.run()
        except Exception:
            message = f"error: problem in '{query_type}'"
            self.logger.exception(message)
            result['message'] = message
            return result

        result['completed'] = True
        result['result'] = query_result

        return result


class BaseQuery():

    def run_query(self, tx, **kwargs):
        result = tx.run(self.query, **kwargs)

        return result

    def get_key_fields(self, keys):
        key_fields = ""
        for field, field_value in keys.items():
            values = json.dumps(field_value, default=str)
            key_fields += f"{field}: {values}, "
        key_fields = key_fields[:-2]

        return key_fields

    def get_add_key_fields(self, keys={}, additional=[]):
        key_fields = ""
        for field, field_value in keys.items():
            values = json.dumps(field_value, default=str)
            key_fields += f"r.{field} = {values} AND "
        if len(additional):
            for field, operator, value in additional:
                values = json.dumps(value, default=str)
                key_fields += f"r.{field} {operator} {values} AND "
        key_fields = key_fields[:-5]

        return key_fields


class Selecter(BaseQuery):

    def __init__(self, data, driver):
        self.node = data['table']
        self.items = data.get('items')
        self.additional_keys = data.get('additional_keys')
        self.count = data.get('count')

        self.driver = driver

    def run(self):
        result = []
        if isinstance(self.items, list):
            for item in self.items:
                key_fields = self.get_add_key_fields(item)

                cur_result = self._select(key_fields)
                result.extend(cur_result)
        elif isinstance(self.items, dict) or self.additional_keys is not None:
            key_fields = self.get_add_key_fields(self.items or {},
                                                 self.additional_keys or [])
            cur_result = self._select(filter_fields=key_fields)
            result.extend(cur_result)

        return result

    def _select(self, key_fields=None, filter_fields=None):
        node = self.node
        if key_fields is not None:
            self.query = f"MATCH (r:{node} {{{key_fields}}}) RETURN r"
        elif filter_fields is not None:
            self.query = f"MATCH (r:{node}) WHERE {filter_fields} RETURN r"

        result = []
        with self.driver.session() as session:
            result = session.read_transaction(self.run_query)

            result = list(result.records())
            result = [dict(res['r']) for res in result]

        return result


class Updater(BaseQuery):

    def __init__(self, data, driver):
        self.node = data['table']
        self.items = data['items']

        self.driver = driver

    def run(self):
        for keys, values in self.items:
            self._update(keys, values)

    def _update(self, keys, values):
        if not len(keys) or not len(values):
            return

        key_fields = self.get_key_fields(keys)

        upd_fields = ""
        for field, field_value in values.items():
            values = json.dumps(field_value, default=str)
            upd_fields += f"r.{field} = {values}, "
        upd_fields = upd_fields[:-2]

        self.query = f"MATCH (r:{self.node} {{{key_fields}}}) SET {upd_fields}"

        with self.driver.session() as session:
            session.write_transaction(self.run_query)


class Inserter(BaseQuery):

    def __init__(self, data, driver):
        self.node = data['table']
        self.items = data['items']

        self.driver = driver

    def run(self):
        if isinstance(self.items, dict):
            self._insert(self.items)
        elif isinstance(self.items, list):
            for item in self.items:
                self._insert(item)
        else:
            raise Exception("Items not a dict or list")

    def _insert(self, data):
        if not len(data):
            return

        data_fields = self.get_key_fields(data)

        self.query = f"CREATE (:{self.node} {{{data_fields}}})"

        with self.driver.session() as session:
            session.write_transaction(self.run_query)


class Deleter(BaseQuery):

    def __init__(self, data, driver):
        self.node = data['table']
        self.items = data['items']

        self.driver = driver

    def run(self):
        if isinstance(self.items, dict):
            self._delete(self.items)
        elif isinstance(self.items, list):
            for item in self.items:
                self._delete(item)
        else:
            raise Exception("Items not a dict or list")

    def _delete(self, data):
        if not len(data):
            return

        data_fields = self.get_key_fields(data)

        self.query = f"MATCH (r:{self.node} {{{data_fields}}}) DELETE r"

        with self.driver.session() as session:
            session.write_transaction(self.run_query)


class RelationInserter(BaseQuery):

    def __init__(self, data, driver):
        self.from_node = data['from']
        self.to_node = data['to']
        self.relation = data['relation']
        self.items = data['items']

        self.driver = driver

    def run(self):
        if isinstance(self.items, dict):
            self._insert(self.items)
        elif isinstance(self.items, list):
            for item in self.items:
                self._insert(item)
        else:
            raise Exception("Items not a dict or list")

    def _insert(self, items):

        from_data_fields = self.get_key_fields(items['from'])
        to_data_fields = self.get_key_fields(items['to'])

        query = (f"MERGE (a:{self.from_node} {{{from_data_fields}}}) "
                 f"MERGE (b:{self.to_node} {{{to_data_fields}}}) "
                 f"MERGE (a)-[r:{self.relation}]->(b)")

        self.query = query

        with self.driver.session() as session:
            session.write_transaction(self.run_query)


class RelationDeleter(BaseQuery):

    def __init__(self, data, driver):
        self.from_node = data['from']
        self.to_node = data['to']
        self.relation = data['relation']
        self.items = data['items']

        self.driver = driver

    def run(self):
        if isinstance(self.items, dict):
            self._insert(self.items)
        elif isinstance(self.items, list):
            for item in self.items:
                self._insert(item)
        else:
            raise Exception("Items not a dict or list")

    def _insert(self, items):

        if items.get('from') is not None:
            from_data_fields = self.get_key_fields(items['from'])
            query = f"MATCH (a:{self.from_node} {{{from_data_fields}}}), "
        else:
            f"MATCH (a:{self.from_node}), "

        if items.get('to') is not None:
            to_data_fields = self.get_key_fields(items['to'])
            query += f"(b:{self.to_node} {{{to_data_fields}}}) "
        else:
            f"(b:{self.to_node}) "

        query += f"MATCH (a)-[r:{self.relation}]->(b) DELETE r"

        self.query = query

        with self.driver.session() as session:
            session.write_transaction(self.run_query)
