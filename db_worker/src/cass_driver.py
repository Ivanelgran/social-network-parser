import logging
from cassandra.cqlengine import connection, management
from cassandra.cqlengine.query import BatchQuery, ContextQuery
from common_lib.cass_worker import CassQueryGenerator


class CassandraDriver():

    host = "cassandra"
    keyspace = "sma_service"

    def prepare_db(self):
        self.LOGGER = logging.getLogger('DB_worker.cass_worker')
        self.all_tables = CassQueryGenerator.all_tables
        self.setup_connection()
        self.sync_tables()
        self.mat_views = CassQueryGenerator.mat_views
        # self.sync_mat_views()
        self.all_tables.extend(self.mat_views)
        self.tables_names = self.get_tables_names()

    def setup_connection(self):
        self.connection = connection.setup(
            [self.host], self.keyspace,
            protocol_version=3, retry_connect=True)

        management.create_keyspace_network_topology(self.keyspace, {'DC1': 1})

    def sync_tables(self):
        for table in self.all_tables:
            management.sync_table(table, [self.keyspace])

    def sync_mat_views(self):
        # TODO: add sync
        raise NotImplementedError

    def get_tables_names(self):
        names = [table.__table_name__ for table in self.all_tables]
        tables_names = dict((n, cl) for n, cl in zip(names, self.all_tables))

        return tables_names

    def get_records(self,
                    table: str,
                    keys: dict = None,
                    count: int = None,
                    order_by: str = None,
                    sort_asc: bool = True):
        """Получение записей по набору ключей

        Args:
            table: Название таблицы
            keys: Поля для фильтрации. Ключи - названия полей,
                значения - значения полей
            count: Количество возвращаемых записей
            order_by: Поле для сортировки
            sort_asc: Задает тип сортировкуи. True - ascending,
                False - descending

        Returns:
            list<dict()>: найденные записи
        """

        with ContextQuery(
            self.tables_names[table], connection=self.connection
        ) as T:
            if keys is not None:
                records = T.objects().filter(**keys).allow_filtering().all()
            else:
                records = T.objects().allow_filtering().all()

            if order_by is not None:
                sort = "" if sort_asc else "-"
                records = records.order_by(sort + order_by)

        records = list(map(lambda x: dict(x), records))

        if count is not None:
            return records[:count]

        return records

    def get_multikeys_records(self,
                              table: str,
                              all_keys: list):
        """Получение записей по разным наборам ключей

        Args:
            table: Название таблицы
            all_keys: Список словарей. Каждый словарь - поля для поиска записи.
                Ключи - названия полей, значения - значения полей

        Returns:
            list<dict()>: найденные записи
        """

        records = []
        with ContextQuery(
            self.tables_names[table], connection=self.connection
        ) as T:
            for keys in all_keys:
                cur_records = T.objects().filter(**keys) \
                    .allow_filtering().all()
                cur_records = list(map(lambda x: dict(x), cur_records))
                records.extend(cur_records)

        return records

    def delete_record(self,
                      table: str,
                      keys: dict):
        """Удаление уникальной записи

        Args:
            table: название таблицы
            keys: Поля для поиска записи. Ключи - названия полей,
                значения - значения полей
        """
        self.tables_names[table].objects(**keys).delete()

    def delete_records(self,
                       table: str,
                       all_keys: list):
        """Удаление записей

        Args:
            table: название таблицы
            all_keys: Список словарей. Каждый словарь - поля для поиска записи.
                Ключи - названия полей, значения - значения полей
        """
        with BatchQuery() as b:
            for keys in all_keys:
                self.tables_names[table].objects(**keys).batch(b).delete()

    def update_record(self,
                      table: str,
                      keys: dict,
                      values: dict):
        """Изменение уникальной записи

        Args:
            table: название таблицы
            keys: Поля для поиска записи. Ключи - названия полей,
                значения - значения полей
            values: Поля для внесения изменений. Ключи - названия полей,
                значения - значения полей
        """
        self.tables_names[table].objects(**keys).update(**values)

    def update_records(self,
                       table: str,
                       all_values: list):
        """Изменение записей

        Args:
            table: название таблицы
            all_values: Список кортежей (dict, dict). Первый элемент кортежа -
                поля для поиска записи, второй элемент кортежа -
                поля для внесения изменений. В полях ключи - названия полей,
                значения - значения полей
        """
        with BatchQuery() as b:
            for keys, values in all_values:
                self.tables_names[table].objects(**keys).batch(b) \
                    .update(**values)

    def insert_record(self,
                      table: str,
                      values: dict):
        """Вставка записи

        Args:
            table: название таблицы
            values: Поля для вставки. Ключи - названия полей,
                значения - значения полей
        """
        record = self.tables_names[table](**values)
        record.save()

    def insert_records(self,
                       table: str,
                       all_values: list):
        """Вставка записей

        Args:
            table: название таблицы
            all_values: Список словарей. Каждый элемент - поля для вставки.
                Ключи - названия полей, значения - значения полей
        """
        with BatchQuery() as b:
            for values in all_values:
                self.tables_names[table].batch(b).create(**values)
