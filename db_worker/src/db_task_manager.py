import pickle
import json
import os
import logging
from pathlib import Path
from db_worker import CassandraWorker
from cass_driver import CassandraDriver
from neo4j_driver import Neo4jWorker
from common_lib.queue_manager import make_server
from common_lib.src import config
from threading import Thread

LOGGER = logging.getLogger('DB_worker')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')

SRC_DIR = Path(__name__).resolve().parents[0]

fh = logging.FileHandler(os.path.join(SRC_DIR, 'logs/logs.log'))
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)


def on_request(ch, method, props, body):
    with_json = False
    try:
        task = pickle.loads(body)
    except Exception:
        task = json.loads(body)

        with_json = True

    if task.get('task_name') in cass_method_list:
        task_method = getattr(cass, task.get('task_name'))
        task_args = task['args']
        try:
            result = task_method(**task_args)
        except Exception:
            LOGGER.exception(f"Error occured in {task.get('task_name')}")
            result = None
    elif task.get('task_name') in cass_new_method_list:
        task_method = getattr(cass_new, task.get('task_name'))
        task_args = task['args']
        try:
            result = task_method(**task_args)
        except Exception:
            LOGGER.exception(f"Error occured in {task.get('task_name')}")
            result = None
    elif task.get('type') in neo4j.queries:
        LOGGER.info('Run neo4j task')
        task_result = neo4j.run_query(task)
        if not task_result['completed']:
            LOGGER.warning(task_result['message'])
        result = task_result['result']
        LOGGER.info('Neo4j task completed')
    else:
        result = "Current method is not supported"

    if with_json:
        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=json.dumps(result))
    else:
        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=pickle.dumps(result))
    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    CONFIG = config.get_config('./common_lib/config/db_worker.yml')

    cass = CassandraWorker()
    cass_method_list = [
        func for func in dir(cass) if callable(getattr(cass, func))
        and not func.startswith("__")
    ]

    cass_new = CassandraDriver()
    cass_new.prepare_db()
    cass_new_method_list = [
        func for func in dir(cass_new) if callable(getattr(cass_new, func))
        and not func.startswith("__")
    ]

    neo4j = Neo4jWorker(CONFIG['neo4j']['host'],
                        CONFIG['neo4j']['port'],
                        CONFIG['neo4j']['login'],
                        CONFIG['neo4j']['password'],
                        LOGGER)

    # TODO: Process Pools
    process_one = Thread(target=make_server, args=('db_tasks', on_request,))
    process_two = Thread(target=make_server,
                         args=("db_api_tasks", on_request,))
    process_three = Thread(target=make_server,
                           args=("cache_services", on_request,))

    process_one.start()
    process_two.start()
    process_three.start()

    process_one.join()
    process_two.join()
    process_three.join()
