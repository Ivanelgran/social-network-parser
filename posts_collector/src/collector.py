import pandas as pd
import numpy as np
from common_lib.queue_manager import RpcClient, make_server
from common_lib.cass_worker import CassandraTables, CassQueryGenerator
from datetime import datetime, timedelta
import pickle
from threading import Thread
import time
import logging
from pathlib import Path
import os

from common_lib.src.neo4j_driver import Models, select_with_filters

LOGGER = logging.getLogger('Collector')
LOGGER.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')

SRC_DIR = Path(__name__).resolve().parents[0]

fh = logging.FileHandler(os.path.join(SRC_DIR, 'logs/logs.log'))
fh.setFormatter(formatter)
LOGGER.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOGGER.addHandler(sh)


class PostsCollector():

    posts = None
    all_cities = {1: 158,  # 'Челябинск',
                  2: 794,  # 'Златоуст',
                  3: 1573,  # 'Каменск-Уральский',
                  4: 5098,  # 'Копейск',
                  5: 5542,  # 'Касли',
                  6: 12126,  # 'Коркино',
                  7: 74,  # 'Курган',
                  8: 6050,  # 'Кыштым',
                  9: 82,  # 'Магнитогорск',
                  10: 7159,  # 'Миасс',
                  11: 941,  # 'Озерск',
                  12: 3034,  # 'Пласт',
                  13: 6820,  # 'Рощино',
                  14: 145,  # 'Троицк',
                  15: 3255,  # 'Чебаркуль',
                  16: 5401,  # 'Еманжелинск',
                  17: 1268}  # 'Южноуральск'}

    def __init__(self):
        self.cass = RpcClient('cache_services')
        self.LOGGER = logging.getLogger('Collector.main')
        self.update_rate = 60

    def run(self):
        while True:
            start_time = time.time()
            try:
                self.update_posts()
            except Exception:
                self.LOGGER.exception("Cant update posts")

            if time.time() - start_time < self.update_rate:
                time.sleep(self.update_rate - (time.time() - start_time))

    def get_new_posts(self, cond=None):

        self.LOGGER.info("Getting new posts")

        filter_viewed = False
        min_likes = min_replies = min_shares = min_views = start_time = None
        posts_count = cond.get('posts_count') or 100 if cond is not None else 100
        if cond is not None and len(cond) > 0:
            if cond.get('likes') is not None:
                min_likes = cond['likes']
            if cond.get('replies') is not None:
                min_replies = cond['replies']
            if cond.get('shares') is not None:
                min_shares = cond['shares']
            if cond.get('views') is not None:
                min_views = cond['views']
            if cond.get('start_time') is not None:
                start_time = cond['start_date'] + " " + cond['start_time']
                start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M')
            if cond.get('filter_viewed') is not None:
                filter_viewed = cond['filter_viewed']

        posts = self.posts

        # фильтр соцсети
        if cond is not None and cond.get('checked_socials') is not None:
            socials = cond.get('checked_socials')
            if len(socials) > 0:
                posts = posts[posts.social.isin(socials)]

        self.LOGGER.info("Getting viewed posts")

        # сбор просмотренных постов
        viewed_posts_ids = []
        # task = {'task_name': 'get_viewed_posts',
        #         'args': {'username': cond['username']}}
        # viewed_posts = self.cass.call(task)
        query = CassQueryGenerator(self.cass, CassandraTables.user_posts,
                                   'select')
        user_posts = CassandraTables.user_posts(username=cond['username'])
        query.add_record(user_posts),
        viewed_posts = query.run_task()
        if viewed_posts is not None:
            for post in viewed_posts:
                viewed_posts_ids.append(
                    str(post['owner_id']) + "_" + str(post['post_id']))
        posts['full_post_id'] = posts.owner_id.apply(
            str) + "_" + posts.post_id.apply(str)
        posts['is_viewed'] = posts.full_post_id.isin(viewed_posts_ids)

        self.LOGGER.info("Getting hidden groups")

        # сбор скрытых групп
        hidden_groups = []
        # task = {'task_name': 'get_user_groups',
        #         'args': {'username': cond['username']}}
        # user_groups = self.cass.call(task)
        query = CassQueryGenerator(self.cass, CassandraTables.user_groups,
                                   'select')
        user_group = CassandraTables.user_groups(username=cond['username'])
        query.add_record(user_group),
        user_groups = query.run_task()
        for group in user_groups:
            if group['state'] == -1:
                hidden_groups.append(group['group_id'])

        posts = posts[~posts.group_id.isin(hidden_groups)]

        # Фильтрация неподходящих городов
        if cond.get('checked_cities') is not None and len(cond.get('checked_cities')):
            cities = [self.all_cities[int(i)] for i in cond['checked_cities']]
            if len(self.all_cities) == len(cond['checked_cities']):
                cities.append(np.nan)
                cities.append("")
                cities.append(None)
            posts = posts[posts.main_city.isin(cities)]

        # Фильтрация по отслеживаемым темам

        if cond.get('checked_topics') is not None and len(cond['checked_topics']):
            posts = posts[posts.topic.str.contains('|'.join(cond['checked_topics']))]

        posts = posts.drop_duplicates(subset='post_id')

        self.LOGGER.info("Getting tracked posts")

        # отслеживаемые посты
        # task = {'task_name': 'get_tracked_posts',
        #         'args': {'username': cond['username']}}
        # tracked_posts = self.cass.call(task)
        query = CassQueryGenerator(self.cass, CassandraTables.tracked_posts,
                                   'select')
        user_group = CassandraTables.tracked_posts(username=cond['username'])
        query.add_record(user_group),
        tracked_posts = query.run_task()
        if len(tracked_posts) == 0:
            posts['is_tracked'] = False
        else:
            tracked_posts = pd.DataFrame(tracked_posts)[
                ['social', 'owner_id', 'post_id']]
            tracked_posts['is_tracked'] = True
            posts = pd.merge(posts, tracked_posts, how="left",
                             on=['social', 'owner_id', 'post_id'])
            posts.is_tracked.fillna(False, inplace=True)

        self.LOGGER.info("Final filter and sorting")

        # финальная фильтрация и сортировка
        if min_likes is not None:
            posts = posts[posts.likes >= min_likes]
        if min_shares is not None:
            posts = posts[posts.shares >= min_shares]
        if min_views is not None:
            posts = posts[posts.views >= min_views]
        if min_replies is not None:
            posts = posts[posts.replies >= min_replies]
        if start_time is not None:
            posts.date = pd.to_datetime(posts.date)
            posts = posts[posts.date >= start_time]

        posts.drop_duplicates(subset=['social', 'owner_id', 'post_id'],
                              inplace=True)

        if filter_viewed:
            posts['view_filter'] = ~posts.is_viewed
            sorting = ['view_filter', 'topic_tracked']
        else:
            sorting = ['topic_tracked']
        sorting.extend(cond.get("sort") or ['date'])
        posts = posts.sort_values(
            by=sorting, ascending=False).iloc[:posts_count]

        # posts.comments.fillna(0, inplace=True)
        posts.name.fillna("", inplace=True)
        posts.group_id.fillna('0', inplace=True)
        posts.is_closed.fillna(False, inplace=True)
        posts.main_city.fillna("", inplace=True)
        posts.main_prop.fillna("", inplace=True)
        posts.members_count.fillna("", inplace=True)
        posts.our_prop.fillna("", inplace=True)
        # posts.post_per_day.fillna("", inplace=True)
        # posts.views_per_post.fillna("", inplace=True)
        posts.text.fillna("", inplace=True)

        if "sentiment" not in posts.columns:
            posts['sentiment'] = 0
        posts.sentiment.fillna(0, inplace=True)

        self.LOGGER.info("Getting complete")

        valid_columns = ['has_trace', 'is_tracked', 'topic_tracked',
                         'is_viewed', 'social', 'owner_id', 'post_id', 'name',
                         'group_href', 'members_count', 'main_city',
                         'main_prop', 'post_href', 'date', 'text', 'href',
                         'link', 'views', 'views_increase', 'likes',
                         'likes_increase', 'replies', 'replies_increase',
                         'shares', 'shares_increase', 'topic', 'sentiment']

        return posts[valid_columns].to_dict('records')

    def get_last_vectors(self):
        columns = ['social', 'owner_id', 'post_id', 'vector']

        return self.posts[columns].to_dict('records')

    def update_posts(self):
        self.LOGGER.info("Updating posts...")
        if self.posts is not None:
            self.drop_posts_by_date()

        self.LOGGER.info("Getting posts")
        posts = self.get_last_posts()
        self.LOGGER.info(f"New posts count: {len(posts)}")

        if not len(posts):
            return

        self.LOGGER.info("Getting groups")
        groups_info = self.get_posts_groups(posts[['social',
                                                   'owner_id']])

        posts = pd.DataFrame(posts)

        posts = pd.merge(posts, groups_info,
                         left_on=['social', 'owner_id'],
                         right_on=['social', 'group_id'],
                         how='left')

        self.LOGGER.info("Getting topics, stat, attachments and additional")
        posts = self.apply_topics(posts)
        posts = self.apply_stat(posts)
        posts = self.apply_attachments(posts)
        posts = self.add_additional_fields(posts)
        posts = self.add_vectors(posts)

        if self.posts is not None:
            self.posts = pd.concat([self.posts, posts])
        else:
            self.posts = posts

        self.LOGGER.info("Updating complete")

    def drop_posts_by_date(self):
        date_limit = datetime.now() - timedelta(days=2)
        date_limit = datetime(year=date_limit.year,
                              month=date_limit.month,
                              day=date_limit.day)

        posts = self.posts
        posts.date = pd.to_datetime(posts.date)
        posts = posts[posts.date >= date_limit]

        self.posts = posts

    def get_last_posts(self):
        days_count = 2 if self.posts is None else 1
        posts_date = datetime.now() - timedelta(days=days_count)
        # posts_date = posts_date.strftime("%Y-%m-%d %H:%M:%S")
        # posts_date = f"date >= '{posts_date}'"
        # task = {'task_name': 'get_last_posts',
        #         'args': {'date': [posts_date]}}
        # posts = self.cass.call(task)
        query = CassQueryGenerator(self.cass, CassandraTables.posts_dates,
                                   'select')
        query.handler.add_filter('date', '>=', posts_date)
        query.handler.add_count(100000)
        posts = query.run_task()

        self.LOGGER.info(f"Last posts: {len(posts)}")

        if not len(posts):
            return pd.DataFrame([])

        query = CassQueryGenerator(self.cass, CassandraTables.all_posts,
                                   'select')
        for post in posts:
            cur_post = CassandraTables.all_posts(social=post['social'],
                                                 owner_id=post['owner_id'],
                                                 post_id=post['post_id'])
            query.add_record(cur_post)
        posts = query.run_task()

        if posts is None or not len(posts):
            return pd.DataFrame([])

        all_ids = []
        if self.posts is not None:
            for post in self.posts.iterrows():
                row = post[1]
                cur_id = row['social'] + "_" + row['owner_id'] + \
                    "_" + row['post_id']
                all_ids.append(cur_id)

        result = []
        for post in posts:
            cur_id = post['social'] + "_" + post['owner_id'] + \
                "_" + post['post_id']

            if cur_id not in all_ids:
                result.append(post)

        return pd.DataFrame(result)

    @staticmethod
    def add_hyphen(post):
        new_post = post.copy()
        if new_post['social'] == 'vk':
            new_post['group_id'] = '-' + new_post['group_id']

        return new_post

    def get_posts_groups(self, posts):
        result = []
        socials = posts.social.unique()
        for social in socials:
            groups = list(posts[posts.social == social].owner_id.unique())
            if social == 'vk':
                groups = [*map(lambda x: x[1:] if x.startswith('-') else x,
                               groups)]
            groups_info = select_with_filters(
                Models.group,
                {'social': social, 'group_id__in': groups})
            groups_info = [*map(PostsCollector.add_hyphen,
                                map(dict, groups_info))]

            result.extend(groups_info)

        result = pd.DataFrame(result)

        return result

    def add_vectors(self, posts):
        posts_dict = posts[['social', 'owner_id', 'post_id']] \
            .to_dict('records')
        # task = {'task_name': 'get_posts_vectors_by_ids',
        #         'args': {'posts_ids': posts_dict}}
        # vectors = self.cass.call(task)
        query = CassQueryGenerator(self.cass, CassandraTables.posts_vectors,
                                   'select')
        for post in posts_dict:
            vector = CassandraTables.posts_vectors(social=post['social'],
                                                   owner_id=post['owner_id'],
                                                   post_id=post['post_id'])
            query.add_record(vector)
        vectors = query.run_task()

        if len(vectors):
            vectors = pd.DataFrame(vectors)
        else:
            vectors = pd.DataFrame(
                columns=['social', 'owner_id', 'post_id', 'vector'])

        posts = pd.merge(posts, vectors, how="left",
                         on=['social', 'owner_id', 'post_id'])
        posts.vector.fillna("", inplace=True)

        return posts

    def apply_topics(self, posts):
        posts_dict = posts[['social', 'owner_id', 'post_id']] \
            .to_dict('records')
        # task = {'task_name': 'get_vector_posts_topics',
        #         'args': {'posts': posts_dict}}
        # vector_posts_topics = self.cass.call(task)
        query = CassQueryGenerator(self.cass,
                                   CassandraTables.vector_topics_prob,
                                   'select')
        for post in posts_dict:
            vector = CassandraTables.vector_topics_prob(
                social=post['social'],
                owner_id=post['owner_id'],
                post_id=post['post_id'])
            query.add_record(vector)
        vector_posts_topics = query.run_task()

        if len(vector_posts_topics) > 0:
            vector_posts_topics = pd.DataFrame(vector_posts_topics)
        else:
            vector_posts_topics = pd.DataFrame(
                columns=['social', 'owner_id', 'post_id', 'topic'])
        posts.drop(columns=['topic'], inplace=True)
        posts = pd.merge(posts, vector_posts_topics, how="left",
                         on=['social', 'owner_id', 'post_id'])
        posts.topic.fillna('-1', inplace=True)
        posts['topic_tracked'] = False

        return posts

    def apply_stat(self, posts):
        posts_dict = posts[['social', 'owner_id', 'post_id']] \
            .to_dict('records')
        posts_stats = self.get_posts_stat(posts_dict)

        posts_df = pd.merge(posts, posts_stats, how="left",
                            on=['social', 'owner_id', 'post_id'])
        posts_df.likes_y.fillna(posts_df.likes_x, inplace=True)
        posts_df.shares_y.fillna(posts_df.shares_x, inplace=True)
        posts_df.views_y.fillna(posts_df.views_x, inplace=True)
        posts_df.replies_y.fillna(posts_df.replies_x, inplace=True)
        posts_df.likes_increase.fillna(0, inplace=True)
        posts_df.shares_increase.fillna(0, inplace=True)
        posts_df.views_increase.fillna(0, inplace=True)
        posts_df.replies_increase.fillna(0, inplace=True)

        posts_df.drop(columns=['likes_x', 'shares_x',
                               'views_x', 'replies_x'], inplace=True)
        posts_df.rename(columns={'likes_y': 'likes', 'shares_y': 'shares',
                                 'views_y': 'views', 'replies_y': 'replies'},
                        inplace=True)

        posts_df.likes.fillna(0, inplace=True)
        posts_df.shares.fillna(0, inplace=True)
        posts_df.views.fillna(0, inplace=True)
        posts_df.replies.fillna(0, inplace=True)

        return posts_df

    def apply_attachments(self, posts):
        posts_dict = posts[['social', 'owner_id', 'post_id']] \
            .to_dict('records')
        # task = {'task_name': 'get_posts_attachments',
        #         'args': {'posts': posts_dict}}
        # posts_atts = self.cass.call(task)
        query = CassQueryGenerator(self.cass,
                                   CassandraTables.post_attachments,
                                   'select')
        for post in posts_dict:
            vector = CassandraTables.post_attachments(
                social=post['social'],
                owner_id=post['owner_id'],
                post_id=post['post_id'])
            query.add_record(vector)
        posts_atts = query.run_task()
        posts_atts = pd.DataFrame(posts_atts)

        if posts_atts.shape[0] > 0:
            posts_atts = posts_atts[posts_atts.type == 'image'] \
                .drop_duplicates(['social', 'owner_id', 'post_id'])
            posts_atts = posts_atts[['social', 'owner_id', 'post_id', 'link']]
            posts = pd.merge(posts, posts_atts, how="left",
                             on=['social', 'owner_id', 'post_id'])
            posts.link.fillna("", inplace=True)
        else:
            posts['link'] = ""

        return posts

    def get_posts_stat(self, posts):
        # task = {'task_name': 'get_posts_stat',
        #         'args': {'posts': posts}}
        # posts_stats = self.cass.call(task)
        query = CassQueryGenerator(self.cass, CassandraTables.post_stats,
                                   'select')
        for post in posts:
            stat = CassandraTables.post_stats(social=post['social'],
                                              owner_id=post['owner_id'],
                                              post_id=post['post_id'])
            query.add_record(stat)
        posts_stats = query.run_task()
        posts_stats = pd.DataFrame(posts_stats).sort_values(
            'ts', ascending=False).groupby(['social', 'owner_id', 'post_id']) \
            .head(2)

        posts_stats = posts_stats.groupby(['social', 'owner_id', 'post_id'])[
            ['likes', 'shares', 'views', 'replies']]
        posts_stats = posts_stats.agg(self.get_stat).reset_index()

        posts_stats['likes_increase'] = posts_stats.likes.apply(
            lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
        posts_stats['shares_increase'] = posts_stats.shares.apply(
            lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
        posts_stats['views_increase'] = posts_stats.views.apply(
            lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)
        posts_stats['replies_increase'] = posts_stats.replies.apply(
            lambda x: int(x[0]) - int(x[1]) if len(x) > 1 else 0)

        posts_stats['likes'] = posts_stats.likes.apply(lambda x: x[0])
        posts_stats['shares'] = posts_stats.shares.apply(lambda x: x[0])
        posts_stats['views'] = posts_stats.views.apply(lambda x: x[0])
        posts_stats['replies'] = posts_stats.replies.apply(lambda x: x[0])

        return posts_stats

    def get_stat(self, stats):
        stats = [s if s is not None and not np.isnan(s) else 0 for s in stats]

        return tuple(stats)

    def add_additional_fields(self, posts_df):
        posts_df['href'] = "/post?owner-id=" + \
            posts_df.owner_id.astype(str) + \
            "&post-id=" + posts_df.post_id.astype(str) + \
            "&social=" + posts_df.social.astype(str)
        posts_df['group_href'] = self.get_group_href(posts_df.social,
                                                     posts_df.owner_id)
        posts_df['post_href'] = self.get_post_href(
            posts_df.social, posts_df.owner_id, posts_df.post_id)
        posts_df['date'] = posts_df['date'].astype(str)

        # task = {'task_name': 'get_traces',
        #         'args': {}}
        # traces = self.cass.call(task)
        query = CassQueryGenerator(self.cass, CassandraTables.traces, 'select')
        traces = query.run_task()
        if len(traces) == 0:
            posts_df['has_trace'] = False
        else:
            traces = pd.DataFrame(traces)
            traces = traces[traces.status == "completed"]
            traces = traces[['social', 'owner_id', 'post_id']]
            traces['has_trace'] = True
            posts_df = pd.merge(posts_df, traces, how="left",
                                on=['social', 'owner_id', 'post_id'])
            posts_df.has_trace.fillna(False, inplace=True)

        return posts_df

    def get_group_href(self, socials, owner_ids):
        supported_socials = ['vk', 'ok', 'inst']
        result = []
        for i in range(len(socials)):
            social = socials.iloc[i]
            if social not in supported_socials:
                result.append("")
                continue
            base = self.get_group_base(social)
            group = owner_ids.iloc[i]
            if social == "vk":
                group = group[1:]

            result.append(base + group)

        return pd.Series(result)

    def get_href_for_group(self, socials, owner_ids):
        supported_socials = ['vk', 'ok', 'inst']
        result = []
        social = str(socials)
        owner_id = str(owner_ids)
        if social not in supported_socials:
            result.append("")
            return 0
        base = self.get_group_base(social)
        group = owner_id
        if social == "vk":
            group = owner_id[1:]

        result.append(base + group)

        return result

    def get_post_href(self, socials, owner_ids, post_ids):
        result = []
        for i in range(len(socials)):
            social = socials.iloc[i]
            base = self.get_post_base(social)
            group = owner_ids.iloc[i]
            if social == "vk":
                between = "_"
            elif social == "ok":
                between = "/topic/"
            elif social == "inst":
                between = "p/"
            post = post_ids.iloc[i]
            if social == "inst":
                result.append(base + between + post)
            else:
                result.append(base + group + between + post)

        return result

    def get_group_base(self, social):
        if social == "vk":
            return 'https://vk.com/club'
        elif social == "ok":
            return 'https://ok.ru/group/'
        elif social == "inst":
            return 'https://instagram.com/'

    def get_post_base(self, social):
        if social == "vk":
            return 'https://vk.com/wall'
        elif social == "ok":
            return 'https://ok.ru/group/'
        elif social == "inst":
            return 'https://instagram.com/'


class CollectorTaskManager():
    rabbit_host = "rabbit"
    queue_name = "posts_collector"

    def __init__(self):
        self.collector = PostsCollector()
        self.method_list = [func for func in dir(self.collector) if
                            callable(getattr(self.collector, func))
                            and not func.startswith("__")]

    def on_request(self, ch, method, props, body):

        task = pickle.loads(body)

        if task.get('task_name') in self.method_list:
            task_method = getattr(self.collector, task.get('task_name'))
            task_args = task['args']
            try:
                result = task_method(**task_args)
            except Exception:
                LOGGER.exception(f"problem in {task.get('task_name')}")
                result = None
        else:
            result = "Current method is not supported"

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         body=pickle.dumps(result))
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    task_manager = CollectorTaskManager()

    run_process = Thread(target=task_manager.collector.run)
    collector_process = Thread(target=make_server,
                               args=(task_manager.queue_name,
                                     task_manager.on_request))

    run_process.start()
    collector_process.start()

    run_process.join()
    collector_process.join()
